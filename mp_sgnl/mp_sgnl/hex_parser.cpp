#include "stdafx.h"
#include <stdio.h>
#include <string>
#include "hex_parser.h"

using namespace std;

int hex_parser(char *pathname)
{
	FILE *  fHEX = NULL;
	char	readLine[1024];
	string	readstring;

	char	ram_addr[4];
	
	fopen_s(&fHEX, pathname, "rb");
	if (!fHEX)
	{
		printf("Failed to open HCD file %s\n", pathname);
		return 0;
	}

	while (!feof(fHEX))
	{
		fgets(readLine, 1024, fHEX);

		if (readLine[0] == ':')
		{
			readstring = readLine;

			int len = (((readLine[1] & 0x0F) << 4) | (readLine[2] & 0x0F));
			int type = atoi(readstring.substr(7, 2).c_str());

			for (int i = 0; i < len * 2 + 8; i++)
			{
				if ((readLine[i + 3] >= '0') && (readLine[i + 3] <= '9'))
				{
					readLine[i + 3] -= '0';
				}
				else if ((readLine[i + 3] >= 'a') && (readLine[i + 3] <= 'f'))
				{
					readLine[i + 3] -= 'a';
					readLine[i + 3] += 10;
				}
				else if ((readLine[i + 3] >= 'A') && (readLine[i + 3] <= 'F'))
				{
					readLine[i + 3] -= 'A';
					readLine[i + 3] += 10;
				}
			}

			switch (type)
			{
			case 0x00 ://data
			{
				ram_addr[1] = ((( readLine[3] & 0x0F ) << 4) | (readLine[4] & 0x0F) );
				ram_addr[0] = ((( readLine[5] & 0x0F ) << 4) | (readLine[6] & 0x0F) );

				unsigned char hci_write_ram[0xff] = { 0x4C, 0xFC, 4+len, ram_addr[0], ram_addr[1], ram_addr[2], ram_addr[3],};
				unsigned char hci_write_ram_cmd_complete_event[] = { 0x0e, 0x04, 0x01, 0x4c, 0xfc, 0x00 };

				int j = 2;
				for (int i = 0; i < len+7; i++ )
				{
					if (i > 6)
					{
						hci_write_ram[i] = (((readLine[i+j] & 0x0F) << 4) | (readLine[i+(j++)+1] & 0x0F));
					}
				}

#if 0
				for (int i = 0; i < 7 + len; i++)
				{
					printf("%02X ", hci_write_ram[i]);
				}
				printf("\n");
#endif
				
			}
				break;
			case 0x01 ://end data
				break;
			case 0x04://start address
			{
				ram_addr[3] = (((readLine[9] & 0x0F) << 4) | (readLine[10] & 0x0F));
				ram_addr[2] = (((readLine[11] & 0x0F) << 4) | (readLine[12] & 0x0F));
			}
				break;
			default :
				break;
			}

			if (!len)
			{
				fclose(fHEX);
				return 0;
			}
		}
	}

	fclose(fHEX);
}