
// mp_sgnlDlg.cpp : implementation file
//

#include "stdafx.h"
#include <stdint.h>
#include "mp_sgnl.h"
#include "mp_sgnlDlg.h"
#include "afxdialogex.h"
#include "shlwapi.h"

#include "mbt.h"
#include "mbt_com.h"
#include "hex_parser.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

enum {
	STATUS_DMP_FAIL = 1,
	STATUS_SERIAL_FAIL = 2,
	STATUS_MAC_FAIL = 4,
	STATUS_MCU_FAIL = 8,
	STATUS_BLE_FAIL = 16,
};

CFont m_font, m_font2;
CButton m_mcu_btn;

#define ENABLE_ASSY
#define PROGRESSBAR_TIMER	1
// Cmp_sgnlDlg dialog


Cmp_sgnlDlg::Cmp_sgnlDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_MP_SGNL_DIALOG, pParent)
	, m_baudrate(0)
	, m_val_cnt(0)
{
	//Create(IDD_MP_SGNL_DIALOG, NULL);
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cmp_sgnlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_BAUD, m_ctlBaud);
	DDX_Control(pDX, IDC_COMBO_PORT, m_ctlPort);
	DDX_Control(pDX, IDC_EDIT_HEX_PATH, m_ctrHexpath);
	DDX_Control(pDX, IDC_EDIT_HEX_PATH2, m_ctrHexpath2);
	DDX_Control(pDX, IDC_EDIT_ST_SN, m_CtrSTSN);
	DDX_Control(pDX, IDC_EDIT_SERIAL, m_CtrSerial);
	DDX_Control(pDX, IDC_EDIT_NOW_MAC, m_CtrNowMAC);
	DDX_Control(pDX, IDC_EDIT_START_MAC, m_CtrStartMAC);
	DDX_Control(pDX, IDC_EDIT_END_MAC, m_CtrEndMAC);
	DDX_Control(pDX, IDC_EDIT_SERIAL3, m_CtrSerial3);
	DDX_Control(pDX, IDC_EDIT_SERIAL2, m_CtrSerial2);
	DDX_Control(pDX, IDC_EDIT_SERIAL4, m_CtrSerial4);
	DDX_Control(pDX, IDC_STATIC_DMP, m_static_dmp);
	DDX_Control(pDX, IDC_STATIC_SERIAL, m_static_serial);
	DDX_Control(pDX, IDC_STATIC_MAC, m_static_mac);
	DDX_Control(pDX, IDC_STATIC_DMP2, m_static_dmp2);
	DDX_Control(pDX, IDC_STATIC_BLE, m_static_ble);
	DDX_Control(pDX, IDC_STATIC_BLE2, m_static_ble2);
	DDX_Control(pDX, IDC_STATIC_SERIAL2, m_static_serial2);
	DDX_Control(pDX, IDC_STATIC_MAC2, m_static_mac2);
	DDX_Control(pDX, IDC_STATIC_MCU, m_static_mcu);
	DDX_Control(pDX, IDC_STATIC_MCU2, m_static_mcu2);
	DDX_Control(pDX, IDC_PROGRESS1, m_progress);
	DDX_Control(pDX, IDC_STATIC_PASS_FAIL, m_static_pass_fail);
	DDX_Control(pDX, IDC_SMT_CHECK, m_btn_smt_check);
}

BEGIN_MESSAGE_MAP(Cmp_sgnlDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_CBN_SELCHANGE(IDC_COMBO_BAUD, OnCbnSelchangeCombo2)
	ON_CBN_SELCHANGE(IDC_COMBO_PORT, OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_HEX_BROWSE, OnBnClickedHexBrowse)
	ON_BN_CLICKED(IDC_BLEDOWN, OnBnClickedBLEdown)
	ON_EN_CHANGE(IDC_EDIT_SERIAL, OnEnChangeEditSerial)
	ON_BN_CLICKED(IDC_DMPDOWN, OnBnClickedDMP)
	ON_EN_CHANGE(IDC_EDIT_SERIAL3, OnEnChangeEditSerial3)
ON_WM_CTLCOLOR()
ON_BN_CLICKED(IDC_MCUDOWN, OnBnClickedMCU)
ON_BN_CLICKED(IDC_DOWNLOAD, OnBnClickedDownload)
ON_BN_CLICKED(IDC_HEX_BROWSE2, &Cmp_sgnlDlg::OnBnClickedHexBrowse2)
ON_WM_TIMER()
ON_WM_CREATE()
ON_BN_CLICKED(IDCANCEL, &Cmp_sgnlDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// Cmp_sgnlDlg message handlers

BOOL Cmp_sgnlDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CTime cTime = CTime::GetCurrentTime(); // 현재 시스템으로부터 날짜 및 시간을 얻어 온다.

	CString strDate; // 반환되는 날짜와 시간을 저장할 CString 변수 선언

	if (cTime.GetMonth() < 10)
	{
		strDate.Format("%c%d", (cTime.GetYear() % 2017)+'A', cTime.GetMonth());
	}
	else
	{
		char month;

		switch (cTime.GetMonth())
		{
		case 10: month = 'O';
			break;
		case 11: month = 'N';
			break;
		case 12: month = 'D';
			break;
		}
		strDate.Format("%c%c", (cTime.GetYear() % 2017) + 'A', month);
	}

	m_CtrSerial2.SetWindowTextA(strDate);

	m_baud[0] = _T("115200");
	m_baud[1] = _T("3000000");

	m_ctlBaud.AddString(m_baud[0]);
	m_ctlBaud.AddString(m_baud[1]);


	m_ctlBaud.SetCurSel(1);

	HKEY hKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, TEXT("HARDWARE\\DEVICEMAP\\SERIALCOMM"), &hKey);

	TCHAR szData[20], szName[100];
	DWORD index = 0, dwSize = 100, dwSize2 = 20, dwType = REG_SZ;
	m_ctlPort.ResetContent();
	memset(szData, 0x00, sizeof(szData));
	memset(szName, 0x00, sizeof(szName));

	while (ERROR_SUCCESS == RegEnumValue(hKey, index, szName, &dwSize, NULL, NULL, NULL, NULL))
	{
		RegQueryValueEx(hKey, szName, NULL, &dwType, (LPBYTE)szData, &dwSize2);
		m_ctlPort.AddString(CString(szData));

		m_port[index] = CString(szData);

		memset(szData, 0x00, sizeof(szData));
		memset(szName, 0x00, sizeof(szName));
		dwSize = 100;
		dwSize2 = 20;

		index++;
	}

	RegCloseKey(hKey);

	CString strFilename;
	strFilename = __targv[0];

	CString strConfig;
	strConfig = (".\\files\\");
	strConfig += strFilename.Right(strFilename.GetLength() - strFilename.ReverseFind('\\') - 1);
	strConfig += _T("_config");

	CStdioFile file;
	//if (file.Open(".\\files\\config", CFile::modeRead))
	if (file.Open(strConfig, CFile::modeRead))
	{
		CString readStr;
		
		int Port;
		int Baud;
		CString strSTSN;
		CString strSerial;
		CString strSerial3;
		CString strMAC;

		while (file.ReadString(readStr))
		{
			if (!readStr.Left(7).Compare("COMPORT"))
			{
				Port = atoi(readStr.Right(1));
			}
			else if (!readStr.Left(8).Compare("BAUDRATE"))
			{
				Baud = atoi(readStr.Right(1));
			}
			else if (!readStr.Left(4).Compare("STSN"))
			{
				strSTSN = readStr.Right(24);
			}
			else if (!readStr.Left(7).Compare("SERIAL1"))
			{
				strSerial = readStr.Right(4);
			}
			else if (!readStr.Left(7).Compare("SERIAL3"))
			{
				strSerial3 = readStr.Right(4);
			}
			else if (!readStr.Left(7).Compare("MACADDR"))
			{
				strMAC = readStr.Right(6);
				CString out;
				char *hexout;
				hexout = strMAC.GetBuffer(0);
				if ( strstr(hexout, "=") )
				{
					strMAC = "FFFFFF";
				}
				strMAC.ReleaseBuffer();
			}
			else if (!readStr.Left(8).Compare("HEXPATH1"))
			{
				m_hex_path = readStr.Right(readStr.GetLength() - 9);
			}
			else if (!readStr.Left(8).Compare("HEXPATH2"))
			{
				m_hex_path2 = readStr.Right(readStr.GetLength() - 9);
			}
			else if (!readStr.Left(8).Compare("SMTCHECK"))
			{
				m_smt_check = atoi(readStr.Right(1));
				(m_smt_check)?
					m_btn_smt_check.SetCheck(TRUE) :m_btn_smt_check.SetCheck(FALSE);
			}
		}
		file.Close();

		m_ctlPort.SetCurSel(Port);
		m_curPort = m_port[Port];

		m_ctlBaud.SetCurSel(Baud);
		m_curBaud = m_baud[Baud];

		m_CtrSTSN.SetWindowTextA(strSTSN);
		m_CtrSerial.SetWindowTextA(strSerial);
		m_CtrSerial3.SetWindowTextA(strSerial3);
		m_CtrSerial4.SetWindowTextA(strMAC);
		m_CtrStartMAC.SetWindowTextA(strMAC);
		m_CtrNowMAC.SetWindowTextA(strMAC);
		m_CtrEndMAC.SetWindowTextA(strMAC);
		m_ctrHexpath.SetWindowTextA(m_hex_path);
		m_ctrHexpath2.SetWindowTextA(m_hex_path2);
	}
	else
	{
		m_ctlPort.SetCurSel(0);

		m_curPort = m_port[0];

		m_ctlBaud.SetCurSel(1);

		m_curBaud = m_baud[1];

		m_CtrSTSN.SetWindowTextA("000000000000000000000000");
		m_CtrSerial.SetWindowTextA("S100");
		m_CtrSerial3.SetWindowTextA("0001");
		m_CtrSerial4.SetWindowTextA("FFFFFF");
		m_CtrNowMAC.SetWindowTextA("FFFFFF");
	}

	//GetDlgItem(IDC_BUTTON_DMP)->EnableWindow(FALSE);
	//GetDlgItem(IDC_BUTTON_MAC)->EnableWindow(FALSE);
	//GetDlgItem(IDC_BUTTON_SERIAL)->EnableWindow(FALSE);

	//m_font.CreatePointFont(180, "굴림");
	m_font.CreateFont(25,                     // 글자높이
		0,                     // 글자너비
		0,                      // 출력각도
		0,                      // 기준 선에서의각도
		FW_BOLD,              // 글자굵기
		FALSE,                  // Italic 적용여부
		FALSE,                  // 밑줄적용여부
		FALSE,                  // 취소선적용여부
		DEFAULT_CHARSET,       // 문자셋종류
		OUT_DEFAULT_PRECIS,    // 출력정밀도
		CLIP_DEFAULT_PRECIS,   // 클리핑정밀도
		DEFAULT_QUALITY,       // 출력문자품질
		DEFAULT_PITCH,         // 글꼴Pitch
		"굴림"           // 글꼴
		);

	m_font2.CreateFont(50,                     // 글자높이
		0,                     // 글자너비
		0,                      // 출력각도
		0,                      // 기준 선에서의각도
		FW_HEAVY,              // 글자굵기
		FALSE,                  // Italic 적용여부
		FALSE,                  // 밑줄적용여부
		FALSE,                  // 취소선적용여부
		DEFAULT_CHARSET,       // 문자셋종류
		OUT_DEFAULT_PRECIS,    // 출력정밀도
		CLIP_DEFAULT_PRECIS,   // 클리핑정밀도
		DEFAULT_QUALITY,       // 출력문자품질
		DEFAULT_PITCH,         // 글꼴Pitch
		"굴림"           // 글꼴
		);

	GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
	if (m_smt_check)//smt
	{
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(TRUE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(TRUE);
		GetDlgItem(IDC_DMPDOWN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MCUDOWN)->ShowWindow(SW_HIDE);
	}
	else
	{
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
	}
	GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);

	m_progress.SetRange(0, 100);
	progressbar_status = 0;
	m_progress.SetPos(0);

	CString str;
	str.Format(_T("Time: 00:00"));
	m_progress.SetWindowText(str);

	download_status = 0;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Cmp_sgnlDlg::config_write(void)
{
	CString strSTSN;
	CString strSerial;
	CString strSerial3;
	CString strMAC;
	CString strHEX;

	GetDlgItemText(IDC_EDIT_ST_SN, strSTSN);
	GetDlgItemText(IDC_EDIT_SERIAL, strSerial);
	GetDlgItemText(IDC_EDIT_SERIAL3, strSerial3);
	GetDlgItemText(IDC_EDIT_NOW_MAC, strMAC);

	CString strFilename;
	strFilename = __targv[0];

	CString strConfig;
	strConfig = (".\\files\\");
	strConfig += strFilename.Right(strFilename.GetLength() - strFilename.ReverseFind('\\') - 1);
	strConfig += _T("_config");

	CStdioFile file;
	//if (file.Open(".\\files\\config", CFile::modeRead))
	if (file.Open(strConfig, CFile::modeRead))
	{
		file.Close();
		//file.Remove(".\\files\\config");
		file.Remove(strConfig);
	}
	//file.Open(".\\files\\config", CFile::modeCreate | CFile::modeWrite);
	file.Open(strConfig, CFile::modeCreate | CFile::modeWrite);

	CString tmpStr;

	tmpStr.Format("%d\n", m_ctlPort.GetCurSel());
	file.WriteString("COMPORT="+tmpStr);
	tmpStr.Format("%d\n", m_ctlBaud.GetCurSel());
	file.WriteString("BAUDRATE=" + tmpStr);
	file.WriteString("STSN=" + strSTSN + "\n");
	file.WriteString("SERIAL1=" + strSerial + "\n");
	file.WriteString("SERIAL3=" + strSerial3 + "\n");
	file.WriteString("MACADDR=" + strMAC+ "\n");
	file.WriteString("HEXPATH1=" + m_hex_path + "\n");
	file.WriteString("HEXPATH2=" + m_hex_path2 + "\n");
	//tmpStr.Format("%d\n", m_smt_check);
	(m_btn_smt_check.GetCheck() == TRUE) ? tmpStr.Format("1\n") : tmpStr.Format("0\n");
	file.WriteString("SMTCHECK=" + tmpStr + "\n");
	file.Close();
}

void Cmp_sgnlDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Cmp_sgnlDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Cmp_sgnlDlg::OnCbnSelchangeCombo2()
{
	// TODO: Add your control notification handler code here
	if (m_ctlBaud.GetCurSel() == 0)
	{
		m_baudrate = 115200;
	}
	else if (m_ctlBaud.GetCurSel() == 1)
	{
		m_baudrate = 3000000;
	}

	m_curBaud = m_baud[m_ctlBaud.GetCurSel()];

	config_write();

	//set environment variable for BT
	CString env_name, env_data;
	env_name = "APPLICATION_BAUDRATE";
	env_data.Format("%ld", m_baudrate);
	SetEnvironmentVariable(env_name, env_data);
	env_name = "DOWNLOAD_BAUDRATE";
	SetEnvironmentVariable(env_name, env_data);
}


void Cmp_sgnlDlg::OnCbnSelchangeCombo1()
{
	// TODO: Add your control notification handler code here
	m_curPort = m_port[m_ctlPort.GetCurSel()];

	config_write();

	//set environment variable for BT
	CString env_name, env_data;
	env_name = "MBT_TRANSPORT";
	env_data = m_curPort;
	SetEnvironmentVariable(env_name, env_data);
}


void Cmp_sgnlDlg::OnBnClickedHexBrowse()
{
	// TODO: Add your control notification handler code here
	TCHAR szFilters[] = _T("Hex Files (*.hex)|*.hex|All Files (*.*)|*.*||");

	// Create an Open dialog; the default file name extension is ".my".
	CFileDialog fileDlg(TRUE, _T("hex"), _T("*.hex"),
		OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilters);

	// Display the file dialog. When user clicks OK, fileDlg.DoModal() 
	// returns IDOK.
	if (fileDlg.DoModal() == IDOK)
	{
		m_hex_path = fileDlg.GetPathName();

		// Implement opening and reading file in here.

		//Change the window's title to the opened file's title.
		CString fileName = fileDlg.GetFileTitle();


		CEdit *p = (CEdit *)GetDlgItem(IDC_EDIT_HEX_PATH);
		p->SetWindowText(m_hex_path);
	}

	config_write();
}

int Cmp_sgnlDlg::DownloadBLE()
{
	m_static_ble2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_BLE2, "BLE ");

	m_static_ble.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_BLE, "");

	m_static_pass_fail.SetFont(&m_font2, TRUE);
	SetDlgItemText(IDC_STATIC_PASS_FAIL, "");

	// TODO: Add your control notification handler code here
	char * tmpPort = LPSTR(LPCTSTR(m_curPort));
	int baud_sel = 0;
#if 01
	CString m_chipload_path = _T("files\\ChipLoad.exe");
	CString m_btp_path = _T("files\\20706_SFLASH.btp");
	CString m_minidriver_path = _T("files\\uart.hex");

	CString m_param;

	m_param = m_chipload_path;
	m_param += _T(" -BLUETOOLMODE -PORT ");
	m_param += m_curPort;
	m_param += _T(" -BAUDRATE ");
	m_param += m_curBaud;

	m_param += _T(" -MINIDRIVER ");
	m_param += m_minidriver_path;
	m_param += _T(" -BTP ");
	m_param += m_btp_path;
	m_param += _T(" -CONFIG ");

#if 01
	CString Thispath;
	char *hex_out;
	hex_out = m_hex_path.GetBuffer(0);
	if (strstr(hex_out, ":\\"))
	{
		Thispath = m_hex_path;
	}
	else
	{
		char getdir[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, getdir);
		char setdir[MAX_PATH];
		sprintf_s(setdir, "%s\\%s", getdir, m_hex_path); // 뒤에 경로를 붙히고 setdir로 저장
		Thispath = setdir;
	}
	m_hex_path.ReleaseBuffer();
	m_param += Thispath;
#else
	m_param += m_hex_path;
#endif
	m_param += _T(" -DLMINIDRIVERCHUNKSIZE 251");

	TCHAR *buffer;
	int nLen = 0;
	LPTSTR lpszBuf;
	nLen = m_param.GetLength();
	lpszBuf = m_param.GetBuffer(nLen);
	CmdRun(lpszBuf, &buffer);
	m_param.ReleaseBuffer();

	if (strstr(buffer, "Completed successfully"))//success bt hex Download
	{
		ok_fail = TRUE;
		m_static_ble.SetFont(&m_font, TRUE);
		progressbar_status = 60;
		SetDlgItemText(IDC_STATIC_BLE, "OK");
		//UpdateData(FALSE);

		download_status &= ~(STATUS_BLE_FAIL);
		GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
		return 0;
	}
	else//if (strstr(buffer, "HCI Reset")) //connection fail
	{
		ok_fail = FALSE;
		m_static_ble.SetFont(&m_font, TRUE);
		SetDlgItemText(IDC_STATIC_BLE, "FAIL");
		//UpdateData(FALSE);

		//GetDlgItem(IDC_BUTTON_DMP)->EnableWindow(TRUE);
		download_status |= STATUS_BLE_FAIL;
		GetDlgItem(IDC_BLEDOWN)->EnableWindow(TRUE);
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);

		m_static_pass_fail.SetFont(&m_font2, TRUE);
		SetDlgItemText(IDC_STATIC_PASS_FAIL, "FAIL");
		return -1;
	}
#else
	char setpath[MAX_PATH];
	sprintf_s(setpath, "%s", m_hex_path); // 뒤에 경로를 붙히고 setdir로 저장
	if (!execute_hex_download(tmpPort, 3000000, "files\\uart.hex", setpath))
	{
		ok_fail = TRUE;
		m_static_ble.SetFont(&m_font, TRUE);
		SetDlgItemText(IDC_STATIC_BLE, "OK");
		progressbar_status = 33;
		//UpdateData(FALSE);

		download_status &= ~(STATUS_BLE_FAIL);
		GetDlgItem(IDC_BLEDOWN)->EnableWindow(TRUE);
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
		return;
	}
	else//if (strstr(buffer, "HCI Reset")) //connection fail
	{
		ok_fail = FALSE;
		m_static_ble.SetFont(&m_font, TRUE);
		SetDlgItemText(IDC_STATIC_BLE, "FAIL");
		progressbar_status = 0;
		//UpdateData(FALSE);

		//GetDlgItem(IDC_BUTTON_DMP)->EnableWindow(TRUE);
		download_status |= STATUS_BLE_FAIL;
		GetDlgItem(IDC_BLEDOWN)->EnableWindow(TRUE);
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);

		m_static_pass_fail.SetFont(&m_font2, TRUE);
		SetDlgItemText(IDC_STATIC_PASS_FAIL, "FAIL");
		return;
	}
	return;
#endif
}

void Cmp_sgnlDlg::OnBnClickedBLEdown()
{
	SetTimer(100, 1000, NULL);
	m_val_elapsed_sec = 0;
	m_start_btn_event = TRUE;
	m_thread_event_e = BLE_DOWN;
}

bool Cmp_sgnlDlg::ExecuteProcessRedirection(CString sExeName,CEdit *p)
{
	CString sExecute = sExeName;

	HANDLE hReadPipe, hWritePipe;

	SECURITY_ATTRIBUTES securityAttr;
	ZeroMemory(&securityAttr, sizeof(securityAttr));
	securityAttr.nLength = sizeof(securityAttr);
	securityAttr.bInheritHandle = TRUE;

	CreatePipe(&hReadPipe, &hWritePipe, &securityAttr, 0);

	STARTUPINFO sStartupInfo;
	PROCESS_INFORMATION process_Info;
	ZeroMemory(&sStartupInfo, sizeof(sStartupInfo));
	ZeroMemory(&process_Info, sizeof(process_Info));

	sStartupInfo.cb = sizeof(STARTUPINFO);
	sStartupInfo.dwFlags = STARTF_USESTDHANDLES;
	sStartupInfo.hStdInput = NULL;
	sStartupInfo.hStdOutput = hWritePipe;
	sStartupInfo.hStdError = hWritePipe;

	BOOL bSuccessProcess = CreateProcess(0, sExecute.GetBuffer(0), 0, 0, TRUE, NORMAL_PRIORITY_CLASS ,
		0, 0, &sStartupInfo, &process_Info);
	CloseHandle(hWritePipe);


	if (FALSE == bSuccessProcess)
	{
		CloseHandle(hReadPipe);
		return false;
	}

	char szTemp[1024];
	DWORD dwReadByte;
	BOOL bResult;
	do
	{
		ZeroMemory(szTemp, sizeof(szTemp));
		bResult = ::ReadFile(hReadPipe, szTemp, sizeof(szTemp) - 1, &dwReadByte, 0);

		if (TRUE == bResult)
		{
			//TRACE("%s", szTemp);

			p->SetWindowText(szTemp);
			//UpdateData(FALSE);

		}
	} while (TRUE == bResult);

	CloseHandle(hReadPipe);

	return true;
}

#define SERIAL_1_LEN	4
#define SERIAL_2_LEN	2
#define SERIAL_3_LEN	4
#define SERIAL_4_LEN	6

void Cmp_sgnlDlg::OnEnChangeEditSerial()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	//UpdateData(TRUE);

	CString strSerial;

	int sel = m_CtrSerial.GetSel();

	GetDlgItemText(IDC_EDIT_SERIAL, strSerial);

	int length = strSerial.GetLength();

	if (length < SERIAL_1_LEN)
	{
		for (int i = length; i < SERIAL_1_LEN; i++)
		{
			strSerial += '0';
		}
	}

	for (int i = 0; i < length; i++)
	{
		if (strSerial.GetAt(i) < '0')
		{
			strSerial.SetAt(i,'0');
		}
		else if ((strSerial.GetAt(i) > '9') && (strSerial.GetAt(i) < 'A'))
		{
			strSerial.SetAt(i, '0');
		}
		else if ((strSerial.GetAt(i) > 'Z') && (strSerial.GetAt(i) < 'a'))
		{
			strSerial.SetAt(i, '0');
		}
		else if (strSerial.GetAt(i) > 'z')
		{
			strSerial.SetAt(i, '0');
		}
	}

	SetDlgItemText(IDC_EDIT_SERIAL, strSerial.MakeUpper().GetBufferSetLength(SERIAL_1_LEN));
	m_CtrSerial.SetSel(sel);

	config_write();
}


void Cmp_sgnlDlg::OnEnChangeEditSerial3()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	//UpdateData(TRUE);

	CString tmpSerial;

	int sel = m_CtrSerial3.GetSel();

	GetDlgItemText(IDC_EDIT_SERIAL3, tmpSerial);

	CString strSerial;

	strSerial = tmpSerial.MakeUpper();

	int length = strSerial.GetLength();

	if (length < SERIAL_3_LEN)
	{
		for (int i = length; i < SERIAL_3_LEN; i++)
		{
			strSerial += 'F';
		}
	}

	for (int i = 0; i < length; i++)
	{
		if (strSerial.GetAt(i) < '0')
		{
			strSerial.SetAt(i, 'F');
		}
		else if ((strSerial.GetAt(i) > '9') && (strSerial.GetAt(i) < 'A'))
		{
			strSerial.SetAt(i, 'F');
		}
		else if ((strSerial.GetAt(i) > 'F') && (strSerial.GetAt(i) < 'a'))
		{
			strSerial.SetAt(i, 'F');
		}
		else if ((strSerial.GetAt(i) >= 'A') && (strSerial.GetAt(i) <= 'F'))
		{
			strSerial.SetAt(i, strSerial.GetAt(i));
		}
		else if ((strSerial.GetAt(i) >= '0') && (strSerial.GetAt(i) <= '9'))
		{
			strSerial.SetAt(i, strSerial.GetAt(i));
		}
		else if (strSerial.GetAt(i) > 'f')
		{
			strSerial.SetAt(i, 'F');
		}
	}

#if 0
	if (!atoi(strSerial))
	{
		strSerial = "0001";
	}
#endif

	SetDlgItemText(IDC_EDIT_SERIAL3, strSerial.MakeUpper().GetBufferSetLength(SERIAL_3_LEN));
	m_CtrSerial3.SetSel(sel);

	config_write();
}

void Cmp_sgnlDlg::OnEnChangeEditStartMac()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	//UpdateData(TRUE);

	CString tmpMAC;


	int sel = m_CtrStartMAC.GetSel();

	GetDlgItemText(IDC_EDIT_START_MAC, tmpMAC);
	int length = tmpMAC.GetLength();

	CString strMAC;

	strMAC = tmpMAC.MakeUpper();

	if (length < 6)
	{
		for (int i = 0; i < 6 - length; i++)
		{
			strMAC += 'F';
		}
	}

	for (int i = 0; i < 6; i++)
	{
		if (strMAC.GetAt(i) < '0')
		{
			strMAC.SetAt(i, 'F');
		}
		else if ((strMAC.GetAt(i) > '9') && (strMAC.GetAt(i) < 'A'))
		{
			strMAC.SetAt(i, 'F');
		}
		else if ((strMAC.GetAt(i) > 'F') && (strMAC.GetAt(i) < 'a'))
		{
			strMAC.SetAt(i, 'F');
		}
		else if (strMAC.GetAt(i) > 'f')
		{
			strMAC.SetAt(i, 'F');
		}
	}

	SetDlgItemText(IDC_EDIT_START_MAC, strMAC.MakeUpper().GetBufferSetLength(6));

	m_CtrStartMAC.SetSel(sel);
}

void Cmp_sgnlDlg::OnEnChangeEditNowMac()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	//UpdateData(TRUE);

	CString tmpMAC;

	
	int sel = m_CtrNowMAC.GetSel();

	GetDlgItemText(IDC_EDIT_NOW_MAC, tmpMAC);
	int length = tmpMAC.GetLength();

	CString strMAC;

	strMAC = tmpMAC.MakeUpper();

	if (length < 6)
	{
		for (int i = 0; i < 6 - length; i++)
		{
			strMAC += 'F';
		}
	}

	for (int i = 0; i < 6; i++)
	{
		if (strMAC.GetAt(i) < '0')
		{
			strMAC.SetAt(i, 'F');
		}
		else if ((strMAC.GetAt(i) > '9') && (strMAC.GetAt(i) < 'A'))
		{
			strMAC.SetAt(i, 'F');
		}
		else if ((strMAC.GetAt(i) > 'F') && (strMAC.GetAt(i) < 'a'))
		{
			strMAC.SetAt(i, 'F');
		}
		else if (strMAC.GetAt(i) > 'f')
		{
			strMAC.SetAt(i, 'F');
		}
	}

#if 0
	if ( !atoi(strMAC) )
	{
		strMAC = "000001";
	}
#endif
	SetDlgItemText(IDC_EDIT_NOW_MAC, strMAC.MakeUpper().GetBufferSetLength(6));
	SetDlgItemText(IDC_EDIT_SERIAL4, strMAC.MakeUpper().GetBufferSetLength(6));

	m_CtrNowMAC.SetSel(sel);

	config_write();
}

void Cmp_sgnlDlg::OnEnChangeEditEndMac()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	//UpdateData(TRUE);

	CString tmpMAC;
	
	int sel = m_CtrEndMAC.GetSel();

	GetDlgItemText(IDC_EDIT_END_MAC, tmpMAC);
	int length = tmpMAC.GetLength();

	CString strMAC;

	strMAC = tmpMAC.MakeUpper();

	if (length < 6)
	{
		for (int i = 0; i < 6 - length; i++)
		{
			strMAC += 'F';
		}
	}

	for (int i = 0; i < 6; i++)
	{
		if (strMAC.GetAt(i) < '0')
		{
			strMAC.SetAt(i, 'F');
		}
		else if ((strMAC.GetAt(i) > '9') && (strMAC.GetAt(i) < 'A'))
		{
			strMAC.SetAt(i, 'F');
		}
		else if ((strMAC.GetAt(i) > 'F') && (strMAC.GetAt(i) < 'a'))
		{
			strMAC.SetAt(i, 'F');
		}
		else if (strMAC.GetAt(i) > 'f')
		{
			strMAC.SetAt(i, 'F');
		}
	}

	SetDlgItemText(IDC_EDIT_END_MAC, strMAC.MakeUpper().GetBufferSetLength(6));

	m_CtrEndMAC.SetSel(sel);
}

int Cmp_sgnlDlg::DownloadDMP()
{
	// TODO: Add your control notification handler code here
	m_static_dmp2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_DMP2, "DMP ");
	m_static_serial2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_SERIAL2, "SERIAL ");
	m_static_mac2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_MAC2, "MAC ");

	m_static_dmp.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_DMP, "");
	m_static_serial.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_SERIAL, "");
	m_static_mac.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_MAC, "");

	m_static_pass_fail.SetFont(&m_font2, TRUE);
	SetDlgItemText(IDC_STATIC_PASS_FAIL, "");

	char * tmpPort = LPSTR(LPCTSTR(m_curPort));
#ifdef ENABLE_ASSY
	CString xl_path;
	char chThisPath[_MAX_PATH];
	GetCurrentDirectory(_MAX_PATH, chThisPath);
#if 0
	if (__argc == 1)
		xl_path.Format("%s\\files\\mac_addr.csv", chThisPath);
	else if (__argc == 2)
#endif
	{
		xl_path.Format("%s\\files\\mac_addr_", chThisPath);

		//xl_path += __targv[1];
		CString start_mac;
		CString end_mac;
		int start_mac_addr;
		int end_mac_addr;

		GetDlgItemText(IDC_EDIT_START_MAC, start_mac);
		GetDlgItemText(IDC_EDIT_END_MAC, end_mac);

		sscanf_s((LPCTSTR)start_mac, "%x", &start_mac_addr);
		sscanf_s((LPCTSTR)end_mac, "%x", &end_mac_addr);

		xl_path += start_mac;
		xl_path += _T("-");
		xl_path += end_mac;
		xl_path += _T(".csv");
	}

	CString strMAC;
	GetDlgItemText(IDC_EDIT_NOW_MAC, strMAC);

	CString strSerial;
	CString strSerial2;
	CString strSerial3;
	CString strSerial4;
	GetDlgItemText(IDC_EDIT_SERIAL, strSerial);
	GetDlgItemText(IDC_EDIT_SERIAL2, strSerial2);
	GetDlgItemText(IDC_EDIT_SERIAL3, strSerial3);
	GetDlgItemText(IDC_EDIT_SERIAL4, strSerial4);

	CString strSerial_all;

	strSerial_all = strSerial + "-" + strSerial2 + "-" + strSerial3 + "-" + strSerial4;

	CStdioFile file;

	CString readStr;
	CString tmpStr;

	CString tmpMAC;
	CString tmpSerial;

	download_status = 0;

	if (file.Open(xl_path, CFile::modeRead))//if exist
	{
		// check mac & serial
		int mac_index;
		int serial_index;
		int line_num = 0;
		//file.SeekToEnd();
		while (file.ReadString(readStr))
		{
			line_num++;

			mac_index = readStr.Find(',', 0);
			tmpStr = readStr.Mid(mac_index + 1);
			serial_index = tmpStr.Find(',', 0);

			tmpMAC = readStr.Left(mac_index);
			tmpSerial = tmpStr.Left(serial_index);

			if (!tmpMAC.Compare(strMAC))
			{
				CString strline;
				strline.Format("%d", line_num);
				//eual
				AfxMessageBox("MAC addr 중복 데이터가 있습니다.\nLine:" + strline);
				download_status |= STATUS_MAC_FAIL;
				return -1;
			}

			if (!tmpSerial.Compare(strSerial_all))
			{
				CString strline;
				strline.Format("%d", line_num);
				//eual
				AfxMessageBox("Serial number 중복 데이터가 있습니다.\nLine:" + strline);
				download_status |= STATUS_SERIAL_FAIL;
				return -1;
			}
		}

		file.Close();
	}
#endif
	progressbar_status = 60;
	//UpdateData(FALSE);
	int tmpBaud = 3000000;//atoi(m_curBaud);

	if (execute_dmp_download(tmpPort, tmpBaud))//fail
	{
		ok_fail = FALSE;
		m_static_dmp.SetFont(&m_font, TRUE);
		SetDlgItemText(IDC_STATIC_DMP, "FAIL");
		progressbar_status = 0;
		//UpdateData(FALSE);

		download_status |= STATUS_DMP_FAIL;
		GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(TRUE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);

		m_static_pass_fail.SetFont(&m_font2, TRUE);
		SetDlgItemText(IDC_STATIC_PASS_FAIL, "FAIL");
		return -1;
	}
	else
	{
		ok_fail = TRUE;
		progressbar_status = 70;
		m_static_dmp.SetFont(&m_font, TRUE);
		SetDlgItemText(IDC_STATIC_DMP, "OK");
		//UpdateData(FALSE);

		download_status &= ~(STATUS_DMP_FAIL);
	}
#ifdef ENABLE_ASSY
	CString strSerialNum;

	GetDlgItemText(IDC_EDIT_SERIAL, strSerial);
	GetDlgItemText(IDC_EDIT_SERIAL2, strSerial2);
	GetDlgItemText(IDC_EDIT_SERIAL3, strSerial3);
	GetDlgItemText(IDC_EDIT_SERIAL4, strSerial4);

	strSerialNum = strSerial + strSerial2 + strSerial3 + strSerial4;

	char * chrSerial = LPSTR(LPCTSTR(strSerialNum));

	if (!m_smt_check)//mp_mode
	{
		if (execute_serial_number_write(tmpPort, tmpBaud, chrSerial))//fail
		{
			ok_fail = FALSE;
			progressbar_status = 0;
			m_static_serial.SetFont(&m_font, TRUE);
			SetDlgItemText(IDC_STATIC_SERIAL, "FAIL");
			//UpdateData(FALSE);

			download_status |= STATUS_SERIAL_FAIL;
			GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
			GetDlgItem(IDC_DMPDOWN)->EnableWindow(TRUE);
			GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
			return -1;
		}
		else//success
		{
			ok_fail = TRUE;
			progressbar_status = 80;
			m_static_serial.SetFont(&m_font, TRUE);
			SetDlgItemText(IDC_STATIC_SERIAL, "OK");
			//UpdateData(FALSE);
			download_status &= ~(STATUS_SERIAL_FAIL);
		}

		char * chrMAC = LPSTR(LPCTSTR(strMAC));

		if (execute_mac_write(tmpPort, tmpBaud, chrMAC))//fail
		{
			ok_fail = FALSE;
			progressbar_status = 0;
			m_static_mac.SetFont(&m_font, TRUE);
			SetDlgItemText(IDC_STATIC_MAC, "FAIL");
			//UpdateData(FALSE);

			download_status |= STATUS_MAC_FAIL;
			GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
			GetDlgItem(IDC_DMPDOWN)->EnableWindow(TRUE);
			GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
			return -1;
		}
		else//success
		{
			ok_fail = TRUE;
			progressbar_status = 90;
			m_static_mac.SetFont(&m_font, TRUE);
			SetDlgItemText(IDC_STATIC_MAC, "OK");
			//UpdateData(FALSE);
		}
	}
	else//smt check
	{
		ok_fail = TRUE;
		progressbar_status = 80;
		m_static_serial.SetFont(&m_font, TRUE);
		SetDlgItemText(IDC_STATIC_SERIAL, "OK");
		//UpdateData(FALSE);
		download_status &= ~(STATUS_SERIAL_FAIL);

		ok_fail = TRUE;
		progressbar_status = 90;
		m_static_mac.SetFont(&m_font, TRUE);
		SetDlgItemText(IDC_STATIC_MAC, "OK");
		//UpdateData(FALSE);

		OnBnClickedMCU();
	}

	if (!download_status) // all success
	{
		download_status = 0;
		//next_download_check();
		//m_mcu_btn.Create("MCU", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, CRect(222, 121, 92, 35), this, IDC_MCUDOWN);
		//m_mcu_btn.ShowWindow(SW_SHOW);
	}

	return 0;
#endif
}

void Cmp_sgnlDlg::OnBnClickedDMP()
{
	SetTimer(100, 1000, NULL);
	m_val_elapsed_sec = 0;
	m_start_btn_event = TRUE;
	m_thread_event_e = DMP_DOWN;
}


void Cmp_sgnlDlg::excel_management(void)
{
	int last_flag = 0;

	CTime cTime = CTime::GetCurrentTime(); // 현재 시스템으로부터 날짜 및 시간을 얻어 온다.

	CString strDate, strTime; // 반환되는 날짜와 시간을 저장할 CString 변수 선언
	strDate.Format("%04d년 %02d월 %02d일", cTime.GetYear(), // 현재 년도 반환
		cTime.GetMonth(), // 현재 월 반환
		cTime.GetDay()); // 현재 일 반환


	strTime.Format("%02d시 %02d분 %02d초", cTime.GetHour(), // 현재 시간 반환
		cTime.GetMinute(), // 현재 분 반환
		cTime.GetSecond()); // 현재 초 반환

	CString xl_path;
	char chThisPath[_MAX_PATH];
	GetCurrentDirectory(_MAX_PATH, chThisPath);
#if 0
	if (__argc == 1)
		xl_path.Format("%s\\files\\mac_addr.csv", chThisPath);
	else if (__argc == 2)
#endif
	{
		xl_path.Format("%s\\files\\mac_addr_", chThisPath);

		//xl_path += __targv[1];
		CString start_mac;
		CString end_mac;
		int start_mac_addr;
		int end_mac_addr;

		GetDlgItemText(IDC_EDIT_START_MAC, start_mac);
		GetDlgItemText(IDC_EDIT_END_MAC, end_mac);

		sscanf_s((LPCTSTR)start_mac, "%x", &start_mac_addr);
		sscanf_s((LPCTSTR)end_mac, "%x", &end_mac_addr);

		xl_path += start_mac;
		xl_path += _T("-");
		xl_path += end_mac;
		xl_path += _T(".csv");
	}

	CString strMAC;
	GetDlgItemText(IDC_EDIT_NOW_MAC, strMAC);

	CString strSerial;
	CString strSerial2;
	CString strSerial3;
	CString strSerial4;
	GetDlgItemText(IDC_EDIT_SERIAL, strSerial);
	GetDlgItemText(IDC_EDIT_SERIAL2, strSerial2);
	GetDlgItemText(IDC_EDIT_SERIAL3, strSerial3);
	GetDlgItemText(IDC_EDIT_SERIAL4, strSerial4);

	CString strSerial_all;

	strSerial_all = strSerial + "-" + strSerial2 + "-" + strSerial3 + "-" + strSerial4;

	CStdioFile file;

	CString readStr;
	CString tmpStr;

	CString tmpMAC;
	CString tmpSerial;

	if (file.Open(xl_path, CFile::modeReadWrite | CFile::modeNoTruncate))//if exist
	{
		// check mac & serial
		int mac_index;
		int serial_index;
		int line_num = 0;
		//file.SeekToEnd();
		while (file.ReadString(readStr))
		{
			line_num++;

			mac_index = readStr.Find(',', 0);
			tmpStr = readStr.Mid(mac_index + 1);
			serial_index = tmpStr.Find(',', 0);

			tmpMAC = readStr.Left(mac_index);
			tmpSerial = tmpStr.Left(serial_index);

			if (!tmpMAC.Compare(strMAC))
			{
				CString strline;
				strline.Format("%d", line_num);
				//eual
				AfxMessageBox("MAC addr 중복 데이터가 있습니다.\nLine:"+strline);
			}

			if (!tmpSerial.Compare(strSerial_all))
			{
				CString strline;
				strline.Format("%d", line_num);
				//eual
				AfxMessageBox("Serial number 중복 데이터가 있습니다.\nLine:" + strline);
			}
		}
	}
	else
	{
		file.Open(xl_path, CFile::modeReadWrite | CFile::modeCreate | CFile::modeNoTruncate);
	}

	file.WriteString("88-E9-0F-");
	file.WriteString(strMAC);
	file.WriteString(",");
	file.WriteString(strSerial_all);
	file.WriteString(",");
	file.WriteString(strDate);
	file.WriteString(" ");
	file.WriteString(strTime);
	file.WriteString("\n");
	file.Close();
}


void Cmp_sgnlDlg::next_download_check(void)
{
	if (!m_smt_check)
	{
		//AfxMessageBox("Yes");

		excel_management();
		//addr, serial update
		CString strMAC;
		GetDlgItemText(IDC_EDIT_NOW_MAC, strMAC);

		CString out;
		uint32_t hexout;
		out = strMAC.GetBuffer(0);
		out = "0x" + out;
		sscanf_s(out, "%x", &hexout);

		if (hexout == 0xFFFFFF)
		{
			hexout = 1;
		}
		else
		{
			hexout++;
		}

		strMAC.Format("%06x", hexout & 0xFFFFFF);

		SetDlgItemText(IDC_EDIT_NOW_MAC, strMAC);

		CString strSerial;
		GetDlgItemText(IDC_EDIT_SERIAL3, strSerial);

		CString outSerial;
		outSerial = strSerial.GetBuffer(0);
		outSerial = "0x" + outSerial;
		sscanf_s(outSerial, "%x", &hexout);

		if (hexout == 0xFFFF)
		{
			hexout = 1;
		}
		else
		{
			hexout++;
		}

		strSerial.Format("%04x", hexout & 0xFFFF);

		SetDlgItemText(IDC_EDIT_SERIAL3, strSerial);
	}

	GetDlgItem(IDC_DOWNLOAD)->EnableWindow(TRUE);
	GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
	GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
	GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
}


HBRUSH Cmp_sgnlDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	switch (pWnd->GetDlgCtrlID())
	{
		case IDC_STATIC_BLE:
		case IDC_STATIC_DMP:
		case IDC_STATIC_SERIAL:
		case IDC_STATIC_MAC:
		case IDC_STATIC_MCU:
		case IDC_STATIC_PASS_FAIL:
			(ok_fail) ? pDC->SetTextColor(RGB(0, 0, 255)) : pDC->SetTextColor(RGB(255, 0, 0));
			break;

		case IDC_STATIC_BLE2:
		case IDC_STATIC_DMP2:
		case IDC_STATIC_SERIAL2:
		case IDC_STATIC_MAC2:
		case IDC_STATIC_MCU2:
			pDC->SetTextColor(RGB(0, 0, 0));
			break;

		default:
			break;
	}
	// TODO:  Return a different brush if the default is not desired
	return hbr;
}


void ProcessWindowMessage()
{
	MSG msg;
	while (::PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
	{
		::SendMessage(msg.hwnd, msg.message, msg.wParam, msg.lParam);
	}
}

BOOL Cmp_sgnlDlg::CmdRun(LPSTR lpszCmd, LPSTR *lpszBuffer)
{
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	BOOL bCreated = FALSE;
	SECURITY_ATTRIBUTES sa;

	memset(&pi, 0, sizeof(PROCESS_INFORMATION));
	memset(&si, 0, sizeof(STARTUPINFO));
	memset(&sa, 0, sizeof(SECURITY_ATTRIBUTES));

	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.bInheritHandle = TRUE;
	char szCmdOut[255];
	sprintf_s(szCmdOut, "files\\ld_out_%d.txt", GetCurrentThreadId());
	HANDLE hFile = CreateFile(szCmdOut, GENERIC_WRITE, 0, &sa, CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		printf("CreateFile cmd_out.txt failed\n");
		return FALSE;
	}

	char szCmd[255];
	sprintf_s(szCmd, "files\\ld_%d.cmd", GetCurrentThreadId());
	HANDLE hCmdFile = CreateFile(szCmd, GENERIC_WRITE, 0, &sa, CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);
	if (hCmdFile == INVALID_HANDLE_VALUE)
	{
		printf("CreateFile cmd.cmd failed\n");
		return FALSE;
	}
	DWORD dwSize = 0;
	WriteFile(hCmdFile, lpszCmd, strlen(lpszCmd), &dwSize, NULL);
	CloseHandle(hCmdFile);

	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
	si.hStdOutput = hFile;
	si.hStdInput = hFile;
	si.hStdError = hFile;
	si.wShowWindow = SW_HIDE;

	bCreated = CreateProcess(NULL, szCmd, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi);
	if (bCreated == FALSE)
	{
		printf("CreateProcess failed\n");
		return FALSE;
	}

	int ret = 0;
	while (1)
	{
		ret = WaitForSingleObject(pi.hProcess, INFINITE);
		//ret = WaitForSingleObject(pi.hProcess, 500);

		//ProcessWindowMessage();
		if (ret == WAIT_FAILED)
			return 0;
		else if (ret == WAIT_ABANDONED)
		{
			ResetEvent(pi.hProcess);
			continue;
		}
		else if (ret == WAIT_TIMEOUT)
		{
			continue;
		}
		else
		{
			ResetEvent(pi.hProcess);
			break;
		}
	}
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	CloseHandle(hFile);

	HANDLE hReadCmdFile = CreateFile(szCmdOut, GENERIC_READ, 0, &sa,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	dwSize = GetFileSize(hReadCmdFile, NULL);

	*lpszBuffer = new char[dwSize + 1];
	memset(*lpszBuffer, 0, dwSize);
	DWORD dwRead = 0;
	ReadFile(hReadCmdFile, *lpszBuffer, dwSize, &dwRead, NULL);
	*(*lpszBuffer + dwRead) = 0;

	CloseHandle(hReadCmdFile);

	DeleteFile(szCmd);
	DeleteFile(szCmdOut);

	return TRUE;
}

int Cmp_sgnlDlg::DownloadMCU()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString m_stlink_path = _T("files\\ST-LINK\\ST-LINK_CLI.exe");
	CString m_param;
	TCHAR *buffer;
	int nLen = 0;
	LPTSTR lpszBuf;

	m_static_mcu2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_MCU2, "MCU ");

	m_static_mcu2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_MCU, " ");

	m_static_pass_fail.SetFont(&m_font2, TRUE);
	SetDlgItemText(IDC_STATIC_PASS_FAIL, "");

	CString m_param2;
	ok_fail = TRUE;
	progressbar_status = 90;
	m_static_mcu.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_MCU, " ");
	//UpdateData(FALSE);

	m_param2 = m_stlink_path;

	CString strSTSN;
	GetDlgItemText(IDC_EDIT_ST_SN, strSTSN);
	m_param2 += _T(" -c SN=");
	m_param2 += strSTSN;

	m_param2 += _T(" SWD UR Hrst -P ");
	m_param2 += m_hex_path2;
	m_param2 += _T(" -HardRst ");//power off after mcu Download
	m_param2 += _T(" -V ");//verifying flash Download

	nLen = m_param2.GetLength();
	lpszBuf = m_param2.GetBuffer(nLen);

	//execute Download hex
	CmdRun(lpszBuf, &buffer);
	if (strstr(buffer, "Complete")) //connection success
	{
		delete buffer;

		ok_fail = TRUE;
		progressbar_status = 100;
		m_static_mcu.SetFont(&m_font, TRUE);
		//SetDlgItemText(IDC_STATIC_MCU, "OK2");
		SetDlgItemText(IDC_STATIC_MCU, "OK");
		//UpdateData(FALSE);

		download_status &= ~(STATUS_MCU_FAIL);

		if (m_smt_check)//smt
		{
			GetDlgItem(IDC_DMPDOWN)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_MCUDOWN)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
			GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
		}

		if (!download_status) // all success
		{
			download_status = 0;
			next_download_check();
		}
	}
	else
	{
		delete buffer;

		ok_fail = FALSE;
		progressbar_status = 0;
		m_static_mcu.SetFont(&m_font, TRUE);
		SetDlgItemText(IDC_STATIC_MCU, "FAIL");
		//UpdateData(FALSE);
		download_status |= STATUS_MCU_FAIL;

		if (m_smt_check)//smt
		{
			GetDlgItem(IDC_MCUDOWN)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_MCUDOWN)->ShowWindow(SW_SHOW);
		}

		GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(TRUE);
		return -1;
	}

	return 0;
}

void Cmp_sgnlDlg::OnBnClickedMCU()
{
	SetTimer(100, 1000, NULL);
	m_val_elapsed_sec = 0;
	m_start_btn_event = TRUE;
	m_thread_event_e = MCU_DOWN;
}

enum ThreadWorkingType
{
	THREAD_STOP = 0,
	THREAD_RUNNING,
	THREAD_PAUSE,
};
ThreadWorkingType m_ePTThreadWork;

int Cmp_sgnlDlg::DownloadAll()
{
	CString start_mac;
	CString now_mac;
	CString end_mac;
	int start_mac_addr;
	int now_mac_addr;
	int end_mac_addr;

	GetDlgItemText(IDC_EDIT_START_MAC, start_mac);
	GetDlgItemText(IDC_EDIT_NOW_MAC, now_mac);
	GetDlgItemText(IDC_EDIT_END_MAC, end_mac);

	sscanf_s((LPCTSTR)start_mac, "%x", &start_mac_addr);
	sscanf_s((LPCTSTR)now_mac, "%x", &now_mac_addr);
	sscanf_s((LPCTSTR)end_mac, "%x", &end_mac_addr);

	if ((now_mac_addr < start_mac_addr) || (now_mac_addr > end_mac_addr))
	{
		KillTimer(100);
		MessageBox("MAC Addr 범위를 확인하세요!!");
		return -1;
	}

	m_static_ble2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_BLE2, "BLE ");
	m_static_dmp2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_DMP2, "DMP ");
	m_static_serial2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_SERIAL2, "SERIAL ");
	m_static_mac2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_MAC2, "MAC ");
	m_static_mcu2.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_MCU2, "MCU ");

	m_static_ble.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_BLE, "");
	m_static_dmp.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_DMP, "");
	m_static_serial.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_SERIAL, "");
	m_static_mac.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_MAC, "");
	m_static_mcu.SetFont(&m_font, TRUE);
	SetDlgItemText(IDC_STATIC_MCU, "");



	m_static_pass_fail.SetFont(&m_font2, TRUE);
	SetDlgItemText(IDC_STATIC_PASS_FAIL, "");

	progressbar_status = 10;
	download_status = 0;
	return 0;
#if 0
	//////////////////////////////////Hex Down////////////////////////////////////
	DownloadBLE();
	if (download_status)
	{
		KillTimer(100);
		GetDlgItem(IDC_BLEDOWN)->EnableWindow(TRUE);
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
		progressbar_status = 0;
		//UpdateData(FALSE);

		m_static_pass_fail.SetFont(&m_font2, TRUE);
		SetDlgItemText(IDC_STATIC_PASS_FAIL, "FAIL");
		return;
	}

	//////////////////////////////////DMP DOWN////////////////////////////////////
	DownloadDMP();
	if (download_status)
	{
		KillTimer(100);
		GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(TRUE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(TRUE);
		progressbar_status = 0;
		//UpdateData(FALSE);

		m_static_pass_fail.SetFont(&m_font2, TRUE);
		SetDlgItemText(IDC_STATIC_PASS_FAIL, "FAIL");

		if (m_smt_check)//smt
		{
			SetTimer(101, 2000, NULL);
			if (AfxMessageBox("지그의 전원을 끄고 켜주세요") == IDOK)
			{
			}

			GetDlgItem(IDC_DMPDOWN)->EnableWindow(TRUE);
			GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
			GetDlgItem(IDC_DMPDOWN)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_MCUDOWN)->ShowWindow(SW_HIDE);
		}
		return;
	}

	/////////////////////////////////////MCU DOWN/////////////////////////////////
	DownloadMCU();
	if (download_status)//fail
	{
		GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(TRUE);
		progressbar_status = 0;
		//UpdateData(FALSE);

		m_static_pass_fail.SetFont(&m_font2, TRUE);
		SetDlgItemText(IDC_STATIC_PASS_FAIL, "FAIL");
	}
	else//success
	{
		GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
		progressbar_status = 0;
		//UpdateData(FALSE);

		m_static_pass_fail.SetFont(&m_font2, TRUE);
		SetDlgItemText(IDC_STATIC_PASS_FAIL, "PASS");
	}

	KillTimer(100);
	return;
	//////////////////////////////////////////////////////////////////////////////
#endif
}

void Cmp_sgnlDlg::OnBnClickedDownload()
{
	SetTimer(100, 1000, NULL);
	m_val_elapsed_sec = 0;
	m_start_btn_event = TRUE;
	m_thread_event_e = ALL_DOWN;
}

void Cmp_sgnlDlg::OnBnClickedHexBrowse2()
{
	// TODO: Add your control notification handler code here
	TCHAR szFilters[] = _T("Hex Files (*.hex)|*.hex|All Files (*.*)|*.*||");

	// Create an Open dialog; the default file name extension is ".my".
	CFileDialog fileDlg(TRUE, _T("hex"), _T("*.hex"),
		OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilters);

	// Display the file dialog. When user clicks OK, fileDlg.DoModal() 
	// returns IDOK.
	if (fileDlg.DoModal() == IDOK)
	{
		m_hex_path2 = fileDlg.GetPathName();

		// Implement opening and reading file in here.

		//Change the window's title to the opened file's title.
		CString fileName = fileDlg.GetFileTitle();


		CEdit *p = (CEdit *)GetDlgItem(IDC_EDIT_HEX_PATH2);
		p->SetWindowText(m_hex_path2);
	}

	config_write();
}

void Cmp_sgnlDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	UINT m_sec;
	UINT m_min;
	CString str;

	switch (nIDEvent)
	{
	case 100:
		m_val_elapsed_sec++;
		m_sec = m_val_elapsed_sec % 60;
		m_min = m_val_elapsed_sec / 60;
		if (m_val_elapsed_sec >= 3600)
			m_val_elapsed_sec = 0;
#if 0
		m_static_process_time.Format(_T("Time: %02d:%02d"), m_min, m_sec);
		SetDlgItemText(IDC_PROCESS_TIME, m_static_process_time);
		m_progress.SetPos(progressbar_status);
#else
		// Bursting Progress
		SetProgressPos(progressbar_status, m_min, m_sec);
#endif
		if (progressbar_status == 100)
			KillTimer(100);
		break;

	case 101:
		HWND wndDlg = ::GetLastActivePopup(m_hWnd);
		if (wndDlg && wndDlg != m_hWnd)
		{
			char buffer[256] = { 0 };
			::GetClassName(wndDlg, buffer, 256);
			if (CString("#32770") == buffer) //메시지 박스는 분명히 다이얼로그이며 클래스명이 #32770
			{
				::EndDialog(wndDlg, IDOK);
				KillTimer(101);
			}
		}
		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}

UINT Cmp_sgnlDlg::ExecuteProcessThread(LPVOID _method)
{
	// TODO: Add your specialized code here and/or call the base class
	//UI Thread 처리
	Cmp_sgnlApp* pApp = (Cmp_sgnlApp*)AfxGetApp();
	Cmp_sgnlDlg* pDlg = (Cmp_sgnlDlg*)pApp->m_pMainWnd;

	while (1)
	{
		if (pDlg->m_start_btn_event)
		{
			switch (pDlg->m_thread_event_e)
			{
			case ALL_DOWN:
				pDlg->GetDlgItem(IDC_DOWNLOAD)->EnableWindow(FALSE);
				if (pDlg->DownloadAll() < 0)
				{
					pDlg->KillTimer(100);
					pDlg->progressbar_status = 0;
					pDlg->m_start_btn_event = FALSE;
					pDlg->m_thread_event_e = FAIL_DOWN;
					pDlg->m_val_elapsed_sec = 0;

					pDlg->GetDlgItem(IDC_DOWNLOAD)->EnableWindow(TRUE);
				}
				else
				{
					pDlg->m_thread_event_e = BLE_DOWN;
				}
				break;
			case BLE_DOWN:
				if (pDlg->DownloadBLE() < 0)
				{
					if (pDlg->download_status)
					{
						pDlg->KillTimer(100);
						pDlg->progressbar_status = 0;
						pDlg->m_start_btn_event = FALSE;
						pDlg->m_thread_event_e = FAIL_DOWN;

						pDlg->GetDlgItem(IDC_BLEDOWN)->EnableWindow(TRUE);
						pDlg->GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
						pDlg->GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
						pDlg->m_static_pass_fail.SetFont(&m_font2, TRUE);
						pDlg->SetDlgItemText(IDC_STATIC_PASS_FAIL, "FAIL");
					}
				}
				else
				{
					pDlg->m_thread_event_e = DMP_DOWN;
				}
				break;
			case DMP_DOWN:
				if( pDlg->DownloadDMP() < 0 )
				{
					if (pDlg->download_status)
					{
						pDlg->KillTimer(100);
						pDlg->progressbar_status = 0;
						pDlg->m_start_btn_event = FALSE;
						pDlg->m_thread_event_e = FAIL_DOWN;

						pDlg->GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
						pDlg->GetDlgItem(IDC_DMPDOWN)->EnableWindow(TRUE);
						pDlg->GetDlgItem(IDC_MCUDOWN)->EnableWindow(TRUE);
						pDlg->m_static_pass_fail.SetFont(&m_font2, TRUE);
						pDlg->SetDlgItemText(IDC_STATIC_PASS_FAIL, "FAIL");

						if (pDlg->m_smt_check)//smt
						{
							pDlg->SetTimer(101, 2000, NULL);

							pDlg->GetDlgItem(IDC_DMPDOWN)->EnableWindow(TRUE);
							pDlg->GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
							pDlg->GetDlgItem(IDC_DMPDOWN)->ShowWindow(SW_SHOW);
							pDlg->GetDlgItem(IDC_MCUDOWN)->ShowWindow(SW_HIDE);
							if (AfxMessageBox("지그의 전원을 끄고 켜주세요") == IDOK)
							{
							}
						}
					}
				}
				else
				{
					pDlg->m_thread_event_e = MCU_DOWN;
				}
				break;
			case MCU_DOWN:
				/////////////////////////////////////MCU DOWN/////////////////////////////////
				if (pDlg->DownloadMCU() < 0)
				{
					if (pDlg->download_status)//fail
					{
						pDlg->KillTimer(100);
						pDlg->progressbar_status = 0;
						pDlg->m_start_btn_event = FALSE;
						pDlg->m_thread_event_e = FAIL_DOWN;

						pDlg->GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
						pDlg->GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
						pDlg->GetDlgItem(IDC_MCUDOWN)->EnableWindow(TRUE);
						pDlg->m_static_pass_fail.SetFont(&m_font2, TRUE);
						pDlg->SetDlgItemText(IDC_STATIC_PASS_FAIL, "FAIL");
					}
				}
				else//success
				{
					pDlg->progressbar_status = 100;
					pDlg->m_start_btn_event = FALSE;
					pDlg->m_thread_event_e = PASS_DOWN;

					pDlg->GetDlgItem(IDC_BLEDOWN)->EnableWindow(FALSE);
					pDlg->GetDlgItem(IDC_DMPDOWN)->EnableWindow(FALSE);
					pDlg->GetDlgItem(IDC_MCUDOWN)->EnableWindow(FALSE);
					pDlg->GetDlgItem(IDC_DOWNLOAD)->EnableWindow(TRUE);
					pDlg->m_static_pass_fail.SetFont(&m_font2, TRUE);
					pDlg->SetDlgItemText(IDC_STATIC_PASS_FAIL, "PASS");
				}

				break;

			default:
				break;
			}
		}

		Sleep(1);
	}
	return 0;
}

int Cmp_sgnlDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	CWinThread *p1 = NULL;
	p1 = AfxBeginThread(ExecuteProcessThread, this);

	if (p1 == NULL)
	{
		MessageBox("Error");
		CloseHandle(p1);
	}

	return 0;
}

// 값이 변경될때마다 다음과 같이 설정하면 된다.
// Progress Set Pos
void Cmp_sgnlDlg::SetProgressPos( int nPos, int min, int sec )
{
	// Range는 Max Burst Interval로 정한다.
	//m_progress.SetRange( 0, 100 ) ;

	CString str ;
	str.Format( _T( "Time: %02d:%02d"), min, sec ) ;
	m_progress.SetWindowText( str ) ;

	// Bursting Progress
	m_progress.SetPos( nPos ) ;
}

BOOL Cmp_sgnlDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_KEYDOWN)
	{
		// ESC 키, Enter 키
		if ((pMsg->wParam == VK_ESCAPE) || (pMsg->wParam == VK_RETURN))
		{
			// 여기에 원하는 동작의 코드를 삽입
			return true;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void Cmp_sgnlDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	config_write();
	CDialogEx::OnCancel();
}
