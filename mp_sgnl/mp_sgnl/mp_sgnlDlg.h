
// mp_sgnlDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "TextProgressCtrl.h"


// Cmp_sgnlDlg dialog
class Cmp_sgnlDlg : public CDialogEx
{
// Construction
public:
	Cmp_sgnlDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MP_SGNL_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	void config_write();
	void excel_management();
	void next_download_check();

	CComboBox m_ctlBaud;
	CString m_baud[3];
	CString m_curBaud;
	afx_msg void OnCbnSelchangeCombo2();
	long m_baudrate;
	CComboBox m_ctlPort;
	CString m_port[20];
	CString m_curPort;
	CString m_hex_path;
	CString m_hex_path2;
	int m_smt_check;
	CButton m_btn_smt_check;
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnBnClickedHexBrowse();
	CEdit m_ctrHexpath;
	CEdit m_ctrHexpath2;
	afx_msg void OnEnChangeEditSerial();
	afx_msg void OnEnChangeEditStartMac();
	afx_msg void OnEnChangeEditNowMac();
	afx_msg void OnEnChangeEditEndMac();
	//CString m_CtrSerial;
	//CString m_CtrMac;
	CEdit m_CtrSerial;
	CEdit m_CtrSTSN;
	CEdit m_CtrStartMAC;
	CEdit m_CtrNowMAC;
	CEdit m_CtrEndMAC;

	bool ExecuteProcessRedirection(CString sExeName, CEdit *p);

	int download_status;
	afx_msg void OnEnChangeEditSerial3();
	CEdit m_CtrSerial3;
	CEdit m_CtrSerial2;
	CEdit m_CtrSerial4;
	CStatic m_static_ble;
	CStatic m_static_dmp;
	CStatic m_static_serial;
	CStatic m_static_mac;
	CStatic m_static_mcu;
	CStatic m_static_time;

	CBrush m_brush;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CStatic m_static_ble2;
	CStatic m_static_dmp2;
	CStatic m_static_serial2;
	CStatic m_static_mac2;
	CStatic m_static_mcu2;
	CStatic m_static_pass_fail;
	CEdit m_ctr_count;
	UINT m_val_cnt;

	CButton m_ctr_Chk;
	afx_msg void OnBnClickedDownload();
	BOOL CmdRun(LPSTR lpszCmd, LPSTR *lpszBuffer);

	UINT progressbar_status;
	UINT ok_fail;
	CString m_static_process_time;
	UINT m_val_elapsed_sec;
	UINT m_val_elapsed_sec_backup;

	//CProgressCtrl m_progress;
	afx_msg void OnBnClickedBLEdown();
	afx_msg void OnBnClickedDMP();
	afx_msg void OnBnClickedMCU();

	//	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedHexBrowse2();
	afx_msg void DisplayResult(CString strText);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	static UINT ExecuteProcessThread(LPVOID _method);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	int DownloadAll();
	int DownloadBLE();
	int DownloadDMP();
	int DownloadMCU();
	
	typedef enum download_event_thread_e_ {
		ALL_DOWN = 0,
		BLE_DOWN,
		DMP_DOWN,
		MCU_DOWN,
		PASS_DOWN,
		FAIL_DOWN,
	}download_event_thread_e;

	download_event_thread_e m_thread_event_e;
	BOOL m_start_btn_event;

	// Bursting Progress
	CTextProgressCtrl m_progress;
	void SetProgressPos( int nPos, int min, int sec );
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedCancel();
};
