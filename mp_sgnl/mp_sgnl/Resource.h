//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by mp_sgnl.rc
//
#define IDD_MP_SGNL_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_COMBO_PORT                  1000
#define IDC_COMBO_BAUD                  1001
#define IDC_EDIT_HEX_PATH               1004
#define IDC_HEX_BROWSE                  1005
#define IDC_BLEDOWN                     1006
#define IDC_EDIT_HEX_PATH2              1007
#define IDC_EDIT_SERIAL                 1009
#define IDC_EDIT_NOW_MAC                1010
#define IDC_DMPDOWN                     1014
#define IDC_EDIT_SERIAL2                1015
#define IDC_EDIT_SERIAL3                1016
#define IDC_EDIT_SERIAL4                1017
#define IDC_STATIC_DMP                  1018
#define IDC_STATIC_SERIAL               1019
#define IDC_STATIC_MAC                  1020
#define IDC_MCUDOWN                     1024
#define IDC_STATIC_SERIAL2              1025
#define IDC_STATIC_MAC2                 1026
#define IDC_STATIC_DMP2                 1027
#define IDC_STATIC_MCU                  1028
#define IDC_STATIC_MCU2                 1029
#define IDC_DOWNLOAD                    1030
#define IDC_PROGRESS1                   1031
#define IDC_STATIC_BLE                  1032
#define IDC_STATIC_BLE2                 1033
#define IDC_HEX_BROWSE2                 1034
#define IDC_STATIC_PASS_FAIL            1035
#define IDC_EDIT_END_MAC                1036
#define IDC_EDIT_START_MAC              1037
#define IDC_EDIT_ST_SN                  1038
#define IDC_STATIC_MCU3                 1041
#define IDC_PROCESS_TIME                1042
#define IDC_SMT_CHECK                   1044

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1045
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
