
// mp_sgnl.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// Cmp_sgnlApp:
// See mp_sgnl.cpp for the implementation of this class
//

class Cmp_sgnlApp : public CWinApp
{
public:
	Cmp_sgnlApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern Cmp_sgnlApp theApp;