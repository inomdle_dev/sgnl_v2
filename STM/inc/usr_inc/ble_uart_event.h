#ifndef _BLE_UART_EVENT_H_
#define _BLE_UART_EVENT_H_

/* USART peripheral configuration defines */
#define BLE_UART				USART1
#define BLE_UART_CLK				RCC_APB2Periph_USART1
#define BLE_UART_IRQn				USART1_IRQn

#define BLE_UART_GPIO_PORT			GPIOA
#define BLE_UART_GPIO_CLK			RCC_AHB1Periph_GPIOA
#define BLE_UART_AF				GPIO_AF_USART1

#define BLE_UART_TX_PIN				GPIO_Pin_9
#define BLE_UART_TX_SOURCE			GPIO_PinSource9

#define BLE_UART_RX_PIN				GPIO_Pin_10
#define BLE_UART_RX_SOURCE			GPIO_PinSource10

#define ble_uart_isr				USART1_IRQHandler

#define BLE_UART_BAUDRATE			115200
#define UART_BUFFERSIZE				256

void ble_uart_init( void );
void ble_uart_config( uint32_t baud_rate );
uint8_t ble_uart_read_byte( void );

void BLE_CommandHandler( void );
void ble_uart_send_data( uint8_t byte );
void ble_uart_send( uint8_t* ui8_byte, uint16_t ui16_length );
void ble_command_send_data( uint8_t command, uint8_t param );
void ble_lowpower_uart_tx( void );
void ble_lowpower_uart_send( uint8_t* ui8_byte, uint16_t ui16_length );
void ble_uart_send_packet(uint8_t cmd, uint8_t *data, uint8_t size);
void ble_printf( const char *form, ... );

#endif //_BLE_UART_EVENT_H_
