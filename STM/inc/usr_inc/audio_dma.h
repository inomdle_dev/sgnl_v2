#ifndef _AUDIO_DMA_H_
#define _AUDIO_DMA_H_

/* I2S DMA Stream definitions */
#define AUDIO_I2S_DMA_CLOCK		RCC_AHB1Periph_DMA1
#define AUDIO_I2S_DMA_STREAM		DMA1_Stream5
#define AUDIO_I2S_DMA_CHANNEL		DMA_Channel_0
#define AUDIO_I2S_DMA_IRQ		DMA1_Stream5_IRQn
#define AUDIO_I2S_DMA_FLAG_TC		DMA_FLAG_TCIF5
#define AUDIO_I2S_DMA_FLAG_HT		DMA_FLAG_HTIF5
#define AUDIO_I2S_DMA_FLAG_FE		DMA_FLAG_FEIF5
#define AUDIO_I2S_DMA_FLAG_TE		DMA_FLAG_TEIF5
#define AUDIO_I2S_DMA_FLAG_DME		DMA_FLAG_DMEIF5

#define AUDIO_I2S_DMA_PERIPH_DATA_SIZE	DMA_PeripheralDataSize_HalfWord
#define AUDIO_I2S_DMA_MEM_DATA_SIZE	DMA_MemoryDataSize_HalfWord

void Audio_DMA_config(void);
void Audio_MAL_Play(uint32_t *Addr, uint32_t Size);
void Audio_MAL_Stop(void);

#endif //_AUDIO_DMA_H_