#ifndef _LED_CONTROL_H_
#define _LED_CONTROL_H_

#define LED_POWER_OFF_CNT		333
#define LED_POWER_ON_CNT		333

#define LED_DEVICE_RESET_CNT		300

#define LED_BATT_GAUGE_CNT		360
#define LED_BATT_LOW_CNT		333
#define LED_BATT_CHARGING_CNT		333
#define LED_BATT_FULL_CNT		333

#define LED_UNPAIRED_CNT		600
#define LED_UNPAIR_CNT			333
#define LED_WAIT_PAIRING_CNT		333
#define LED_PAIRING_COMPLETE_CNT	333
#define LED_DISCONN_TIMEOUT_CNT		333

#define LED_DND_ON_CNT			333
#define LED_DND_OFF_CNT			333

#define LED_FAVORITE_SELECT_CNT		333
#define LED_FAVORITE_CALL_CNT		333

#define LED_INCOMING_CALL_CNT		333
#define LED_ACTIVITY_NOTI_CNT		333
#define LED_LONG_SIT_NOTI_CNT		200

#define LED_APP_NOTI_CNT		200

void led_power_off( void );
void led_power_on( void );

void led_device_reset( void );

void led_batt_gauge( void );
void led_batt_low( void );
void led_batt_charging( void );
void led_batt_full( void );

void led_unpaired( void );
void led_unpair( void );
void led_wait_pairing( void );
void led_pairing_complete( void );
void led_disconn_timeout( void );

void led_dnd_on( void );
void led_dnd_off( void );

void led_favorite_select( void );
void led_favorite_call( void );

void led_incoming_call( void );
void led_activity_noti( void );
void led_long_sit_noti( void );

void led_app_noti( void );

#endif //_LED_CONTROL_H_