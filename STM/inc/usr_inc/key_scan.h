#ifndef _KEY_SCAN_H_
#define _KEY_SCAN_H_

typedef enum key_state_{
  	KEY_NONE			= 0x0000,
	KEY_POWER_PRESS		= 0x0001,
	KEY_POWER_RELEASE	= 0x0002,
	KEY_POWER_SHORT		= 0x0003,

	KEY_VOLUP_PRESS		= 0x0010,
	KEY_VOLUP_RELEASE	= 0x0020,
	KEY_VOLUP_SHORT		= 0x0030,

	KEY_VOLDN_PRESS		= 0x0040,
	KEY_VOLDN_RELEASE	= 0x0080,
	KEY_VOLDN_SHORT		= 0x00C0,
	
	KEY_PWRUP_PRESS		= 0x0011,
	KEY_UPDN_PRESS		= 0x0050,
	KEY_PWRUPDN_PRESS	= 0x0051,
	
	KEY_ANY_RELEASE		= 0x00A2,
}key_state_t;

typedef enum key_event_{
  	KEY_EVENT_NONE		= 0x00,
	KEY_EVENT_POWER		= 0x01,
	KEY_EVENT_VOLUP		= 0x02,
	KEY_EVENT_VOLDN		= 0x04,
	
}key_event_t;

typedef enum key_action_{
  	KEY_ACTION_READY	= 0x00,
	KEY_ACTION_FUNC		= 0x01,
	KEY_ACTION_VOLUP	= 0x02,
	KEY_ACTION_VOLDN	= 0x04,

}key_action_t;

extern key_state_t g_key_state;
extern key_event_t g_key_event;

void key_scan( void );
void key_action( void );
void key_long_press( void );
void key_short_press( void );
#endif //_KEY_SCAN_H_