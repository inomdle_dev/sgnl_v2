#ifndef STRUCT_H
#define STRUCT_H

#include "global_define.h"

typedef enum power_mode_{
	
	PWR_MODE_NONE			= 0x0,
	PWR_MODE_LOW			= 0x1,
	PWR_MODE_HIGH			= 0x2,
	
}power_mode_t;

typedef struct timer_struct_{
	
	volatile uint16_t ui16_pairing_cnt;
	volatile uint16_t ui16_sleep_cnt;
	
#ifdef ENABLE_BLE_UART
	volatile uint16_t ui16_ble_delay_cnt;
#endif

#ifdef ENABLE_PWM
	volatile uint16_t ui16_pattern_cnt;
	volatile uint16_t ui16_sub_pattern_cnt;
#endif
	
#ifdef ENABLE_FUEL_GAUGE
	volatile uint16_t ui16_charger_cnt;
#endif
}timer_struct_t;

extern timer_struct_t g_str_timer;

#ifdef ENABLE_AUDIO
typedef enum audio_state_{
	
	AUDIO_STATE_STOP		= 0,
	AUDIO_STATE_PLAY		= 1,
	
}audio_state_t;

typedef enum audio_frame_{
	
	AUDIO_FRAME_OK = 0,
	AUDIO_FRAME_ERR = 1,
	
}audio_frame_t;

typedef enum audio_channel_{
	
	AUDIO_CHANNEL_NONE		= 0,
	AUDIO_CHANNEL_LEFT		= 0,
	AUDIO_CHANNEL_RIGHT		= 1,
	AUDIO_CHANNEL_TOGGLE	= 1,
	
}audio_channel_t;

typedef enum audio_input_freq_{
	
	AUDIO_FREQ_NONE = I2S_AudioFreq_8k,
	AUDIO_FREQ_8K = I2S_AudioFreq_8k,
	AUDIO_FREQ_16K = I2S_AudioFreq_16k,
	AUDIO_FREQ_44_1K = I2S_AudioFreq_44k,
	AUDIO_FREQ_48K = I2S_AudioFreq_48k,
	
}audio_input_freq_t;

typedef struct audio_struct_{
	
	volatile uint16_t ui16_data[AUDIO_FRAME_BUFFER][AUDIO_FRAME_SIZE];
	volatile uint16_t ui16_rx_idx;
	volatile uint16_t ui16_tx_idx;

	volatile uint16_t ui16_rx_frame_no;
	volatile uint16_t ui16_tx_frame_no;
	
	volatile audio_input_freq_t ui16_audio_type;

	volatile uint16_t ui16_data_format;
#ifdef APP_GAIN_CONTROL
	volatile float32_t f32_volume_step;
#endif
}audio_struct_t;
extern audio_struct_t g_str_audio;

#ifdef ENABLE_HY_FE
typedef struct fe_struct_{
	
	volatile uint16_t ui16_frame_no;
	volatile uint16_t ui16_data[AUDIO_FRAME_BUFFER][160];
}fe_struct_t;

extern fe_struct_t g_str_fe;
#endif //ENABLE_HY_FE

#endif //ENABLE_AUDIO

#ifdef ENABLE_BLE_UART
typedef struct bt_uart_struct_{
	
	volatile uint8_t	ui8_rx_buf[UART_BUFFERSIZE];
	volatile uint16_t	ui16_rx_head;
	volatile uint16_t	ui16_rx_tail;
	volatile uint16_t	ui16_rx_cnt;
	volatile uint16_t	ui16_rx_flag;
	
	volatile uint8_t	ui8_tx_buf[UART_BUFFERSIZE];
	volatile uint16_t	ui16_tx_head;
	volatile uint16_t	ui16_tx_tail;
	volatile uint16_t	ui16_tx_cnt;
	volatile uint16_t	ui16_tx_flag;
	
}bt_uart_struct_t;
extern bt_uart_struct_t g_str_ble;
#endif
#ifdef ENABLE_BT_UART
extern bt_uart_struct_t g_str_bt;
#endif

#ifdef ENABLE_DBG
typedef struct dbg_uart_struct_{
        volatile uint16_t       ui16_rx_cnt;
        volatile uint16_t       ui16_tx_cnt;

        volatile uint8_t        ui8_rx_buf[DBG_UART_BUFFERSIZE];
        volatile uint8_t        ui8_tx_buf[DBG_UART_BUFFERSIZE];
}dbg_uart_struct_t;

extern dbg_uart_struct_t g_str_dbg;

#endif //ENABLE_DBG

typedef enum ble_report_e_{
	BLE_REPORT_INIT			= 0x00,
	BLE_REPORT_BT_PAIRING_ON	= 0x01,
	BLE_REPORT_CALL_STATUS		= 0x02,
	BLE_REPORT_VOLUME_CONTROL	= 0x03,
	BLE_REPORT_DEFAULT_RESET	= 0x04,
	BLE_REPORT_MCU_SLEEP		= 0x05,
}ble_report_e;
extern volatile ble_report_e ble_report;


typedef enum cyw20706_report_e_{

	CYW20706_REPORT_INIT		= 0x00,
	
	CMD_MCU_STATUS			= 0x80,
	CMD_CALL_STATUS			= 0x81,
	CMD_POWER_STATUS		= 0x82,
	CMD_APP_NOTI_STATUS		= 0x83,
	CMD_BLE_CONNECTION_STATUS	= 0x84,
	CMD_BT_CONNECTION_STATUS	= 0x85,
	CMD_BATTERY_STATUS		= 0x86,
	CMD_BT_PAIRING_STATUS		= 0x87,
	CMD_SELECT_FAVORITE_STATUS	= 0x88,
	CMD_CHARGER_STATUS		= 0x89,
	CMD_VOLUME_CONTROL		= 0x8A,
	CMD_HFP_A2DP			= 0x8B,
	CMD_DND_STATUS			= 0x8C,
	CMD_AMP_STATUS			= 0x8D,
	CMD_MCU_SLEEP_STATUS		= 0x8E,
	CMD_FW_UPDATE_STATUS		= 0x8F,

#ifdef HW_PP4
	CMD_BATTERY_TIMER_STATUS	= 0x90,
#endif
	CMD_CALL_FREQ_STATUS		= 0x91,
	CMD_CGEQ_STATUS			= 0x92,
	
	CMD_SOUND_CHECK			= 0xF0,
#ifdef SMT_CHECK
	CMD_PEDO_STATUS_CHECK		= 0xF1,
	CMD_PEDO_COUNT_CHECK		= 0xF2,
#endif
	
	CMD_EXHIBITION_MODE		= 0xFD,
	CMD_FACTORY_RESET		= 0xFE,

} cyw20706_cmd_status_e;

typedef enum mcu_status_e_{
	
	MCU_SLEEP	= 0x0,
	MCU_WAKEUP	= 0x1,
	
	MCU_INIT	= 0xF,
} mcu_status_e;

typedef enum call_status_e_{
	
	CALL_READY	= 0x0,
	CALL_CALLING	= 0x1,
	CALL_INCOMING	= 0x2,
	
	CALL_OUTGOING	= 0x4,
	
	CALL_DND	= 0x6,
	CALL_REJECT	= 0x7,
	
	CALL_FAVORITE	= 0x8,

	CALL_INIT	= 0xF,
} call_status_e;

typedef enum power_status_e_{
	
	POWER_OFF	= 0x0,
	POWER_ON	= 0x1,

	POWER_INIT	= 0xF,
} power_status_e;

typedef enum ble_connection_e_{
	
	BLE_DISC_DISC		= 0x0,
	BLE_DISC_CONN		= 0x1,
	BLE_CONN_DISC		= 0x2,
	BLE_CONN_CONN		= 0x3,

	BLE_CONNECTION_INIT	= 0xF,
} ble_conn_status_e;

typedef enum bt_connection_e_{
	
	BT_DISC_DISC		= 0x0,
	BT_DISC_CONN		= 0x1,
	BT_CONN_DISC		= 0x2,
	BT_CONN_CONN		= 0x3,

	BT_CONNECTION_INIT	= 0xF,
} bt_conn_status_e;

typedef enum bt_pairing_e_{
	
	BT_PAIRING_START	= 0x1,
	BT_PAIRING_TIMEOUT	= 0x2,
	BT_PAIRING_SUCCESS	= 0x3,
	BT_PAIRING_FAIL		= 0x4,
	BT_PAIRING_UNPAIRED	= 0x5,
	BT_PAIRING		= 0x6,

	BT_PAIRING_INIT		= 0xF,
} bt_pairing_status_e;

typedef enum favorite_number_e_{
	
	FAVORITE_1ST		= 0x0,
	FAVORITE_2ND		= 0x1,
	FAVORITE_3RD		= 0x2,
	FAVORITE_4TH		= 0x3,
	FAVORITE_5TH		= 0x4,

	FAVORITE_INIT		= 0xF,
} favorite_number_e;

typedef enum charger_status_e_{
	
	CHARGER_DETACHED	= 0x0,
	CHARGER_ATTACHED	= 0x1,
	
	CHARGER_STATUS_INIT	= 0xF,
} charger_status_e;

typedef enum dnd_status_e_{
	
	DND_STATUS_DISABLE	= 0x0,
	DND_STATUS_ENABLE	= 0x1,

	DND_STATUS_INIT		= 0xF,
} dnd_status_e;

typedef enum amp_status_e_{
	
	AMP_DISABLE	= 0x0,
	AMP_ENABLE	= 0x1,

	AMP_STATUS_INIT		= 0xF,
} amp_status_e;

typedef enum haptic_status_e_{
	
	HAPTIC_STATUS_OFF	= 0x0,
	HAPTIC_STATUS_ON	= 0x1,
	
	HAPTIC_STATUS_INIT	= 0xF,
} haptic_status_e;

typedef enum fw_upgrade_status_e_{
	
	FW_UPGRADE_READY	= 0x0,
	FW_UPGRADE_START	= 0x1,
	
	FW_UPGRADE_STATUS_INIT	= 0xF,
} fw_upgrade_status_e;

typedef enum volume_level_e_{
	VOLUME_LEVEL_0	= 0x0,
	VOLUME_LEVEL_1	= 0x1,
	VOLUME_LEVEL_2	= 0x2,
	VOLUME_LEVEL_3	= 0x3,
	VOLUME_LEVEL_4	= 0x4,
	VOLUME_LEVEL_5	= 0x5,

	VOLUME_LEVEL_INIT,
} volume_level_status_e;

#ifdef APP_GAIN_CONTROL
typedef enum app_volume_level_e_{
	APP_MAX_VOLUME_LEVEL_0	= 0,
	APP_MAX_VOLUME_LEVEL_1	= 1,
	APP_MAX_VOLUME_LEVEL_2	= 2,
	APP_MAX_VOLUME_LEVEL_3	= 3,
	APP_MAX_VOLUME_LEVEL_4	= 4,
	APP_MAX_VOLUME_LEVEL_5	= 5,
	APP_MAX_VOLUME_LEVEL_6	= 6,
	APP_MAX_VOLUME_LEVEL_7	= 7,
	APP_MAX_VOLUME_LEVEL_8	= 8,
	APP_MAX_VOLUME_LEVEL_9	= 9,
	APP_MAX_VOLUME_LEVEL_10	= 10,
	APP_MAX_VOLUME_LEVEL_11	= 11,
	APP_MAX_VOLUME_LEVEL_12	= 12,
	APP_MAX_VOLUME_LEVEL_13	= 13,
	APP_MAX_VOLUME_LEVEL_14	= 14,
	APP_MAX_VOLUME_LEVEL_15	= 15,
	APP_MAX_VOLUME_LEVEL_16	= 16,
	APP_MAX_VOLUME_LEVEL_17	= 17,
	APP_MAX_VOLUME_LEVEL_18	= 18,
	APP_MAX_VOLUME_LEVEL_19	= 19,
	APP_MAX_VOLUME_LEVEL_20	= 20,
	APP_MAX_VOLUME_LEVEL_21	= 21,
	APP_MAX_VOLUME_LEVEL_22	= 22,
	APP_MAX_VOLUME_LEVEL_23	= 23,
	APP_MAX_VOLUME_LEVEL_24	= 24,
	APP_MAX_VOLUME_LEVEL_25	= 25,
	APP_MAX_VOLUME_LEVEL_26	= 26,
	APP_MAX_VOLUME_LEVEL_27	= 27,
	APP_MAX_VOLUME_LEVEL_28	= 28,
	APP_MAX_VOLUME_LEVEL_29	= 29,
	APP_MAX_VOLUME_LEVEL_30	= 30,
	APP_MAX_VOLUME_LEVEL_31	= 31,

	APP_MAX_VOLUME_LEVEL_INIT = 32,
} app_max_vol_status_e;
#endif

typedef enum audio_type_e_{
	
	AUDIO_HFP		= 0x0,
	AUDIO_A2DP		= 0x1,
	
	AUDIO_TYPE_INIT		= 0xF,
} audio_type_status_e;

typedef enum app_notification_e_{
	
	APP_NOTI_OFF	= 0x0,
	APP_NOTI_ON	= 0x1,

	APP_NOTI_INIT	= 0xF,
} app_notification_e;

extern volatile uint8_t g_favorite_num[LED_NUM];
extern volatile uint8_t g_battery_percentage;


typedef struct bitflag_struct_{

#ifdef ENABLE_AUDIO
	volatile audio_frame_t	b1_frame_state:1;
	
	volatile audio_state_t	b1_audio_rx_state:1;
	volatile audio_state_t	b1_audio_tx_state:1;
	volatile uint32_t	b1_audio_stream_flag:1;
#endif

#ifdef ENABLE_HY_FE
	volatile uint32_t	b1_fe_enable:1;
	volatile uint32_t	b1_cgeq_enable:1;
	volatile uint32_t	b2_fe_start_flag:2;
	volatile uint32_t	b1_fft_enable:1;
#endif
	
#ifdef ENABLE_BLE_UART
	volatile uint32_t	b1_incoming_call:1;
	volatile uint32_t	b2_ble_rcv_flag:2;
	volatile uint32_t	b1_ble_send_flag:1;
#endif

	volatile uint32_t	b1_charger_wakeup_flag:1;
	volatile uint32_t	b1_power_on_start:1;
	volatile uint32_t	b1_mcu_sleep_status:1;

#ifdef ENABLE_DBG
	volatile uint32_t	b3_dbg_mode:3;
	volatile uint32_t	b1_dbg_parsing:1;
	volatile uint32_t	b1_dbg_enable:1;
#endif
	
	volatile uint32_t	b1_mic_on_off:1;
	volatile uint32_t	b1_beep_on_off:1;
	
	volatile uint32_t	b1_forced_sleep:1;
	volatile uint32_t	b1_charging_complete:1;
	volatile uint32_t	b1_exhibition_mode:1;
	volatile uint32_t	b1_charger_led_disp:1;
}bitflag_struct_t;

extern bitflag_struct_t g_str_bitflag;

typedef enum pattern_type_e_{

	PATTERN_TYPE_POWER_OFF		= 0x00,
	PATTERN_TYPE_POWER_ON		= 0x01,
        PATTERN_TYPE_APP_NOTI		= 0x02,
        PATTERN_TYPE_INCOMING_CALL	= 0x03,
        PATTERN_TYPE_OUTGOING_CALL	= 0x04,
        PATTERN_TYPE_FAVORITE		= 0x05,
        PATTERN_TYPE_BT_CONN		= 0x06,
        PATTERN_TYPE_BT_DISCONN		= 0x07,
	PATTERN_TYPE_BLE_CONN		= 0x08,
        PATTERN_TYPE_BLE_DISCONN	= 0x09,
        PATTERN_TYPE_BATT_LEVEL		= 0x0A,
        PATTERN_TYPE_CHARGING		= 0x0B,
        PATTERN_TYPE_PAIRING		= 0x0C,
	
        PATTERN_TYPE_NONE		= 0x0F,
}pattern_type_e;

extern pattern_type_e g_pattern_type;

#ifdef ENABLE_PWM
typedef struct pwm_struct_{

        volatile uint16_t ui16_dimming_flag;
        volatile uint16_t ui16_dimming_data;
        volatile uint16_t ui16_pwm;

}pwm_struct_t;

extern pwm_struct_t g_str_led[LED_NUM];
#endif

typedef struct ble_status_t_
{
	volatile cyw20706_cmd_status_e	cyw20706_cmd_status;
	volatile mcu_status_e		mcu_status;
	volatile call_status_e		call_status;
	volatile power_status_e		power_status;
	volatile ble_conn_status_e	ble_conn_status;
	volatile bt_conn_status_e	bt_conn_status;
	volatile bt_pairing_status_e	bt_pairing_status;
	volatile favorite_number_e	favorite_number;
	volatile charger_status_e	charger_status;
	volatile dnd_status_e		dnd_status;
	volatile amp_status_e		amp_status;
	volatile haptic_status_e	haptic_status;
	volatile fw_upgrade_status_e	fw_upgrade_status;
	volatile volume_level_status_e	volume_level_status;
	volatile app_max_vol_status_e	app_max_volume_status;
	volatile audio_type_status_e	audio_type_status;
#ifdef SMT_CHECK
	volatile uint8_t		pedo_fuel_check;
	volatile uint8_t		pedo_count_check;
#endif
}ble_status_t;

extern ble_status_t g_str_ble_status;


#ifdef ENABLE_FUEL_GAUGE

typedef struct fuel_gauge_batt_status_{

        uint16_t b1_soc_low_detection:1;
        uint16_t b1_soc_high_detection:1;
        uint16_t b1_full_discharge:1;
        uint16_t b1_full_charge:1;

        uint16_t b1_discharge:1;
        uint16_t b1_remaining_run_time_alarm:1;
        uint16_t b1_usable_capacity_low_alarm:1;
        uint16_t b1_battery_degradation_alert:1;

        uint16_t b1_under_temperature:1;
        uint16_t b1_over_temperature:1;

        uint16_t b1_discharge_over_current:1;
        uint16_t b1_charge_over_current:1;
        uint16_t b1_over_discharge:1;
        uint16_t b1_over_charge:1;

}fg_batt_status_struct_t;
extern fg_batt_status_struct_t g_str_fuel_gauge_batt;

typedef struct fuel_gauge_status_{

        uint16_t b4_command_response_status:4;
        uint16_t b1_alert_status:1;
        uint16_t b2_system_failure_alarm:2;
        uint16_t b1_battery_alert:1;

        uint16_t b1_correct_remaining_capacity:1;
        uint16_t b1_update_battery_capacity:1;

        uint16_t b1_detect_stable_current:1;
        uint16_t b1_data_not_ready:1;

        uint16_t ui16_firmware_version;
}fg_status_sturct_t;
extern fg_status_sturct_t g_str_fuel_gauge;
#endif //ENABLE_FUEL_GAUGE

typedef enum battery_status_{

        //BATT_STATUS_DANGER                              = 0x00,
    BATT_STATUS_NO_PLUG             = 0x00,
    BATT_STATUS_LOW                 = 0x01,
    BATT_STATUS_NORMAL              = 0x02,
    BATT_STATUS_HIGH                = 0x03,
    BATT_STATUS_FULL                = 0x04,
    BATT_STATUS_IN_CHARGING         = 0x05,
    BATT_STATUS_CHARGING_COMPLETE   = 0x06,

    BATT_STATUS_INITIAL             = 0xFF,
}battery_status_t;

typedef enum battery_level_{

        BATT_LEVEL_3_0          = 0x00,
        BATT_LEVEL_3_1          = 0x01,
        BATT_LEVEL_3_2          = 0x02,
        BATT_LEVEL_3_3          = 0x03,
        BATT_LEVEL_3_4          = 0x04,
        BATT_LEVEL_3_5          = 0x05,
        BATT_LEVEL_3_6          = 0x06,
        BATT_LEVEL_3_7          = 0x07,
        BATT_LEVEL_3_8          = 0x08,
        BATT_LEVEL_3_9          = 0x09,
        BATT_LEVEL_4_0          = 0x0A,
        BATT_LEVEL_4_1          = 0x0B,
        BATT_LEVEL_4_2          = 0x0C,

        BATT_LEVEL_ERR          = 0xEE,

        BATT_LEVEL_INITIAL      = 0xFF,

}battery_level_t;

typedef enum battery_low_level_{

    BATT_LOW_NONE       = 0x00,
    BATT_LOW_20         = 0x01,
    BATT_LOW_10         = 0x02,
    BATT_LOW_5          = 0x03,

}battery_low_level_t;

typedef struct batt_struct_{
	
	volatile uint8_t ui8_batt_led_level;
	volatile battery_level_t ui16_batt_level;
	
	volatile uint8_t ui8_buf_per[8];
	volatile uint8_t ui8_abs_buf_per[8];
	
	volatile uint8_t ui8_percent;
	volatile uint8_t ui8_previous_percent;
	
	volatile battery_status_t ui16_batt_status;
	volatile int8_t i8_batt_temperature;
	volatile int16_t i16_batt_using_current;
	volatile int16_t i16_batt_avg_current;
	
	volatile uint32_t ui32_relative_state_charge;
	volatile uint32_t ui32_absolute_state_charge;
	
	volatile uint16_t ui16_batt_usable_capacity;
	volatile uint16_t ui16_batt_remain_capacity;
	volatile uint16_t ui16_batt_full_charge_capacity;
	
}batt_struct_t;
extern batt_struct_t g_str_batt;

#endif //STRUCT_H
