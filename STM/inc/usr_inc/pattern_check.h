#ifndef _PATTERN_CHECK_H_
#define _PATTERN_CHECK_H_

void pattern_command_check(void);

void call_power_on( void );
void call_power_off( void );

void call_batt_gauge( void );
void call_batt_low( void );
void call_batt_charging( void );
void call_batt_full( void );

void call_wait_pairing( void );
void call_pairing_complete( void );
void call_disconn_timeout( void );

void call_dnd_on( void );
void call_dnd_off( void );

void call_incoming_call( void );

void call_incoming_favorite( void );
void call_favorite_select( void );
void call_favorite_call( void );

void call_haptic_action( void );

void call_bt_conn_success( void );
void call_bt_disconn( void );
void call_ble_conn_success( void );
void call_ble_disconn( void );
void call_bt_conn_success( void );
void call_bt_disconn( void );
void call_haptic_action( void );

void call_vol_display( void );

void call_app_noti( void );

void call_exhibition_mode_off( void );
void call_exhibition_mode_on( void );
void call_device_reset( void );

void call_unpaired( void );
void call_sub_app_noti( void );

#ifdef SMT_CHECK
void call_pedo_fuel_check( void );
#endif
void call_fw_upgrade( void );

void force_off_pattern( void );

typedef void ( *pattern_func )( void );

extern pattern_func p_pattern_func;

#endif //_PATTERN_CHECK_H_