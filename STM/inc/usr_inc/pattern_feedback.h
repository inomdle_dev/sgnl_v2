#ifndef _PATTERN_FEEDBACK_H_
#define _PATTERN_FEEDBACK_H_

void pattern_unpaired( void );

void pattern_pairing( void );
void pattern_batt_level( void );
void pattern_app_noti( void );
void pattern_bt_conn( void );
void pattern_bt_disconn( void );
void pattern_power_on( void );
void pattern_incoming_call( void );

void pattern_favorite_call( void );

#ifdef ENABLE_BLE_STATUS
void pattern_ble_conn( void );
void pattern_ble_disconn( void );
#endif

#endif //_PATTERN_FEEDBACK_H_