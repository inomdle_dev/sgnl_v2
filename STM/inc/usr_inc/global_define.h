#ifndef GLOBAL_DEFINE_H
#define GLOBAL_DEFINE_H


/* Base address of the Flash sectors */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */	// bootloader
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */	// pedometer data
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */	// favorite call data
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */	// jump app
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */	// user app1 data
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */	//           data2
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */	// user app2 data
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */	//           data2
#define ADDR_FLASH_SEC_END      ((uint32_t)0x08080000)

#define FLASH_SECTOR_0     FLASH_Sector_0 /* Base @ of Sector 0, 16 Kbyte */
#define FLASH_SECTOR_1     FLASH_Sector_1 /* Base @ of Sector 1, 16 Kbyte */
#define FLASH_SECTOR_2     FLASH_Sector_2 /* Base @ of Sector 2, 16 Kbyte */
#define FLASH_SECTOR_3     FLASH_Sector_3 /* Base @ of Sector 3, 16 Kbyte */
#define FLASH_SECTOR_4     FLASH_Sector_4 /* Base @ of Sector 4, 64 Kbyte */
#define FLASH_SECTOR_5     FLASH_Sector_5 /* Base @ of Sector 5, 128 Kbyte */
#define FLASH_SECTOR_6     FLASH_Sector_6 /* Base @ of Sector 6, 128 Kbyte */
#define FLASH_SECTOR_7     FLASH_Sector_7 /* Base @ of Sector 7, 128 Kbyte */
#define FLASH_SECTOR_8     FLASH_Sector_8 /* Base @ of Sector 8, 128 Kbyte */

#define	FLASH_ADDR_START_APP1		ADDR_FLASH_SECTOR_4
#define	FLASH_ADDR_END_APP1		ADDR_FLASH_SECTOR_6
#define	FLASH_ADDR_BACKUP_APP		ADDR_FLASH_SECTOR_6
#define	FLASH_ADDR_END_APP2		ADDR_FLASH_SEC_END

#if defined(CURRENT_APP_1)
#define	FLASH_ADDR_CURRENT_APP		FLASH_ADDR_START_APP1
#define	FLASH_ADDR_UPDATE_APP		FLASH_ADDR_BACKUP_APP
#define ERASE_START_SECTOR		FLASH_Sector_6
#define ERASE_END_SECTOR		FLASH_Sector_8
#elif defined(CURRENT_APP_2)
#define	FLASH_ADDR_CURRENT_APP		FLASH_ADDR_BACKUP_APP
#define	FLASH_ADDR_UPDATE_APP		FLASH_ADDR_START_APP1
#define ERASE_START_SECTOR		FLASH_Sector_4
#define ERASE_END_SECTOR		FLASH_Sector_6	
#elif defined(CURRENT_APP)
#define	FLASH_ADDR_CURRENT_APP		FLASH_ADDR_START_APP1
#define	FLASH_ADDR_UPDATE_APP		FLASH_ADDR_BACKUP_APP
#define ERASE_START_SECTOR		FLASH_Sector_6
#define ERASE_END_SECTOR		FLASH_Sector_8
#endif

/* pre define module */

#define ENABLE_MONITOR
#define ENABLE_STOPMODE
#define ENABLE_BLE_UART
//#define ENABLE_DBG

//#define SMT_CHECK
#define ENABLE_MASS_PRODUCT

#define ENABLE_AUDIO
#define ENABLE_AUDIO_DMA

#if defined(SMT_CHECK)
#undef ENABLE_MASS_PRODUCT
#undef ENABLE_STOPMODE
#undef ENABLE_HY_FE
#undef ENABLE_VOL_SHIFT

#elif defined(ENABLE_MASS_PRODUCT)
#undef SMT_CHECK
#define ENABLE_VOL_SHIFT
//#define ENABLE_AGC
#define ENABLE_HY_FE
//#define AUDIO_BYPASS
//#define AUDIO_BYPASS_DOWN
//#define AUDIO_BUFFERING
#define APP_GAIN_CONTROL
#endif


#define HW_PP4
#if defined(HW_PP4)
#undef HW_PP3
#define ENABLE_FUEL_GAUGE
#else
#define HW_PP3
#undef ENABLE_FUEL_GAUGE
#endif

#define ENABLE_PWM
#define ENABLE_HAPTIC
#define ENABLE_HAPTIC_PWM

#define ENABLE_BOOTLOADER_FLASH

#define	ON			1
#define	OFF			0

#define SLEEP_COUNT     	300
#define PAIRING_COUNT		3000

#define LED_NUM			5

#define	CALLING_FREQ		AUDIO_FREQ_16K

/* include header files */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "arm_math.h"
#include "main.h"

#include "stm32f4xx.h"

#include "pin_enable.h"
#include "timer_event.h"

#ifdef ENABLE_BLE_UART
#include "ble_uart_event.h"
#include "ble_protocol.h"
#endif //ENABLE_BLE_UART

#ifdef ENABLE_BT_UART
#include "bt_uart_event.h"
#endif //ENABLE_BT_UART

#ifdef ENABLE_PWM
#include "led_event.h"
#include "pattern_control.h"
#include "pattern_check.h"
#endif

#ifdef ENABLE_HAPTIC
#include "haptic_event.h"
#endif

#ifdef ENABLE_DBG
#include "dbg_uart_event.h"
#endif

#ifdef ENABLE_FUEL_GAUGE
#include "i2c.h"
#include "fuel_gauge.h"
#endif

#ifdef ENABLE_AUDIO
#include "audio_event.h"
	#ifdef ENABLE_AUDIO_DMA
		#include "audio_dma.h"
	#endif
#endif //ENABLE_AUDIO

#ifdef ENABLE_HY_FE
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <time.h>
#include "fe_init.h"
#include "Common.h"
#include "typedef.h"
#include "typedefs.h"
#include "fe_uart.h"
#include "EQfilter.h"
#include "tm_stm32f4_fft.h"
#ifdef ENABLE_AGC
#include "audio_fw_glo.h"
#include "svc_glo.h"
#endif
#endif

#ifdef ENABLE_STOPMODE
#include "low_power.h"
#include "tm_stm32f4_rcc.h"
#endif //ENABLE_STOPMODE

#ifdef ENABLE_BOOTLOADER_FLASH
#include "flash_event.h"
#endif

#include "struct.h"

#endif //GLOBAL_DEFINE_H
