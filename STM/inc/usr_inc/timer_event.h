#ifndef TIMER_EVENT_H
#define TIMER_EVENT_H

#define LED_TIM_IRQ		TIM1_CC_IRQn//TIM1_IRQn
#define LED_TIM_RCC		RCC_APB2Periph_TIM1
#define LED_TIM_BASE		TIM1

#define MONITOR_TIM_IRQ		TIM5_IRQn
#define MONITOR_TIM_RCC		RCC_APB1Periph_TIM5
#define MONITOR_TIM_BASE	TIM5

void timer_init( void );
void monitor_timer_config( void );
void monitor_timer_stop( void );
void monitor_timer_start( void );

void pattern_timer_config( void );
void pattern_timer_stop( void );
void pattern_timer_start( void );

extern void delay_10ms(uint32_t cnt);

#ifdef ENABLE_FUEL_GAUGE
extern void delay_ms(uint32_t nTime);
#endif

#define pattern_timer_isr		TIM1_CC_IRQHandler//TIM1_IRQHandler
#define monitor_timer_isr		TIM5_IRQHandler

#endif //TIMER_EVENT_H
