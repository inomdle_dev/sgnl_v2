#ifndef _HAPTIC_EVENT_H_
#define _HAPTIC_EVENT_H_

#define GPIO_HAPTIC_MOTOR_PORT		GPIOB
#define GPIO_HAPTIC_MOTOR_RCC		RCC_AHB1Periph_GPIOB
#define GPIO_HAPTIC_MOTOR_SRC		GPIO_PinSource7
#define GPIO_HAPTIC_MOTOR_PIN		GPIO_Pin_7

#define HAPTIC_TIM					TIM4
#define HAPTIC_GPIO_PORT			GPIOB
#define HAPTIC_GPIO_PIN				GPIO_Pin_7
#define HAPTIC_GPIO_RCC				RCC_AHB1Periph_GPIOB
#define HAPTIC_PWM_GPIO_SOURCE		GPIO_PinSource7
#define HAPTIC_PWM_GPIO_RCC			RCC_APB1Periph_TIM4
#define HAPTIC_CCR					HAPTIC_TIM->CCR2

#define HAPTIC_ACTIVE			GPIO_HAPTIC_MOTOR_PORT->BSRRL = GPIO_HAPTIC_MOTOR_PIN
#define HAPTIC_IDLE			GPIO_HAPTIC_MOTOR_PORT->BSRRH = GPIO_HAPTIC_MOTOR_PIN

extern void haptic_init(void);
void haptic_config_gpio(void);

void haptic_on( void );
void haptic_off( void );

#endif //_HAPTIC_EVENT_H_
