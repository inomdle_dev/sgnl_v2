#ifndef AUDIO_EVENT_H
#define AUDIO_EVENT_H

/* I2S2 Receiver peripheral configuration defines */
#define AUDIO_I2S_RX			SPI2
#define AUDIO_I2S_RX_CLK		RCC_APB1Periph_SPI2
#define AUDIO_I2S_RX_GPIO_AF		GPIO_AF_SPI2
#define AUDIO_I2S_RX_GPIO_EXT_AF	GPIO_AF6_SPI2
#define AUDIO_I2S_RX_IRQ		SPI2_IRQn
#define AUDIO_I2S_RX_GPIO_CLOCK		RCC_AHB1Periph_GPIOB
#define AUDIO_I2S_RX_WS_PIN		GPIO_Pin_12
#define AUDIO_I2S_RX_CK_PIN		GPIO_Pin_13
#define AUDIO_I2S_RX_EXT_PIN		GPIO_Pin_14
#define AUDIO_I2S_RX_SD_PIN		GPIO_Pin_15
#define AUDIO_I2S_RX_WS_SOURCE		GPIO_PinSource12
#define AUDIO_I2S_RX_CK_SOURCE		GPIO_PinSource13
#define AUDIO_I2S_RX_EXT_SOURCE		GPIO_PinSource14
#define AUDIO_I2S_RX_SD_SOURCE		GPIO_PinSource15
#define AUDIO_I2S_RX_GPIO		GPIOB


/* I2S3 Transmitter peripheral configuration defines */
#define AUDIO_I2S_TX			SPI3
#define AUDIO_I2S_TX_CLK		RCC_APB1Periph_SPI3
#define AUDIO_I2S_TX_GPIO_AF		GPIO_AF_SPI3
#define AUDIO_I2S_TX_SCK_GPIO_AF	GPIO_AF_SPI3
#define AUDIO_I2S_TX_IRQ		SPI3_IRQn
#define AUDIO_I2S_TX_GPIO_CLOCK		(RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOA)
#define AUDIO_I2S_TX_WS_PIN		GPIO_Pin_15
#define AUDIO_I2S_TX_SCK_PIN		GPIO_Pin_3
#define AUDIO_I2S_TX_SD_PIN		GPIO_Pin_5
#define AUDIO_I2S_TX_MCK_PIN		GPIO_Pin_10
#define AUDIO_I2S_TX_WS_SOURCE		GPIO_PinSource15
#define AUDIO_I2S_TX_SCK_SOURCE		GPIO_PinSource3
#define AUDIO_I2S_TX_SD_SOURCE		GPIO_PinSource5
#define AUDIO_I2S_TX_MCK_SOURCE		GPIO_PinSource10
#define AUDIO_I2S_TX_WS_GPIO		GPIOA
#define AUDIO_I2S_TX_GPIO		GPIOB

#define AUDIO_FRAME_SIZE		640//960//320//160
#define AUDIO_FRAME_BUFFER		4//2//3//10
#define AUDIO_FRAME_HALF		(AUDIO_FRAME_BUFFER>>1)
#define AUDIO_BUFFER_SIZE		320

void audio_init(uint16_t audio);
void audio_i2s_config_rx(uint16_t audio);
void audio_i2s_config_tx(uint16_t audio);
void audio_freq_check(void);
void audio_rx_start( void );
void audio_rx_stop( void );
void audio_tx_start(uint32_t *addr, uint16_t frame_no);
extern uint16_t volume_shift(uint16_t data);

#define audio_rx_isr			SPI2_IRQHandler
#define audio_tx_isr			SPI3_IRQHandler

#endif //AUDIO_EVENT_H
