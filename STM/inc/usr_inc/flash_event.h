#ifndef __FLASH_EVENT_H__
#define __FLASH_EVENT_H__

//#if defined (CURRENT_APP_1)
#define STATUS_FLASH_SECTOR		FLASH_Sector_1
#define STATUS_FLASH_ADDR		ADDR_FLASH_SECTOR_1 /* Base @ of Sector 0, 16 Kbytes */	// bootloader
//#endif

void user_flash_erase( void );
void user_flash_write(uint32_t variable);
void user_flash_read(uint32_t *variable);

void jump_addr_write( uint32_t variable );

uint16_t FLASH_If_GetWriteProtectionStatus(uint32_t addr);
uint32_t FLASH_If_DisableWriteProtection(uint32_t addr);
uint32_t FLASH_If_SectorErase(uint32_t addr);

void flash_read_32(uint32_t st_addr, uint32_t *variable, uint32_t interval, uint32_t size);
void flash_write_32(uint32_t st_addr, uint32_t *variable, uint32_t interval, uint32_t size);
uint32_t GetSector(uint32_t Address);
#endif //__FLASH_EVENT_H__