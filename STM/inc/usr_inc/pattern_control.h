#ifndef _LED_CONTROL_H_
#define _LED_CONTROL_H_

#define PATTERN_POWER_ON_CNT			510
#define PATTERN_POWER_OFF_CNT			330

#define PATTERN_BATT_GAUGE_CNT			450
#define PATTERN_BATT_LOW_CNT			370

#define PATTERN_BATT_CHARGING_CNT		210
#define PATTERN_BATT_FULL_CNT			300

#define PATTERN_WAIT_PAIRING_CNT		600
#define PATTERN_PAIRING_COMPLETE_CNT	350
#define PATTERN_DISCONN_TIMEOUT_CNT		400

#define PATTERN_DND_ON_CNT				250
#define PATTERN_DND_OFF_CNT				250

#define PATTERN_HAPTIC_ACTION_CNT		10
#define PATTERN_INCOMING_CALL_CNT		210

#define PATTERN_FAVORITE_SELECT_CNT		310
#define PATTERN_FAVORITE_CALL_CNT		460

#define PATTERN_VOL_DISPLAY_CNT			210

#define PATTERN_APP_NOTI_CNT			260

#define PATTERN_DEVICE_RESET_CNT		310
#define PATTERN_EXHIBITION_MODE_CNT		310
#define PATTERN_PEDO_FUEL_CHECK_CNT		210


#define PATTERN_UNPAIRED_CNT		600
#define PATTERN_UNPAIR_CNT			333

#define PATTERN_BT_CONN_CHK_CNT		400
#define PATTERN_BLE_CONN_CHK_CNT	400

#define PATTERN_FW_UPGRADE_CNT		300

#ifdef SMT_CHECK
void set_led_pwm( uint16_t index, uint16_t value );
#endif

void pattern_power_on( void );
void pattern_power_off( void );

void pattern_batt_gauge( void );
void pattern_batt_low( void );
void pattern_batt_charging( void );
void pattern_batt_full( void );

void pattern_wait_pairing( void );
void pattern_pairing_complete( void );
void pattern_disconn_timeout( void );

void pattern_dnd_on( void );
void pattern_dnd_off( void );

void pattern_haptic_action( void );
void pattern_incoming_call( void );

void pattern_incoming_favorite( void );
void pattern_favorite_select( void );
void pattern_favorite_call( void );

void pattern_vol_display( void );

void pattern_app_noti( void );

void pattern_device_reset( void );
void pattern_exhibition_mode_off( void );
void pattern_exhibition_mode_on( void );

void pattern_unpaired( void );
void pattern_unpair( void );

void sub_pattern_app_noti( void );

void pattern_ble_conn_success( void );
void pattern_ble_disconn( void );
void pattern_bt_conn_success( void );
void pattern_bt_disconn( void );

#ifdef SMT_CHECK
void pattern_pedo_fuel_check( void );
#endif

void pattern_fw_upgrade_progress( void );
void set_led_pwm( uint16_t index, uint16_t value );

#endif //_LED_CONTROL_H_