#ifndef _BLE_PROTOCOL_H_
#define _BLE_PROTOCOL_H_

#include <string.h>

void cyw20706_protocol(uint8_t* ui8_data, uint8_t ui8_size);

extern uint32_t g_ui32_pattern_check;

#endif //_BLE_PROTOCOL_H_
