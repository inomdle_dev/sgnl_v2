#ifndef _LED_PATTERN_H_
#define _LED_PATTERN_H_

void led_pattern_unpaired( void );

void led_pattern_pairing( void );
void led_pattern_pairing_complete( void );
void led_pattern_batt_level( void );
void led_pattern_app_noti( void );
void led_pattern_bt_conn( void );
void led_pattern_bt_disconn( void );
void led_pattern_ble_conn( void );
void led_pattern_ble_disconn( void );
void led_pattern_power_on( void );
void led_pattern_incoming_call( void );

void led_pattern_favorite_call( void );

#endif //_LED_PATTERN_H_