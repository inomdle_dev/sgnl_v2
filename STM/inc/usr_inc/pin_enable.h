#ifndef PIN_ENABLE_H
#define PIN_ENABLE_H

void pin_enable_init(void);
void pin_enable_gpio(void);
void pin_enable_bt_gpio(void);
void pin_enable_ble_gpio(void);
void pin_enable_wakeup_gpio(void);

void pin_disable_init(void);
void pin_disable_gpio(void);
void pin_disable_bt_gpio(void);
void pin_disable_ble_gpio(void);
void pin_disable_all_gpio(void);
void check_charger_status(void);

void charger_detect_isr(void);

#define GPIO_WAKEUP_FLAG_PORT			GPIOA
#define GPIO_WAKEUP_FLAG_RCC			RCC_AHB1Periph_GPIOA
#define GPIO_WAKEUP_PIN_SOURCE			GPIO_PinSource0
#define GPIO_WAKEUP_PIN				GPIO_Pin_0
#define GPIO_WAKEUP_EXTI_IRQ			EXTI0_IRQn
#define GPIO_WAKEUP_EXTI_PIN_SOURCE		EXTI_PinSource0
#define GPIO_WAKEUP_EXTI_PORT			EXTI_PortSourceGPIOA
#define GPIO_WAKEUP_EXTI_LINE			EXTI_Line0

#define GPIO_AMP_STATUS_PORT			GPIOB
#define GPIO_AMP_STATUS_RCC			RCC_AHB1Periph_GPIOB
#define GPIO_AMP_STATUS_SRC			GPIO_PinSource6
#define GPIO_AMP_STATUS_PIN			GPIO_Pin_6

#if defined(HW_PP3) | defined(HW_PP4)
#define GPIO_DAC_STATUS_PORT			GPIOA
#define GPIO_DAC_STATUS_RCC			RCC_AHB1Periph_GPIOA
#define GPIO_DAC_STATUS_SRC			GPIO_PinSource11
#define GPIO_DAC_STATUS_PIN			GPIO_Pin_11

#define GPIO_PEDO_STATUS_PORT			GPIOA
#define GPIO_PEDO_STATUS_RCC			RCC_AHB1Periph_GPIOA
#define GPIO_PEDO_STATUS_SRC			GPIO_PinSource3
#define GPIO_PEDO_STATUS_PIN			GPIO_Pin_3

#define GPIO_CHARGER_PORT			GPIOA
#define GPIO_CHARGER_RCC			RCC_AHB1Periph_GPIOA
#define GPIO_CHARGER_PIN_SOURCE			GPIO_PinSource12
#define GPIO_CHARGER_PIN			GPIO_Pin_12
#define GPIO_CHARGER_EXTI_IRQ			EXTI15_10_IRQn
#define GPIO_CHARGER_EXTI_PIN_SOURCE		EXTI_PinSource12
#define GPIO_CHARGER_EXTI_PORT			EXTI_PortSourceGPIOA
#define GPIO_CHARGER_EXTI_LINE			EXTI_Line12

#define GPIO_BLE_WAKEUP_PORT			GPIOB
#define GPIO_BLE_WAKEUP_RCC			RCC_AHB1Periph_GPIOB
#define GPIO_BLE_WAKEUP_SRC			GPIO_PinSource4
#define GPIO_BLE_WAKEUP_PIN			GPIO_Pin_4

#define BLE_WAKEUP_ENABLE			GPIO_WriteBit(GPIO_BLE_WAKEUP_PORT, GPIO_BLE_WAKEUP_PIN, Bit_SET)
#define BLE_WAKEUP_DISABLE			GPIO_WriteBit(GPIO_BLE_WAKEUP_PORT, GPIO_BLE_WAKEUP_PIN, Bit_RESET)

#define DAC_STATUS_ENABLE			GPIO_WriteBit(GPIO_DAC_STATUS_PORT, GPIO_DAC_STATUS_PIN, Bit_RESET)
#define DAC_STATUS_DISABLE			GPIO_WriteBit(GPIO_DAC_STATUS_PORT, GPIO_DAC_STATUS_PIN, Bit_SET)

#define PEDO_STATUS_ENABLE			GPIO_WriteBit(GPIO_PEDO_STATUS_PORT, GPIO_PEDO_STATUS_PIN, Bit_RESET)
#define PEDO_STATUS_DISABLE			GPIO_WriteBit(GPIO_PEDO_STATUS_PORT, GPIO_PEDO_STATUS_PIN, Bit_SET)

#define charger_detect_isr			EXTI15_10_IRQHandler
#endif

#define AMP_STATUS_ENABLE			GPIO_WriteBit(GPIO_AMP_STATUS_PORT, GPIO_AMP_STATUS_PIN, Bit_SET)
#define AMP_STATUS_DISABLE			GPIO_WriteBit(GPIO_AMP_STATUS_PORT, GPIO_AMP_STATUS_PIN, Bit_RESET)

#define GPIO_POWER_HOLD_PORT			GPIOA
#define GPIO_POWER_HOLD_RCC			RCC_AHB1Periph_GPIOA
#define GPIO_POWER_HOLD_SRC			GPIO_PinSource8
#define GPIO_POWER_HOLD_PIN			GPIO_Pin_8

#define POWER_HOLD_ENABLE			GPIO_WriteBit(GPIO_POWER_HOLD_PORT, GPIO_POWER_HOLD_PIN, Bit_SET)
#define POWER_HOLD_DISABLE			GPIO_WriteBit(GPIO_POWER_HOLD_PORT, GPIO_POWER_HOLD_PIN, Bit_RESET)

#define wakeup_isr				EXTI0_IRQHandler

#endif //PIN_ENABLE_H
