/*********************************************************************
File    : i2c.h
Purpose : 
**********************************************************************/
#ifndef __I2C_H__
#define __I2C_H__
/****************************** Includes *****************************/
#include "tm_stm32f4_i2c.h"
#include "tm_stm32f4_gpio.h"

/****************************** Defines *******************************/
#define I2C_SPEED                 100000
#define I2C_OWN_ADDRESS           0x00

#define SENSORS_I2C                       I2C1
#define SENSORS_I2C_SCL_GPIO_PORT         GPIOB
#define SENSORS_I2C_SCL_GPIO_CLK          RCC_AHB1Periph_GPIOB
#define SENSORS_I2C_SCL_GPIO_PIN          GPIO_Pin_8
#define SENSORS_I2C_SCL_GPIO_PINSOURCE    GPIO_PinSource8
 
#define SENSORS_I2C_SDA_GPIO_PORT         GPIOB
#define SENSORS_I2C_SDA_GPIO_CLK          RCC_AHB1Periph_GPIOB
#define SENSORS_I2C_SDA_GPIO_PIN          GPIO_Pin_9
#define SENSORS_I2C_SDA_GPIO_PINSOURCE    GPIO_PinSource9

#define SENSORS_I2C_RCC_CLK               RCC_APB1Periph_I2C1
#define SENSORS_I2C_AF                    GPIO_AF_I2C1

void i2c_init(void);

int Sensors_I2C_ReadRegister(uint8_t Address, uint8_t RegisterAddr, uint8_t RegisterLen, uint8_t *RegisterValue);
int Sensors_I2C_WriteRegister(uint8_t Address, uint8_t RegisterAddr, uint8_t RegisterLen, uint8_t *RegisterValue);

unsigned long ST_Sensors_I2C_WriteRegister(unsigned char Address, unsigned char RegisterAddr, unsigned short RegisterLen, const unsigned char *RegisterValue);
unsigned long ST_Sensors_I2C_ReadRegister(unsigned char Address, unsigned char RegisterAddr, unsigned short RegisterLen, unsigned char *RegisterValue);

#endif // __I2C_H__


