/*
    This file is part of EqualizerAPO, a system-wide equalizer.
    Copyright (C) 2014  Jonas Thedering

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef _BIQUADFILTER_H_
#define _BIQUADFILTER_H_

#include <math.h>
#include <stdlib.h>
#include <float.h>
#include "arm_math.h"

#define M_E        2.71828182845904523536
#define M_LOG2E    1.44269504088896340736
#define M_LOG10E   0.434294481903251827651
#define M_LN2      0.693147180559945309417
#define M_LN10     2.30258509299404568402
#define M_PI       3.14159265358979323846
#define M_PI_2     1.57079632679489661923
#define M_PI_4     0.785398163397448309616
#define M_1_PI     0.318309886183790671538
#define M_2_PI     0.636619772367581343076
#define M_2_SQRTPI 1.12837916709551257390

#define M_SQRT2    1.41421356237309504880
#define M_SQRT1_2  0.707106781186547524401

#define IS_DENORMAL(d) (abs(d) < DBL_MIN)
#define BUFSIZE		80
#define SAMPLING_RATE	8000


#ifdef ENABLE_EQ
typedef struct biquad_t_
{
	double a[4];
	double a0;

	double x1, x2;
	double y1, y2;
}biquad_t;

typedef enum filter_type_e_
{
	LOW_PASS,
	HIGH_PASS,
	BAND_PASS,
	NOTCH,
	ALL_PASS,
	PEAKING,
	LOW_SHELF,
	HIGH_SHELF
}filter_type_e;

typedef struct biquadfilter_t_
{
	filter_type_e	type;
	double	dbGain;
	double	freq;
	double	bandwidthOrQOrS;
	int	isBandwidth;
	int	isCornerFreq;

	biquad_t biquads;
}biquadfilter_t;
extern biquadfilter_t *allfilter1;
extern biquadfilter_t *allfilter2;
extern biquadfilter_t *allfilter3;
extern biquadfilter_t *allfilter4;
extern biquadfilter_t *allfilter5;
extern biquadfilter_t *allfilter6;

extern void equalizer(void);
extern void init_equalize(void);
extern void free_equalize(void);
void removeDenormals(biquad_t *bq);
inline double biquad_process(biquad_t *bq, short sample);
extern void biquad_init(biquadfilter_t *filter);

extern void biquadfilter_init(biquadfilter_t *filter);
extern inline void biquadfilter_process(biquadfilter_t *filter, short* output, short* input, unsigned frameCount);

#endif
#endif