/*
********************************************************************************
*
*      GSM AMR-NB speech codec   R98   Version 7.6.0   December 12, 2001
*                                R99   Version 3.3.0                
*                                REL-4 Version 4.1.0                
*
********************************************************************************
*
*      File             : lag_wind.h
*      Purpose          : Lag windowing of autocorrelations.
*
********************************************************************************
*/
#ifndef lag_wind_h
#define lag_wind_h "$Id $"
 

#endif
