#ifndef _EQ_FILTER_H_
#define _EQ_FILTER_H_

void cgeq_init(void);
// Initialization peak/notch filter
void init_PNfilter(float G[], float *freq, float *bw);
void init_PNfilter_coefficients(float G, float freq, float bw, float Q, float *b, float *a);
//void calculate_gain_opt(float g[], float **A);
void calculate_gain_opt(float g[], float A[][BAND_L]);

// Inverse matrix calculator
//void cofactor(float **A,float f);
//void transpose(float **num,float **fac,float r);
//float determinant(float **A, float k);

void cofactor(float A[][BAND_L],int f);
void transpose(float num[][BAND_L],float fac[][BAND_L],int r);
float determinant(float A[][BAND_L], int k);

//extern float filter[BAND_L][NS1], **A;
extern float cgeq_filter[BAND_L][NS1], A[BAND_L][BAND_L];

#endif