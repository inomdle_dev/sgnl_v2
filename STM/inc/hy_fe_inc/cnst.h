/*
*****************************************************************************
*
*      GSM AMR-NB speech codec   R98   Version 7.6.0   December 12, 2001
*                                R99   Version 3.3.0                
*                                REL-4 Version 4.1.0                
*
*****************************************************************************
*
*      File             : cnst.h
*      Purpose          : Speech codec constant parameters
*                       :  (encoder, decoder, and postfilter)
*
*****************************************************************************
*/
#ifndef cnst_h
#define cnst_h "$Id $"


#endif
