#ifndef _COMMON_H_
#define _COMMON_H_

#include "tm_stm32f4_fft.h"
#ifdef ENABLE_AGC
#include "audio_fw_glo.h"
#include "svc_glo.h"
#endif

//#define HYU_FFT

#define FRAMESIZE	(80)
#define FRONTMARGIN	(48)
#define FFTSIZE		((FRAMESIZE+FRONTMARGIN)<<1)
#define PI2		6.283185307179586476f
#define PI1		(PI2 * 0.5)
#define ALPHA		0.8
#define WINDOW_BY_TWO	(FFTSIZE >> 1)

#define LPC_ORDER	10        /* Order of LP filter                       */
#define NS2		256
#define NS1		(NS2>>1)
#define FE_FRAME_L	80
#define BAND_L		8
#define FS		8000

#define MAX_32		(int32_t)0x7fffffffL
#define MIN_32		(int32_t)0x80000000L

#define MAX_16		(int16_t)0x7fff
#define MIN_16		(int16_t)0x8000

extern void r_fft(float *farray_ptr, int isign);
extern void BufferCopy(float *frombf, float *tobf, short num);
extern void preemphasis(int len,float *buf,float *prev);
extern void deemphasis(int len,float *buf,float *prev);

extern void FEnh_func(int16_t *x);
extern void BufferReset(float *buf, short n);
extern short levinson_durbin(float *a, const float *r, const short m, float epsP[]);
extern void autocorr(float32_t x[], uint16_t m, float32_t r[], float32_t window[]);

#ifdef ENABLE_AGC
extern buffer_t input_buf, output_buf;
extern buffer_t *pinput_buf, *poutput_buf;
extern svc_static_param_t svc_static_param;
extern svc_dynamic_param_t svc_dynamic_param;
extern void *pSvcStaticMem;
extern void *pSvcDynamicMem;
#endif

// Overlap-add를 위한 변수
extern float32_t Previn_buf[NS2];
extern float32_t Prevsynth_buf[NS2];
extern TM_FFT_F32_t FFT;

void move16 (void);
void move32 (void);
void logic16 (void);
void logic32 (void);
void test (void);

/*
**************************************************************************
*
*  Function    : autocorr
*  Purpose     : Compute autocorrelations of signal with windowing
*  Description : - Windowing of input speech:   s'[n] = s[n] * w[n]
*                - Autocorrelations of input speech:
*                  r[k] = sum_{i=k}^{239} s'[i]*s'[i-k]    k=0,...,10
*                The autocorrelations are expressed in normalized 
*                double precision format.
*  Returns     : Autocorrelation
*
**************************************************************************
*/
int16_t Autocorr (
    int16_t x[],        /* (i)    : Input signal (L_WINDOW)             */
    int16_t m,          /* (i)    : LPC order                           */
    int16_t r_h[],      /* (o)    : Autocorrelations  (msb)  (MP1)      */
    int16_t r_l[],      /* (o)    : Autocorrelations  (lsb)  (MP1)      */
    int16_t wind[]/* (i)    : window for LPC analysis. (L_WINDOW) */
);
 

extern uint8_t Overflow;
extern uint8_t Carry;


/*___________________________________________________________________________
 |                                                                           |
 |   Prototypes for basic arithmetic operators                               |
 |___________________________________________________________________________|
*/

int16_t add (int16_t var1, int16_t var2);    /* Short add,           1   */
int16_t sub (int16_t var1, int16_t var2);    /* Short sub,           1   */
int16_t abs_s (int16_t var1);               /* Short abs,           1   */
int16_t shl (int16_t var1, int16_t var2);    /* Short shift left,    1   */
int16_t shr (int16_t var1, int16_t var2);    /* Short shift right,   1   */
int16_t mult (int16_t var1, int16_t var2);   /* Short mult,          1   */
int32_t L_mult (int16_t var1, int16_t var2); /* Long mult,           1   */
int16_t negate (int16_t var1);              /* Short negate,        1   */
int16_t extract_h (int32_t L_var1);         /* Extract high,        1   */
int16_t extract_l (int32_t L_var1);         /* Extract low,         1   */
int16_t L_round (int32_t L_var1);             /* Round,               1   */
int32_t L_mac (int32_t L_var3, int16_t var1, int16_t var2);   /* Mac,  1  */
int32_t L_msu (int32_t L_var3, int16_t var1, int16_t var2);   /* Msu,  1  */
int32_t L_macNs (int32_t L_var3, int16_t var1, int16_t var2); /* Mac without
                                                             sat, 1   */
int32_t L_msuNs (int32_t L_var3, int16_t var1, int16_t var2); /* Msu without
                                                             sat, 1   */
int32_t L_add (int32_t L_var1, int32_t L_var2);    /* Long add,        2 */
int32_t L_sub (int32_t L_var1, int32_t L_var2);    /* Long sub,        2 */
int32_t L_add_c (int32_t L_var1, int32_t L_var2);  /* Long add with c, 2 */
int32_t L_sub_c (int32_t L_var1, int32_t L_var2);  /* Long sub with c, 2 */
int32_t L_negate (int32_t L_var1);                /* Long negate,     2 */
int16_t mult_r (int16_t var1, int16_t var2);       /* Mult with round, 2 */
int32_t L_shl (int32_t L_var1, int16_t var2);      /* Long shift left, 2 */
int32_t L_shr (int32_t L_var1, int16_t var2);      /* Long shift right, 2*/
int16_t shr_r (int16_t var1, int16_t var2);        /* Shift right with
                                                   round, 2           */
int16_t mac_r (int32_t L_var3, int16_t var1, int16_t var2); /* Mac with
                                                           rounding,2 */
int16_t msu_r (int32_t L_var3, int16_t var1, int16_t var2); /* Msu with
                                                           rounding,2 */
int32_t L_deposit_h (int16_t var1);        /* 16 bit var1 -> MSB,     2 */
int32_t L_deposit_l (int16_t var1);        /* 16 bit var1 -> LSB,     2 */

int32_t L_shr_r (int32_t L_var1, int16_t var2); /* Long shift right with
                                                round,  3             */
int32_t L_abs (int32_t L_var1);            /* Long abs,              3  */
int32_t L_sat (int32_t L_var1);            /* Long saturation,       4  */
int16_t norm_s (int16_t var1);             /* Short norm,           15  */
int16_t div_s (int16_t var1, int16_t var2); /* Short division,       18  */
int16_t norm_l (int32_t L_var1);           /* Long norm,            30  */   


/*
 * The functions above increases the corresponding operation counter for
 * the current counter group.
 */

typedef struct
{
    int32_t add;        /* Complexity Weight of 1 */
    int32_t sub;
    int32_t abs_s;
    int32_t shl;
    int32_t shr;
    int32_t extract_h;
    int32_t extract_l;
    int32_t mult;
    int32_t L_mult;
    int32_t negate;
    int32_t round;
    int32_t L_mac;
    int32_t L_msu;
    int32_t L_macNs;
    int32_t L_msuNs;
    int32_t L_add;      /* Complexity Weight of 2 */
    int32_t L_sub;
    int32_t L_add_c;
    int32_t L_sub_c;
    int32_t L_negate;
    int32_t L_shl;
    int32_t L_shr;
    int32_t mult_r;
    int32_t shr_r;
    int32_t shift_r;
    int32_t mac_r;
    int32_t msu_r;
    int32_t L_deposit_h;
    int32_t L_deposit_l;
    int32_t L_shr_r;    /* Complexity Weight of 3 */
    int32_t L_shift_r;
    int32_t L_abs;
    int32_t L_sat;      /* Complexity Weight of 4 */
    int32_t norm_s;     /* Complexity Weight of 15 */
    int32_t div_s;      /* Complexity Weight of 18 */
    int32_t norm_l;     /* Complexity Weight of 30 */
    int32_t DataMove16; /* Complexity Weight of 1 */
    int32_t DataMove32; /* Complexity Weight of 2 */
    int32_t Logic16;    /* Complexity Weight of 1 */
    int32_t Logic32;    /* Complexity Weight of 2 */
    int32_t Test;       /* Complexity Weight of 2 */
}
BASIC_OP;


 
/*
********************************************************************************
*                         DEFINITION OF DATA TYPES
********************************************************************************
*/
 
/*
********************************************************************************
*                         DECLARATION OF PROTOTYPES
********************************************************************************
*/
void Lag_window (
    int16_t m,          /* (i)    : LPC order                                */
    int16_t r_h[],      /* (i/o)  : Autocorrelations  (msb)                  */
    int16_t r_l[]       /* (i/o)  : Autocorrelations  (lsb)                  */
);









/*
********************************************************************************
*                         INCLUDE FILES
********************************************************************************
*/

 
/*
********************************************************************************
*                         LOCAL VARIABLES AND TABLES
********************************************************************************
*/
 
/*
********************************************************************************
*                         DEFINITION OF DATA TYPES
********************************************************************************
*/
typedef struct {
  int16_t old_A[LPC_ORDER + 1];     /* Last A(z) for case of unstable filter */
} LevinsonState;
 
/*
********************************************************************************
*                         DECLARATION OF PROTOTYPES
********************************************************************************
*/
 
int Levinson_init (LevinsonState **st);
/* initialize one instance of the pre processing state.
   Stores pointer to filter status struct in *st. This pointer has to
   be passed to Levinson in each call.
   returns 0 on success
 */
 
int Levinson_reset (LevinsonState *st);
/* reset of pre processing state (i.e. set state memory to zero)
   returns 0 on success
 */
void Levinson_exit (LevinsonState **st);
/* de-initialize pre processing state (i.e. free status struct)
   stores NULL in *st
 */

int Levinson (
    int16_t old_A[],
    int16_t Rh[],       /* i : Rh[m+1] Vector of autocorrelations (msb) */
    int16_t Rl[],       /* i : Rl[m+1] Vector of autocorrelations (lsb) */
    int16_t A[],        /* o : A[m]    LPC coefficients  (m = 10)       */
    int16_t rc[]        /* o : rc[4]   First 4 reflection coefficients  */
);
 


void L_Extract (int32_t L_32, int16_t *hi, int16_t *lo);
int32_t L_Comp (int16_t hi, int16_t lo);
int32_t Mpy_32 (int16_t hi1, int16_t lo1, int16_t hi2, int16_t lo2);
int32_t Mpy_32_16 (int16_t hi, int16_t lo, int16_t n);
int32_t Div_32 (int32_t L_num, int16_t denom_hi, int16_t denom_lo);


#endif
