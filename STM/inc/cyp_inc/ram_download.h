#ifndef _RAM_DOWNLOAD_H_
#define _RAM_DOWNLOAD_H_

typedef enum ram_download_e_
{
	RAM_INIT		= 0x00,
	HCI_RESET		= 0x01,
	UPDATE_BAUDRATE		= 0x02,
	DOWNLOAD_MINIDRIVER	= 0x03,
	WRITE_RAM		= 0x04,
	LAUNCH_RAM		= 0x05,
	DOWNLOAD_COMPLETE	= 0x06,
}ram_download_e;

extern ram_download_e ram_download;

void cyw20706_ram_down_protocol(uint8_t* ui8_data, uint8_t ui8_size);
void cyw20706_ram_download(void);

#endif