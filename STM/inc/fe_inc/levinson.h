/*
********************************************************************************
*
*      GSM AMR-NB speech codec   R98   Version 7.6.0   December 12, 2001
*                                R99   Version 3.3.0                
*                                REL-4 Version 4.1.0                
*
********************************************************************************
*
*      File             : levinson.h
*      Purpose          : Levinson-Durbin algorithm in double precision.
*                       : To compute the LP filter parameters from the
*                       : speech autocorrelations.
*
********************************************************************************
*/
#ifndef levinson_h
#define levinson_h "$Id $"

#include "arm_math.h"
#include "arm_const_structs.h"
#include "typedefs.h"

extern int Levinson (
    int16_t old_A[],
    int16_t Rh[],       /* i : Rh[m+1] Vector of autocorrelations (msb) */
    int16_t Rl[],       /* i : Rl[m+1] Vector of autocorrelations (lsb) */
    int16_t A[]         /* o : A[m]    LPC coefficients  (m = 10)       */
    //int16_t rc[]        /* o : rc[4]   First 4 reflection coefficients  */
);
extern int16_t Autocorr(int16_t x[], int16_t m, int16_t r_h[], int16_t r_l[], float wind[]);

extern short levinson_durbin(	/* o:   energy of prediction error   */
    float       *a,		/* o:   LP coefficients (a[0] = 1.0) */
    const float *r,		/* i:   vector of autocorrelations   */
    const short m,		/* i:   order of LP filter           */
    float       epsP[]		/* o:   prediction error energy      */
);
extern void autocorr(
	float32_t   x[],	/* (i) : Input signal         */
	uint16_t    m,		/* (i) : LPC order            */
	float32_t   r[],	/* (o) : Autocorrelations     */
	float32_t   window[]	/* (i) : LPC Analysis window  */
);

#endif
