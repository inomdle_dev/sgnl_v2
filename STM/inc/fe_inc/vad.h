/*
********************************************************************************
**-------------------------------------------------------------------------**
**                                                                         **
**     GSM AMR-NB speech codec   R98   Version 7.6.0   December 12, 2001       **
**                               R99   Version 3.3.0                       **
**                               REL-4 Version 4.1.0                       **
**                                                                         **
**-------------------------------------------------------------------------**
********************************************************************************
*
*      File             : vad1.h
*      Purpose          : Voice Activity Detection (VAD) for AMR (option 1)
*
********************************************************************************
*/
#ifndef vad_h
#define vad_h "$Id $"
 
/*
********************************************************************************
*                         INCLUDE FILES
********************************************************************************
*/
#include "typedefs.h"

#define FRAME_LEN 80    /* Length (samples) of the input frame          */
#define COMPLEN 9        /* Number of sub-bands used by VAD              */
#define INV_COMPLEN 3641 /* 1.0/COMPLEN*2^15                             */
#define LOOKAHEAD 40     /* length of the lookahead used by speech coder */

#define UNITY 512        /* Scaling used with SNR calculation            */
#define UNIRSHFT 6       /* = log2(MAX_16/UNITY)                         */

#define TONE_THR (int16_t)(0.65*MAX_16) /* Threshold for tone detection   */

/* Constants for background spectrum update */
#define ALPHA_UP1   (int16_t)((1.0 - 0.95)*MAX_16)  /* Normal update, upwards:   */
#define ALPHA_DOWN1 (int16_t)((1.0 - 0.936)*MAX_16) /* Normal update, downwards  */
#define ALPHA_UP2   (int16_t)((1.0 - 0.985)*MAX_16) /* Forced update, upwards    */
#define ALPHA_DOWN2 (int16_t)((1.0 - 0.943)*MAX_16) /* Forced update, downwards  */
#define ALPHA3      (int16_t)((1.0 - 0.95)*MAX_16)  /* Update downwards          */
#define ALPHA4      (int16_t)((1.0 - 0.9)*MAX_16)   /* For stationary estimation */
#define ALPHA5      (int16_t)((1.0 - 0.5)*MAX_16)   /* For stationary estimation */

/* Constants for VAD threshold */
#define VAD_THR_HIGH 1260 /* Highest threshold                 */
#define VAD_THR_LOW  720  /* Lowest threshold                  */
#define VAD_P1 0          /* Noise level for highest threshold */
#define VAD_P2 6300       /* Noise level for lowest threshold  */
#define VAD_SLOPE (int16_t)(MAX_16*(float)(VAD_THR_LOW-VAD_THR_HIGH)/(float)(VAD_P2-VAD_P1))

/* Parameters for background spectrum recovery function */
#define STAT_COUNT 20         /* threshold of stationary detection counter         */
#define STAT_COUNT_BY_2 10    /* threshold of stationary detection counter         */
#define CAD_MIN_STAT_COUNT 5  /* threshold of stationary detection counter         */

#define STAT_THR_LEVEL 184    /* Threshold level for stationarity detection        */
#define STAT_THR 1000         /* Threshold for stationarity detection              */

/* Limits for background noise estimate */
#define NOISE_MIN 40          /* minimum */
#define NOISE_MAX 16000       /* maximum */
#define NOISE_INIT 150        /* initial */

/* Constants for VAD hangover addition */
#define HANG_NOISE_THR 100
#define BURST_LEN_HIGH_NOISE 4
#define HANG_LEN_HIGH_NOISE 7
#define BURST_LEN_LOW_NOISE 5
#define HANG_LEN_LOW_NOISE 4

/* Thresholds for signal power */
#define VAD_POW_LOW (Word32)15000     /* If input power is lower,                    */
/*     VAD is set to 0                         */
#define POW_PITCH_THR (Word32)343040  /* If input power is lower, pitch              */
/*     detection is ignored                    */

#define POW_COMPLEX_THR (Word32)15000 /* If input power is lower, complex            */
/* flags  value for previous frame  is un-set  */


/* Constants for the filter bank */
#define LEVEL_SHIFT 0      /* scaling                                  */
#define COEFF3   13363     /* coefficient for the 3rd order filter     */
#define COEFF5_1 21955     /* 1st coefficient the for 5th order filter */
#define COEFF5_2 6390      /* 2nd coefficient the for 5th order filter */

/* Constants for pitch detection */
#define LTHRESH 4
#define NTHRESH 4

/* Constants for complex signal VAD  */
#define CVAD_THRESH_ADAPT_HIGH  (int16_t)(0.6 * MAX_16) /* threshold for adapt stopping high    */
#define CVAD_THRESH_ADAPT_LOW  (int16_t)(0.5 * MAX_16)  /* threshold for adapt stopping low     */
#define CVAD_THRESH_IN_NOISE  (int16_t)(0.65 * MAX_16)  /* threshold going into speech on
a short term basis                   */

#define CVAD_THRESH_HANG  (int16_t)(0.70 * MAX_16)      /* threshold                            */
#define CVAD_HANG_LIMIT  (int16_t)(100)                 /* 2 second estimation time             */
#define CVAD_HANG_LENGTH  (int16_t)(250)                /* 5 second hangover                    */

#define CVAD_LOWPOW_RESET (int16_t) (0.40 * MAX_16)     /* init in low power segment            */
#define CVAD_MIN_CORR (int16_t) (0.40 * MAX_16)         /* lowest adaptation value              */

#define CVAD_BURST 20                                  /* speech burst length for speech reset */
#define CVAD_ADAPT_SLOW (int16_t)(( 1.0 - 0.98) * MAX_16)        /* threshold for slow adaption */
#define CVAD_ADAPT_FAST (int16_t)((1.0 - 0.92) * MAX_16)         /* threshold for fast adaption */
#define CVAD_ADAPT_REALLY_FAST (int16_t)((1.0 - 0.80) * MAX_16)  /* threshold for really fast
adaption                    */

/*
********************************************************************************
*                         LOCAL VARIABLES AND TABLES
********************************************************************************
*/

/*
********************************************************************************
*                         DEFINITION OF DATA TYPES
********************************************************************************
*/

/* state variable */
typedef struct {
   
   int16_t bckr_est[COMPLEN];    /* background noise estimate                */
   int16_t ave_level[COMPLEN];   /* averaged input components for stationary */
                                /*    estimation                            */
   int16_t old_level[COMPLEN];   /* input levels of the previous frame       */
   int16_t sub_level[COMPLEN];   /* input levels calculated at the end of
                                      a frame (lookahead)                   */
   int16_t a_data5[3][2];        /* memory for the filter bank               */
   int16_t a_data3[5];           /* memory for the filter bank               */

   int16_t burst_count;          /* counts length of a speech burst          */
   int16_t hang_count;           /* hangover counter                         */
   int16_t stat_count;           /* stationary counter                       */

   /* Note that each of the following three variables (vadreg, pitch and tone)
      holds 15 flags. Each flag reserves 1 bit of the variable. The newest
      flag is in the bit 15 (assuming that LSB is bit 1 and MSB is bit 16). */
   int16_t vadreg;               /* flags for intermediate VAD decisions     */
   int16_t pitch;                /* flags for pitch detection                */
   int16_t tone;                 /* flags for tone detection                 */
   int16_t complex_high;         /* flags for complex detection              */
   int16_t complex_low;          /* flags for complex detection              */

   int16_t oldlag_count, oldlag; /* variables for pitch detection            */
 
   int16_t complex_hang_count;   /* complex hangover counter, used by VAD    */
   int16_t complex_hang_timer;   /* hangover initiator, used by CAD          */
    
   int16_t best_corr_hp;         /* FIP filtered value Q15                   */ 

   int16_t speech_vad_decision;  /* final decision                           */
   int16_t complex_warning;      /* complex background warning               */

   int16_t sp_burst_count;       /* counts length of a speech burst incl
                                   HO addition                              */
   int16_t corr_hp_fast;         /* filtered value                           */ 
} vadState1;

/*
********************************************************************************
*                         DECLARATION OF PROTOTYPES
********************************************************************************
*/
void vad_complex_detection_update (vadState1 *st,      /* i/o : State struct     */
                                   int16_t best_corr_hp /* i   : best Corr Q15    */
                                   );

void vad_tone_detection (vadState1 *st, /* i/o : State struct            */
                         Word32 t0,     /* i   : autocorrelation maxima  */
                         Word32 t1      /* i   : energy                  */
                         );

void vad_tone_detection_update (
                vadState1 *st,             /* i/o : State struct              */
                int16_t one_lag_per_frame   /* i   : 1 if one open-loop lag is
                                              calculated per each frame,
                                              otherwise 0                     */
                );

void vad_pitch_detection (vadState1 *st,  /* i/o : State struct                  */
                          int16_t lags[]   /* i   : speech encoder open loop lags */
                          );

int16_t vad1 (  /* i/o : State struct                      */
            int16_t in_buf[]  /* i   : samples of the input frame 
                                inbuf[159] is the very last sample,
                                incl lookahead                          */
            );

#define vadState vadState1

#endif
