#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "AHDR.h"
#include "rom_s-peaker.h"
#include "enh_formant.h"
#include "levinson.h"
#include "typedefs.h"
#include "basic_op.h"
#include "vad.h"
#include "tm_stm32f4_fft.h"
#include "policy.h"
#include "echo_suppress.h"
#include "typedefs.h"

//#define AMR_NB
//#define FIXED
//#define UART_OUTPUT

#define ALPHA 0.8f
#define PI2  6.283185307f
#define PI1  3.141592653f

#define Hz 62.5F /* 8000/128 */

#define NO_ERROR		0

#define M			10
#define FFTSIZE			128
#define FFTSIZE_BY_TWO		64
#define FRAMESIZE		80
#define Fs			8000
#define FRONTMARGIN		48


#define EQNSIZE              10
#define WINDOW_BY_TWO        (FFTSIZE>>1)
#define FORMANTNUM           10
#define LOOPMAX              1000


extern void Init_fe1(void);
extern void Apply_Window(float *buf, float *w, short size);
extern void HammingGainFilter(int16_t BW, float *GainFilter, float GainSize);
extern void bairstow(float *equ, float *arroot_R, float arroot_I[][2], float Accuracy);
extern void SortArray(float arroot_I[][2]);
extern void SortArraySingle(float *Formants, float *BandWidth, int Size);
extern void FormantEnhancement1(int16_t *Signal, int Mode);
extern void r_fft(float *farray_ptr, int isign);
extern void init_vad1(void);
extern int reset_AHDR1(void);
extern void AcousticHighDynamicRange_exe(short *x, int l);

extern float Previn_Buf[FFTSIZE];
extern float Prevsynth_Buf[FFTSIZE];
extern int FrameNo;

#endif