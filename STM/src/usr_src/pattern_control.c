#include "global_define.h"

#ifdef ENABLE_PWM
void ( *pwm_pulse_led[5] )( uint16_t ) = { pwm_pulse_led5, pwm_pulse_led4, pwm_pulse_led3, pwm_pulse_led2, pwm_pulse_led1 };

static void add_pwm( uint16_t index, uint16_t add_val )
{
	pwm_pulse_led[index](g_str_led[index].ui16_pwm);

	g_str_led[index].ui16_pwm += add_val;
	if( g_str_led[index].ui16_pwm > 100 )
	{
		g_str_led[index].ui16_pwm = 100;
	}
}

static void add_pwm_all(uint16_t add_val)
{
	for( int i = 0; i < 5; i++ )
	{
		add_pwm(i, add_val);
	}
}

static void sub_pwm( uint16_t index, uint16_t sub_val )
{
	pwm_pulse_led[index](g_str_led[index].ui16_pwm);

	if( g_str_led[index].ui16_pwm > sub_val )
	{
		g_str_led[index].ui16_pwm -= sub_val;
	}
	else
	{
		g_str_led[index].ui16_pwm = 0;
	}
}

static void sub_pwm_all(uint16_t sub_val)
{
	for( int i = 0; i < 5; i++ )
	{
		sub_pwm(i, sub_val);
	}
}

static void set_pwm_all( uint16_t val )
{
	for( int i = 0; i < 5; i++ )
	{
		g_str_led[i].ui16_pwm = val;
		pwm_pulse_led[i](val);
	}
}

static void set_pwm( uint16_t index, uint16_t value )
{
	pwm_pulse_led[index](value);
}

void set_led_pwm( uint16_t index, uint16_t value )
{
	set_pwm(index, value);
}

void pattern_power_on( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_POWER_ON_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 50 )
	{
		add_pwm(0, 3);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 100 )
	{
		set_pwm(0, 100);
		add_pwm(1, 3);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 150 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		add_pwm(2, 3);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 200 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		add_pwm(3, 3);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 250 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		set_pwm(3, 100);
		add_pwm(4, 3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 300 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 350 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 400 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 450 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 500 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_POWER_ON_CNT )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_ON_CNT - 50 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_power_off( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_POWER_OFF_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 80 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 120 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		set_pwm(3, 100);
		sub_pwm(4, 3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 160 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		sub_pwm(3, 3);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 200 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		sub_pwm(2, 3);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 240 )
	{
		set_pwm(0, 100);
		sub_pwm(1, 3);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 280 )
	{
		sub_pwm(0, 3);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 320 )
	{
		//power_off sequence
		g_str_bitflag.b1_ble_send_flag = 1;
		
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_POWER_OFF_CNT )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_POWER_OFF_CNT - 50 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_batt_gauge( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BATT_GAUGE_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 40 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 80 )
	{
		if( g_str_batt.ui16_batt_status == BATT_STATUS_FULL )
		{
			g_str_timer.ui16_pattern_cnt = 0;
			set_pwm_all(0);
		}
		else if( g_str_batt.ui8_batt_led_level < 4 )
		{
			set_pwm(0, 100);
			set_pwm(1, 100);
			set_pwm(2, 100);
			set_pwm(3, 100);
			set_pwm(4, 0);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 120 )
	{
		if( g_str_batt.ui8_batt_led_level == 4 )
		{
			g_str_timer.ui16_pattern_cnt = 200;
			
		}
		else if( g_str_batt.ui8_batt_led_level < 3 )
		{
			set_pwm(0, 100);
			set_pwm(1, 100);
			set_pwm(2, 100);
			set_pwm(3, 0);
			set_pwm(4, 0);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 160 )
	{
		if( g_str_batt.ui8_batt_led_level == 3 )
		{
			g_str_timer.ui16_pattern_cnt = 200;
		}
		else if( g_str_batt.ui8_batt_led_level < 2 )
		{
			set_pwm(0, 100);
			set_pwm(1, 100);
			set_pwm(2, 0);
			set_pwm(3, 0);
			set_pwm(4, 0);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_GAUGE_CNT - 200 )
	{
		if( g_str_batt.ui8_batt_led_level == 2 )
		{
			g_str_timer.ui16_pattern_cnt = 200;

		}
		else if( g_str_batt.ui8_batt_led_level < 1 )
		{
			set_pwm(0, 100);
			set_pwm(1, 0);
			set_pwm(2, 0);
			set_pwm(3, 0);
			set_pwm(4, 0);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt < PATTERN_BATT_GAUGE_CNT - 400 )
	{
		g_str_timer.ui16_pattern_cnt = 0;
		set_pwm_all(0);
	}
}


void pattern_batt_low( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BATT_LOW_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 40 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 80 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		set_pwm(3, 100);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 120 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 160 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 200 )
	{
		set_pwm(0, 100);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 220 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 240 )
	{
		set_pwm(0, 100);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 260 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 280 )
	{
		set_pwm(0, 100);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 300 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 320 )
	{
		set_pwm(0, 100);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 340 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 360 )
	{
		set_pwm(0, 100);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else
	{
		g_str_timer.ui16_pattern_cnt = 0;
		set_pwm_all(0);
	}

	// haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BATT_LOW_CNT )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_LOW_CNT - 50 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_batt_charging_level( void )
{
	for( int i = 0; i < g_str_batt.ui8_batt_led_level; i++ )
	{
		set_pwm(i, 100);
	}

	if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 40 )
	{
		sub_pwm(g_str_batt.ui8_batt_led_level, 3);
		for( int j = g_str_batt.ui8_batt_led_level+1; j < 5; j++ )
		{
			set_pwm(j, 0);
		}
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 80 )
	{
		add_pwm(g_str_batt.ui8_batt_led_level, 3);
		for( int j = g_str_batt.ui8_batt_led_level+1; j < 5; j++ )
		{
			set_pwm(j, 0);
		}
	}
	else
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
	}
	
}

void pattern_batt_charging( void )
{
	// led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BATT_CHARGING_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 40 )
	{
		add_pwm(0, 3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 80 )
	{
		if( g_str_batt.ui8_batt_led_level == 0 )
		{
			g_str_timer.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
			p_pattern_func = pattern_batt_charging_level;
		}
		add_pwm(1, 3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 120 )
	{
		if( g_str_batt.ui8_batt_led_level == 1 )
		{
			g_str_timer.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
			p_pattern_func = pattern_batt_charging_level;
		}
		add_pwm(2, 3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 160 )
	{
		if( g_str_batt.ui8_batt_led_level == 2 )
		{
			g_str_timer.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
			p_pattern_func = pattern_batt_charging_level;
		}
		add_pwm(3, 3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 200 )
	{
		if( g_str_batt.ui8_batt_led_level == 3 )
		{
			g_str_timer.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
			p_pattern_func = pattern_batt_charging_level;
		}
		add_pwm(4, 3);
	}
	else
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
		p_pattern_func = pattern_batt_charging_level;
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_BATT_CHARGING_CNT )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BATT_CHARGING_CNT - 50 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_batt_full( void )
{
#if 01
	if(g_str_ble_status.cyw20706_cmd_status == CMD_BATTERY_STATUS)
	{
		if( g_str_timer.ui16_pattern_cnt == 1 )
		{
			g_str_timer.ui16_pattern_cnt = 0;
			//set_pwm_all(0);
			force_off_pattern();
		}
		else
		{
			set_pwm_all(100);
		}
	}
	else
#endif
		set_pwm_all(100);
}

void pattern_wait_pairing( void )
{	
	if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 40 )
	{
		add_pwm(0, 2);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 80 )
	{
		add_pwm(0, 2);
		add_pwm(1, 2);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 120 )
	{
		sub_pwm(0, 2);
		add_pwm(1, 2);
		add_pwm(2, 2);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 160 )
	{
		sub_pwm(0, 2);
		sub_pwm(1, 2);
		add_pwm(2, 2);
		add_pwm(3, 2);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 200 )
	{
		set_pwm(0, 0);
		sub_pwm(1, 2);
		sub_pwm(2, 2);
		add_pwm(3, 2);
		add_pwm(4, 2);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 240 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		sub_pwm(2, 2);
		sub_pwm(3, 2);
		add_pwm(4, 2);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 280 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		sub_pwm(3, 2);
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 320 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		add_pwm(3, 2);
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 360 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		add_pwm(2, 2);
		add_pwm(3, 2);
		sub_pwm(4, 2);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 400 )
	{
		set_pwm(0, 0);
		add_pwm(1, 2);
		add_pwm(2, 2);
		sub_pwm(3, 2);
		sub_pwm(4, 2);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 440 )
	{
		add_pwm(0, 2);
		add_pwm(1, 2);
		sub_pwm(2, 2);
		sub_pwm(3, 2);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 520 )
	{
		add_pwm(0, 2);
		sub_pwm(1, 2);
		sub_pwm(2, 2);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_WAIT_PAIRING_CNT - 560 )
	{
		sub_pwm(0, 2);
		sub_pwm(1, 2);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_WAIT_PAIRING_CNT;
	}
}


void pattern_pairing_complete( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_PAIRING_COMPLETE_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 60 )
	{
		add_pwm(0, 2);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		add_pwm(4, 2);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 120 )
	{
		set_pwm(0, 100);
		add_pwm(1, 2);
		set_pwm(2, 0);
		add_pwm(3, 2);
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 180 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		add_pwm(2, 3);
		set_pwm(3, 100);
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 200 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 220 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 240 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 260 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 280 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 300 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 320 )
	{
		set_pwm_all(100);
	}
	else
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_PAIRING_COMPLETE_CNT )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_PAIRING_COMPLETE_CNT - 50 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_disconn_timeout( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DISCONN_TIMEOUT_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 20 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 80 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		sub_pwm(2, 2);
		set_pwm(3, 100);
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 140 )
	{
		set_pwm(0, 100);
		sub_pwm(1, 2);
		set_pwm(2, 0);
		sub_pwm(3, 2);
		set_pwm(0, 100);
		
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 200 )
	{
		sub_pwm(0, 2);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		sub_pwm(4, 2);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 260 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 280 )
	{
		set_pwm(0, 100);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 300 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 320 )
	{
		set_pwm(0, 100);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 340 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 360 )
	{
		set_pwm(0, 100);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 100);
	}
	else
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	// haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DISCONN_TIMEOUT_CNT )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DISCONN_TIMEOUT_CNT - 50 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}


void pattern_dnd_on( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DND_ON_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 50 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_ON_CNT - 200 )
	{
		if( g_str_timer.ui16_pattern_cnt % 2 )
		{
			if( g_str_timer.ui16_pattern_cnt > 100 )
			{
				sub_pwm_all(2);
			}
			else
			{
				sub_pwm_all(1);
			}
		}
	}
	else
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
}

void pattern_dnd_off( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DND_OFF_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 100 )
	{
		add_pwm_all(1);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 120 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 140 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 160 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 180 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 200 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 220 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 240 )
	{
		set_pwm_all(100);
	}
	else
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 160 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DND_OFF_CNT - 240 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_haptic_action( void )
{
	//if( g_str_timer.ui16_pattern_cnt >= PATTERN_HAPTIC_ACTION_CNT )
	if( g_str_timer.ui16_pattern_cnt > 0 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		haptic_off();
	}
}

void pattern_incoming_call( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_INCOMING_CALL_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 40 )
	{
		add_pwm_all(3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 50 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 90 )
	{
		sub_pwm_all(3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 100 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 140 )
	{
		add_pwm_all(3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 150 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 190 )
	{
		sub_pwm_all(3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 200 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = PATTERN_INCOMING_CALL_CNT;
	}	
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_INCOMING_CALL_CNT - 10 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 70 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_incoming_favorite( void )
{
	if( g_str_ble_status.favorite_number > FAVORITE_5TH )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
		return;
	}
	
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_INCOMING_CALL_CNT )
	{
		set_pwm_all(0);
		set_pwm(g_str_ble_status.favorite_number, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 40 )
	{
		add_pwm_all(3);
		set_pwm(g_str_ble_status.favorite_number, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 50 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 90 )
	{
		sub_pwm_all(3);
		set_pwm(g_str_ble_status.favorite_number, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 100 )
	{
		set_pwm_all(0);
		set_pwm(g_str_ble_status.favorite_number, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 140 )
	{
		add_pwm_all(3);
		set_pwm(g_str_ble_status.favorite_number, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 150 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 190 )
	{
		sub_pwm_all(3);
		set_pwm(g_str_ble_status.favorite_number, 100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 200 )
	{
		set_pwm_all(0);
		set_pwm(g_str_ble_status.favorite_number, 100);
		g_str_timer.ui16_pattern_cnt = PATTERN_INCOMING_CALL_CNT;
	}	
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_INCOMING_CALL_CNT - 10 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_INCOMING_CALL_CNT - 70 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_favorite_select( void )
{
	uint8_t cnt = 0;
	
#if 01 //display saved and choosed number
	for(cnt=FAVORITE_1ST; cnt<=FAVORITE_5TH; cnt++)
	{
		switch(g_favorite_num[cnt])
		{
			case 0:
				set_pwm(cnt, 0);
				break;
			case 1:
				set_pwm(cnt, 10);
				break;
			case 2:
				if( g_str_timer.ui16_pattern_cnt >= PATTERN_FAVORITE_SELECT_CNT )
				{
					set_pwm(cnt, 10);
				}
				else if( g_str_timer.ui16_pattern_cnt >= PATTERN_FAVORITE_SELECT_CNT - 50 )
				{
					add_pwm(cnt, 2);
				}
				else if( g_str_timer.ui16_pattern_cnt >= PATTERN_FAVORITE_SELECT_CNT - 100 )
				{
					sub_pwm(cnt, 2);
				}
				else if( g_str_timer.ui16_pattern_cnt >= PATTERN_FAVORITE_SELECT_CNT - 150 )
				{
					add_pwm(cnt, 2);
				}
				else if( g_str_timer.ui16_pattern_cnt >= PATTERN_FAVORITE_SELECT_CNT - 200 )
				{
					sub_pwm(cnt, 2);
				}
				else if( g_str_timer.ui16_pattern_cnt >= PATTERN_FAVORITE_SELECT_CNT - 250 )
				{
					add_pwm(cnt, 2);
				}
				else if( g_str_timer.ui16_pattern_cnt >= PATTERN_FAVORITE_SELECT_CNT - 300 )
				{
					sub_pwm(cnt, 2);
				}
				break;
		}
	}
#else
	if( favorite_number > FAVORITE_5TH )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
		return;
	}
	set_pwm_all(0);
	set_pwm(favorite_number, 100);
#endif
	if( g_str_timer.ui16_pattern_cnt == 10 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
}

void pattern_favorite_call( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_FAVORITE_CALL_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 40 )
	{
		add_pwm(0, 3);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 80 )
	{
		sub_pwm(0, 3);
		add_pwm(1, 3);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 120 )
	{
		set_pwm(0, 0);
		sub_pwm(1, 3);
		add_pwm(2, 3);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 160 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		sub_pwm(2, 3);
		add_pwm(3, 3);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 200 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		sub_pwm(3, 3);
		add_pwm(4, 3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 240 )
	{
		add_pwm(0, 3);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);		
		sub_pwm(4, 3);
		
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 280 )
	{
		sub_pwm(0, 3);
		add_pwm(1, 3);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 320 )
	{
		set_pwm(0, 0);
		sub_pwm(1, 3);
		add_pwm(2, 3);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 360 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		sub_pwm(2, 3);
		add_pwm(3, 3);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 400 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		sub_pwm(3, 3);
		add_pwm(4, 3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FAVORITE_CALL_CNT - 440 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		sub_pwm(4, 3);
	}
	else
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = PATTERN_FAVORITE_CALL_CNT;
	}	
}

void pattern_vol_display( void )
{
	switch( g_str_ble_status.volume_level_status )
	{
	case VOLUME_LEVEL_0 :
		set_pwm_all(0);
		break;
	case VOLUME_LEVEL_1 :
		set_pwm_all(0);
		set_pwm(0,100);
		break;
	case VOLUME_LEVEL_2 :
		set_pwm_all(0);
		set_pwm(0,100);
		set_pwm(1,100);
		break;
	case VOLUME_LEVEL_3 :
		set_pwm_all(0);
		set_pwm(0,100);
		set_pwm(1,100);
		set_pwm(2,100);
		break;
	case VOLUME_LEVEL_4 :
		set_pwm_all(0);
		set_pwm(0,100);
		set_pwm(1,100);
		set_pwm(2,100);
		set_pwm(3,100);
		break;
	case VOLUME_LEVEL_5 :
		set_pwm_all(100);
		set_pwm(0,100);
		set_pwm(1,100);
		set_pwm(2,100);
		set_pwm(3,100);
		set_pwm(4,100);
		break;
	default :
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
		break;
	}	

	if( g_str_timer.ui16_pattern_cnt == 10 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
}

void pattern_app_noti( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_APP_NOTI_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 5 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 20 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 50 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 100 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 150 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 155 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 170 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 200 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 250 )
	{
		set_pwm_all(0);
	}
	else
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 10 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 40 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 60 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_APP_NOTI_CNT - 90 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_exhibition_mode_off( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_EXHIBITION_MODE_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 40 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		set_pwm(3, 100);
		sub_pwm(4, 5);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 60 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		sub_pwm(3, 5);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 80 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		sub_pwm(2, 5);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 100 )
	{
		set_pwm(0, 100);
		sub_pwm(1, 5);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 120 )
	{
		sub_pwm(0, 5);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 140 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		set_pwm(3, 100);
		sub_pwm(4, 5);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 160 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		sub_pwm(3, 5);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 180 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		sub_pwm(2, 5);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 200 )
	{
		set_pwm(0, 100);
		sub_pwm(1, 5);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 220 )
	{
		sub_pwm(0, 5);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else
	{
		g_str_timer.ui16_pattern_cnt = 0;
		set_pwm_all(0);
	}

	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 10 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 50 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_exhibition_mode_on( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_EXHIBITION_MODE_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 40 )
	{
		add_pwm(0, 5);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 60 )
	{
		set_pwm(0, 100);
		add_pwm(1, 5);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 80 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		add_pwm(2, 5);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 100 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		add_pwm(3, 5);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 120 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		set_pwm(3, 100);
		add_pwm(4, 5);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 140 )
	{
		add_pwm(0, 5);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 160 )
	{
		set_pwm(0, 100);
		add_pwm(1, 5);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 180 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		add_pwm(2, 5);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 200 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		add_pwm(3, 5);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 220 )
	{
		set_pwm(0, 100);
		set_pwm(1, 100);
		set_pwm(2, 100);
		set_pwm(3, 100);
		add_pwm(4, 5);
	}
	else
	{
		g_str_timer.ui16_pattern_cnt = 0;
		set_pwm_all(0);
	}

	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 10 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_EXHIBITION_MODE_CNT - 50 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

#ifdef SMT_CHECK
void pattern_pedo_fuel_check( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_PEDO_FUEL_CHECK_CNT )
	{
		set_pwm_all(0);
	}
	else
	{
		if(g_str_ble_status.pedo_fuel_check == 0x01)
		{
			set_pwm(0, 100);
			set_pwm(1, 0);
			set_pwm(2, 0);
			set_pwm(3, 0);
			set_pwm(4, 0);
		}
		else if(g_str_ble_status.pedo_fuel_check == 0x02)
		{
			set_pwm(0, 0);
			set_pwm(1, 0);
			set_pwm(2, 0);
			set_pwm(3, 0);
			set_pwm(4, 100);
		}
		else if(g_str_ble_status.pedo_fuel_check == 0x03)
		{
			set_pwm(0, 100);
			set_pwm(1, 0);
			set_pwm(2, 0);
			set_pwm(3, 0);
			set_pwm(4, 100);
		}
	}
}
#endif

void pattern_device_reset( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_DEVICE_RESET_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 40 )
	{
		add_pwm_all(3);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 60 )
	{
		add_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 120 )
	{
		sub_pwm(0, 2);
		set_pwm(1, 100);
		set_pwm(2, 100);
		set_pwm(3, 100);
		sub_pwm(4, 2);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 180 )
	{
		set_pwm(0, 0);
		sub_pwm(1, 2);
		set_pwm(2, 100);
		sub_pwm(3, 2);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 240 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		sub_pwm(2, 2);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else
	{
		g_str_timer.ui16_pattern_cnt = 0;
		set_pwm_all(0);
	}

	// haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 10 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_DEVICE_RESET_CNT - 50 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}


void pattern_unpaired( void )
{
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_UNPAIRED_CNT )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_UNPAIRED_CNT - 300 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_UNPAIRED_CNT - 500 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_UNPAIRED_CNT;
		
		call_unpaired();
	}
}









void sub_pattern_app_noti( void )
{
	//haptic control
	if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 8 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 14 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 24 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_sub_pattern_cnt > PATTERN_APP_NOTI_CNT - 30 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_ble_conn_success( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 100 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 150 )
	{
		set_pwm_all(0);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 175 )
	{
		set_pwm_all(0);
		set_pwm(0,100);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 200 )
	{
		set_pwm(0,100);
		set_pwm(1,0);
		set_pwm(2,0);
		set_pwm(3,100);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 225 )
	{
		set_pwm_all(100);
		set_pwm(2,0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 250 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 275 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 300 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 350 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 15 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 30 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 50 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 60 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}
void pattern_ble_disconn( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 50 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 150 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 200 )
	{
		set_pwm(0,100);
		set_pwm(1,100);
		set_pwm(2,0);
		set_pwm(3,100);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 225 )
	{
		set_pwm(0,100);
		set_pwm(1,0);
		set_pwm(2,0);
		set_pwm(3,100);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 250 )
	{
		set_pwm(0,100);
		set_pwm(1,0);
		set_pwm(2,0);
		set_pwm(3,0);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 275 )
	{
		set_pwm_all(0);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 300 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 350 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 15 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 30 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 50 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BLE_CONN_CHK_CNT - 60 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}


void pattern_bt_conn_success( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 100 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 150 )
	{
		set_pwm_all(0);
		set_pwm(0,100);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 200 )
	{
		set_pwm_all(100);
		set_pwm(2,0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 250 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 300 )
	{
		set_pwm_all(100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 350 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 15 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 30 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 50 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 60 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}
void pattern_bt_disconn( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 50 )
	{
		set_pwm_all(0);
		
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 150 )
	{
		set_pwm_all(100);
		
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 200 )
	{
		set_pwm_all(100);
		set_pwm(2, 0);
		
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 250 )
	{
		set_pwm(0,100);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4,100);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 300 )
	{
		set_pwm_all(0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 350 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = 0;
	}
	
	//haptic control
	if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 15 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 30 )
	{
		haptic_on();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 50 )
	{
		haptic_off();
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_BT_CONN_CHK_CNT - 60 )
	{
		haptic_on();
	}
	else
	{
		haptic_off();
	}
}

void pattern_fw_upgrade_progress( void )
{
	//led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_FW_UPGRADE_CNT )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FW_UPGRADE_CNT - 50 )
	{
		add_pwm(0, 2);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FW_UPGRADE_CNT - 100 )
	{
		set_pwm(0, 0);
		add_pwm(1, 2);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FW_UPGRADE_CNT - 150 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		add_pwm(2, 2);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FW_UPGRADE_CNT - 200 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		add_pwm(3, 2);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FW_UPGRADE_CNT - 250 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		add_pwm(4, 2);
	}
	else if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		set_pwm_all(0);
		g_str_timer.ui16_pattern_cnt = PATTERN_FW_UPGRADE_CNT;
	}
}
#endif