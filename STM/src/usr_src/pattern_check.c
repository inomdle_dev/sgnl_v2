#include "global_define.h"

#ifdef ENABLE_PWM
union{
	uint32_t value;
	struct {
		uint32_t power_on:1;		//1
		uint32_t power_off:1;		//2
		
		uint32_t batt_gauge:1;		//3
		uint32_t batt_low:1;		//4
		uint32_t batt_charging:1;	//5
		uint32_t batt_full:1;		//6
		
		uint32_t wait_pairing:1;	//7
		uint32_t pairing_complete:1;	//8
		uint32_t disconn_timeout:1;	//9
		
		uint32_t dnd_on:1;		//10
		uint32_t dnd_off:1;		//11
		
		uint32_t haptic_action:1;	//12
		uint32_t incoming_call:1;	//13
		uint32_t incoming_favorite:1;	//14
		
		uint32_t favorite_select:1;	//15
		uint32_t favorite_call:1;	//16
		
		uint32_t vol_display:1;		//17
		
		uint32_t app_noti:1;		//18
		
		uint32_t device_reset:1;	//19
		
		uint32_t unpaired:1;		//20
		uint32_t ble_conn:1;		//21
		uint32_t ble_disconn:1;		//22
		uint32_t bt_conn:1;		//23
		uint32_t bt_disconn:1;		//24
		
		uint32_t fw_upgrade:1;		//25
		uint32_t exhibition_mode:1;	//26
#ifdef SMT_CHECK
		uint32_t pedo_fuel_check:1;	//27
#endif
	};
} pattern_check;

union{
	uint32_t value;
	struct {
		
		uint32_t app_noti:1;
		
	};
} sub_pattern_check;

pattern_func p_pattern_func = NULL;
pattern_func p_sub_pattern_func = NULL;

void pattern_command_check(void)
{
	if( pattern_check.value )
	{
		if( pattern_check.power_on )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_POWER_ON_CNT;
				p_pattern_func = pattern_power_on;
				pattern_check.power_on = 0;
			}
		}
		else if( pattern_check.power_off )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_POWER_OFF_CNT;
				p_pattern_func = pattern_power_off;
				pattern_check.power_off = 0;
			}
		}
		else if( pattern_check.batt_gauge )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BATT_GAUGE_CNT;
				p_pattern_func = pattern_batt_gauge;
				pattern_check.batt_gauge = 0;
			}
		}
		else if( pattern_check.batt_low )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BATT_LOW_CNT;
				p_pattern_func = pattern_batt_low;
				pattern_check.batt_low = 0;
			}
		}
		else if( pattern_check.batt_charging )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
				p_pattern_func = pattern_batt_charging;
				pattern_check.batt_charging = 0;
			}
		}
		else if( pattern_check.batt_full )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BATT_FULL_CNT;
				p_pattern_func = pattern_batt_full;
				pattern_check.batt_full = 0;
			}
		}
		else if( pattern_check.wait_pairing )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_WAIT_PAIRING_CNT;
				p_pattern_func = pattern_wait_pairing;
				pattern_check.wait_pairing = 0;
			}
		}
		else if( pattern_check.pairing_complete )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_PAIRING_COMPLETE_CNT;
				p_pattern_func = pattern_pairing_complete;
				pattern_check.pairing_complete = 0;
			}
		}
		else if( pattern_check.ble_conn )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BLE_CONN_CHK_CNT;
				p_pattern_func = pattern_ble_conn_success;
				pattern_check.ble_conn = 0;
			}
		}
		else if( pattern_check.ble_disconn )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BLE_CONN_CHK_CNT;
				p_pattern_func = pattern_ble_disconn;
				pattern_check.ble_disconn = 0;
			}
		}		
		else if( pattern_check.bt_conn )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BT_CONN_CHK_CNT;
				p_pattern_func = pattern_bt_conn_success;
				pattern_check.bt_conn = 0;
			}
		}
		else if( pattern_check.bt_disconn )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_BT_CONN_CHK_CNT;
				p_pattern_func = pattern_bt_disconn;
				pattern_check.bt_disconn = 0;
			}
		}		
				
		else if( pattern_check.disconn_timeout )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_DISCONN_TIMEOUT_CNT;
				p_pattern_func = pattern_disconn_timeout;
				pattern_check.disconn_timeout = 0;
			}
		}
		
		else if( pattern_check.dnd_on )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_DND_ON_CNT;
				p_pattern_func = pattern_dnd_on;
				pattern_check.dnd_on = 0;
			}
		}
		else if( pattern_check.dnd_off )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_DND_OFF_CNT;
				p_pattern_func = pattern_dnd_off;
				pattern_check.dnd_off = 0;
			}
		}
		else if( pattern_check.haptic_action )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_HAPTIC_ACTION_CNT;
				p_pattern_func = pattern_haptic_action;
				pattern_check.haptic_action = 0;
			}
		}
		else if( pattern_check.incoming_call )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_INCOMING_CALL_CNT;
				p_pattern_func = pattern_incoming_call;
				pattern_check.incoming_call = 0;
			}
		}
		else if( pattern_check.favorite_call )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_FAVORITE_CALL_CNT;
				p_pattern_func = pattern_favorite_call;
				pattern_check.favorite_call = 0;
			}
		}
		else if( pattern_check.favorite_select )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_FAVORITE_SELECT_CNT;
				p_pattern_func = pattern_favorite_select;
				pattern_check.favorite_select = 0;
			}
		}
		else if( pattern_check.vol_display )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_VOL_DISPLAY_CNT;
				p_pattern_func = pattern_vol_display;
				pattern_check.vol_display = 0;
			}
		}
		
		else if( pattern_check.app_noti )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_APP_NOTI_CNT;
				p_pattern_func = pattern_app_noti;
				pattern_check.app_noti = 0;
			}
		}
		else if( pattern_check.device_reset )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_DEVICE_RESET_CNT;
				p_pattern_func = pattern_device_reset;
				pattern_check.device_reset = 0;
			}
		}
		else if( pattern_check.exhibition_mode )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_EXHIBITION_MODE_CNT;
				p_pattern_func = pattern_exhibition_mode_on;
				pattern_check.exhibition_mode = 0;
			}
		}
#ifdef SMT_CHECK
		else if( pattern_check.pedo_fuel_check )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_PEDO_FUEL_CHECK_CNT;
				p_pattern_func = pattern_pedo_fuel_check;
				pattern_check.pedo_fuel_check = 0;
			}
		}
#endif
		
		else if( pattern_check.unpaired )
		{
			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				g_str_timer.ui16_pattern_cnt = PATTERN_UNPAIRED_CNT;
				p_pattern_func = pattern_unpaired;
				pattern_check.unpaired = 0;
			}
		}		
	}
	else
	{
		force_off_pattern();//pattern_timer_stop();
		check_charger_status();
#if 0
		if( ( g_str_ble_status.charger_status == CHARGER_ATTACHED )
		   && ( g_str_ble_status.cyw20706_cmd_status != CMD_BATTERY_STATUS )
			   && ( g_str_ble_status.cyw20706_cmd_status != CMD_CHARGER_STATUS ) )
#else
		if( g_str_ble_status.charger_status == CHARGER_ATTACHED )
#endif
		{
			g_battery_percentage = g_str_batt.ui8_percent;
			if( g_battery_percentage == 100 )
			{
				if(!g_str_bitflag.b1_charging_complete)
				{
					force_off_pattern();
					g_str_batt.ui16_batt_status = BATT_STATUS_FULL;
					g_str_bitflag.b1_charging_complete = 1;
				}
				call_batt_full();
			}
			else
			{
				g_str_ble_status.haptic_status = HAPTIC_STATUS_OFF;
				g_str_batt.ui16_batt_status = BATT_STATUS_IN_CHARGING;
				call_batt_charging();
			}
		}
	}
}


void sub_pattern_command_check(void)
{	
	if( sub_pattern_check.value )
	{
		if( sub_pattern_check.app_noti )
		{
			if( g_str_timer.ui16_sub_pattern_cnt == 0 )
			{
				g_str_timer.ui16_sub_pattern_cnt = PATTERN_APP_NOTI_CNT;
				p_pattern_func = pattern_app_noti;
				sub_pattern_check.app_noti = 0;
			}
		}		
	}
}

void call_power_on( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_POWER_ON_CNT;
		p_pattern_func = pattern_power_on;
		pattern_check.power_on = 0;
	}
	else
	{
		pattern_check.power_on = 1;
	}
}

void call_power_off( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_POWER_OFF_CNT;
		p_pattern_func = pattern_power_off;
		pattern_check.power_off = 0;
	}
	else
	{
		pattern_check.power_off = 1;
	}
}


void call_batt_gauge( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BATT_GAUGE_CNT;
		p_pattern_func = pattern_batt_gauge;
		pattern_check.batt_gauge = 0;
	}
	else
	{
		pattern_check.batt_gauge = 1;
	}
}


void call_batt_low( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BATT_LOW_CNT;
		p_pattern_func = pattern_batt_low;
		pattern_check.batt_low = 0;
	}
	else
	{
		pattern_check.batt_low = 1;
	}
}


void call_batt_charging( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BATT_CHARGING_CNT;
		p_pattern_func = pattern_batt_charging;
		pattern_check.batt_charging = 0;
	}
	else
	{
		pattern_check.batt_charging = 1;
	}
}


void call_batt_full( void )
{
	if(g_str_ble_status.cyw20706_cmd_status == CMD_BATTERY_STATUS)
	{
		pattern_timer_start();
		if( g_str_timer.ui16_pattern_cnt == 0 )
		{
			g_str_timer.ui16_pattern_cnt = PATTERN_BATT_FULL_CNT;
			p_pattern_func = pattern_batt_full;
			pattern_check.batt_full = 0;
		}
		else
		{
			pattern_check.batt_full = 1;
		}
	}
	else
	{
		if( g_str_batt.ui16_batt_status == BATT_STATUS_FULL )
		{
			pwm_enable();
			pattern_batt_full();
		}
	}
}


void call_wait_pairing( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_WAIT_PAIRING_CNT;
		p_pattern_func = pattern_wait_pairing;
		pattern_check.wait_pairing = 0;
	}
	else
	{
		pattern_check.wait_pairing = 1;
	}
}


void call_pairing_complete( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_PAIRING_COMPLETE_CNT;
		p_pattern_func = pattern_pairing_complete;
		pattern_check.pairing_complete = 0;
	}
	else
	{
		pattern_check.pairing_complete = 1;
	}
}

void call_disconn_timeout( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_DISCONN_TIMEOUT_CNT;
		p_pattern_func = pattern_disconn_timeout;
		pattern_check.disconn_timeout = 0;
	}
	else
	{
		pattern_check.disconn_timeout = 1;
	}
}

void call_dnd_on( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_DND_ON_CNT;
		p_pattern_func = pattern_dnd_on;
		pattern_check.dnd_on = 0;
	}
	else
	{
		pattern_check.dnd_on = 1;
	}
}

void call_dnd_off( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_DND_OFF_CNT;
		p_pattern_func = pattern_dnd_off;
		pattern_check.dnd_off = 0;
	}
	else
	{
		pattern_check.dnd_off = 1;
	}
}

void call_haptic_action( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_HAPTIC_ACTION_CNT;
		p_pattern_func = pattern_haptic_action;
		pattern_check.haptic_action = 0;
	}
	else
	{
		pattern_check.haptic_action = 1;
	}
}

void call_incoming_call( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_INCOMING_CALL_CNT;
		p_pattern_func = pattern_incoming_call;
		pattern_check.incoming_call = 0;
	}
	else
	{
		pattern_check.incoming_call = 1;
	}
}

void call_incoming_favorite( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_INCOMING_CALL_CNT;
		p_pattern_func = pattern_incoming_favorite;
		pattern_check.incoming_favorite = 0;
	}
	else
	{
		pattern_check.incoming_call = 1;
	}
}

void call_favorite_select( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_FAVORITE_SELECT_CNT;
		p_pattern_func = pattern_favorite_select;
		pattern_check.favorite_select = 0;
	}
	else
	{
		pattern_check.favorite_select = 1;
	}
}

void call_favorite_call( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_FAVORITE_CALL_CNT;
		p_pattern_func = pattern_favorite_call;
		pattern_check.favorite_call = 0;
	}
	else
	{
		pattern_check.favorite_call = 1;
	}
}

void call_vol_display( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_VOL_DISPLAY_CNT;
		p_pattern_func = pattern_vol_display;
		pattern_check.vol_display = 0;
	}
	else
	{
		pattern_check.vol_display = 1;
	}
}

void call_app_noti( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_APP_NOTI_CNT;
		p_pattern_func = pattern_app_noti;
		pattern_check.app_noti = 0;
	}
	else
	{
		pattern_check.app_noti = 1;
	}
}

void call_exhibition_mode_off( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_EXHIBITION_MODE_CNT;
		p_pattern_func = pattern_exhibition_mode_off;
		pattern_check.exhibition_mode = 0;
	}
	else
	{
		pattern_check.exhibition_mode = 1;
	}
}

void call_exhibition_mode_on( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_EXHIBITION_MODE_CNT;
		p_pattern_func = pattern_exhibition_mode_on;
		pattern_check.exhibition_mode = 0;
	}
	else
	{
		pattern_check.exhibition_mode = 1;
	}
}

#ifdef SMT_CHECK
void call_pedo_fuel_check( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_PEDO_FUEL_CHECK_CNT;
		p_pattern_func = pattern_pedo_fuel_check;
		pattern_check.pedo_fuel_check = 0;
	}
	else
	{
		pattern_check.pedo_fuel_check = 1;
	}
}
#endif

void call_device_reset( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_DEVICE_RESET_CNT;
		p_pattern_func = pattern_device_reset;
		pattern_check.device_reset = 0;
	}
	else
	{
		pattern_check.device_reset = 1;
	}
}

void call_unpaired( void )
{
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_UNPAIRED_CNT;
		p_pattern_func = pattern_unpaired;
		pattern_check.unpaired = 0;
	}
	else
	{
		pattern_check.unpaired = 1;
	}
}

void call_ble_conn_success( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BLE_CONN_CHK_CNT;
		p_pattern_func = pattern_ble_conn_success;
		pattern_check.ble_conn = 0;
	}
	else
	{
		pattern_check.ble_conn = 1;
	}
	
}

void call_ble_disconn( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BLE_CONN_CHK_CNT;
		p_pattern_func = pattern_ble_disconn;
		pattern_check.ble_disconn = 0;
	}
	else
	{
		pattern_check.ble_disconn = 1;
	}
	
}

void call_bt_conn_success( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BT_CONN_CHK_CNT;
		p_pattern_func = pattern_bt_conn_success;
		pattern_check.bt_conn = 0;
	}
	else
	{
		pattern_check.bt_conn = 1;
	}
	
}

void call_bt_disconn( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_BT_CONN_CHK_CNT;
		p_pattern_func = pattern_bt_disconn;
		pattern_check.bt_disconn = 0;
	}
	else
	{
		pattern_check.bt_disconn = 1;
	}
	
}

void call_sub_app_noti( void )
{
	//sub_pattern_timer_start();
	if( g_str_timer.ui16_sub_pattern_cnt == 0 )
	{
		g_str_timer.ui16_sub_pattern_cnt = PATTERN_APP_NOTI_CNT;
		p_sub_pattern_func = sub_pattern_app_noti;
		sub_pattern_check.app_noti = 0;
	}
	else
	{
		sub_pattern_check.app_noti = 1;
	}
}

void call_fw_upgrade( void )
{
	pattern_timer_start();
	if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		g_str_timer.ui16_pattern_cnt = PATTERN_FW_UPGRADE_CNT;
		p_pattern_func = pattern_fw_upgrade_progress;
		pattern_check.fw_upgrade = 0;
	}
	else
	{
		pattern_check.fw_upgrade = 1;
	}
}

void force_off_pattern( void )
{
	if ( g_str_ble_status.power_status == POWER_OFF )
		return;
	pattern_timer_stop();
	haptic_off();
	pwm_enable();
	pwm_disable();
	pattern_check.value = 0;
	g_str_timer.ui16_pattern_cnt = 0;
	p_pattern_func = NULL;
}

#endif