#include "global_define.h"

volatile uint32_t ui32_delay_cnt = 0;

void timer_init(void)
{
	monitor_timer_config();
	
	pattern_timer_config();
	
	monitor_timer_start();
}

/* Timer Configuration
* Use Timer 5
*/
void monitor_timer_config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Enable the TIM5 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = MONITOR_TIM_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 4;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	TIM_TimeBaseInitTypeDef TIM_BaseStruct;
	
	TIM_DeInit(MONITOR_TIM_BASE);
	
	/* TIM5 clock enable */
	RCC_APB1PeriphClockCmd(MONITOR_TIM_RCC, ENABLE);
	/* Time base configuration */
	TIM_BaseStruct.TIM_Period = (SystemCoreClock / 10000) - 1; // N (us)
	TIM_BaseStruct.TIM_Prescaler = 50; // 50 MHz Clock down to 1 MHz (adjust per your clock)
	TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(MONITOR_TIM_BASE, &TIM_BaseStruct);
	
	/* TIM IT enable */
	TIM_ITConfig(MONITOR_TIM_BASE, TIM_IT_Update, ENABLE);
}

void monitor_timer_stop(void)
{
	TIM_Cmd(MONITOR_TIM_BASE, DISABLE);
}

void monitor_timer_start(void)
{
	TIM_Cmd(MONITOR_TIM_BASE, ENABLE);
}

void monitor_timer_isr(void)
{
	if (TIM_GetITStatus(MONITOR_TIM_BASE, TIM_IT_Update) != RESET)
	{
#ifdef ENABLE_STOPMODE
		
#ifndef SMT_CHECK
		//count sleep time
		g_str_timer.ui16_sleep_cnt++;
#endif
		if( ( g_str_ble_status.call_status == CALL_INCOMING ) || ( g_str_ble_status.call_status == CALL_OUTGOING ) || ( g_str_ble_status.call_status == CALL_CALLING )
			|| ( g_str_ble_status.fw_upgrade_status == FW_UPGRADE_READY )
			|| ( g_str_ble_status.charger_status == CHARGER_ATTACHED )
			|| ( g_str_ble_status.bt_pairing_status == BT_PAIRING_START ) 
			|| ( g_str_bitflag.b1_power_on_start ) )//start power on
		{
			g_str_timer.ui16_sleep_cnt = 0;
			if( g_str_ble_status.bt_pairing_status == BT_PAIRING_START )//waiting bt pairing
			{
				if(++g_str_timer.ui16_pairing_cnt >= PAIRING_COUNT)//30 secs
				{
					g_str_timer.ui16_pairing_cnt = 0;
					g_str_ble_status.bt_pairing_status = BT_PAIRING_TIMEOUT;
					g_str_bitflag.b1_ble_send_flag = 1;
				}
			}
		}
		
		if(g_str_bitflag.b1_mcu_sleep_status)
		{
			if(!g_str_bitflag.b1_forced_sleep)
			{
				//when mcu don't receive sleep cmd, set sleep count before 100ms.
				if(g_str_timer.ui16_sleep_cnt >= (SLEEP_COUNT + 50) )
				{
					g_str_timer.ui16_sleep_cnt = (SLEEP_COUNT - 10);
					g_str_bitflag.b1_mcu_sleep_status = 0;
				}
			}
		}
		
		//detect charger pin every 2 seconds
		if(g_str_ble_status.charger_status == CHARGER_ATTACHED)
		{
			if(++g_str_timer.ui16_charger_cnt >= 200)
			{
				check_charger_status();
				if(g_str_ble_status.charger_status == CHARGER_DETACHED)
				{
					g_str_bitflag.b1_charger_wakeup_flag = 1;
				}
				
				g_str_timer.ui16_charger_cnt = 0;
			}
		}
		else
		{
			g_str_timer.ui16_charger_cnt = 0;
		}
		
#endif
		
		if(ui32_delay_cnt)
			ui32_delay_cnt--;
		
		TIM_ClearITPendingBit(MONITOR_TIM_BASE, TIM_IT_Update);
	}
}

void delay_10ms(volatile uint32_t cnt)
{
	ui32_delay_cnt = cnt;
	while(ui32_delay_cnt);
}

/* Timer Configuration
* Use Timer 1
*/
void pattern_timer_config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Enable the TIM1 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = LED_TIM_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	TIM_TimeBaseInitTypeDef TIM_BaseStruct;
	
	TIM_DeInit(LED_TIM_BASE);
	
	/* TIM1 clock enable */
	RCC_APB2PeriphClockCmd(LED_TIM_RCC, ENABLE);
	/* Time base configuration */
	TIM_BaseStruct.TIM_Period = (SystemCoreClock / 10000) - 1; // N (us)
	TIM_BaseStruct.TIM_Prescaler = 100; // 50 MHz Clock down to 1 MHz (adjust per your clock)
	TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(LED_TIM_BASE, &TIM_BaseStruct);
	
	/* TIM IT enable */
	TIM_ITConfig(LED_TIM_BASE, TIM_IT_CC1, ENABLE);
}

void pattern_timer_stop(void)
{
	pwm_disable();
	TIM_Cmd(LED_TIM_BASE, DISABLE);
}

void pattern_timer_start(void)
{
	TIM_Cmd(LED_TIM_BASE, ENABLE);
	pwm_enable();
}

void pattern_timer_isr(void)
{
	if (TIM_GetITStatus(LED_TIM_BASE, TIM_IT_CC1) != RESET)
	{
		TIM_ClearITPendingBit(LED_TIM_BASE, TIM_IT_CC1);
		
		if( p_pattern_func != NULL )
		{
			p_pattern_func();

			if( g_str_timer.ui16_pattern_cnt == 0 )
			{
				p_pattern_func = NULL;
			}
			else
			{
				g_str_timer.ui16_pattern_cnt--;
				g_str_timer.ui16_sleep_cnt = 0;
			}
		}
		else
		{
			pattern_command_check();
		}
	}
}

#ifdef HW_PP4
volatile uint32_t g_ui32_1ms_count = 0;
void SysTick_Handler(void)
{
	if (g_ui32_1ms_count)
		g_ui32_1ms_count--;
}

void delay_ms(uint32_t nTime)
{
	g_ui32_1ms_count = nTime;
	while (g_ui32_1ms_count);
}
#endif