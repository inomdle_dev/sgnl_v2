#include "global_define.h"

#ifdef ENABLE_BLE_UART

void cyw20706_protocol(uint8_t* ui8_data, uint8_t ui8_size)
{
	static uint8_t haptic_action = 0;
	static uint8_t battery_shortage_count = 0;
	static uint8_t incoming_call_flag = 0;
	app_notification_e g_app_notification;
	uint8_t data = 0;
	uint8_t data2[2] = {0,};
	static uint8_t real_call_status = 0;

	g_str_ble_status.cyw20706_cmd_status = (cyw20706_cmd_status_e)ui8_data[1];

	switch( g_str_ble_status.cyw20706_cmd_status )
	{
		case CMD_MCU_STATUS:
			data = (uint8_t)MCU_WAKEUP;
			ble_uart_send_packet(CMD_MCU_STATUS, &data, 1);
			g_str_timer.ui16_sleep_cnt = 0;
			break;
			
		case CMD_CALL_STATUS:
			g_str_ble_status.call_status = (call_status_e)ui8_data[4];
			g_str_ble_status.audio_type_status = AUDIO_HFP;
			if(g_str_ble_status.call_status == CALL_DND)
			{
				g_str_ble_status.haptic_status = HAPTIC_STATUS_OFF;
				break;
			}
			
			else if(g_str_ble_status.call_status == CALL_REJECT)
			{
				haptic_action = 0;
				incoming_call_flag = 0;
				real_call_status = 0;
				audio_rx_stop();
				force_off_pattern();
				break;
			}
			
			if(g_str_ble_status.call_status == CALL_READY)
			{
				haptic_action = 0;
				incoming_call_flag = 0;
				real_call_status = 0;
#ifdef ENABLE_AUDIO
				memset((uint16_t *)&g_str_audio.ui16_data, 0x00, sizeof(g_str_audio.ui16_data));
				g_str_audio.ui16_rx_frame_no = 0;
				g_str_audio.ui16_tx_frame_no = 0;
				g_str_audio.ui16_rx_idx = 0;
				g_str_audio.ui16_tx_idx = 0;
				audio_rx_stop();
#endif
				force_off_pattern();
				call_haptic_action();
				break;
			}
			
			if(g_str_ble_status.call_status & CALL_CALLING)
			{
				g_str_ble_status.call_status = CALL_CALLING;
				real_call_status = ui8_data[5];

				force_off_pattern();
				if(haptic_action)
				{
					call_haptic_action();
				}
				
				if(real_call_status)
				{
					g_str_ble_status.amp_status = AMP_ENABLE;
#ifdef ENABLE_AUDIO
					memset((uint16_t *)&g_str_audio.ui16_data, 0x00, sizeof(g_str_audio.ui16_data));
					g_str_audio.ui16_rx_frame_no = 0;
					g_str_audio.ui16_tx_frame_no = 0;
					g_str_audio.ui16_rx_idx = 0;
					g_str_audio.ui16_tx_idx = 0;
					if(incoming_call_flag)
					{
						audio_init(g_str_audio.ui16_audio_type);
						incoming_call_flag = 0;
					}
					audio_rx_start();
#ifdef ENABLE_HY_FE
					g_str_bitflag.b1_fft_enable = 1;
					g_str_bitflag.b1_fe_enable = 1;
					g_str_bitflag.b1_cgeq_enable = 1;
#endif
#endif
				}
				else //when ble is disconnected, correct call status
				{
					data = g_str_ble_status.call_status;
					ble_uart_send_packet(CMD_CALL_STATUS, &data, 1);
				}
				break;
			}
			
			if(g_str_ble_status.call_status & CALL_INCOMING)
			{
				g_str_ble_status.call_status = CALL_INCOMING;
				
				if(!incoming_call_flag)
				{
					incoming_call_flag = 1;
					g_str_ble_status.haptic_status = (haptic_status_e)ui8_data[6];
					g_str_ble_status.audio_type_status = AUDIO_HFP;
					
					haptic_action = 1;
					force_off_pattern();
					if(ui8_data[5] == 0)//normal incoming call
					{
						call_incoming_call();
					}
					else//favorite incoming call
					{
						g_str_ble_status.favorite_number = (favorite_number_e)ui8_data[5];
						g_str_ble_status.favorite_number--;
						call_incoming_favorite();
					}
					

#ifdef ENABLE_AUDIO				
					SPI_I2S_DeInit(AUDIO_I2S_RX);
					SPI_I2S_DeInit(AUDIO_I2S_TX);
#endif
					
					//if(g_str_audio.ui16_audio_type == AUDIO_FREQ_8K)
					//	g_str_audio.ui16_audio_type = CALLING_FREQ;
					data = (uint8_t)CALL_INCOMING;
					ble_uart_send_packet(CMD_CALL_STATUS, &data, 1);
				}
				
				break;
			}
			else if(g_str_ble_status.call_status & CALL_OUTGOING)
			{
				g_str_ble_status.call_status = CALL_OUTGOING;
				g_str_ble_status.audio_type_status = AUDIO_HFP;

				haptic_action = 0;
				if(ui8_data[5] == 0)//normal outgoing call
				{
				}
				else//favorite outgoing call
				{
					force_off_pattern();
					g_str_ble_status.favorite_number = (favorite_number_e)ui8_data[5];
					g_str_ble_status.favorite_number--;
					call_favorite_call();
				}

				real_call_status = 1;
				g_str_ble_status.amp_status = AMP_ENABLE;
				break;
			}
			
		case CMD_POWER_STATUS:
			force_off_pattern();
			g_str_ble_status.power_status = (power_status_e)ui8_data[4];
			g_str_ble_status.haptic_status = (haptic_status_e)ui8_data[5];
			if( g_str_ble_status.power_status == POWER_ON )
			{
				g_str_bitflag.b1_power_on_start = 0;
				g_str_timer.ui16_sleep_cnt = 0;
				POWER_HOLD_ENABLE;
				
				data = (uint8_t)POWER_ON;
				ble_uart_send_packet(CMD_POWER_STATUS, &data, 1);
				
				call_power_on();
#ifdef ENABLE_HY_FE
				/////////////////////////////////////////////
				cgeq_init();
				fe_init();
				/////////////////////////////////////////////
#endif
			}
			else
			{
				USART_ITConfig(BLE_UART, USART_IT_RXNE, DISABLE);
				call_power_off();
				return;
			}
			break;
			
		case CMD_APP_NOTI_STATUS:
			g_app_notification = (app_notification_e)ui8_data[4];
			g_str_ble_status.haptic_status = (haptic_status_e)ui8_data[5];
			if( g_app_notification == APP_NOTI_ON )
			{
				force_off_pattern();
				call_app_noti();
			}
			break;

		case CMD_BLE_CONNECTION_STATUS:
			g_str_ble_status.ble_conn_status = (ble_conn_status_e)ui8_data[4];
			g_str_ble_status.haptic_status = (haptic_status_e)ui8_data[5];
			if(g_str_ble_status.bt_pairing_status == BT_PAIRING_START)
			{
				g_str_ble_status.bt_pairing_status = BT_PAIRING_SUCCESS;
				force_off_pattern();
			}

			if( g_str_ble_status.ble_conn_status == BLE_DISC_DISC )
			{
				;
			}
			else if( g_str_ble_status.ble_conn_status == BLE_DISC_CONN )
			{
#ifdef ENABLE_FUEL_GAUGE
				check_charger_status();
				data2[0] = g_str_batt.ui8_percent;
				data2[1] = g_str_ble_status.charger_status;
				ble_uart_send_packet(CMD_BATTERY_STATUS, data2, 2);
#endif				
				call_ble_conn_success();
			}
			else if( g_str_ble_status.ble_conn_status == BLE_CONN_DISC )
			{
				call_ble_disconn();
			}
			else if( g_str_ble_status.ble_conn_status == BLE_CONN_CONN )
			{
				;
			}
			break;

		case CMD_BT_CONNECTION_STATUS:
			g_str_ble_status.bt_conn_status = (bt_conn_status_e)ui8_data[4];
			g_str_ble_status.haptic_status = (haptic_status_e)ui8_data[5];
			if(g_str_ble_status.bt_pairing_status == BT_PAIRING_START)
			{
				g_str_ble_status.bt_pairing_status = BT_PAIRING_SUCCESS;
				force_off_pattern();
			}
			
			if( g_str_ble_status.bt_conn_status == BT_DISC_DISC )
			{
				;
			}
			else if( g_str_ble_status.bt_conn_status == BT_DISC_CONN )
			{
				call_bt_conn_success();
				g_str_ble_status.bt_pairing_status = BT_PAIRING_SUCCESS;
			}
			else if( g_str_ble_status.bt_conn_status == BT_CONN_DISC )
			{
				call_bt_disconn();
				g_str_ble_status.bt_pairing_status = BT_PAIRING_FAIL;
			}
			else if( g_str_ble_status.bt_conn_status == BT_CONN_CONN )
			{
				;
			}
			break;

		case CMD_BATTERY_STATUS:
#ifdef HW_PP4
			battery_shortage_count = ui8_data[5];
			g_str_ble_status.haptic_status = (haptic_status_e)ui8_data[6];

			g_battery_percentage = g_str_batt.ui8_percent;
			check_charger_status();
			data2[0] = g_battery_percentage;
			data2[1] = g_str_ble_status.charger_status;
			ble_uart_send_packet(CMD_BATTERY_STATUS, data2, 2);
#else
			g_battery_percentage = ui8_data[4];
			battery_shortage_count = ui8_data[5];
			g_str_ble_status.haptic_status = (haptic_status_e)ui8_data[6];
#endif
			if(g_battery_percentage >= 80)		g_str_batt.ui8_batt_led_level = 4;
			else if(g_battery_percentage >= 60)	g_str_batt.ui8_batt_led_level = 3;
			else if(g_battery_percentage >= 40)	g_str_batt.ui8_batt_led_level = 2;
			else if(g_battery_percentage >= 20)	g_str_batt.ui8_batt_led_level = 1;
			else					g_str_batt.ui8_batt_led_level = 0;
			
			force_off_pattern();
			if(g_str_ble_status.bt_pairing_status == BT_PAIRING_START)
			{
				g_str_timer.ui16_pairing_cnt = 0;
				g_str_ble_status.bt_pairing_status = BT_PAIRING_TIMEOUT;
			}
			
			if( g_battery_percentage == 100 )
			{
				g_str_batt.ui16_batt_status = BATT_STATUS_FULL;
				call_batt_full();
			}
			else
			{
				if(g_battery_percentage > 20)
				{
					g_str_batt.ui16_batt_status = BATT_STATUS_NORMAL;
					call_batt_gauge();
				}
				else// 0~20% battery shortage alarm
				{
					g_str_batt.ui16_batt_status = BATT_STATUS_LOW;
					if(battery_shortage_count == 0xF)
					{
						call_batt_gauge();
						break;
					}
					
					if(g_battery_percentage > 10)// 10~20%
					{
						if( (battery_shortage_count == 1) )
						{
							call_batt_low();
						}
					}
					else if(g_battery_percentage > 5)// 5~10%
					{
						if( (battery_shortage_count == 2) )
						{
							call_batt_low();
						}
					}
					else // 0~5%
					{
						if( (battery_shortage_count == 3) )
						{
							call_batt_low();
						}
					}
				}
			}
			
#if 0
			if( g_str_ble_status.bt_pairing_status == BT_PAIRING_START )
			{
				call_wait_pairing();
			}			
#endif
			break;
		
#ifdef HW_PP4
		case CMD_BATTERY_TIMER_STATUS:
			check_battery_status();
			g_battery_percentage = g_str_batt.ui8_percent;
			if(g_battery_percentage >= 80)		g_str_batt.ui8_batt_led_level = 4;
			else if(g_battery_percentage >= 60)	g_str_batt.ui8_batt_led_level = 3;
			else if(g_battery_percentage >= 40)	g_str_batt.ui8_batt_led_level = 2;
			else if(g_battery_percentage >= 20)	g_str_batt.ui8_batt_led_level = 1;
			else					g_str_batt.ui8_batt_led_level = 0;

			check_charger_status();
			data2[0] = g_battery_percentage;
			data2[1] = g_str_ble_status.charger_status;
			ble_uart_send_packet(CMD_BATTERY_STATUS, data2, 2);
			break;
#endif
			
		case CMD_CALL_FREQ_STATUS:
#ifdef ENABLE_AUDIO				
			g_str_audio.ui16_audio_type = (!ui8_data[4])? AUDIO_FREQ_8K : AUDIO_FREQ_16K;
			g_str_ble_status.amp_status = AMP_ENABLE;
			g_str_ble_status.audio_type_status = AUDIO_HFP;
#ifdef ENABLE_HY_FE
			fe_init();
#endif
			audio_input_freq_t ui16_audio_type_temp;
			volume_level_status_e ui16_vol_level_temp;
			
			ui16_audio_type_temp = g_str_audio.ui16_audio_type;
			ui16_vol_level_temp = g_str_ble_status.volume_level_status;
			memset((audio_struct_t *)&g_str_audio, 0x00, sizeof(audio_struct_t));
			g_str_audio.ui16_audio_type = ui16_audio_type_temp;
			g_str_ble_status.volume_level_status = ui16_vol_level_temp;

			audio_rx_stop();
			audio_init(g_str_audio.ui16_audio_type);
#endif
			break;

		case CMD_BT_PAIRING_STATUS:
			g_str_ble_status.bt_pairing_status = (bt_pairing_status_e)ui8_data[4];
			if( g_str_ble_status.bt_pairing_status == BT_PAIRING_START )
			{
				g_str_timer.ui16_pairing_cnt = 0;
				call_wait_pairing();
			}
			else if( g_str_ble_status.bt_pairing_status == BT_PAIRING_TIMEOUT )
			{
				force_off_pattern();
			}
			else if( g_str_ble_status.bt_pairing_status == BT_PAIRING_SUCCESS )
			{
				force_off_pattern();
				//call_ble_conn_success();
			}
			else if( g_str_ble_status.bt_pairing_status == BT_PAIRING_FAIL )
			{
				force_off_pattern();
			}
			else if( g_str_ble_status.bt_pairing_status == BT_PAIRING_UNPAIRED )
			{
				call_unpaired();//????
			}
			break;
			
		case CMD_SELECT_FAVORITE_STATUS:
#if 01
			g_favorite_num[0] = ui8_data[4];//0
			g_favorite_num[1] = ui8_data[5];//1
			g_favorite_num[2] = ui8_data[6];//2
			g_favorite_num[3] = ui8_data[7];//3
			g_favorite_num[4] = ui8_data[8];//4
#else
			favorite_number = (favorite_number_e)ui8_data[4];
			favorite_number--;
#endif
			force_off_pattern();
			call_favorite_select();
			break;

		case CMD_CHARGER_STATUS:

#ifdef HW_PP4
			check_charger_status();
			if(ui8_data[7])
				check_battery_status();
				
			data2[0] = g_str_batt.ui8_percent;
			data2[1] = g_str_ble_status.charger_status;
			ble_uart_send_packet(CMD_BATTERY_STATUS, data2, 2);
#endif
			
#ifdef ENABLE_FUEL_GAUGE
			g_battery_percentage = g_str_batt.ui8_percent;
#else
			g_str_ble_status.charger_status = (charger_status_e)ui8_data[4];
			g_battery_percentage = ui8_data[5];
#endif
			g_str_ble_status.haptic_status = (haptic_status_e)ui8_data[6];
			if(g_str_bitflag.b1_charger_led_disp)
			{
				g_str_ble_status.haptic_status = HAPTIC_STATUS_OFF;
			}
			
			if(g_battery_percentage >= 80)		g_str_batt.ui8_batt_led_level = 4;
			else if(g_battery_percentage >= 60)	g_str_batt.ui8_batt_led_level = 3;
			else if(g_battery_percentage >= 40)	g_str_batt.ui8_batt_led_level = 2;
			else if(g_battery_percentage >= 20)	g_str_batt.ui8_batt_led_level = 1;
			else					g_str_batt.ui8_batt_led_level = 0;
			
			if( (g_str_ble_status.call_status == CALL_CALLING)
			   || (g_str_ble_status.call_status == CALL_INCOMING)
				   || (g_str_ble_status.call_status == CALL_OUTGOING)
					   || (g_str_ble_status.fw_upgrade_status == FW_UPGRADE_READY) )
				break;

			if( g_str_ble_status.bt_pairing_status == BT_PAIRING_START )
			{
				force_off_pattern();
				g_str_timer.ui16_pairing_cnt = 0;
				g_str_ble_status.bt_pairing_status = BT_PAIRING_TIMEOUT;
			}
			
			if( !g_str_ble_status.charger_status )
			{
				//force_off_pattern();
				g_str_bitflag.b1_charging_complete = 0;
				break;
			}
			else
			{
				if( g_battery_percentage == 100 )
				{
					if(!g_str_bitflag.b1_charging_complete)
					{
						force_off_pattern();
						g_str_batt.ui16_batt_status = BATT_STATUS_FULL;
						g_str_bitflag.b1_charging_complete = 1;
					}
					call_batt_full();
				}
				else
				{
					g_str_batt.ui16_batt_status = BATT_STATUS_IN_CHARGING;
					call_batt_charging();
				}
			}
			break;

		case CMD_VOLUME_CONTROL:
			g_str_ble_status.volume_level_status = (volume_level_status_e)ui8_data[4];
			if(g_str_ble_status.volume_level_status >= VOLUME_LEVEL_5)
				g_str_ble_status.volume_level_status = VOLUME_LEVEL_5;
			else if(g_str_ble_status.volume_level_status <= VOLUME_LEVEL_0)
				g_str_ble_status.volume_level_status = VOLUME_LEVEL_0;

			if(ui8_data[5])
				call_vol_display();
			
#ifdef APP_GAIN_CONTROL
			g_str_ble_status.app_max_volume_status = (app_max_vol_status_e)ui8_data[6];
			if(g_str_ble_status.app_max_volume_status >= APP_MAX_VOLUME_LEVEL_INIT)
				g_str_ble_status.app_max_volume_status = APP_MAX_VOLUME_LEVEL_31;
			else if(g_str_ble_status.app_max_volume_status <= APP_MAX_VOLUME_LEVEL_0)
				g_str_ble_status.app_max_volume_status = APP_MAX_VOLUME_LEVEL_1;
			
			float32_t f32_sfunc = roundf(powf((float32_t)(g_str_ble_status.app_max_volume_status+8),(float32_t)2.0)* 1000.0);//((x+8)^2)*1000.0
			g_str_audio.f32_volume_step = (f32_sfunc * (float32_t)0.001)/powf((float32_t)(APP_MAX_VOLUME_LEVEL_31+8),(float32_t)2.0);
#endif
			break;

		case CMD_HFP_A2DP:
			g_str_ble_status.audio_type_status = (audio_type_status_e)ui8_data[4];
#ifdef ENABLE_AUDIO
			if(g_str_ble_status.audio_type_status == AUDIO_HFP)
			{
				audio_rx_stop();
			}
			else
			{
				g_str_ble_status.amp_status = AMP_ENABLE;
				g_str_audio.ui16_audio_type = AUDIO_FREQ_44_1K;
				audio_init(g_str_audio.ui16_audio_type);
				audio_rx_start();
			}
#endif
			
			break;
			
		case CMD_DND_STATUS://real dnd
			force_off_pattern();
			g_str_ble_status.haptic_status = (haptic_status_e)(ui8_data[4] ^ 1);
			if( g_str_ble_status.haptic_status )
			{
				call_dnd_off();
			}
			else
			{
				call_dnd_on();
			}
			if( g_str_ble_status.bt_pairing_status == BT_PAIRING_START )
			{
				call_wait_pairing();
			}			
			break;

		case CMD_AMP_STATUS://sco link conn/disconn
			if( (g_str_ble_status.call_status & CALL_CALLING) || (g_str_ble_status.call_status & CALL_OUTGOING) )
			{
				g_str_ble_status.amp_status = (amp_status_e)ui8_data[4];
				if( g_str_ble_status.amp_status )//on
				{
					if(real_call_status)
					{
#ifdef ENABLE_AUDIO
						audio_input_freq_t ui16_audio_type_temp;
						volume_level_status_e ui16_vol_level_temp;
						
#ifdef ENABLE_HY_FE
						fe_init();
#endif
						ui16_audio_type_temp = g_str_audio.ui16_audio_type;
						ui16_vol_level_temp = g_str_ble_status.volume_level_status;
						memset((audio_struct_t *)&g_str_audio, 0x00, sizeof(audio_struct_t));
						g_str_audio.ui16_audio_type = ui16_audio_type_temp;
#ifdef APP_GAIN_CONTROL
						g_str_audio.f32_volume_step = 1.0;
#endif
						g_str_ble_status.volume_level_status = ui16_vol_level_temp;
						audio_rx_stop();
						audio_init(g_str_audio.ui16_audio_type);
						audio_rx_start();
#endif
					}
					else
					{
						audio_rx_stop();
						AMP_STATUS_DISABLE;
					}
				}
				else//off
				{
					audio_rx_stop();
					AMP_STATUS_DISABLE;
					force_off_pattern();
				}
			}
			break;
			
		case CMD_MCU_SLEEP_STATUS:
			if(ui8_data[4] == 0x00)
			{
			}
			else
			{
				g_str_bitflag.b1_forced_sleep = ON;
			}
			break;
			
		case CMD_FW_UPDATE_STATUS:
			g_str_ble_status.fw_upgrade_status = (fw_upgrade_status_e)ui8_data[4];
			if(g_str_ble_status.fw_upgrade_status == FW_UPGRADE_READY)
			{
				g_str_timer.ui16_sleep_cnt = 0;

				data = FW_UPGRADE_READY;//fw update ready
				ble_uart_send_packet(CMD_FW_UPDATE_STATUS, &data, 1);
				
				force_off_pattern();
				call_fw_upgrade();
			}
			else
			{
				data = FW_UPGRADE_START;//fw update start
				ble_uart_send_packet(CMD_FW_UPDATE_STATUS, &data, 1);
				delay_10ms(1);
				NVIC_SystemReset();
			}
			break;
			
		case CMD_FACTORY_RESET:
			force_off_pattern();
			g_str_ble_status.haptic_status = HAPTIC_STATUS_ON;
			call_device_reset();
			break;
			
		case CMD_EXHIBITION_MODE:
			{
				if(!ui8_data[4])
				{
					g_str_bitflag.b1_exhibition_mode = 0;
					g_str_ble_status.volume_level_status = VOLUME_LEVEL_0;
					call_exhibition_mode_off();
				}
				else
				{
					g_str_bitflag.b1_exhibition_mode = 1;
					g_str_ble_status.volume_level_status = VOLUME_LEVEL_5;
					call_exhibition_mode_on();
				}
			}
			break;

		case CMD_SOUND_CHECK:
			if(ui8_data[4] == 0x01)
			{
				//haptic_on();
				g_str_bitflag.b1_beep_on_off ^= 1;
				if(g_str_bitflag.b1_beep_on_off)//beep on
				{
					audio_init(AUDIO_FREQ_8K);
					I2S_Cmd(AUDIO_I2S_TX, ENABLE);
					AMP_STATUS_ENABLE;
				}
				else//beep off
				{
					I2S_Cmd(AUDIO_I2S_TX, DISABLE);
					AMP_STATUS_DISABLE;
				}

				memset(&g_str_audio, 0x00, sizeof(g_str_audio));
			}
			else
			{
				//haptic_off();
				g_str_bitflag.b1_mic_on_off ^= 1;
				if(g_str_bitflag.b1_mic_on_off)//mic on
				{
					audio_init(AUDIO_FREQ_8K);
					audio_rx_start();
				}
				else//mic off
				{
					audio_rx_stop();
				}

				memset(&g_str_audio, 0x00, sizeof(g_str_audio));
			}
			break;
		
		case CMD_CGEQ_STATUS:
			g_str_bitflag.b1_cgeq_enable = ui8_data[4];
			break;
#ifdef SMT_CHECK
		case CMD_PEDO_STATUS_CHECK:
			if(ui8_data[4] == 0xE0)//pedo check
			{
				g_str_ble_status.pedo_fuel_check = 0x00;
			}
			else
			{
				g_str_ble_status.pedo_fuel_check |= 0x01;	//  error
			}
			
			delay_ms(100);
			fuel_gauge_communication(IDENTIFY);
			if( g_str_fuel_gauge.ui16_firmware_version >= 0x0702 )//fuel check
			{
				g_str_ble_status.pedo_fuel_check |= 0x00;
			}
			else
			{
				g_str_ble_status.pedo_fuel_check |= 0x02;	// error
			}
			if(g_str_ble_status.pedo_fuel_check != 0x00)
			{
				call_pedo_fuel_check();
			}

			break;
			
		case CMD_PEDO_COUNT_CHECK:
			g_str_ble_status.pedo_count_check = ui8_data[4];
			if(g_str_ble_status.pedo_count_check >= 30)//pedo check
			{
				set_led_pwm(1, 100);
				set_led_pwm(2, 100);
				set_led_pwm(3, 100);
			}
			else if(g_str_ble_status.pedo_count_check >= 20)//pedo check
			{
				set_led_pwm(1, 100);
				set_led_pwm(2, 100);
				set_led_pwm(3, 0);
			}
			else if(g_str_ble_status.pedo_count_check >= 10)//pedo check
			{
				set_led_pwm(1, 100);
				set_led_pwm(2, 0);
				set_led_pwm(3, 0);
			}
			break;
#endif

		default:
			break;	
	}
}

#endif //ENABLE_BLE_UART
