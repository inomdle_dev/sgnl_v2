#include "global_define.h"

#ifdef ENABLE_BOOTLOADER_FLASH
/* Private functions ---------------------------------------------------------*/
void user_flash_erase()
{
	/* Unlock the Flash *********************************************************/
	/* Enable the flash control register access */
	FLASH_Unlock();

	/* Erase the user Flash area ************************************************/
	/* area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR */

	/* Clear pending flags (if any) */
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
			FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

	if (FLASH_EraseSector(STATUS_FLASH_SECTOR, VoltageRange_3) != FLASH_COMPLETE)
	{
		/* Error occurred while sector erase.
		User can add here some code to deal with this error  */
#ifdef ENABLE_DBG
		dbg_print("FLASH_EraseSector error!!\n");
#endif
	}

	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) */
	FLASH_Lock();
}

void user_flash_write(uint32_t variable)
{
	uint32_t uwAddress  = STATUS_FLASH_ADDR;

	/* Unlock the Flash *********************************************************/
	/* Enable the flash control register access */
	FLASH_Unlock();
        
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);
	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) */
	if (FLASH_ProgramWord(uwAddress, variable) != FLASH_COMPLETE)
	{
		//fail
	}
    
	FLASH_Lock();
}

void user_flash_read(uint32_t *variable)
{
	uint32_t uwAddress = STATUS_FLASH_ADDR;

	/* Check if the programmed data is OK ***************************************/
	/*  MemoryProgramStatus = 0: data programmed correctly
	MemoryProgramStatus != 0: number of words not programmed correctly */

	*variable = *(__IO uint32_t*)uwAddress;
}

void jump_addr_write( uint32_t variable )
{
	user_flash_erase();
	user_flash_write(variable);
}

/**
* @brief  Returns the write protection status of user flash area.
* @param  None
* @retval 0: Some sectors inside the user flash area are write protected
*         1: No write protected sectors inside the user flash area
*/
uint16_t FLASH_If_GetWriteProtectionStatus(uint32_t addr)
{
	uint32_t UserStartSector = 0;
	
	/* Get the sector where start the user flash area */
	UserStartSector = GetSector(addr);
	
	/* Check if there are write protected sectors inside the user flash area */
	if ((FLASH_OB_GetWRP() >> (UserStartSector/8)) == (0xFFF >> (UserStartSector/8)))
	{ /* No write protected sectors inside the user flash area */
		return 1;
	}
	else
	{ /* Some sectors inside the user flash area are write protected */
		return 0;
	}
}

/**
* @brief  Disables the write protection of user flash area.
* @param  None
* @retval 1: Write Protection successfully disabled
*         2: Error: Flash write unprotection failed
*/
uint32_t FLASH_If_DisableWriteProtection(uint32_t addr)
{
	__IO uint32_t UserStartSector = 0, UserWrpSectors = 0;

	/* Get the sector where start the user flash area */
	UserStartSector = GetSector(addr);
	
	switch(UserStartSector)
	{
		case FLASH_Sector_0:	UserWrpSectors = OB_WRP_Sector_0;	break;
		case FLASH_Sector_1:	UserWrpSectors = OB_WRP_Sector_1;	break;
		case FLASH_Sector_2:	UserWrpSectors = OB_WRP_Sector_2;	break;
		case FLASH_Sector_3:	UserWrpSectors = OB_WRP_Sector_3;	break;
		case FLASH_Sector_4:	UserWrpSectors = OB_WRP_Sector_4;	break;
		case FLASH_Sector_5:	UserWrpSectors = OB_WRP_Sector_5;	break;
		case FLASH_Sector_6:	UserWrpSectors = OB_WRP_Sector_6;	break;
		case FLASH_Sector_7:	UserWrpSectors = OB_WRP_Sector_7;	break;
		case FLASH_Sector_8:	UserWrpSectors = OB_WRP_Sector_8;	break;
		case FLASH_Sector_9:	UserWrpSectors = OB_WRP_Sector_9;	break;
		case FLASH_Sector_10:	UserWrpSectors = OB_WRP_Sector_10;	break;
		case FLASH_Sector_11:	UserWrpSectors = OB_WRP_Sector_11;	break;
		case FLASH_Sector_12:	UserWrpSectors = OB_WRP_Sector_12;	break;
		case FLASH_Sector_13:	UserWrpSectors = OB_WRP_Sector_13;	break;
		case FLASH_Sector_14:	UserWrpSectors = OB_WRP_Sector_14;	break;
		case FLASH_Sector_15:	UserWrpSectors = OB_WRP_Sector_15;	break;
		case FLASH_Sector_16:	UserWrpSectors = OB_WRP_Sector_16;	break;
		case FLASH_Sector_17:	UserWrpSectors = OB_WRP_Sector_17;	break;
		case FLASH_Sector_18:	UserWrpSectors = OB_WRP_Sector_18;	break;
		case FLASH_Sector_19:	UserWrpSectors = OB_WRP_Sector_19;	break;
		case FLASH_Sector_20:	UserWrpSectors = OB_WRP_Sector_20;	break;
		case FLASH_Sector_21:	UserWrpSectors = OB_WRP_Sector_21;	break;
		case FLASH_Sector_22:	UserWrpSectors = OB_WRP_Sector_22;	break;
		case FLASH_Sector_23:	UserWrpSectors = OB_WRP_Sector_23;	break;
	}
	
	/* Mark all sectors inside the user flash area as non protected */  
	UserWrpSectors = 0xFFF-((1 << (UserStartSector/8))-1);
	
	/* Unlock the Option Bytes */
	FLASH_OB_Unlock();
	
	/* Disable the write protection for all sectors inside the user flash area */
	FLASH_OB_WRPConfig(UserWrpSectors, DISABLE);
	
	/* Start the Option Bytes programming process. */  
	if (FLASH_OB_Launch() != FLASH_COMPLETE)
	{
		/* Error: Flash write unprotection failed */
		return (2);
	}
	
	/* Lock the Option Bytes */
	FLASH_OB_Lock();
	
	/* Write Protection successfully disabled */
	return (1);
}

/**
* @brief  Unlocks Flash for write access
* @param  None
* @retval None
*/
void FLASH_If_Init(void)
{ 
	FLASH_Unlock(); 
	
	/* Clear pending flags (if any) */  
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
			FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
}

uint32_t FLASH_If_SectorErase(uint32_t addr)
{
	uint32_t ChoosingSector = GetSector(addr);
	
	FLASH_If_Init();

	/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
	be done by word */ 
	if (FLASH_EraseSector(ChoosingSector, VoltageRange_3) != FLASH_COMPLETE)
	{
		/* Error occurred while page erase */
		return (1);
	}

	if( !FLASH_If_GetWriteProtectionStatus(addr) )
		FLASH_If_DisableWriteProtection(addr);
	
	FLASH_Lock();
	
	return (0);
}

void flash_read_32(uint32_t st_addr, uint32_t *variable, uint32_t interval, uint32_t size)
{
	uint32_t uwAddress = 0;
	uint32_t start_addr = st_addr;
	uint32_t var_size = size;
	int i = 0;
	int number = (var_size / sizeof(uint32_t));

	uwAddress = start_addr + (interval * var_size);

	/* Check if the programmed data is OK ***************************************/
	/*  MemoryProgramStatus = 0: data programmed correctly
	MemoryProgramStatus != 0: number of words not programmed correctly */

	for (i = 0; i<number; i++)
	{
		variable[i] = *(__IO uint32_t*)uwAddress;
		uwAddress += sizeof(uint32_t);
	}
}

void flash_write_32(uint32_t st_addr, uint32_t *variable, uint32_t interval, uint32_t size)
{
	uint32_t uwAddress = 0;
	uint32_t start_addr = st_addr;
	uint32_t var_size = size;
	int i = 0;
	int number = (var_size / sizeof(uint32_t));
        //__IO uint32_t SectorsWRPStatus;

	uwAddress = start_addr + (interval * var_size);

	/* Unlock the Flash *********************************************************/
	/* Enable the flash control register access */
	FLASH_Unlock();
        
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);
	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) */
        for (i = 0; i<number; i++)
        {
                if (FLASH_ProgramWord(uwAddress, variable[i]) == FLASH_COMPLETE)
                        uwAddress += sizeof(uint32_t);
                else;
        }

	FLASH_Lock();
}

/**
* @brief  Gets the sector of a given address
* @param  Address: Flash address
* @retval The sector of a given address
*/
static uint32_t GetSector(uint32_t Address)
{
	uint32_t sector = 0;
	
	if((Address < ADDR_FLASH_SECTOR_1) && (Address >= ADDR_FLASH_SECTOR_0))			sector = FLASH_Sector_0;  
	else if((Address < ADDR_FLASH_SECTOR_2) && (Address >= ADDR_FLASH_SECTOR_1))		sector = FLASH_Sector_1;  
	else if((Address < ADDR_FLASH_SECTOR_3) && (Address >= ADDR_FLASH_SECTOR_2))		sector = FLASH_Sector_2;  
	else if((Address < ADDR_FLASH_SECTOR_4) && (Address >= ADDR_FLASH_SECTOR_3))		sector = FLASH_Sector_3;  
	else if((Address < ADDR_FLASH_SECTOR_5) && (Address >= ADDR_FLASH_SECTOR_4))		sector = FLASH_Sector_4;  
	else if((Address < ADDR_FLASH_SECTOR_6) && (Address >= ADDR_FLASH_SECTOR_5))		sector = FLASH_Sector_5;  
	else if((Address < ADDR_FLASH_SECTOR_7) && (Address >= ADDR_FLASH_SECTOR_6))		sector = FLASH_Sector_6;  
	else/*(Address < FLASH_END_ADDR) && (Address >= ADDR_FLASH_SECTOR_11))*/		sector = FLASH_Sector_7;  
	return sector;
}

#endif
