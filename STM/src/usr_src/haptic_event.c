#include "global_define.h"

#ifdef ENABLE_HAPTIC

#ifdef ENABLE_HAPTIC_PWM
static const GPIO_InitTypeDef HAPTIC_GpioConfiguration  =  {HAPTIC_GPIO_PIN, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_DOWN};
#else
static const GPIO_InitTypeDef HAPTIC_GpioConfiguration  =  {GPIO_HAPTIC_MOTOR_PIN, GPIO_Mode_OUT,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_DOWN};
#endif


void haptic_init( void )
{
	haptic_config_gpio();
	
	haptic_off();
}

void haptic_config_gpio( void )
{
	/* GPIOA clock enable */
#ifdef ENABLE_HAPTIC_PWM
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	
	RCC_AHB1PeriphClockCmd(HAPTIC_GPIO_RCC, ENABLE);

	GPIO_PinAFConfig(HAPTIC_GPIO_PORT, HAPTIC_PWM_GPIO_SOURCE, GPIO_AF_TIM4);
	GPIO_Init(HAPTIC_GPIO_PORT, (GPIO_InitTypeDef *)&HAPTIC_GpioConfiguration );
	
	/* TIM2 clock enable */
	RCC_APB1PeriphClockCmd(HAPTIC_PWM_GPIO_RCC, ENABLE);
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 100-1;
	TIM_TimeBaseStructure.TIM_Prescaler = 25-1;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(HAPTIC_TIM, &TIM_TimeBaseStructure);
	
		/* PWM Mode configuration: Channel2 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_Pulse = 100;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;//The output phase, TIM_OCNPolarity_High output from the inverting
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	TIM_OC2Init(HAPTIC_TIM, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(HAPTIC_TIM, TIM_OCPreload_Enable);

	TIM_Cmd(HAPTIC_TIM, ENABLE);
	TIM_CtrlPWMOutputs(HAPTIC_TIM, ENABLE);
	
	haptic_off();
#else
	RCC_AHB1PeriphClockCmd(GPIO_HAPTIC_MOTOR_RCC, ENABLE);
	GPIO_Init(GPIO_HAPTIC_MOTOR_PORT, (GPIO_InitTypeDef *)&HAPTIC_GpioConfiguration );
#endif
}

void haptic_on( void )
{
	if(g_str_ble_status.call_status != CALL_DND)//when once push down btn
	{
		if( g_str_ble_status.haptic_status )
		{
#ifdef ENABLE_HAPTIC_PWM
	#if defined(HW_PP3) | defined(HW_PP4)
			HAPTIC_CCR = 75;
	#endif
#else
			HAPTIC_ACTIVE;
#endif
		}
	}
}

void haptic_off( void )
{
#ifdef ENABLE_HAPTIC_PWM
	HAPTIC_CCR = 0;
#else
	HAPTIC_IDLE;
#endif
	
}

#endif //ENABLE_HAPTIC