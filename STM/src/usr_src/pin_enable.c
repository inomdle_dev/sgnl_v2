#include "global_define.h"

void pin_enable_init(void)
{
	pin_enable_gpio();
}

void pin_enable_gpio(void)
{
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	GPIO_InitTypeDef power_gpio_config = {GPIO_POWER_HOLD_PIN, GPIO_Mode_OUT, GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_DOWN};

	RCC_AHB1PeriphClockCmd(GPIO_POWER_HOLD_RCC, ENABLE);
	GPIO_Init(GPIO_POWER_HOLD_PORT, &power_gpio_config);

#ifdef ENABLE_STOPMODE
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	SYSCFG_EXTILineConfig(GPIO_WAKEUP_EXTI_PORT, GPIO_WAKEUP_EXTI_PIN_SOURCE);
	
	/* PA0 is connected to EXTI_Line0 */
	EXTI_InitStructure.EXTI_Line = GPIO_WAKEUP_EXTI_LINE;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;//EXTI_Trigger_Rising_Falling;//EXTI_Trigger_Rising;
	EXTI_Init(&EXTI_InitStructure);
	
	/* EXTI0 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = GPIO_WAKEUP_EXTI_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif

#if defined(HW_PP3) | defined(HW_PP4)
	/* Enable clock for GPIOA */
	RCC_AHB1PeriphClockCmd(GPIO_CHARGER_RCC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	GPIO_InitTypeDef charger_gpio_config = {GPIO_CHARGER_PIN, GPIO_Mode_IN, GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
	GPIO_Init(GPIO_CHARGER_PORT, &charger_gpio_config);
	
	SYSCFG_EXTILineConfig(GPIO_CHARGER_EXTI_PORT, GPIO_CHARGER_EXTI_PIN_SOURCE);

	/* PA11 is connected to EXTI_Line11 */
	EXTI_InitStructure.EXTI_Line = GPIO_CHARGER_EXTI_LINE;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	EXTI_Init(&EXTI_InitStructure);
	
	/* EXTI15~10 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = GPIO_CHARGER_EXTI_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);	
	
	GPIO_InitTypeDef ble_wakeup_gpio_config = {GPIO_BLE_WAKEUP_PIN, GPIO_Mode_OUT, GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_DOWN};
	RCC_AHB1PeriphClockCmd(GPIO_BLE_WAKEUP_RCC, ENABLE);
	GPIO_Init(GPIO_BLE_WAKEUP_PORT, &ble_wakeup_gpio_config);
	
#ifdef HW_PP4
	GPIO_InitTypeDef pedo_gpio_config = {GPIO_PEDO_STATUS_PIN, GPIO_Mode_OUT, GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
	RCC_AHB1PeriphClockCmd(GPIO_PEDO_STATUS_RCC, ENABLE);
	GPIO_Init(GPIO_PEDO_STATUS_PORT, &pedo_gpio_config);
	PEDO_STATUS_ENABLE;

	//if fuel gauge entered shutdown mode, enter active mode by rising edge from SCL
	GPIO_InitTypeDef fg_scl_gpio_config = {GPIO_Pin_8, GPIO_Mode_OUT, GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	GPIO_Init(GPIOB, &fg_scl_gpio_config);
	GPIO_WriteBit(GPIOB, GPIO_Pin_8, Bit_RESET);//off
	GPIO_WriteBit(GPIOB, GPIO_Pin_8, Bit_SET);//on
#endif
#endif

#ifdef ENABLE_AUDIO
	GPIO_InitTypeDef amp_gpio_config = {GPIO_AMP_STATUS_PIN, GPIO_Mode_OUT, GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
	RCC_AHB1PeriphClockCmd(GPIO_AMP_STATUS_RCC, ENABLE);
	GPIO_Init(GPIO_AMP_STATUS_PORT, &amp_gpio_config);
	AMP_STATUS_DISABLE;

#ifdef HW_PP4
	GPIO_InitTypeDef dac_gpio_config = {GPIO_DAC_STATUS_PIN, GPIO_Mode_OUT, GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
	RCC_AHB1PeriphClockCmd(GPIO_DAC_STATUS_RCC, ENABLE);
	GPIO_Init(GPIO_DAC_STATUS_PORT, &dac_gpio_config);
	DAC_STATUS_ENABLE;
#endif
#endif	//ENABLE_AUDIO
}

void pin_disable_all_gpio(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Configure all GPIO port pins in Analog Input mode (floating input trigger OFF) */
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;

	GPIO_InitStructure.GPIO_Pin = ~(GPIO_WAKEUP_PIN | LED1_GPIO_PIN | LED2_GPIO_PIN | LED3_GPIO_PIN | GPIO_POWER_HOLD_PIN
#if defined(HW_PP3) | defined(HW_PP4)
					| GPIO_CHARGER_PIN | GPIO_DAC_STATUS_PIN | GPIO_PEDO_STATUS_PIN
#endif
#ifndef ENABLE_MASS_PRODUCT
					| GPIO_Pin_13 | GPIO_Pin_14
#endif
					);
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = ~( LED4_GPIO_PIN | LED5_GPIO_PIN | GPIO_AMP_STATUS_PIN
#if defined(HW_PP4)
					| GPIO_BLE_WAKEUP_PIN | GPIO_HAPTIC_MOTOR_PIN
#elif defined(HW_PP3)
					| GPIO_BLE_WAKEUP_PIN | GPIO_HAPTIC_MOTOR_PIN
#else //HW_PP2
#endif
					);
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}

#if defined(HW_PP3) | defined(HW_PP4)
void check_charger_status(void)
{
	g_str_ble_status.charger_status = 
		(!GPIO_ReadInputDataBit(GPIO_CHARGER_PORT, GPIO_CHARGER_PIN))? CHARGER_ATTACHED : CHARGER_DETACHED;
}

void charger_detect_isr(void)
{
	if(EXTI_GetITStatus(EXTI_Line10) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line10);
	}
	else if(EXTI_GetITStatus(EXTI_Line11) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line11);
	}
	else if(EXTI_GetITStatus(GPIO_CHARGER_EXTI_LINE) != RESET)
	{
		EXTI_ClearITPendingBit(GPIO_CHARGER_EXTI_LINE);
		g_str_bitflag.b1_charger_wakeup_flag = ON;
		check_charger_status();
#ifdef HW_PP4
		if(g_str_timer.ui16_sleep_cnt >= (SLEEP_COUNT + 50))
		{
			uint8_t data = 0;
			data = (uint8_t)MCU_WAKEUP;
			ble_uart_send_packet(CMD_MCU_STATUS, &data, 1);
		}
#endif
		g_str_bitflag.b1_mcu_sleep_status = 0;
		g_str_timer.ui16_sleep_cnt = 0;
	}
	else if(EXTI_GetITStatus(EXTI_Line13) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line13);
	}
	else if(EXTI_GetITStatus(EXTI_Line14) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line14);
	}
	else if(EXTI_GetITStatus(EXTI_Line15) != RESET)
	{
		EXTI_ClearITPendingBit(EXTI_Line15);
	}
}
#endif