#include "global_define.h"

bitflag_struct_t g_str_bitflag;

#ifdef ENABLE_BLE_UART
bt_uart_struct_t g_str_ble;
#endif //ENABLE_BLE_UART

#ifdef ENABLE_DBG
dbg_uart_struct_t g_str_dbg;
#endif //ENABLE_DBG

#ifdef ENABLE_AUDIO
audio_struct_t g_str_audio;
#endif

batt_struct_t g_str_batt;

#ifdef ENABLE_FUEL_GAUGE
fg_batt_status_struct_t g_str_fuel_gauge_batt;
fg_status_sturct_t g_str_fuel_gauge;
#endif

timer_struct_t g_str_timer;

#ifdef ENABLE_AUDIO
#ifdef ENABLE_HY_FE
fe_struct_t g_str_fe;
#endif
#endif

#ifdef ENABLE_PWM
pwm_struct_t g_str_led[LED_NUM];
pattern_type_e g_pattern_type = PATTERN_TYPE_NONE;
#endif


ble_status_t g_str_ble_status;

volatile uint8_t g_battery_percentage = 0;
volatile uint8_t g_favorite_num[LED_NUM] = {0,};

uint32_t g_ui32_pattern_check = 0;

void peripheral_init(void)
{
	pin_enable_init();
	
#ifdef ENABLE_MONITOR
	timer_init();
#endif

#ifdef HW_PP4
  	SysTick_Config( SystemCoreClock / 1000 );
	i2c_init();
#endif

#ifdef ENABLE_BLE_UART
	ble_uart_init();
#endif

#ifdef ENABLE_DBG
	dbg_init();
#endif
	
#ifdef ENABLE_PWM
	led_init();
#endif

#ifdef ENABLE_HAPTIC
	haptic_init();
#endif
	
#ifdef ENABLE_AUDIO
	audio_init(CALLING_FREQ);
	g_str_audio.ui16_audio_type = CALLING_FREQ;
#endif
}

void user_data_init(void)
{
	memset(&g_str_bitflag, 0, sizeof(bitflag_struct_t));
#ifdef ENABLE_HY_FE
	g_str_bitflag.b1_fft_enable = 1;
	g_str_bitflag.b1_fe_enable = 1;
	g_str_bitflag.b1_cgeq_enable = 1;
#endif
	
#ifdef ENABLE_BLE_UART
	memset(&g_str_ble, 0, sizeof(bt_uart_struct_t));
	g_str_ble.ui16_tx_cnt = UART_BUFFERSIZE;
#endif	
	
#ifdef ENABLE_AUDIO
	memset(&g_str_audio, 0, sizeof(audio_struct_t));
	g_str_audio.ui16_audio_type = CALLING_FREQ;
#ifdef APP_GAIN_CONTROL
	g_str_audio.f32_volume_step = 1.0;
#endif
	
#endif //ENABLE_AUDIO
	memset(&g_str_timer, 0, sizeof(timer_struct_t));
		
#ifdef ENABLE_BOOTLOADER_FLASH
	uint32_t start_Addr;
	user_flash_read(&start_Addr);
	
	if( start_Addr != FLASH_ADDR_CURRENT_APP )
	{
		jump_addr_write(FLASH_ADDR_CURRENT_APP);
	}
#endif
	
#ifdef HW_PP4
	memset(&g_str_batt, 0, sizeof(batt_struct_t));
	memset(&g_str_fuel_gauge_batt, 0, sizeof(fg_batt_status_struct_t));
	memset(&g_str_fuel_gauge, 0, sizeof(fg_status_sturct_t));
#endif
	
	g_str_ble_status.cyw20706_cmd_status = CYW20706_REPORT_INIT;
	g_str_ble_status.mcu_status = MCU_INIT;
	g_str_ble_status.call_status = CALL_INIT;
	g_str_ble_status.power_status = POWER_INIT;
	g_str_ble_status.ble_conn_status = BLE_CONNECTION_INIT;
	g_str_ble_status.bt_conn_status = BT_CONNECTION_INIT;
	g_str_ble_status.bt_pairing_status = BT_PAIRING_INIT;
	g_str_ble_status.favorite_number = FAVORITE_INIT;
	g_str_ble_status.charger_status = CHARGER_STATUS_INIT;
	g_str_ble_status.dnd_status = DND_STATUS_INIT;
	g_str_ble_status.amp_status = AMP_STATUS_INIT;
	g_str_ble_status.haptic_status = HAPTIC_STATUS_INIT;
	g_str_ble_status.fw_upgrade_status = FW_UPGRADE_STATUS_INIT;
	g_str_ble_status.volume_level_status = VOLUME_LEVEL_5;
	g_str_ble_status.audio_type_status = AUDIO_TYPE_INIT;
	g_str_ble_status.app_max_volume_status = APP_MAX_VOLUME_LEVEL_31;
}

void reset_supervisor(void)
{
	if(RCC_GetFlagStatus(RCC_FLAG_PORRST))//power on reset
	{
#ifdef ENABLE_DBG
		dbg_print("RCC_FLAG_PORRST\n");
#endif
		RCC_ClearFlag();
	}
	else if(RCC_GetFlagStatus(RCC_FLAG_IWDGRST))
	{
#ifdef ENABLE_DBG
		dbg_print("RCC_FLAG_IWDGRST\n");
#endif
		RCC_ClearFlag();
	}
	else if(RCC_GetFlagStatus(RCC_FLAG_WWDGRST))
	{
#ifdef ENABLE_DBG
		dbg_print("RCC_FLAG_WWDGRST\n");
#endif
		RCC_ClearFlag();
	}
	else if(RCC_GetFlagStatus(RCC_FLAG_LPWRRST))
	{
#ifdef ENABLE_DBG
		dbg_print("RCC_FLAG_LPWRRST\n");
#endif
		RCC_ClearFlag();
	}
	else if(RCC_GetFlagStatus(RCC_FLAG_PINRST))//When hardware reset is occured, enter power off sequence.
	{
#ifdef ENABLE_DBG
		dbg_print("RCC_FLAG_PINRST\n");
#endif
		POWER_HOLD_ENABLE;
		force_off_pattern();
		call_power_off();
		RCC_ClearFlag();
		while(1)
		{
			if(g_str_bitflag.b1_ble_send_flag)
			{
				POWER_HOLD_DISABLE;
			}
		}
	}
}

int main( void )
{
#if 01
	uint32_t ota_jump_addr = 0;
	uint8_t data[3];

TOP:

	//peripheral configuration initialize
	peripheral_init();

	//user data variable 
	user_data_init();
	
	//ota complete
	ota_jump_addr = *(__IO uint32_t*) (ADDR_FLASH_SECTOR_1+4);
	if(ota_jump_addr == FLASH_ADDR_BACKUP_APP)//ota success
	{
		uint32_t target_addr = 0;
		uint32_t write_data = 0;
		FLASH_If_SectorErase(ADDR_FLASH_SECTOR_1);//erase ota address
		
		target_addr = ADDR_FLASH_SECTOR_1;
		if (((*(__IO uint32_t*)FLASH_ADDR_START_APP1) & 0x2FFE0000 ) == 0x20000000)
			write_data = FLASH_ADDR_START_APP1;
		
		flash_write_32(target_addr, &write_data, 0, sizeof(write_data));
		if( !FLASH_If_GetWriteProtectionStatus(ADDR_FLASH_SECTOR_1) )
			FLASH_If_DisableWriteProtection(ADDR_FLASH_SECTOR_1);

		force_off_pattern();
		call_power_off();
		RCC_ClearFlag();
		while(1)
		{
			if(g_str_bitflag.b1_ble_send_flag)
			{
				POWER_HOLD_DISABLE;
			}
		}
	}
	else if(ota_jump_addr == 0xAA55AA55)//ota fail
	{
		uint32_t target_addr = 0;
		uint32_t write_data = 0;
		FLASH_If_SectorErase(ADDR_FLASH_SECTOR_1);//erase ota address
		
		target_addr = ADDR_FLASH_SECTOR_1;
		if (((*(__IO uint32_t*)FLASH_ADDR_START_APP1) & 0x2FFE0000 ) == 0x20000000)
			write_data = FLASH_ADDR_START_APP1;
		
		flash_write_32(target_addr, &write_data, 0, sizeof(write_data));
		if( !FLASH_If_GetWriteProtectionStatus(ADDR_FLASH_SECTOR_1) )
			FLASH_If_DisableWriteProtection(ADDR_FLASH_SECTOR_1);
	}
	
#ifdef ENABLE_MASS_PRODUCT
	//when reset event is occured
	reset_supervisor();
	DBGMCU_Config(DBGMCU_STOP | DBGMCU_STANDBY, DISABLE);
#else
	DBGMCU_Config(DBGMCU_STOP | DBGMCU_STANDBY, ENABLE);
#endif

	//fuel gauge reset
	fuel_gauge_condition(SYSTEM_RESET_REQ);
	delay_ms(700);
	check_charger_status();
	data[0] = 0x0;
	data[1] = g_str_ble_status.charger_status;
#ifdef HW_PP4
	g_str_bitflag.b1_power_on_start = 1;
	check_battery_status();
	
	//battery is low state (battery boot mode case)
	if( ( g_str_batt.ui8_percent <= 2 ) && ( g_str_ble_status.charger_status == CHARGER_DETACHED ) )
	{
		delay_ms(1500);
		POWER_HOLD_ENABLE;
		for(int i=0;i<3;i++)
		{
			set_led_pwm(0, 70);
			delay_ms(150);
			set_led_pwm(0, 0);
			delay_ms(150);
		}
		while(1)
		{
			//next step must not be processed
			POWER_HOLD_DISABLE;
		}
	}
	data[2] = g_str_batt.ui8_percent;

	//send battery percentage to BT
	ble_uart_send_packet(CMD_CHARGER_STATUS, data, 3);
#else
	//send battery percentage to BT
	ble_uart_send_packet(CMD_CHARGER_STATUS, data, 2);
#endif

	while (1)
	{
#ifdef ENABLE_BLE_UART
		//if datas are receivced from BT
		if(g_str_bitflag.b2_ble_rcv_flag == 1)
		{
			BLE_CommandHandler();
			g_str_bitflag.b2_ble_rcv_flag = 0;
		}
#endif

#ifdef ENABLE_STOPMODE
#if defined(HW_PP3) | defined(HW_PP4)
		//MCU asks to enter the sleep mode to BT
		if( ( g_str_timer.ui16_sleep_cnt == SLEEP_COUNT ) && ( !g_str_bitflag.b1_mcu_sleep_status ) && ( !g_str_bitflag.b1_forced_sleep ) )
		{
			//BLE_WAKEUP_ENABLE;//wake up BT
			data[0] = 0;
			ble_uart_send_packet(CMD_MCU_SLEEP_STATUS, data, 1);
			g_str_bitflag.b1_mcu_sleep_status = 1;
		}
		
		//when charger is attached or detached
		if ( (g_str_bitflag.b1_charger_wakeup_flag) && (g_str_ble_status.power_status != POWER_OFF) )
		{
			g_str_bitflag.b1_charger_wakeup_flag = OFF;
			check_charger_status();
			if( g_str_ble_status.charger_status == CHARGER_DETACHED )
			{
				force_off_pattern();
				g_str_bitflag.b1_charging_complete = 0;
				g_str_bitflag.b1_charger_led_disp = 0;

				g_battery_percentage = g_str_batt.ui8_percent;
				data[0] = g_battery_percentage;
				data[1] = CHARGER_DETACHED;
			}
			else
			{
				g_battery_percentage = g_str_batt.ui8_percent;
				g_str_ble_status.haptic_status = HAPTIC_STATUS_ON;
				if(g_str_bitflag.b1_charger_led_disp)
				{
					g_str_ble_status.haptic_status = HAPTIC_STATUS_OFF;
				}
				
				if(g_battery_percentage >= 80)		g_str_batt.ui8_batt_led_level = 4;
				else if(g_battery_percentage >= 60)	g_str_batt.ui8_batt_led_level = 3;
				else if(g_battery_percentage >= 40)	g_str_batt.ui8_batt_led_level = 2;
				else if(g_battery_percentage >= 20)	g_str_batt.ui8_batt_led_level = 1;
				else					g_str_batt.ui8_batt_led_level = 0;
				
				if( (g_str_ble_status.call_status == CALL_CALLING)
				   || (g_str_ble_status.call_status == CALL_INCOMING)
					   || (g_str_ble_status.call_status == CALL_OUTGOING) )
				{
					;
				}
				else
				{
					if( g_str_ble_status.bt_pairing_status == BT_PAIRING_START )
					{
						force_off_pattern();
						g_str_timer.ui16_pairing_cnt = 0;
						g_str_ble_status.bt_pairing_status = BT_PAIRING_TIMEOUT;
					}
					
					if( g_battery_percentage == 100 )
					{
						if(!g_str_bitflag.b1_charging_complete)
						{
							force_off_pattern();
							g_str_batt.ui16_batt_status = BATT_STATUS_FULL;
							g_str_bitflag.b1_charging_complete = 1;
						}
						call_batt_full();
					}
					else
					{
						g_str_batt.ui16_batt_status = BATT_STATUS_IN_CHARGING;
						call_batt_charging();
					}
				}

				data[0] = g_battery_percentage;
				data[1] = CHARGER_ATTACHED;
			}
			ble_uart_send_packet(CMD_BATTERY_STATUS, data, 2);
		}
		
		//mcu enter the sleep mode
		if(g_str_bitflag.b1_forced_sleep)
		{
			g_str_timer.ui16_sleep_cnt = 0;
			g_str_bitflag.b1_mcu_sleep_status = 0;
			enter_standby_mode();
			
			/* After wakeup, call system init to enable PLL as clock source */
			SystemInit();
			
			peripheral_init();//1ms?
			
			// backup wake up status of charger pin interrupt at sleep mode
			data[0] = (g_str_bitflag.b1_charger_wakeup_flag == ON)? ON:OFF;
			data[1] = (g_str_batt.ui8_percent)? 
				g_str_batt.ui8_percent : g_str_batt.ui8_previous_percent;
			data[2] = g_str_bitflag.b1_exhibition_mode;
			
			// user data initialization
			user_data_init();
			
			if(data[0] == ON)
				g_str_bitflag.b1_charger_wakeup_flag = data[0];
			
			if(data[1])
			{
				uint8_t i = 0;
				g_str_batt.ui8_percent = data[1];
				for(i=0;i<8;i++)
					g_str_batt.ui8_buf_per[i] = g_str_batt.ui8_percent;
			}
			
			if(data[2])
				g_str_bitflag.b1_exhibition_mode = data[2];

			//a2dp audio setting after wake up
			audio_rx_stop();
			audio_init(AUDIO_FREQ_44_1K);

			check_charger_status();
			//mcu is waken up, whether charger is attached or not, so g_str_bit.b1_mcu_status = 1 at BT
			BLE_WAKEUP_ENABLE;
			data[0] = (uint8_t)MCU_WAKEUP;
			ble_uart_send_packet(CMD_MCU_STATUS, data, 1);
			BLE_WAKEUP_DISABLE;
		}
#endif
#endif

		if(g_str_bitflag.b1_ble_send_flag)
		{
			g_str_bitflag.b1_ble_send_flag = 0;
			if(g_str_ble_status.power_status == POWER_OFF)//first priority
			{
				data[0] = (uint8_t)POWER_OFF;
				ble_uart_send_packet(CMD_POWER_STATUS, data, 1);
				while(1)
				{
					POWER_HOLD_DISABLE;
					goto TOP;
				}
			}
			else if(g_str_ble_status.bt_pairing_status == BT_PAIRING_TIMEOUT)
			{
				force_off_pattern();
				g_str_ble_status.bt_pairing_status = BT_PAIRING_INIT;

				data[0] = (uint8_t)BT_PAIRING_TIMEOUT;
				ble_uart_send_packet(CMD_BT_PAIRING_STATUS, data, 1);
			}
		}

#ifdef ENABLE_AUDIO
#ifdef ENABLE_HY_FE
		if( g_str_bitflag.b2_fe_start_flag )
		{
			formant_enhancement();
			g_str_bitflag.b2_fe_start_flag = 0;
		}
#endif
		
		if( g_str_bitflag.b1_frame_state == AUDIO_FRAME_ERR )
		{
			audio_rx_start();
		}
#endif
	}
#else
	pin_enable_gpio();

	ble_uart_init();
	
#ifdef ENABLE_HY_FE
	fe_test_init();
#endif
	
#endif
	return 0;
}

void HardFault_Handler(void)
{
	SCB->AIRCR = (0x05FA0000) | 0x04;
}
