#include "global_define.h"

#ifdef ENABLE_STOPMODE
void wakeup_isr(void)
{
	if(EXTI_GetITStatus(GPIO_WAKEUP_EXTI_LINE) != RESET)
	{
		if(g_str_timer.ui16_sleep_cnt >= (SLEEP_COUNT + 50))
		{
			uint8_t data = 0;
			data = (uint8_t)MCU_WAKEUP;
			ble_uart_send_packet(CMD_MCU_STATUS, &data, 1);
		}

		g_str_bitflag.b1_mcu_sleep_status = 0;
		g_str_timer.ui16_sleep_cnt = 0;
		EXTI_ClearITPendingBit(GPIO_WAKEUP_EXTI_LINE);
	}
}
#endif

void enter_standby_mode(void)
{
#ifdef ENABLE_AUDIO
	DAC_STATUS_DISABLE;
	AMP_STATUS_DISABLE;
#ifdef ENABLE_AUDIO_DMA
	SPI_I2S_DMACmd(AUDIO_I2S_TX, SPI_I2S_DMAReq_Tx, DISABLE);
	DMA_Cmd(AUDIO_I2S_DMA_STREAM, DISABLE);
#endif
	
	I2S_Cmd(AUDIO_I2S_TX, DISABLE);
	I2S_Cmd(AUDIO_I2S_RX, DISABLE);
#endif
	
	g_str_timer.ui16_sleep_cnt = 0;

	uint8_t data = (uint8_t)MCU_SLEEP;
	ble_uart_send_packet(CMD_MCU_STATUS, &data, 1);
#ifdef HW_PP4
	delay_ms(1);
#else
	delay_10ms(1);
#endif

#ifdef ENABLE_FUEL_GAUGE
	I2C_SoftwareResetCmd(SENSORS_I2C, DISABLE);
	I2C_Cmd(SENSORS_I2C, DISABLE);
	I2C_DeInit(SENSORS_I2C);
	SysTick->CTRL &= ~(SysTick_CTRL_ENABLE_Msk); //systick counter off
#endif
	

#ifdef ENABLE_STOPMODE	
	force_off_pattern();
	
	monitor_timer_stop();
	pattern_timer_stop();

	pin_disable_all_gpio();
	RCC_AHB1PeriphClockCmd(0xFFFFFFFF, DISABLE);
	RCC_AHB2PeriphClockCmd(0xFFFFFFFF, DISABLE);
	RCC_APB1PeriphClockCmd(0xFFFFFFFF, DISABLE);
	RCC_APB2PeriphClockCmd(0xFFFFFFFF, DISABLE);
	
	/* Go to STOP mode */
	PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
#endif //ENABLE_STOPMODE
}
