#include "global_define.h"

#ifdef ENABLE_PWM

pwm_struct_t pattern_pwm;

void led_pattern_pairing( void )
{
	if( g_str_timer.ui16_led_cnt > 155 )
	{
		pwm_pulse_led5(0);
	}
	else if( g_str_timer.ui16_led_cnt > 63 )
	{
		pwm_pulse_led5(100);
	}
	else
	{
		pwm_pulse_off();
	}
	
	g_str_timer.ui16_led_cnt--;
}

void led_pattern_pairing_complete( void )
{
	if( g_str_timer.ui16_led_cnt > 290 )
	{
		pwm_pulse_off();
	}
	else if( g_str_timer.ui16_led_cnt > 237 )
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100);
		pwm_pulse_led4(100);
		pwm_pulse_led5(100);
	}
	else
	{
		pwm_pulse_off();
	}
	
	g_str_timer.ui16_led_cnt--;
}

void led_pattern_batt_level( void )
{
	if( pattern_pwm.ui16_dimming_flag )
	{
		pattern_pwm.ui16_dimming_data++;
	}
	else
	{
		pattern_pwm.ui16_dimming_data--;
	}
	
	if( pattern_pwm.ui16_dimming_data > 65 )
	{
		pattern_pwm.ui16_dimming_data = 65;
		pattern_pwm.ui16_dimming_flag = 0;
	}
	else if ( pattern_pwm.ui16_dimming_data < 3  )
	{
		pattern_pwm.ui16_dimming_flag = 1;
	}
	
	if( pattern_pwm.ui16_dimming_data > 65 )
	{
		pattern_pwm.ui16_pwm = 95;
	}
	else
	{
		pattern_pwm.ui16_pwm = ( pattern_pwm.ui16_dimming_data * 3 ) >> 1;
	}
	
	switch( battery_voltage )
	{
	case BATT_VOLTAGE_30 :
	case BATT_VOLTAGE_31 :
	case BATT_VOLTAGE_32 :
	case BATT_VOLTAGE_33 :
	case BATT_VOLTAGE_34 :
	case BATT_VOLTAGE_35 :
		pwm_pulse_off();
		pwm_pulse_led1(pattern_pwm.ui16_pwm);
		break;
		
	case BATT_VOLTAGE_36 :
	case BATT_VOLTAGE_37 :
		pwm_pulse_off();
		pwm_pulse_led1(95);
		pwm_pulse_led2(pattern_pwm.ui16_pwm);
		break;
		
	case BATT_VOLTAGE_38 :
	case BATT_VOLTAGE_39 :
		pwm_pulse_off();
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
		pwm_pulse_led3(pattern_pwm.ui16_pwm);
		break;
		
	case BATT_VOLTAGE_40 :
	case BATT_VOLTAGE_41 :
		pwm_pulse_off();
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
		pwm_pulse_led3(95);
		pwm_pulse_led4(pattern_pwm.ui16_pwm);
		break;
		
	case BATT_VOLTAGE_42 :
		pwm_pulse_off();
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
		pwm_pulse_led3(95);
		pwm_pulse_led4(95);
		pwm_pulse_led5(pattern_pwm.ui16_pwm);
		break;
	default :
		break;
	}

#if 0
	if( g_str_chg.ui16_chg_status != CHG_STATUS_IN_CHARGING )
	{
		g_str_timer.ui16_led_cnt--;
	}	
#endif
}


void led_pattern_app_noti( void )
{
	
	if( g_str_timer.ui16_led_cnt > 440 )
	{
		pwm_pulse_led_all(100);
		HAPTIC_ACTIVE;
	}
	else if( g_str_timer.ui16_led_cnt > 400 )
	{
		pwm_pulse_off();
		HAPTIC_IDLE;
	}
	else if( g_str_timer.ui16_led_cnt > 360 )
	{
		pwm_pulse_led_all(100);
		HAPTIC_ACTIVE;
	}
	else if( g_str_timer.ui16_led_cnt > 320 )
	{
		pwm_pulse_off();
		HAPTIC_IDLE;
	}
	else if( g_str_timer.ui16_led_cnt > 280 )
	{
		pwm_pulse_led_all(100);
		HAPTIC_ACTIVE;
	}
	else if( g_str_timer.ui16_led_cnt > 240 )
	{
		pwm_pulse_off();
		HAPTIC_IDLE;
	}
	else if( g_str_timer.ui16_led_cnt > 200 )
	{
		pwm_pulse_led_all(100);
	}
	else if( g_str_timer.ui16_led_cnt > 160 )
	{
		pwm_pulse_off();
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100);
		pwm_pulse_led4(100);
	}
	else if( g_str_timer.ui16_led_cnt > 120 )
	{
		pwm_pulse_off();
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100);
	}
	else if( g_str_timer.ui16_led_cnt > 80 )
	{
		pwm_pulse_off();
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
	}
	else if( g_str_timer.ui16_led_cnt > 40 )
	{
		pwm_pulse_off();
		pwm_pulse_led1(100);
	}
	else
	{
		pwm_pulse_off();
	}
	
	g_str_timer.ui16_led_cnt--;
	
}

void led_pattern_bt_conn( void )
{
	if( pattern_pwm.ui16_pwm > 95 )
	{
		pattern_pwm.ui16_pwm = 95;
	}
	
	else if( pattern_pwm.ui16_pwm < 5 )
	{
		pattern_pwm.ui16_pwm = 5;
	}
	
	if( g_str_timer.ui16_led_cnt > 330 )
	{
		pwm_pulse_off();
		pwm_pulse_led1(pattern_pwm.ui16_pwm++);
		pwm_pulse_led5(pattern_pwm.ui16_pwm++);
	}
	else if( g_str_timer.ui16_led_cnt > 300 )
	{
		pwm_pulse_led2(100 - pattern_pwm.ui16_pwm--);
		pwm_pulse_led4(100 - pattern_pwm.ui16_pwm--);
	}
	else if( g_str_timer.ui16_led_cnt > 270 )
	{
		pwm_pulse_led3(pattern_pwm.ui16_pwm++);
		pattern_pwm.ui16_pwm++;
	}
	else if( g_str_timer.ui16_led_cnt > 240 )
	{
		pwm_pulse_off();
	}
	else if( g_str_timer.ui16_led_cnt > 210 )
	{
		pwm_pulse_off();
		pwm_pulse_led1(pattern_pwm.ui16_pwm++);
		pwm_pulse_led5(pattern_pwm.ui16_pwm++);
	}
	else if( g_str_timer.ui16_led_cnt > 180 )
	{
		pwm_pulse_led2(100 - pattern_pwm.ui16_pwm--);
		pwm_pulse_led4(100 - pattern_pwm.ui16_pwm--);
	}
	else if( g_str_timer.ui16_led_cnt > 150 )
	{
		pwm_pulse_led3(pattern_pwm.ui16_pwm++);
		pattern_pwm.ui16_pwm++;
	}
	else if( g_str_timer.ui16_led_cnt > 120 )
	{
		pwm_pulse_off();
	}
	else if( g_str_timer.ui16_led_cnt > 90 )
	{
		pwm_pulse_off();
		pwm_pulse_led1(pattern_pwm.ui16_pwm++);
		pwm_pulse_led5(pattern_pwm.ui16_pwm++);
	}
	else if( g_str_timer.ui16_led_cnt > 60 )
	{
		pwm_pulse_led2(100 - pattern_pwm.ui16_pwm--);
		pwm_pulse_led4(100 - pattern_pwm.ui16_pwm--);
	}
	else if( g_str_timer.ui16_led_cnt > 30 )
	{
		pwm_pulse_led3(pattern_pwm.ui16_pwm++);
		pattern_pwm.ui16_pwm++;
	}
	else
	{
		pwm_pulse_off();
	}
	
	g_str_timer.ui16_led_cnt--;
	
}

void led_pattern_bt_disconn( void )
{
	if( pattern_pwm.ui16_pwm > 95 )
	{
		pattern_pwm.ui16_pwm = 95;
	}
	
	else if( pattern_pwm.ui16_pwm < 5 )
	{
		pattern_pwm.ui16_pwm = 5;
	}
	
	if( g_str_timer.ui16_led_cnt > 330 )
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100 - pattern_pwm.ui16_pwm++);
		pwm_pulse_led4(100);
		pwm_pulse_led5(100);
		
		pattern_pwm.ui16_pwm++;
	}
	else if( g_str_timer.ui16_led_cnt > 300 )
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(pattern_pwm.ui16_pwm--);
		pwm_pulse_led3(0);
		pwm_pulse_led4(pattern_pwm.ui16_pwm--);
		pwm_pulse_led5(100);
	}
	else if( g_str_timer.ui16_led_cnt > 270 )
	{
		pwm_pulse_led1(100 - pattern_pwm.ui16_pwm++);
		pwm_pulse_led2(0);
		pwm_pulse_led3(0);
		pwm_pulse_led4(0);
		pwm_pulse_led5(100 - pattern_pwm.ui16_pwm++);
	}
	else if( g_str_timer.ui16_led_cnt > 240 )
	{
		pwm_pulse_off();
	}
	else if( g_str_timer.ui16_led_cnt > 210 )
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100 - pattern_pwm.ui16_pwm++);
		pwm_pulse_led4(100);
		pwm_pulse_led5(100);
		
		pattern_pwm.ui16_pwm++;
	}
	else if( g_str_timer.ui16_led_cnt > 180 )
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(pattern_pwm.ui16_pwm--);
		pwm_pulse_led3(0);
		pwm_pulse_led4(pattern_pwm.ui16_pwm--);
		pwm_pulse_led5(100);
	}
	else if( g_str_timer.ui16_led_cnt > 150 )
	{
		pwm_pulse_led1(100 - pattern_pwm.ui16_pwm++);
		pwm_pulse_led2(0);
		pwm_pulse_led3(0);
		pwm_pulse_led4(0);
		pwm_pulse_led5(100 - pattern_pwm.ui16_pwm++);
	}
	else if( g_str_timer.ui16_led_cnt > 120 )
	{
		pwm_pulse_off();
	}
	else if( g_str_timer.ui16_led_cnt > 90 )
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(100);
		pwm_pulse_led3(100 - pattern_pwm.ui16_pwm++);
		pwm_pulse_led4(100);
		pwm_pulse_led5(100);
		
		pattern_pwm.ui16_pwm++;
	}
	else if( g_str_timer.ui16_led_cnt > 60 )
	{
		pwm_pulse_led1(100);
		pwm_pulse_led2(pattern_pwm.ui16_pwm--);
		pwm_pulse_led3(0);
		pwm_pulse_led4(pattern_pwm.ui16_pwm--);
		pwm_pulse_led5(100);
	}
	else if( g_str_timer.ui16_led_cnt > 30 )
	{
		pwm_pulse_led1(100 - pattern_pwm.ui16_pwm++);
		pwm_pulse_led2(0);
		pwm_pulse_led3(0);
		pwm_pulse_led4(0);
		pwm_pulse_led5(100 - pattern_pwm.ui16_pwm++);
	}
	else
	{
		pwm_pulse_off();
	}
	
	g_str_timer.ui16_led_cnt--;
	
}

void led_pattern_ble_conn( void )
{
	if( pattern_pwm.ui16_pwm > 95 )
	{
		pattern_pwm.ui16_pwm = 95;
	}
	
	else if( pattern_pwm.ui16_pwm < 5 )
	{
		pattern_pwm.ui16_pwm = 5;
	}
	
	if( g_str_timer.ui16_led_cnt > 330 )
	{
		pwm_pulse_led1(95);
	}
	else if( g_str_timer.ui16_led_cnt > 270 )
	{
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
	}
	else if( g_str_timer.ui16_led_cnt > 210 )
	{
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
		pwm_pulse_led3(95);
	}
	else if( g_str_timer.ui16_led_cnt > 150 )
	{
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
		pwm_pulse_led3(95);
		pwm_pulse_led4(95);
	}
	else if( g_str_timer.ui16_led_cnt > 90 )
	{
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
		pwm_pulse_led3(95);
		pwm_pulse_led4(95);
		pwm_pulse_led5(95);
	}
	else
	{
		pwm_pulse_off();
	}
	
	g_str_timer.ui16_led_cnt--;
	
}


void led_pattern_ble_disconn( void )
{
	if( pattern_pwm.ui16_pwm > 95 )
	{
		pattern_pwm.ui16_pwm = 95;
	}
	
	else if( pattern_pwm.ui16_pwm < 5 )
	{
		pattern_pwm.ui16_pwm = 5;
	}
	
	if( g_str_timer.ui16_led_cnt > 330 )
	{
		pwm_pulse_led5(95);
	}
	else if( g_str_timer.ui16_led_cnt > 270 )
	{
		pwm_pulse_led4(95);
		pwm_pulse_led5(95);
	}
	else if( g_str_timer.ui16_led_cnt > 210 )
	{
		pwm_pulse_led3(95);
		pwm_pulse_led4(95);
		pwm_pulse_led5(95);
	}
	else if( g_str_timer.ui16_led_cnt > 150 )
	{
		pwm_pulse_led2(95);
		pwm_pulse_led3(95);
		pwm_pulse_led4(95);
		pwm_pulse_led5(95);
	}
	else if( g_str_timer.ui16_led_cnt > 90 )
	{
		pwm_pulse_led1(95);
		pwm_pulse_led2(95);
		pwm_pulse_led3(95);
		pwm_pulse_led4(95);
		pwm_pulse_led5(95);
	}
	else
	{
		pwm_pulse_off();
	}
	
	g_str_timer.ui16_led_cnt--;
	
}


void led_pattern_power_on( void )
{
	if( pattern_pwm.ui16_pwm > 95 )
	{
		pattern_pwm.ui16_pwm = 95;
	}
	
	else if( pattern_pwm.ui16_pwm < 5 )
	{
		pattern_pwm.ui16_pwm = 5;
	}
	
	if( g_str_timer.ui16_led_cnt > 300 )
	{
		pwm_pulse_off();
		pwm_pulse_led1(pattern_pwm.ui16_pwm++);
		pattern_pwm.ui16_pwm++;
	}
	else if( g_str_timer.ui16_led_cnt > 270 )
	{
		pwm_pulse_led2(100 - pattern_pwm.ui16_pwm--);
		pattern_pwm.ui16_pwm--;
	}
	else if( g_str_timer.ui16_led_cnt > 240 )
	{
		pwm_pulse_led3(pattern_pwm.ui16_pwm++);
		pattern_pwm.ui16_pwm++;
	}
	else if( g_str_timer.ui16_led_cnt > 210 )
	{
		pwm_pulse_led4(100 - pattern_pwm.ui16_pwm--);
		pattern_pwm.ui16_pwm--;
	}
	else if( g_str_timer.ui16_led_cnt > 180 )
	{
		pwm_pulse_led5(pattern_pwm.ui16_pwm++);
		pattern_pwm.ui16_pwm++;
	}
	else if( g_str_timer.ui16_led_cnt > 150 )
	{
		pwm_pulse_off();
		pwm_pulse_led1(pattern_pwm.ui16_pwm++);
		pattern_pwm.ui16_pwm++;
	}
	else if( g_str_timer.ui16_led_cnt > 120 )
	{
		pwm_pulse_led2(100 - pattern_pwm.ui16_pwm--);
		pattern_pwm.ui16_pwm--;
	}
	else if( g_str_timer.ui16_led_cnt > 90 )
	{
		pwm_pulse_led3(pattern_pwm.ui16_pwm++);
		pattern_pwm.ui16_pwm++;
	}
	else if( g_str_timer.ui16_led_cnt > 60 )
	{
		pwm_pulse_led4(100 - pattern_pwm.ui16_pwm--);
		pattern_pwm.ui16_pwm--;
	}
	else if( g_str_timer.ui16_led_cnt > 30 )
	{
		pwm_pulse_led5(pattern_pwm.ui16_pwm++);
		pattern_pwm.ui16_pwm++;
	}
	else
	{
		pwm_pulse_off();
	}
	
	g_str_timer.ui16_led_cnt--;
}

void led_pattern_incoming_call( void )
{
	static uint16_t ui16_level = 0;
	
	if( pattern_pwm.ui16_dimming_flag )
	{
		pattern_pwm.ui16_dimming_data++;
	}
	else
	{
		pattern_pwm.ui16_dimming_data--;
	}
	
	if( pattern_pwm.ui16_dimming_data > 33 )
	{
		pattern_pwm.ui16_dimming_data = 33;
		pattern_pwm.ui16_dimming_flag = 0;
		
		ui16_level++;
	}
	else if ( pattern_pwm.ui16_dimming_data < 3  )
	{
		pattern_pwm.ui16_dimming_flag = 1;
		ui16_level++;
	}
	
	
	if( ui16_level > 5 )
	{
		ui16_level = 0;
		pattern_pwm.ui16_dimming_flag = 1;
	}
	
	pattern_pwm.ui16_pwm = pattern_pwm.ui16_dimming_data * 3;
	
	switch( ui16_level )
	{
	case 0:
		pwm_pulse_off();
		pwm_pulse_led1(pattern_pwm.ui16_pwm);
		
		break;
	case 1:
		pwm_pulse_off();
		pwm_pulse_led1(100);
		pwm_pulse_led2(100 - pattern_pwm.ui16_pwm);
		
		break;
	case 2:
		pwm_pulse_off();
		pwm_pulse_led1(100 - pattern_pwm.ui16_pwm);
		pwm_pulse_led2(100);
		pwm_pulse_led3(pattern_pwm.ui16_pwm);
		
		break;
	case 3:
		pwm_pulse_off();
		pwm_pulse_led2(pattern_pwm.ui16_pwm);
		pwm_pulse_led3(100);
		pwm_pulse_led4(100 - pattern_pwm.ui16_pwm);
		
		break;
	case 4:
		pwm_pulse_off();
		pwm_pulse_led3(100 - pattern_pwm.ui16_pwm);
		pwm_pulse_led4(100);
		pwm_pulse_led5(pattern_pwm.ui16_pwm);
		
		break;
	case 5:
		pwm_pulse_off();
		pwm_pulse_led4(pattern_pwm.ui16_pwm);
		pwm_pulse_led5(100);
		
		break;
	default :
		//ui16_level = 0;
		break;
	}
}

void led_pattern_favorite_call( void )
{
	if( g_device_status == DEVICE_OUTGOING_CALL )
	{
		if( pattern_pwm.ui16_dimming_flag )
		{
			pattern_pwm.ui16_pwm++;
		}
		else
		{
			pattern_pwm.ui16_pwm--;
		}
		
		if( pattern_pwm.ui16_pwm > 90 )
		{
			pattern_pwm.ui16_pwm = 90;
			pattern_pwm.ui16_dimming_flag = 0;
		}
		else if ( pattern_pwm.ui16_pwm < 10  )
		{
			pattern_pwm.ui16_dimming_flag = 1;
		}
	}
	else
	{
		pattern_pwm.ui16_pwm = 100;
	}
	
	switch( favorite_number )
	{
	case FAVORITE_1ST :
		pwm_pulse_off();
		pwm_pulse_led1(pattern_pwm.ui16_pwm);
		break;
	case FAVORITE_2ND :
		pwm_pulse_off();
		pwm_pulse_led2(pattern_pwm.ui16_pwm);
		break;
	case FAVORITE_3RD :
		pwm_pulse_off();
		pwm_pulse_led3(pattern_pwm.ui16_pwm);
		break;
	case FAVORITE_4TH :
		pwm_pulse_off();
		pwm_pulse_led4(pattern_pwm.ui16_pwm);
		break;
	case FAVORITE_5TH :
		pwm_pulse_off();
		pwm_pulse_led5(pattern_pwm.ui16_pwm);
		break;
	default :
		pwm_pulse_off();
		break;
	}
}

#endif //ENABLE_PWM
