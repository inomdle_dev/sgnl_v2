#include "global_define.h"

#ifdef ENABLE_AUDIO_DMA

void Audio_DMA_config(void)
{
	DMA_InitTypeDef DMA_InitStructure;

	/* Enable the DMA clock */
	RCC_AHB1PeriphClockCmd(AUDIO_I2S_DMA_CLOCK, ENABLE); 
	
	/* Enable the I2S DMA request */
	SPI_I2S_DMACmd(AUDIO_I2S_TX, SPI_I2S_DMAReq_Tx, DISABLE);

	/* Configure the DMA Stream */
	DMA_Cmd(AUDIO_I2S_DMA_STREAM, DISABLE);
	while(AUDIO_I2S_DMA_STREAM->CR & DMA_SxCR_EN);
	
	DMA_DeInit(AUDIO_I2S_DMA_STREAM);
	/* Set the parameters to be configured */
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;//DMA_Mode_Normal;//

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(AUDIO_I2S_TX->DR));
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;  
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;

	DMA_InitStructure.DMA_Channel = AUDIO_I2S_DMA_CHANNEL;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_Init(AUDIO_I2S_DMA_STREAM, &DMA_InitStructure);
}

void Audio_MAL_Play(uint32_t *Addr, uint32_t Size)
{
	DMA_InitTypeDef DMA_InitStructure;
	DMA_InitStructure.DMA_BufferSize = Size;      /* This field will be configured in play function */
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;//DMA_Mode_Circular;//

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(AUDIO_I2S_TX->DR));
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;  
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;

	DMA_InitStructure.DMA_Channel = AUDIO_I2S_DMA_CHANNEL;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)Addr;      /* This field will be configured in play function */
	DMA_Init(AUDIO_I2S_DMA_STREAM, &DMA_InitStructure);
	
	SPI_I2S_DMACmd(AUDIO_I2S_TX, SPI_I2S_DMAReq_Tx, ENABLE);
	DMA_Cmd(AUDIO_I2S_DMA_STREAM, ENABLE);
}

void Audio_MAL_Stop(void)
{
	/* Stop the Transfer on the I2S side: Stop and disable the DMA stream */
	/* Enable the I2S DMA request */
	SPI_I2S_DMACmd(AUDIO_I2S_TX, SPI_I2S_DMAReq_Tx, DISABLE);  
	DMA_Cmd(AUDIO_I2S_DMA_STREAM, DISABLE);
	while(AUDIO_I2S_DMA_STREAM->CR & DMA_SxCR_EN);
	
	/* In all modes, disable the I2S peripheral */
	I2S_Cmd(AUDIO_I2S_TX, DISABLE);
}
#endif
