#include "global_define.h"

#ifdef ENABLE_BLE_UART

typedef enum {
	RX_BLE_CMD_INIT,
	RX_BLE_CMD_COMMAND,
	RX_BLE_CMD_ID,
	RX_BLE_CMD_LENGTH,
	RX_BLE_CMD_DATA,
	RX_BLE_CMD_CHECKSUM,
	RX_BLE_CMD_END,
} RX_BLE_MODE;

static RX_BLE_MODE BLE_CmdDecode;

uint8_t  BLE_CmdBuffer[32];
uint16_t BLE_CmdIndex = 0;


static uint8_t ui8_tx_byte[32];
static uint16_t ui16_tx_length;

void ble_uart_init(void)
{
	ble_uart_config(115200);
}

/* UART Configuration
* TxD  PA2
* RxD  PA3
* async. mode
* baudrate 115200
* Word Length = 8 Bits
* one Stop Bit
* No parity
* Hardware flow control disabled (RTS and CTS signals)
* Receive and transmit enabled
*/
void ble_uart_config(uint32_t baud_rate)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	uint32_t baudrate = baud_rate;

	USART_DeInit(BLE_UART);
	
	/* Enable GPIO clock */
	RCC_AHB1PeriphClockCmd(BLE_UART_GPIO_CLK, ENABLE);
	
	/* Enable USART clock */
	RCC_APB2PeriphClockCmd(BLE_UART_CLK, ENABLE);
	
	/* Connect USART pins to AF7 */
	GPIO_PinAFConfig(BLE_UART_GPIO_PORT, BLE_UART_TX_SOURCE, BLE_UART_AF);
	GPIO_PinAFConfig(BLE_UART_GPIO_PORT, BLE_UART_RX_SOURCE, BLE_UART_AF);
	
	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Pin = BLE_UART_TX_PIN | BLE_UART_RX_PIN;
	GPIO_Init(BLE_UART_GPIO_PORT, &GPIO_InitStructure);
	
	/* USART2 configuration ----------------------------------------------------*/
	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(BLE_UART, &USART_InitStructure);

	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = BLE_UART_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Enable USART */
	USART_ITConfig(BLE_UART, USART_IT_RXNE, ENABLE);
	USART_Cmd(BLE_UART, ENABLE);
}

void ble_uart_isr(void)
{
	static uint16_t ui16_rx_data = 0;
#ifdef ENABLE_STOPMODE
	g_str_timer.ui16_sleep_cnt = 0;
#endif
	
	/* USART in Receiver mode */
	if (USART_GetITStatus(BLE_UART, USART_IT_RXNE) == SET)
	{
		/* Receive Transaction data */
		ui16_rx_data = USART_ReceiveData(BLE_UART);
		g_str_ble.ui8_rx_buf[g_str_ble.ui16_rx_head++] = ui16_rx_data;
		g_str_timer.ui16_sleep_cnt = 0;
		if(ui16_rx_data == 0xFA)
			g_str_bitflag.b2_ble_rcv_flag = 1;
		
		if( g_str_ble.ui16_rx_head == UART_BUFFERSIZE )
		{
			g_str_ble.ui16_rx_head = 0;
		}
		
		g_str_ble.ui16_rx_cnt++;
		
		USART_ClearITPendingBit(BLE_UART, USART_IT_RXNE);
	}
}

uint8_t ble_uart_read_byte( void )
{
	uint8_t ui8_data = 0;
	
	ui8_data = g_str_ble.ui8_rx_buf[g_str_ble.ui16_rx_tail++];
	
	if( g_str_ble.ui16_rx_tail == UART_BUFFERSIZE )
	{
		g_str_ble.ui16_rx_tail = 0;
	}
	g_str_ble.ui16_rx_cnt--;
	
	return ui8_data;
}

void BLE_CommandHandler( void )
{
	uint8_t current_byte;
	int8_t i8_length = 0;
	uint16_t ui16_check_sum = 0;
	
	BLE_CmdDecode = RX_BLE_CMD_INIT;
	while (g_str_ble.ui16_rx_cnt)
	{
		current_byte = ble_uart_read_byte();
		g_str_timer.ui16_sleep_cnt = 0;
		switch (BLE_CmdDecode)
		{
			case RX_BLE_CMD_INIT://0xFB [0]
				if(current_byte == 0xFB)
				{
					BLE_CmdBuffer[BLE_CmdIndex++] = current_byte;
					BLE_CmdDecode = RX_BLE_CMD_COMMAND;
				}
				else
				{
					//memset((uint8_t *)&g_str_ble.ui8_tx_buf, 0x00, sizeof(g_str_ble.ui8_tx_buf));
					ble_printf("UART_PROTOCOL_STX_ERROR");//error msg 예정
					memset((uint8_t *)&g_str_ble, 0x00, sizeof(g_str_ble));
					BLE_CmdDecode = RX_BLE_CMD_INIT;
					memset(BLE_CmdBuffer, 0x00, sizeof(BLE_CmdBuffer));
					BLE_CmdIndex = 0;
					g_str_ble.ui16_tx_cnt = 256;
					return;
				}
				break;
			case RX_BLE_CMD_COMMAND://0x80~0xFF [1]
				if(current_byte >= 0x80)
				{
					ui16_check_sum = current_byte;
					BLE_CmdBuffer[BLE_CmdIndex++] = current_byte;
					BLE_CmdDecode = RX_BLE_CMD_ID;
				}
				else
				{
					ble_printf("UART_PROTOCOL_COMMAND_ERROR");//error msg 예정
					memset((uint8_t *)&g_str_ble, 0x00, sizeof(g_str_ble));
					BLE_CmdDecode = RX_BLE_CMD_INIT;
					memset(BLE_CmdBuffer, 0x00, sizeof(BLE_CmdBuffer));
					BLE_CmdIndex = 0;
					g_str_ble.ui16_tx_cnt = 256;
					return;
				}
				break;
			case RX_BLE_CMD_ID://0x00:cypress -> MCU / 0x01:MCU -> cypress [2]
				if(current_byte == 0x00)
				{
					ui16_check_sum += current_byte;
					BLE_CmdBuffer[BLE_CmdIndex++] = current_byte;
					BLE_CmdDecode = RX_BLE_CMD_LENGTH;
				}
				else
				{
					ble_printf("UART_PROTOCOL_ID_ERROR");//error msg 예정
					memset((uint8_t *)&g_str_ble, 0x00, sizeof(g_str_ble));
					BLE_CmdDecode = RX_BLE_CMD_INIT;
					memset(BLE_CmdBuffer, 0x00, sizeof(BLE_CmdBuffer));
					BLE_CmdIndex = 0;
					g_str_ble.ui16_tx_cnt = 256;
					return;
				}
				break;
			case RX_BLE_CMD_LENGTH://Data length [3]
				ui16_check_sum += current_byte;
				i8_length = current_byte;
				BLE_CmdBuffer[BLE_CmdIndex++] = current_byte;
				BLE_CmdDecode = RX_BLE_CMD_DATA;
				break;
			case RX_BLE_CMD_DATA://Data(s) [4, ... , 4+length-1]
				ui16_check_sum += current_byte;
				BLE_CmdBuffer[BLE_CmdIndex++] = current_byte;
				if(--i8_length == 0)
				{
					BLE_CmdDecode = RX_BLE_CMD_CHECKSUM;
				}
				else if(i8_length < 0)
				{
					memset((uint8_t *)&g_str_ble, 0x00, sizeof(g_str_ble));
					BLE_CmdDecode = RX_BLE_CMD_INIT;
					memset(BLE_CmdBuffer, 0x00, sizeof(BLE_CmdBuffer));
					BLE_CmdIndex = 0;
					g_str_ble.ui16_tx_cnt = 256;
					return;
				}
				break;
			case RX_BLE_CMD_CHECKSUM://(command + id + length + data)%128 [4+length]
				if(current_byte == (ui16_check_sum % 128))
				{
					BLE_CmdBuffer[BLE_CmdIndex++] = current_byte;
					BLE_CmdDecode = RX_BLE_CMD_END;
				}
				else
				{
					ble_printf("UART_PROTOCOL_ID_ERROR");//error msg 예정
					memset((uint8_t *)&g_str_ble, 0x00, sizeof(g_str_ble));
					BLE_CmdDecode = RX_BLE_CMD_INIT;
					memset(BLE_CmdBuffer, 0x00, sizeof(BLE_CmdBuffer));
					BLE_CmdIndex = 0;
					g_str_ble.ui16_tx_cnt = 256;
					return;
				}
				break;
			case RX_BLE_CMD_END ://0xFA [4+length+1]
				memset((uint8_t *)&g_str_ble.ui8_tx_buf, 0x00, sizeof(g_str_ble.ui8_tx_buf));
				BLE_CmdBuffer[BLE_CmdIndex++] = current_byte;
				g_str_bitflag.b2_ble_rcv_flag = 0;
				cyw20706_protocol(BLE_CmdBuffer, BLE_CmdIndex);
				BLE_CmdIndex = 0;
				memset(BLE_CmdBuffer, 0x00, sizeof(BLE_CmdBuffer));
				BLE_CmdDecode = RX_BLE_CMD_INIT;
				break;
			default :
				BLE_CmdDecode = RX_BLE_CMD_INIT;
				BLE_CmdIndex = 0;
				break;
		}
	}
}

void ble_uart_send_data( uint8_t byte )
{
	USART_SendData(BLE_UART, byte);
	
	while (USART_GetFlagStatus(BLE_UART, USART_FLAG_TXE) == RESET)
	{}
}

void ble_uart_send( uint8_t* ui8_byte, uint16_t ui16_length )
{
	for( int i = 0; i < ui16_length; i++ )
	{
		ui8_tx_byte[i] = ui8_byte[i];
		ble_uart_send_data(ui8_tx_byte[i]);
	}

	ble_uart_send_data(0xFA);
}

void ble_command_send_data( uint8_t command, uint8_t param )
{
	uint8_t byte[20] = {0,};
	sprintf((char *)byte, "%c%c%c%c", 0x03, command, param, 0xFA);
	ble_uart_send((uint8_t *)byte, 4);
}

void ble_lowpower_uart_tx( void )
{
	for( int i = 0; i < ui16_tx_length; i++ )
	{
		ble_uart_send_data(ui8_tx_byte[i]);
	}
	ble_uart_send_data(0xFA);
	
	g_str_ble.ui16_tx_flag = 0;
}

void ble_lowpower_uart_send( uint8_t* ui8_byte, uint16_t ui16_length )
{
	if( !g_str_ble.ui16_tx_flag )
	{
		g_str_ble.ui16_tx_flag = 1;
		
		for( int i = 0; i < ui16_length; i++ )
		{
			ui8_tx_byte[i] = ui8_byte[i];
		}
		ui16_tx_length = ui16_length;
	}
}

void ble_uart_send_packet(uint8_t cmd, uint8_t *data, uint8_t size)
{
	uint8_t ui8_packet[32] = {0,};
	cyw20706_cmd_status_e ui8_command = CYW20706_REPORT_INIT;
	uint8_t ui8_length = 0;
	uint8_t ui8_idx = 0;
	uint16_t ui16_checksum = 0;

#if defined(HW_PP3) | defined(HW_PP4)
	if(g_str_ble_status.charger_status)
		BLE_WAKEUP_ENABLE;
#endif
	
	ui8_idx = 0;
	ui8_packet[ui8_idx++] = 0xFB;//stx
	
	ui8_command = (cyw20706_cmd_status_e)cmd;
	ui16_checksum = ui8_command;
	ui8_packet[ui8_idx++] = ui8_command;//command
	
	ui16_checksum += 0x01;
	ui8_packet[ui8_idx++] = 0x01;//id 0x00:cyp->mcu / 0x01:mcu->cyp

	ui8_length = size;
	ui16_checksum += ui8_length;
	ui8_packet[ui8_idx++] = ui8_length;//length

	for(int i = 0; i < ui8_length; i++)
	{
		ui16_checksum += data[i];
		ui8_packet[ui8_idx++] = data[i];//data
	}
	
	ui8_packet[ui8_idx++] = (ui16_checksum % 128);//checksum
	ui8_packet[ui8_idx++] = 0xFA;//etx
	//ui8_packet[ui8_idx++] = 0x55;//dummy data
	
	for(int i = 0; i < ui8_idx; i++)
	{
		ble_uart_send_data(ui8_packet[i]);
	}

#if defined(HW_PP3) | defined(HW_PP4)
	if(g_str_ble_status.charger_status)
		BLE_WAKEUP_DISABLE;
#endif
}

void ble_printf( const char *form, ... )
{
#if 0
        static char buf[UART_BUFFERSIZE] = {0,};
	uint16_t ui16_index = 0;
        uint16_t len = 0;
        va_list arg_ptr;
        
        len = strlen(form);
        if(len > 0)
        {
                memset(buf, 0x00, sizeof(buf));
                va_start(arg_ptr, form);
                vsprintf(buf, form, arg_ptr);
                va_end(arg_ptr);

                for( int i = 0; i < UART_BUFFERSIZE; i++ )
                {
                        if( buf[i] == '\n' )
                        {
                                g_str_ble.ui8_tx_buf[ui16_index++] = '\r';
                                g_str_ble.ui8_tx_buf[ui16_index++] = buf[i];

                                if( buf[i+1] == '\0' )
                                {
                                        g_str_ble.ui16_tx_cnt = ui16_index;
                                        break;
                                }
                        }
                        g_str_ble.ui8_tx_buf[ui16_index++] = buf[i];
			USART_SendData(BLE_UART, g_str_ble.ui8_tx_buf[ui16_index-1]);
			
			while (USART_GetFlagStatus(BLE_UART, USART_FLAG_TXE) == RESET)
			{}
                }
        }
#endif
}
#endif //ENABLE_BLE_UART
