/*
	This file is part of EqualizerAPO, a system-wide equalizer.
	Copyright (C) 2014  Jonas Thedering

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along
	with this program; if not, write to the Free Software Foundation, Inc.,
	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "global_define.h"

#ifdef ENABLE_EQ
void init_equalize(void)
{
	memset(&g_str_eq, 0, sizeof(eq_struct_t));

	//optimal2?
	if(!allfilter1)
	{
		allfilter1 = (biquadfilter_t *)malloc(sizeof(biquadfilter_t));
		allfilter1->freq = 50.0;
		allfilter1->dbGain = -4.0;//-1.0;
		allfilter1->bandwidthOrQOrS = 1;
		allfilter1->type = HIGH_PASS;
		allfilter1->isBandwidth = 0;
		allfilter1->isCornerFreq = 0;
		biquad_init(allfilter1);
	}
	if(!allfilter2)
	{
		allfilter2 = (biquadfilter_t *)malloc(sizeof(biquadfilter_t));
		allfilter2->freq = 500.0;
		allfilter2->dbGain = -5.0;//-0.5;
		allfilter2->bandwidthOrQOrS = 0.1;
		allfilter2->type = PEAKING;
		allfilter2->isBandwidth = 0;
		allfilter2->isCornerFreq = 0;
		biquad_init(allfilter2);
	}
	if(!allfilter3)
	{
		allfilter3 = (biquadfilter_t *)malloc(sizeof(biquadfilter_t));
		allfilter3->freq = 1000.0;
		allfilter3->dbGain = 8;//6;
		allfilter3->bandwidthOrQOrS = 0.1;
		allfilter3->type = PEAKING;
		allfilter3->isBandwidth = 0;
		allfilter3->isCornerFreq = 0;
		biquad_init(allfilter3);
	}
	if(!allfilter4)
	{
		allfilter4 = (biquadfilter_t *)malloc(sizeof(biquadfilter_t));
		allfilter4->freq = 1200.0;
		allfilter4->dbGain = 5;//0;
		allfilter4->bandwidthOrQOrS = 1;
		allfilter4->type = PEAKING;
		allfilter4->isBandwidth = 0;
		allfilter4->isCornerFreq = 0;
		biquad_init(allfilter4);
	}
	if(!allfilter5)
	{
		allfilter5 = (biquadfilter_t *)malloc(sizeof(biquadfilter_t));
		allfilter5->freq = 1400;
		allfilter5->dbGain = -11.0;//-9.0;
		allfilter5->bandwidthOrQOrS = 0.1;
		allfilter5->type = LOW_PASS;
		allfilter5->isBandwidth = 0;
		allfilter5->isCornerFreq = 0;
		biquad_init(allfilter5);
	}
	if(!allfilter6)
	{
		allfilter6 = (biquadfilter_t *)malloc(sizeof(biquadfilter_t));
		allfilter6->freq = 1800.0;
		allfilter6->dbGain = -11;//-10;
		allfilter6->bandwidthOrQOrS = 1;
		allfilter6->type = PEAKING;
		allfilter6->isBandwidth = 0;
		allfilter6->isCornerFreq = 0;
		biquad_init(allfilter6);
	}
}


void free_equalize(void)
{
	if(allfilter1)
	{
		free(allfilter1);
		allfilter1 = NULL;
	}
	
	if(allfilter2)
	{
		free(allfilter2);
		allfilter2 = NULL;
	}
	
	if(allfilter3)
	{
		free(allfilter3);
		allfilter3 = NULL;
	}
	
	if(allfilter4)
	{
		free(allfilter4);
		allfilter4 = NULL;
	}
	
	if(allfilter5)
	{
		free(allfilter5);
		allfilter5 = NULL;
	}
	
	if(allfilter6)
	{
		free(allfilter6);
		allfilter6 = NULL;
	}
}

void equalizer(void)
{
	int16_t i16_buf[BUFSIZE*2] = {0,};
	uint16_t ui16_buf_size = 0;
	uint32_t ui32_data = 0;
	
	g_str_eq.ui16_frame_no = (!g_str_audio.ui16_rx_frame_no)? (AUDIO_FRAME_BUFFER - 1):(g_str_audio.ui16_rx_frame_no - 1);
	
	if(g_str_bitflag.b1_eq_enable)
	{
		if(g_str_ble_status.audio_type_status == AUDIO_HFP)
		{
			if( g_str_bitflag.b1_fe_enable )//using fe buffer
			{
				ui16_buf_size = BUFSIZE;
				for (int j = 0; j < ui16_buf_size; j++)
				{
					i16_buf[j] = (int16_t)g_str_fe.ui16_data[g_str_eq.ui16_frame_no][j<<1];
				}
			}
			else//using real audio buffer
			{
				if(g_str_audio.ui16_audio_type == AUDIO_FREQ_8K)
				{
					ui16_buf_size = BUFSIZE;
					for (int j = 0; j < ui16_buf_size; j++)
					{
						ui32_data = g_str_audio.ui16_data[g_str_eq.ui16_frame_no][j<<1] + g_str_audio.ui16_data[g_str_eq.ui16_frame_no][(j<<1)+1];
						i16_buf[j] = ui32_data;
					}
				}
				else if(g_str_audio.ui16_audio_type == AUDIO_FREQ_16K)
				{
					ui16_buf_size = BUFSIZE;
					for (int j = 0; j < ui16_buf_size; ++j)
					{
						ui32_data = g_str_audio.ui16_data[g_str_eq.ui16_frame_no][j<<2] + g_str_audio.ui16_data[g_str_eq.ui16_frame_no][(j<<2)+1];
						i16_buf[j] = ui32_data;
					}
				}
			}
			
			biquadfilter_process(allfilter1, i16_buf, i16_buf, ui16_buf_size);
			biquadfilter_process(allfilter2, i16_buf, i16_buf, ui16_buf_size);
			biquadfilter_process(allfilter3, i16_buf, i16_buf, ui16_buf_size);
			biquadfilter_process(allfilter4, i16_buf, i16_buf, ui16_buf_size);
			biquadfilter_process(allfilter5, i16_buf, i16_buf, ui16_buf_size);
			biquadfilter_process(allfilter6, i16_buf, i16_buf, ui16_buf_size);
			
			for (int j = 0; j < ui16_buf_size; j++)
			{
				g_str_eq.ui16_data[g_str_eq.ui16_frame_no][j<<1] = (uint16_t)i16_buf[j];
				g_str_eq.ui16_data[g_str_eq.ui16_frame_no][(j<<1)+1] = (uint16_t)i16_buf[j];
			}
		}
		else//a2dp
		{
			ui16_buf_size = (BUFSIZE << 1);
			if( g_str_bitflag.b1_fe_enable )//using fe buffer
			{
				for (int j = 0; j < ui16_buf_size; j++)
				{
					i16_buf[j] = (int16_t)g_str_fe.ui16_data[g_str_eq.ui16_frame_no][j];
				}
			}
			else//using real audio buffer
			{
				for (int j = 0; j < ui16_buf_size; j++)
				{
					i16_buf[j] = (int16_t)g_str_audio.ui16_data[g_str_eq.ui16_frame_no][j];
				}
			}

			//biquadfilter_process(allfilter1, i16_buf, i16_buf, ui16_buf_size);
			//biquadfilter_process(allfilter2, i16_buf, i16_buf, ui16_buf_size);
			//biquadfilter_process(allfilter3, i16_buf, i16_buf, ui16_buf_size);
			//biquadfilter_process(allfilter4, i16_buf, i16_buf, ui16_buf_size);
			//biquadfilter_process(allfilter5, i16_buf, i16_buf, ui16_buf_size);
			//biquadfilter_process(allfilter6, i16_buf, i16_buf, ui16_buf_size);
			
			for (int j = 0; j < ui16_buf_size; j++)
			{
				g_str_eq.ui16_data[g_str_eq.ui16_frame_no][j] = (uint16_t)i16_buf[j];
			}
		}
	}
	else
	{
		if( g_str_bitflag.b1_fe_enable )
			memcpy((uint16_t *)&g_str_eq.ui16_data[g_str_eq.ui16_frame_no], (uint16_t *)&g_str_fe.ui16_data[g_str_eq.ui16_frame_no], sizeof(g_str_fe.ui16_data[g_str_eq.ui16_frame_no]));
		else
		{
			//hfp
			if(g_str_ble_status.audio_type_status == AUDIO_HFP)
			{
				if(g_str_audio.ui16_audio_type == AUDIO_FREQ_8K)
				{
					memcpy((uint16_t *)&g_str_eq.ui16_data[g_str_eq.ui16_frame_no], (uint16_t *)&g_str_audio.ui16_data[g_str_eq.ui16_frame_no], sizeof(g_str_audio.ui16_data[g_str_eq.ui16_frame_no]));
				}
				else if(g_str_audio.ui16_audio_type == AUDIO_FREQ_16K)
				{
					ui16_buf_size = (BUFSIZE << 1);
					for (int j = 0; j < ui16_buf_size; ++j)
					{
						ui32_data = g_str_audio.ui16_data[g_str_eq.ui16_frame_no][j<<1] + g_str_audio.ui16_data[g_str_eq.ui16_frame_no][(j<<1)+1];
						g_str_eq.ui16_data[g_str_eq.ui16_frame_no][j] = ui32_data;
					}
				}
			}
			//a2dp
			else
			{
				memcpy((uint16_t *)&g_str_eq.ui16_data[g_str_eq.ui16_frame_no], (uint16_t *)&g_str_audio.ui16_data[g_str_eq.ui16_frame_no], sizeof(g_str_audio.ui16_data[g_str_eq.ui16_frame_no]));
			}
		}
	}
	
	if(++g_str_eq.ui16_frame_no >= AUDIO_FRAME_BUFFER)
		g_str_eq.ui16_frame_no = 0;

	//audio_tx_start((uint32_t *)&g_str_eq.ui16_data, g_str_eq.ui16_frame_no);
	audio_tx_start((uint32_t *)&g_str_eq.ui16_data, g_str_audio.ui16_rx_frame_no);
}

void removeDenormals(biquad_t *bq)
{
	if (IS_DENORMAL(bq->x1))	bq->x1 = 0.0;
	if (IS_DENORMAL(bq->x2))	bq->x2 = 0.0;
	if (IS_DENORMAL(bq->y1))	bq->y1 = 0.0;
	if (IS_DENORMAL(bq->y2))	bq->y2 = 0.0;
}

inline double biquad_process(biquad_t *bq, short sample)
{
	// changed order of additions leads to better pipelining
	float srca[5];
	float srcb[5];
	float dst[5];
	double result = 0;
	
	srca[0] = (float)bq->a0;
	srca[1] = (float)bq->a[1];
	srca[2] = (float)bq->a[0];
	srca[3] = (float)-bq->a[3];
	srca[4] = (float)-bq->a[2];
	
	srcb[0] = (float)sample;
	srcb[1] = (float)bq->x2;
	srcb[2] = (float)bq->x1;
	srcb[3] = (float)bq->y2;
	srcb[4] = (float)bq->y1;
	arm_mult_f32(srca,srcb,dst,5);
	
	result = (double)(dst[0] + dst[1] + dst[2] + dst[3] + dst[4]);

	bq->x2 = bq->x1;
	bq->x1 = sample;

	bq->y2 = bq->y1;
	bq->y1 = result;

	return result;
}

inline void biquadfilter_process(biquadfilter_t *filter, short* output, short* input, unsigned frameCount)
{
	biquad_t bq = filter->biquads;

	short* inputChannel = input;
	short* outputChannel = output;

	for (unsigned j = 0; j < frameCount; j++)
	{
		outputChannel[j] = (short)biquad_process(&bq, inputChannel[j]);
	}

	removeDenormals(&bq);
	filter->biquads = bq;
}

void biquad_init(biquadfilter_t *filter)
{
	double A;
	
	if (filter->isCornerFreq && (filter->type == LOW_SHELF || filter->type == HIGH_SHELF))
	{
		// frequency adjustment for DCX2496
		//double centerFreqFactor = pow(10.0, abs(filter->dbGain) / 80.0 / filter->bandwidthOrQOrS);
		double centerFreqFactor = 0.0;
		if(filter->dbGain == 0.0)
			centerFreqFactor = 1.0;
		else
			centerFreqFactor = (double)powf((float)10.0, fabs(filter->dbGain) * (float)0.0125 / (float)filter->bandwidthOrQOrS);
		
		if (filter->type == LOW_SHELF)
			filter->freq *= centerFreqFactor;
		else
			filter->freq /= centerFreqFactor;
	}

	if(filter->dbGain == 0.0)
	{
		A = 1.0;
	}
	else
	{
		if (filter->type == PEAKING || filter->type == LOW_SHELF || filter->type == HIGH_SHELF)
			A = pow(10, filter->dbGain * 0.025);
		else
			A = pow(10, filter->dbGain * 0.05);
	}

	double omega = 0;
	
	switch(g_str_audio.ui16_audio_type)
	{
	case AUDIO_FREQ_8K:	omega = filter->freq * 0.000785398163397448309615;		break;
	case AUDIO_FREQ_16K:	omega = filter->freq * 0.0003926990816987241548075;		break;
	case AUDIO_FREQ_44_1K:	omega = filter->freq * 0.00014247585730565955729977324257639;	break;
	case AUDIO_FREQ_48K:	omega = filter->freq * 0.00013089969389957471826916666665;	break;
	}

	double sn = arm_sin_f32((float32_t)omega);
	double cs = arm_cos_f32((float32_t)omega);

	double alpha;
	double beta = -1;

	if (filter->type == LOW_SHELF || filter->type == HIGH_SHELF) // S
	{
		float32_t pre_cal = (A + 1 / A) * (1 / filter->bandwidthOrQOrS - 1) + 2;
		float32_t sqrt_out1;
		float32_t sqrt_out2;
		arm_sqrt_f32(pre_cal, &sqrt_out1);
		arm_sqrt_f32(A, &sqrt_out2);
		beta = sqrt_out2 * sn * sqrt_out1;
	}
	else if (filter->isBandwidth) // BW
		alpha = sn * sinh(M_LN2 / 2 * filter->bandwidthOrQOrS * omega / sn);
	else // Q
		alpha = sn * 0.5 / filter->bandwidthOrQOrS;

	double b0, b1, b2, a0, a1, a2;

	switch (filter->type)
	{
	case LOW_PASS:
		b0 = (1 - cs) / 2;
		b1 = 1 - cs;
		b2 = (1 - cs) / 2;
		a0 = 1 + alpha;
		a1 = -2 * cs;
		a2 = 1 - alpha;
		break;
	case HIGH_PASS:
		b0 = (1 + cs) / 2;
		b1 = -(1 + cs);
		b2 = (1 + cs) / 2;
		a0 = 1 + alpha;
		a1 = -2 * cs;
		a2 = 1 - alpha;
		break;
	case BAND_PASS:
		b0 = alpha;
		b1 = 0;
		b2 = -alpha;
		a0 = 1 + alpha;
		a1 = -2 * cs;
		a2 = 1 - alpha;
		break;
	case NOTCH:
		b0 = 1;
		b1 = -2 * cs;
		b2 = 1;
		a0 = 1 + alpha;
		a1 = -2 * cs;
		a2 = 1 - alpha;
		break;
	case ALL_PASS:
		b0 = 1 - alpha;
		b1 = -2 * cs;
		b2 = 1 + alpha;
		a0 = 1 + alpha;
		a1 = -2 * cs;
		a2 = 1 - alpha;
		break;
	case PEAKING:
		b0 = 1 + (alpha * A);
		b1 = -2 * cs;
		b2 = 1 - (alpha * A);
		a0 = 1 + (alpha / A);
		a1 = -2 * cs;
		a2 = 1 - (alpha / A);
		break;
	case LOW_SHELF:
		b0 = A * ((A + 1) - (A - 1) * cs + beta);
		b1 = 2 * A * ((A - 1) - (A + 1) * cs);
		b2 = A * ((A + 1) - (A - 1) * cs - beta);
		a0 = (A + 1) + (A - 1) * cs + beta;
		a1 = -2 * ((A - 1) + (A + 1) * cs);
		a2 = (A + 1) + (A - 1) * cs - beta;
		break;
	case HIGH_SHELF:
		b0 = A * ((A + 1) + (A - 1) * cs + beta);
		b1 = -2 * A * ((A - 1) + (A + 1) * cs);
		b2 = A * ((A + 1) + (A - 1) * cs - beta);
		a0 = (A + 1) - (A - 1) * cs + beta;
		a1 = 2 * ((A - 1) - (A + 1) * cs);
		a2 = (A + 1) - (A - 1) * cs - beta;
		break;
	}
	
	filter->biquads.a0 = b0 / a0;
	filter->biquads.a[0] = b1 / a0;
	filter->biquads.a[1] = b2 / a0;
	filter->biquads.a[2] = a1 / a0;
	filter->biquads.a[3] = a2 / a0;

	filter->biquads.x1 = 0;
	filter->biquads.x2 = 0;
	filter->biquads.y1 = 0;
	filter->biquads.y2 = 0;
}
#endif