#include "global_define.h"

#ifdef ENABLE_EQ
void equalizer( void )
{
	if( g_str_audio.ui16_rx_frame_no )
	{
		g_str_eq.ui16_eq_frame_idx = 0;
	}
	else
	{
		g_str_eq.ui16_eq_frame_idx = 1;
	}
	
	for( int i = 0; i < AUDIO_FRAME_SIZE; i++ )
	{
		g_str_audio.ui16_send_buf[g_str_audio.ui16_rx_frame_no][i] = g_str_audio.ui16_save_buf[g_str_eq.ui16_eq_frame_idx][i];
	}
	
	if( g_str_audio.ui16_rx_frame_no == 0 )
	{
		//start_output
	}
}
#endif