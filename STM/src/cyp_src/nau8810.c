#include "global_define.h"

#ifdef ENABLE_NAU8810
nau8810_register_e nau8810_register;

uint16_t fjdfjsdklfjal[54] = 
{POWER_MANAGEMENT_1,
POWER_MANAGEMENT_2,
POWER_MANAGEMENT_3,
AUDIO_INTERFACE,
COMPANDING,
CLOCK_CONTROL_1,
CLOCK_CONTROL_2,
DAC_CONTROL,
DAC_VOLUME,
ADC_CONTROL,
ADC_VOLUME,
EQ1_LOW_CUTOFF,
EQ2_PEAK1,
EQ3_PEAK2,
EQ4_PEAK3,
EQ5_HIGH_CUTOFF,
DAC_LIMITER_1,
DAC_LIMITER_2,
NOTCH_FILTER0_HIGH,
NOTCH_FILTER0_LOW,
NOTCH_FILTER1_HIGH,
NOTCH_FILTER1_LOW,
ALC_CONTROL_1,
ALC_CONTROL_2,
ALC_CONTROL_3,
NOISE_GATE,
PLL_N_CONTROL,
PLL_K_1,
PLL_K_2,
PLL_K_3,
ATTENUATION_CONTROL,
INPUT_CONTROL,
PGA_GAIN,
ADC_BOOST,
OUTPUT_CONTROL,
MIXER_CONTROL,
SPKOUT_VOLUME,
MONO_MIXER_CONTROL,
POWER_MANAGEMENT_4,
TIME_SLOT,
ADCOUT_DRIVE,
SILICON_REVISION,
TWO_WIRE_ID,
ADDITIONAL_ID,
RESERVED,
HIGH_VOLUME_CONTROL,
ALC_ENHANCEMENT_1,
ALC_ENHANCEMENT_2,
ADDITIONAL_IF_CONTROL,
POWER_TIE_OFF_CONTROL,
AGC_P2P_DETECTOR,
AGC_PEAK_DETECTOR,
CONTROL_AND_STATUS,
OUTPUT_TIE_OFF_CONTROL};

void codec_init(void)
{
	uint8_t data[54][2] = {0,};
	uint8_t dsa[2];
	uint8_t idx = 0;
	
	dsa[0] = 0x01;
	dsa[1] = 0xff;
	nau8810_write(SOFTWARE_RESET, 2, dsa);
	nau8810_read(SOFTWARE_RESET, 2, dsa);
	for(idx = 0;idx < 54;idx++)
	{
		nau8810_read(fjdfjsdklfjal[idx], 2, dsa);
		data[idx][0] = dsa[0];
		data[idx][1] = dsa[1];
	}
	
	idx = 0;
#if 0
	

	data[0] = 0x01;
	data[1] = 0xff;
	nau8810_write(MONO_MIXER_CONTROL, 2, data);//DACMOUT[0],BYPMOUT[1](x) 0x38
	nau8810_read(MONO_MIXER_CONTROL, 2, data);

	data[0] = 0x01;
	data[1] = 0xff;
	nau8810_write(MIXER_CONTROL, 2, data);//DACSPK[0],BYPSPK[1](x) 0x32
	nau8810_read(MIXER_CONTROL, 2, data);

	data[0] = 0x01;
	data[1] = 0xff;
	nau8810_write(POWER_MANAGEMENT_3, 2, data);//MOUTMXEN, SPKMXEN, DACEN[0] 0x03
	nau8810_read(POWER_MANAGEMENT_3, 2, data);

	data[0] = 0x01;
	data[1] = 0xff;
	nau8810_write(COMPANDING, 2, data);//ADDAP[0](x) 0x05
	nau8810_read(COMPANDING, 2, data);

	data[0] = 0x01;
	data[1] = 0xff;
	nau8810_write(COMPANDING, 2, data);//ADDAP[0](x) 0x05
	nau8810_read(COMPANDING, 2, data);

	data[0] = 0x01;
	data[1] = 0xff;
	nau8810_write(HIGH_VOLUME_CONTROL, 2, data);//SPKMOUT[5] 0x45
	nau8810_read(HIGH_VOLUME_CONTROL, 2, data);

	data[0] = 0x01;
	data[1] = 0xff;
	nau8810_write(SPKOUT_VOLUME, 2, data);//SPKGAIN[5:0] 0x36
	nau8810_read(SPKOUT_VOLUME, 2, data);

	data[0] = 0x01;
	data[1] = 0xff;
	nau8810_write(OUTPUT_CONTROL, 2, data);//MOUTBST[3] SPKBST[2] 0x31
	nau8810_read(OUTPUT_CONTROL, 2, data);
#endif
}

int nau8810_read(unsigned char reg_addr, unsigned short len, unsigned char *data_ptr)
{
	return Sensors_I2C_ReadRegister(NAU8810_I2C, NAU8810_ADDR, (reg_addr << 1), len, data_ptr);
}

int nau8810_write(unsigned char reg_addr, unsigned short len, unsigned char *data_ptr)
{
	uint8_t reg = reg_addr;
	uint8_t length = len;
	uint8_t data = 0;
	
	reg = ( ( reg << 1 ) | ( data_ptr[0] & 0x01 ) );
	data = data_ptr[1];
	
	return Sensors_I2C_WriteRegister(NAU8810_I2C, NAU8810_ADDR, reg, length - 1, &data);
}
#endif //ENABLE_NAU8810