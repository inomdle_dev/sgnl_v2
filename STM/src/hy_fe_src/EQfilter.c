#include "global_define.h"
#include "tm_stm32f4_fft.h"


#ifdef ENABLE_HY_FE

//float cgeq_filter[BAND_L][NS1], **A;
float cgeq_filter[BAND_L][NS1], A[BAND_L][BAND_L];
//TM_FFT_F32_t FFT;    /*!< FFT structure */


void cgeq_init(void)
{
	int i, j;
	// EQ parameter
#if 0
	//original fe+eq HYU
	float cgeq_gain[BAND_L] = {1.9275, 1.9953, 1.9275, 1.9275, 1.3804, 1.4962, 3.9811, 3.9811};
	float Fc[BAND_L] = {31.25, 62.50, 125, 250, 500, 1000, 2000, 4000};
	float BW[BAND_L] = {46.88, 93.75, 187.5, 375, 750, 1500, 3000, 5580};
#else
	//white noise로 체크 줄인후 fine tuning
	float cgeq_gain[BAND_L] = { 2.0275, 2.3953, 2.4275, 2.4275, 2.5804, 1.4962, 0.8001, 1.0001 };
	float Fc[BAND_L] = { 31.25, 62.50, 125, 250, 500, 1000, 2000, 4000 };
	float BW[BAND_L] = { 46.88, 93.75, 187.5, 375, 750, 1500, 3000, 500 };
#endif

	float g[BAND_L] = {0,};
	float G_opt[BAND_L] = {0,};
	
	// CGEQ cgeq_filter: cgeq_filter[BAND_L][NS1]
	// Optimal gain matrix: A[BAND_L][BAND_L]

	// Initialization peak/notch cgeq_filter
	init_PNfilter(cgeq_gain, Fc, BW);

	// Exceptional condition
	for(i = 0;i < BAND_L;i++)
	{
		for(j = 0;j < NS1;j++)
		{
			if(isfinite(cgeq_filter[i][j]) == 0)
				cgeq_filter[i][j] = cgeq_filter[i][j - 1]*(cgeq_filter[i][j - 1]/cgeq_filter[i][j - 2]);
		}
	}

	// Convert dB scale about cgeq_filter
	for(i = 0;i < BAND_L;i++)
	{
		for(j = 0;j < NS1;j++)
		{
			cgeq_filter[i][j] = 20*log10f(cgeq_filter[i][j]);
		}
		//g[i] = 20*log10(cgeq_gain[i]);
		g[i] = 20*log10f(cgeq_gain[i]);
	}
	
	// Calculate optimal gain (in dB)
	calculate_gain_opt(g, A);
	for(i = 0;i < BAND_L;i++)
		G_opt[i] = powf(10, g[i]*0.05);

	// Convert cgeq_filter about optimal gain
	// Generate optimal gain cgeq_filter
	init_PNfilter(G_opt, Fc, BW);

	// Exceptional condition
	for(i = 0;i < BAND_L;i++)
	{
		for(j = 0;j < NS1;j++)
		{
			if(isfinite(cgeq_filter[i][j]) == 0)
				cgeq_filter[i][j] = cgeq_filter[i][j - 1]*(cgeq_filter[i][j - 1]/cgeq_filter[i][j - 2]);
		}
	}
}

void init_PNfilter(float G[], float *freq, float *bw)
{
	int i, j;
	
	float32_t oFFT_Buf[NS2] = {0,};
	uint16_t idx = 0;

#if 01
	float32_t *TT = NULL;
	float32_t *fft_b = NULL;
	float32_t *fft_a = NULL;
	
	TT = (float32_t *)malloc(sizeof(float32_t)*(NS2*2));
	memset(TT, 0x00, sizeof(float32_t)*(NS2*2));

	fft_b = (float32_t *)malloc(sizeof(float32_t)*(NS2*2));
	memset(fft_b, 0x00, sizeof(float32_t)*(NS2*2));

	fft_a = (float32_t *)malloc(sizeof(float32_t)*(NS2*2));
	memset(fft_a, 0x00, sizeof(float32_t)*(NS2*2));
#else
	float32_t TT[NS2*2] = {0,};
	float32_t fft_b[NS2*2] = {0,};
	float32_t fft_a[NS2*2] = {0,};
#endif
	// cgeq_filter coefficients
#if 0
	float *b = NULL, *a = NULL;
	
	b = (float *)malloc(sizeof(float)*NS2);
	a = (float *)malloc(sizeof(float)*NS2);
#else
	float b[NS2] = {0,};
	float a[NS2] = {0,};
#endif
	/* Init FFT, FFT_SIZE define is used for FFT_SIZE, samples count is FFT_SIZE * 2, don't use malloc for memory allocation */
	if(TM_FFT_Init_F32(&FFT, NS2, 0, 0) != 0)
		return;
	
	for(i = 0;i < BAND_L;i++)
	{
		memset(b, 0x00, sizeof(float)*NS2);
		memset(a, 0x00, sizeof(float)*NS2);
		
		init_PNfilter_coefficients(G[i], freq[i], bw[i], (float)(freq[i])/(float)bw[i], b, a);
		
#ifdef HYU_FFT
		r_fft(b, 1);
		r_fft(a, 1);
#else
		for(idx = 0; idx < NS2; idx++)
		{
			fft_b[idx] = b[idx]*0.0078125;// 1/128
			fft_a[idx] = a[idx]*0.0078125;// 1/128
		}
		
		/* We didn't used malloc for allocation, so we have to set pointers ourself */
		/* Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part */
		/* Output buffer must be FFT_SIZE in length */
		memcpy(TT, fft_b, sizeof(float32_t)*(NS2*2));
		memset(fft_b, 0x00, sizeof(float32_t)*(NS2*2));
		for(idx = 0; idx < NS2; idx++)
		{
			fft_b[(idx << 1)] = TT[idx];//real
			fft_b[(idx << 1) + 1] = 0;//imaginary
		}
		TM_FFT_SetBuffers_F32(&FFT, fft_b, oFFT_Buf);
		
		/* Do FFT on signal, values at each bin and calculate max value and index where max value happened */
		TM_FFT_Process_F32(&FFT);//real:128, imag:128
		fft_b[1] = fft_b[NS2];
		
		/* We didn't used malloc for allocation, so we have to set pointers ourself */
		/* Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part */
		/* Output buffer must be FFT_SIZE in length */
		memcpy(TT, fft_a, sizeof(float32_t)*(NS2*2));
		memset(fft_a, 0x00, sizeof(float32_t)*(NS2*2));
		for(idx = 0; idx < NS2; idx++)
		{
			fft_a[(idx << 1)] = TT[idx];//real
			fft_a[(idx << 1) + 1] = 0;//imaginary
		}
		TM_FFT_SetBuffers_F32(&FFT, fft_a, oFFT_Buf);
		
		/* Do FFT on signal, values at each bin and calculate max value and index where max value happened */
		TM_FFT_Process_F32(&FFT);//real:128, imag:128
		fft_a[1] = fft_a[NS2];
		
		memcpy(b, fft_b, sizeof(b));
		memcpy(a, fft_a, sizeof(a));
#endif
		
		for(j = 0;j < NS2;j++){
			b[j] = b[j]*NS1;
			a[j] = a[j]*NS1;
		}

		cgeq_filter[i][0] = fabsf(b[0])/fabsf(a[0]);
		cgeq_filter[i][NS1 - 1] = fabsf(b[1])/fabsf(a[1]);
		for(j = 1;j < NS1 - 1;j++)
		{
			cgeq_filter[i][j] = sqrtf(b[(j<<1)]*b[(j<<1)] + b[(j<<1) + 1]*b[(j<<1) + 1])/sqrtf(a[(j<<1)]*a[(j<<1)] + a[(j<<1) + 1]*a[(j<<1) + 1]);
			if(cgeq_filter[i][j] == 0)
				cgeq_filter[i][j] = 2.2204e-16;
			
			cgeq_filter[i][j] = fabsf(cgeq_filter[i][j]);
		}
		
		if(cgeq_filter[i][0] == 0)
			cgeq_filter[i][0] = 2.2204e-16;
		
		if(cgeq_filter[i][NS1 - 1] == 0)
			cgeq_filter[i][NS1 - 1] = 2.2204e-16;
		
		cgeq_filter[i][0] = fabsf(cgeq_filter[i][0]);
		cgeq_filter[i][NS1 - 1] = fabsf(cgeq_filter[i][NS1 - 1]);
	}
	
#if 0
	free(b);
	free(a);
#else
	free(TT);
	if(TT)
		TT = NULL;
	
	free(fft_b);
	if(fft_b)
		fft_b = NULL;
	
	free(fft_a);
	if(fft_a)
		fft_a = NULL;
#endif
}

void init_PNfilter_coefficients(float G, float freq, float bw, float Q, float *b, float *a){
	float GB = 0;
	float beta = 0;
	
	freq = freq*0.0007853981633974483095f;//freq*(PI2/FS)
	bw = (freq/Q)*0.5;
	
	GB = G*powf(G, (float)-0.7);
	
	if(G == 1)
		beta = arm_cos_f32((float)bw)/arm_sin_f32((float)bw);
	else
		beta = sqrtf((fabs(GB*GB - 1))/(fabs(G*G - GB*GB)))*(arm_sin_f32((float)bw)/arm_cos_f32((float)bw));
	
	b[0] = 1 + G*beta;
	b[1] = -2*arm_cos_f32((float)freq);
	b[2] = (1 - G*beta);
	
	a[0] = 1 + beta;
	a[1] = -2*arm_cos_f32((float)freq);
	a[2] = (1 - beta);
}

void calculate_gain_opt(float g[], float A[][BAND_L])
{
	int i = 0, j = 0;
	int fc[BAND_L] = {1, 2, 4, 8, 16, 32, 64, 127};
	int minus_filt_flag = 0;
	float max = 0;
	float g_p = 0;
	float g_opt[BAND_L] = {0,};
	
	for(i = 0;i < BAND_L;i++){
		if(g[i] < 0){
			minus_filt_flag = 1;
			for(j = 0;j < NS1;j++)
				cgeq_filter[i][j] = -(cgeq_filter[i][j]);
		}

		max = cgeq_filter[i][0];

		for(j = 1;j < NS1;j++){
			if(cgeq_filter[i][j] > max){
				max = cgeq_filter[i][j];
			}
		}
		
		g_p = max;
		for(j = 0;j < NS1;j++)
			cgeq_filter[i][j] /= g_p;
		
		if(minus_filt_flag == 1){
			for(j = 0;j < NS1;j++)
				cgeq_filter[i][j] = -cgeq_filter[i][j];
			minus_filt_flag = 0;
		}
	}

	for(i = 0;i < BAND_L;i++)
	{
		for(j = 0;j < BAND_L;j++)
		{
			A[i][j] = cgeq_filter[i][fc[j]];
			if(isfinite(A[i][j]) == 0)
				A[i][j] = 1;
		}
	}
	
	cofactor(A, BAND_L);
	
	for(i = 0;i < BAND_L;i++)
	{
		g_opt[i] = 0;
		for(j = 0;j < BAND_L;j++)
		{
			g_opt[i] += A[i][j]*g[j];
		}
	}
	
	for(i = 0;i < BAND_L;i++)
		g[i] = g_opt[i];
}

float determinant(float A[][BAND_L], int k)
{
	float s;
	float det;
	int i,j,m,n,c;

#if 0
	float **b;
	b = (float **)malloc(sizeof(float)*BAND_L);
	for(i = 0;i < BAND_L;i++)
		b[i] = (float *)malloc(sizeof(float)*BAND_L);
#else
	float b[BAND_L][BAND_L] = {0,};
#endif
	
	s = 1;
	det = 0;
	
	if(k == 1)
	{ 
		return A[0][0];
	}
	else
	{
		det = 0;
		for(c = 0;c < k;c++)
		{
			m = 0;
			n = 0;
			for(i = 0;i < k;i++)
			{ 
				for(j = 0;j < k;j++)
				{ 
					b[i][j] = 0;
					if(i != 0 && j != c)
					{
						b[m][n] = A[i][j];
						if(n < (k - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
#if 0
			det = det + s*(A[0][c]*determinant(b, k - 1));
#else
			det = det + s*(A[0][c]*determinant(b, k - 1));
#endif
			s = -1*s;
		}
	}

#if 0
	for(i = 0;i < BAND_L;i++)
	{
		free(b[i]);
		if(b[i])
			b[i] = NULL;
	}
	free(b);
	if(b)
		b = NULL;
#else
#endif
	
	return det;
}

void cofactor(float A[][BAND_L],int f)
{ 
	int p, q, m, n, i, j;

#if 0
	float **b;
	float **fac;
	fac = (float **)malloc(sizeof(float)*BAND_L);
	for(i = 0;i < BAND_L;i++)
		fac[i] = (float *)malloc(sizeof(float)*BAND_L);
	
	b = (float **)malloc(sizeof(float)*BAND_L);
	for(i = 0;i < BAND_L;i++)
		b[i] = (float *)malloc(sizeof(float)*BAND_L);
#else
	float b[BAND_L][BAND_L] = {0,};
	float fac[BAND_L][BAND_L] = {0,};
#endif
	
	for(q = 0;q < f;q++)
	{ 
		for(p = 0;p < f;p++)
		{ 
			m = 0; 
			n = 0; 
			for(i = 0;i < f;i++)
			{
				for(j = 0;j < f;j++)
				{
					if(i != q && j != p)
					{
						b[m][n] = A[i][j];
						if(n < (f - 2))
							n++;
						else
						{
							n = 0;
							m++; 
						}
					}
				}
			}
			
#if 0
			fac[q][p] = pow(-1, q + p)*determinant(b, f - 1); 
#else
			fac[q][p] = ((q + p) & 1)? -1*determinant(b, f - 1):determinant(b, f - 1); 
#endif
		}
	}
	
	transpose(A, fac, f);
	
#if 0
	for(i = 0;i < BAND_L;i++)
	{
		free(fac[i]);
		if(fac[i])
			fac[i] = NULL;
	}
	free(fac);
	if(fac)
		fac = NULL;
	
	for(i = 0;i < BAND_L;i++)
	{
		free(b[i]);
		if(b[i])
			b[i] = NULL;
	}
	free(b);
	if(b)
		b = NULL;
#else
#endif
}

void transpose(float num[][BAND_L],float fac[][BAND_L],int r){ 
	int i,j;
	float d;
	
#if 0
	float **b, **inverse;
	b = (float **)malloc(sizeof(float)*BAND_L);
	for(i = 0;i < BAND_L;i++)
		b[i] = (float *)malloc(sizeof(float)*BAND_L);
	
	inverse = (float **)malloc(sizeof(float)*BAND_L);
	for(i = 0;i < BAND_L;i++)
		inverse[i] = (float *)malloc(sizeof(float)*BAND_L);
#else
	float b[BAND_L][BAND_L], inverse[BAND_L][BAND_L];
#endif
	
	for(i = 0;i < r;i++)
	{
		for(j = 0;j < r;j++)
		{
			b[i][j]=fac[j][i];
		}
	}
	
	d = determinant(num, r);
	for(i = 0;i < r;i++)
	{
		for(j = 0;j < r;j++)
		{
			inverse[i][j] = b[i][j]/d;
		}
	}
	
	for(i = 0;i < r;i++)
	{
		for(j = 0;j < r;j++)
		{
			num[i][j] = inverse[i][j];
		}
	}

#if 0
	for(i = 0;i < BAND_L;i++)
	{
		free(b[i]);
		if(b[i])
			b[i] = NULL;
	}
	free(b);
	if(b)
		b = NULL;
	
	for(i = 0;i < BAND_L;i++)
	{
		free(inverse[i]);
		if(inverse[i])
			inverse[i] = NULL;
	}
	free(inverse);
	if(inverse)
		inverse = NULL;
#else
#endif
} 
#endif