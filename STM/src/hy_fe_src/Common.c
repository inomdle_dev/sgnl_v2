#include "global_define.h"

#ifdef ENABLE_HY_FE

void preemphasis(int len,float *buf,float *prev){
	short k;
	float temp;
	
	for(k = 0;k < len;k++) {
		temp = buf[k];
		buf[k] = buf[k] - (ALPHA * *prev);
		*prev = temp;
	}
}

void deemphasis(int len,float *buf,float *prev){
	short k;

	for(k = 0;k < len;k++) {
		buf[k] = buf[k] + (ALPHA * *prev);
		*prev = buf[k];
	}
}

void BufferCopy(float *frombf, float *tobf, short num){
	short	k;

	for(k=0; k<num; k++){
		tobf[k] = frombf[k];
	}
}
#endif