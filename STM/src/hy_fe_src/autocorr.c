/*
********************************************************************************
*
*      GSM AMR-NB speech codec   R98   Version 7.6.0   December 12, 2001
*                                R99   Version 3.3.0                
*                                REL-4 Version 4.1.0                
*
********************************************************************************
*
*      File             : autocorr.c
*
********************************************************************************
*/
/*
********************************************************************************
*                         MODULE INCLUDE FILE AND VERSION ID
********************************************************************************
*/
#include "global_define.h"

#ifdef ENABLE_HY_FE
/*
**************************************************************************
*
*  Function    : autocorr
*  Purpose     : Compute autocorrelations of signal with windowing
*
**************************************************************************
*/
int16_t Autocorr (
    int16_t x[],            /* (i)    : Input signal (L_WINDOW)            */
    int16_t m,              /* (i)    : LPC order                          */
    int16_t r_h[],          /* (o)    : Autocorrelations  (msb)            */
    int16_t r_l[],          /* (o)    : Autocorrelations  (lsb)            */
    int16_t wind[]    /* (i)    : window for LPC analysis (L_WINDOW) */
)
{
    int16_t i, j, norm;
    int16_t y[FFTSIZE];
    int32_t sum;
    int16_t overfl, overfl_shft;

    /* Windowing of signal */
	
    for (i = 0; i < FFTSIZE; i++)
    {
        y[i] = mult_r (x[i], wind[i]);
    }

    /* Compute r[0] and test for overflow */

    overfl_shft = 0;

    do
    {
        overfl = 0;
        sum = 0L;

        for (i = 0; i < FFTSIZE; i++)
        {
            sum = L_mac (sum, y[i], y[i]);
        }

        /* If overflow divide y[] by 4 */

        if (L_sub (sum, MAX_32) == 0L)
        {
            overfl_shft = add (overfl_shft, 4);
            overfl = 1;

            for (i = 0; i < FFTSIZE; i++)
            {
                y[i] = shr (y[i], 2);
            }
        }
    }
    while (overfl != 0);
	
    sum = L_add (sum, 1L);             /* Avoid the case of all zeros */

    /* Normalization of r[0] */

    norm = norm_l (sum);
    sum = L_shl (sum, norm);
    L_Extract (sum, &r_h[0], &r_l[0]); /* Put in DPF format (see oper_32b) */

    /* r[1] to r[m] */
    for (i = 1; i <= m; i++)
    {
        sum = 0;

        for (j = 0; j < FFTSIZE - i; j++)
        {
            sum = L_mac (sum, y[j], y[j + i]);
        }

        sum = L_shl (sum, norm);
        L_Extract (sum, &r_h[i], &r_l[i]);
    }
	
    norm = sub (norm, overfl_shft);
	
    return norm;
}
#endif