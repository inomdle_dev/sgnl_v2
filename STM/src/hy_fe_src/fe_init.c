#include "global_define.h"

#ifdef ENABLE_HY_FE

#ifdef ENABLE_AGC
#define AGC_GAIN -36

buffer_t input_buf, output_buf;
buffer_t *pinput_buf, *poutput_buf;
svc_static_param_t svc_static_param;
svc_dynamic_param_t svc_dynamic_param;
void *pSvcStaticMem;
void *pSvcDynamicMem;
uint16_t log_db[16] = {0,};

void agc_init(void)
{
	if(pSvcStaticMem == NULL)
		pSvcStaticMem = (void *)malloc(svc_static_mem_size);	/* 0x8BC  */
	if(pSvcDynamicMem == NULL)
		pSvcDynamicMem = (void *)malloc(svc_dynamic_mem_size);	/* 0x1680 */
	
	input_buf.nb_bytes_per_Sample = 2;//2:16bit, 3:24bit, 4:32bit
	input_buf.nb_channels = 2;//L,R
	input_buf.mode = INTERLEAVED;//In case of stereo stream, left and right channels can be interleaved.
	input_buf.buffer_size = 160;
	pinput_buf = &input_buf;
	
	output_buf.nb_bytes_per_Sample = 2;//2:16bit, 3:24bit, 4:32bit
	output_buf.nb_channels = 2;//L,R
	output_buf.mode = INTERLEAVED;//In case of stereo stream, left and right channels can be interleaved.
	output_buf.buffer_size = 160;
	poutput_buf = &output_buf;
	
	RCC->AHB1ENR |= RCC_AHB1ENR_CRCEN;
	CRC->CR |= CRC_CR_RESET;
	if(svc_reset(pSvcStaticMem, pSvcDynamicMem) != SVC_ERROR_NONE)
	{
		while(1);
	}
	
	svc_static_param.delay_len   = 0;        /* look ahead buffer size */
	svc_static_param.joint_stereo = 0;        /* joint stereo flag */
	if(svc_setParam(&svc_static_param, pSvcStaticMem) != SVC_ERROR_NONE)
	{
		while(1);
	}
	
	
	/* SVC dynamic parameters that can be updated here every frame if required 
	*/
	uint16_t volume_step = VOLUME_LEVEL_INIT;
	float32_t sound_dB = 24.0;
	
	uint16_t i = 0;
	float32_t log_deno = 0.0;
	float32_t sample_term = 0.0;

	log_deno = (float32_t)(32767.0/(powf(10,(sound_dB*0.05))));
	sample_term = (32767.0-log_deno)/(float32_t)volume_step;
	for(i = 0;i<volume_step;i++)
	{
		log_db[i] = (uint16_t)(20.0*log10f(((sample_term * i) + log_deno) / log_deno));
	}
	
	//svc_getConfig(&svc_dynamic_param, pSvcStaticMem);
	svc_dynamic_param.target_volume_dB = 0; /* Target dB volume: 1/2dB steps*/
	svc_dynamic_param.mute = 0; /*Mute Flag*/
	svc_dynamic_param.enable_compr = 1; /* Enables the compression*/
	svc_dynamic_param.attack_time = 2103207220; /*Attack Time in Q31 format*/
	svc_dynamic_param.release_time = 2146924480; /*Release Time in Q31 format*/
	svc_dynamic_param.quality = 1; /*High Quality flag */
	
	if (svc_setConfig(&svc_dynamic_param, pSvcStaticMem) != SVC_ERROR_NONE)
	{
		while(1);
	}
}
#endif

void fe_init(void)
{
	memset(Previn_buf, 0x00, sizeof(Previn_buf));
	memset(Prevsynth_buf, 0x00, sizeof(Prevsynth_buf));

	memset(&g_str_fe, 0, sizeof(fe_struct_t));

#ifdef ENABLE_AGC
	agc_init();
#endif
}

void formant_enhancement( void )
{
	int16_t i16_buf16[FE_FRAME_L] = {0,};
	uint16_t j = 0;

	//hfp
	if(g_str_ble_status.audio_type_status == AUDIO_HFP)
	{
		if( g_str_audio.ui16_data_format == I2S_DataFormat_32b )
		{
			if(g_str_audio.ui16_audio_type == AUDIO_FREQ_8K)
			{
				for (j = 0; j < FE_FRAME_L; j++)//320 -> 80
				{
					i16_buf16[j] = (int16_t)(g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+1])
						+ (int16_t)(g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+2] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+3]);
				}
			}
			else if(g_str_audio.ui16_audio_type == AUDIO_FREQ_16K)
			{
				for (j = 0; j < FE_FRAME_L; ++j)//640 -> 80
				{
					i16_buf16[j] = (int16_t)(g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<3)] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<3)+1]) 
						+ (int16_t)(g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<3)+2] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<3)+3]);
				}
			}
		}
		else if( g_str_audio.ui16_data_format == I2S_DataFormat_16b )
		{
			if(g_str_audio.ui16_audio_type == AUDIO_FREQ_8K)
			{
				for (j = 0; j < FE_FRAME_L; j++)//160 -> 80
				{
					i16_buf16[j] = (int16_t)(g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<1)] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<1)+1]);
				}
			}
			else if(g_str_audio.ui16_audio_type == AUDIO_FREQ_16K)
			{
				for (j = 0; j < FE_FRAME_L; ++j)//320 -> 80
				{
					i16_buf16[j] = (int16_t)(g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+1])
						+ (int16_t)(g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+2] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+3]);
				}
			}
		}

		FEnh_func(i16_buf16);
	}

#if defined(ENABLE_AGC)
	uint16_t ui16_outbuf[FE_FRAME_L<<1];
	
	if(g_str_ble_status.audio_type_status == AUDIO_HFP)
	{
		for (j = 0; j < FE_FRAME_L; ++j)//80 -> 160
		{
			ui16_inbuf[(j<<1)+0] = (uint16_t)i16_buf16[j];
			ui16_inbuf[(j<<1)+1] = (uint16_t)i16_buf16[j];
		}
	}
	else
	{
		for (j = 0; j < FE_FRAME_L; ++j)//320 -> 160
		{
			ui16_inbuf[(j<<1)+0] = (int16_t)(g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+0] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+1]);
			ui16_inbuf[(j<<1)+1] = (int16_t)(g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+2] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+3]);
		}
	}
	
	/*SVC effect processing*/
	input_buf.data_ptr = (uint16_t *)&ui16_inbuf;
	pinput_buf = &input_buf;

	output_buf.data_ptr = (uint16_t *)&ui16_outbuf;
	poutput_buf = &output_buf;

	svc_getConfig(&svc_dynamic_param, pSvcStaticMem);
	svc_dynamic_param.target_volume_dB = (AGC_GAIN+(g_str_ble_status.volume_level_status*12));
	svc_setConfig(&svc_dynamic_param, pSvcStaticMem);
	svc_process(pinput_buf, poutput_buf, pSvcStaticMem);

	memcpy((uint16_t *)&g_str_fe.ui16_data[g_str_fe.ui16_frame_no], (uint16_t *)&ui16_outbuf, sizeof(ui16_outbuf));
	
#elif defined(ENABLE_VOL_SHIFT)
	uint16_t ui16_temp = 0;
	if(g_str_ble_status.audio_type_status == AUDIO_HFP)
	{
		for (j = 0; j < FE_FRAME_L; j++)//80 -> 160
		{
			g_str_fe.ui16_data[g_str_fe.ui16_frame_no][(j<<1)+0] = (uint16_t)i16_buf16[j];
			g_str_fe.ui16_data[g_str_fe.ui16_frame_no][(j<<1)+1] = (uint16_t)i16_buf16[j];
		}
	}
	else
	{
		for (j = 0; j < FE_FRAME_L; ++j)//320 -> 160
		{
			g_str_fe.ui16_data[g_str_fe.ui16_frame_no][(j<<1)+0] = (g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+0] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+1]);
			g_str_fe.ui16_data[g_str_fe.ui16_frame_no][(j<<1)+1] = (g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+2] + g_str_audio.ui16_data[g_str_fe.ui16_frame_no][(j<<2)+3]);
		}
	}
#endif
}
#endif