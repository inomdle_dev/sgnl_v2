/*
*****************************************************************************
*
*      GSM AMR-NB speech codec   R98   Version 7.6.0   December 12, 2001
*                                R99   Version 3.3.0                
*                                REL-4 Version 4.1.0                
*
*****************************************************************************
*
*      File             : levinson.c
*      Purpose          : Levinson-Durbin algorithm in double precision.
*                       : To compute the LP filter parameters from the
*                       : speech autocorrelations.
*
*****************************************************************************
*/

#include "global_define.h"

#ifdef ENABLE_HY_FE

int Levinson_init (LevinsonState **state)
{
  LevinsonState* s;
 
  if (state == (LevinsonState **) NULL){
      //fprintf(stderr, "Levinson_init: invalid parameter\n");
      return -1;
  }
  *state = NULL;
 
  /* allocate memory */
  if ((s= (LevinsonState *) malloc(sizeof(LevinsonState))) == NULL){
      //fprintf(stderr, "Levinson_init: can not malloc state structure\n");
      return -1;
  }
  
  Levinson_reset(s);
  *state = s;
  
  return 0;
}
 
/*************************************************************************
*
*  Function:   Levinson_reset
*  Purpose:    Initializes state memory to zero
*
**************************************************************************
*/
int Levinson_reset (LevinsonState *state)
{
  int16_t i;
  
  if (state == (LevinsonState *) NULL){
      //fprintf(stderr, "Levinson_reset: invalid parameter\n");
      return -1;
  }
  
  state->old_A[0] = 4096;
  for(i = 1; i < LPC_ORDER + 1; i++)
      state->old_A[i] = 0;
 
  return 0;
}
 
/*************************************************************************
*
*  Function:   Levinson_exit
*  Purpose:    The memory used for state memory is freed
*
**************************************************************************
*/
void Levinson_exit (LevinsonState **state)
{
  if (state == NULL || *state == NULL)
      return;
 
  /* deallocate memory */
  free(*state);
  *state = NULL;
  
  return;
}
 
/*************************************************************************
 *
 *   FUNCTION:  Levinson()
 *
 *   PURPOSE:  Levinson-Durbin algorithm in double precision. To compute the
 *             LP filter parameters from the speech autocorrelations.
 *
 *   DESCRIPTION:
 *       R[i]    autocorrelations.
 *       A[i]    filter coefficients.
 *       K       reflection coefficients.
 *       Alpha   prediction gain.
 *
 *       Initialisation:
 *               A[0] = 1
 *               K    = -R[1]/R[0]
 *               A[1] = K
 *               Alpha = R[0] * (1-K**2]
 *
 *       Do for  i = 2 to LPC_ORDER
 *
 *            S =  SUM ( R[j]*A[i-j] ,j=1,i-1 ) +  R[i]
 *
 *            K = -S / Alpha
 *
 *            An[j] = A[j] + K*A[i-j]   for j=1 to i-1
 *                                      where   An[i] = new A[i]
 *            An[i]=K
 *
 *            Alpha=Alpha * (1-K**2)
 *
 *       END
 *
 *************************************************************************/
int Levinson (
    int16_t old_A[],
    int16_t Rh[],       /* i : Rh[m+1] Vector of autocorrelations (msb) */
    int16_t Rl[],       /* i : Rl[m+1] Vector of autocorrelations (lsb) */
    int16_t A[],        /* o : A[m]    LPC coefficients  (m = 10)       */
    int16_t rc[]        /* o : rc[4]   First 4 reflection coefficients  */
)
{
    int16_t i, j;
    int16_t hi, lo;
    int16_t Kh, Kl;                /* reflexion coefficient; hi and lo      */
    int16_t alp_h, alp_l, alp_exp; /* Prediction gain; hi lo and exponent   */
    int16_t Ah[LPC_ORDER + 1], Al[LPC_ORDER + 1];  /* LPC coef. in double prec.             */
    int16_t Anh[LPC_ORDER + 1], Anl[LPC_ORDER + 1];/* LPC coef.for next iteration in double
                                     prec. */
    int32_t t0, t1, t2;            /* temporary variable                    */

    /* K = A[1] = -R[1] / R[0] */
	
    t1 = L_Comp (Rh[1], Rl[1]);
    t2 = L_abs (t1);                    /* abs R[1]         */
    t0 = Div_32 (t2, Rh[0], Rl[0]);     /* R[1]/R[0]        */
    if (t1 > 0)
       t0 = L_negate (t0);             /* -R[1]/R[0]       */
    L_Extract (t0, &Kh, &Kl);           /* K in DPF         */
    
    rc[0] = L_round (t0);

    t0 = L_shr (t0, 4);                 /* A[1] in          */
    L_Extract (t0, &Ah[1], &Al[1]);     /* A[1] in DPF      */

    /*  Alpha = R[0] * (1-K**2) */

    t0 = Mpy_32 (Kh, Kl, Kh, Kl);       /* K*K             */
    t0 = L_abs (t0);                    /* Some case <0 !! */
    t0 = L_sub ((int32_t) 0x7fffffffL, t0); /* 1 - K*K        */
    L_Extract (t0, &hi, &lo);           /* DPF format      */
    t0 = Mpy_32 (Rh[0], Rl[0], hi, lo); /* Alpha in        */

    /* Normalize Alpha */
	
    alp_exp = norm_l (t0);
    t0 = L_shl (t0, alp_exp);
    L_Extract (t0, &alp_h, &alp_l);     /* DPF format    */

    /*--------------------------------------*
     * ITERATIONS  I=2 to LPC_ORDER                 *
     *--------------------------------------*/
	
    for (i = 2; i <= LPC_ORDER; i++)
    {
       /* t0 = SUM ( R[j]*A[i-j] ,j=1,i-1 ) +  R[i] */
       
       t0 = 0;
       for (j = 1; j < i; j++)
       {
          t0 = L_add (t0, Mpy_32 (Rh[j], Rl[j], Ah[i - j], Al[i - j]));
       }
       t0 = L_shl (t0, 4);
       
       t1 = L_Comp (Rh[i], Rl[i]);
       t0 = L_add (t0, t1);            /* add R[i]        */
       
       /* K = -t0 / Alpha */
       
       t1 = L_abs (t0);
       t2 = Div_32 (t1, alp_h, alp_l); /* abs(t0)/Alpha              */
       if (t0 > 0)
          t2 = L_negate (t2);         /* K =-t0/Alpha                */
       t2 = L_shl (t2, alp_exp);       /* denormalize; compare to Alpha */
       L_Extract (t2, &Kh, &Kl);       /* K in DPF                      */
       
       if (sub (i, 5) < 0)
       {
          rc[i - 1] = L_round (t2);
       }
       /* Test for unstable filter. If unstable keep old A(z) */
       
       if (sub (abs_s (Kh), 32750) > 0)
       {
          for (j = 0; j <= LPC_ORDER; j++)
          {
             A[j] = old_A[j];
          }
          
          for (j = 0; j < 4; j++)
          {
             rc[j] = 0;
          }
          
          return 0;
       }
       /*------------------------------------------*
        *  Compute new LPC coeff. -> An[i]         *
        *  An[j]= A[j] + K*A[i-j]     , j=1 to i-1 *
        *  An[i]= K                                *
        *------------------------------------------*/
       
       for (j = 1; j < i; j++)
       {
          t0 = Mpy_32 (Kh, Kl, Ah[i - j], Al[i - j]);
          t0 = L_add(t0, L_Comp(Ah[j], Al[j]));
          L_Extract (t0, &Anh[j], &Anl[j]);
       }
       t2 = L_shr (t2, 4);
       L_Extract (t2, &Anh[i], &Anl[i]);
       
       /*  Alpha = Alpha * (1-K**2) */
       
       t0 = Mpy_32 (Kh, Kl, Kh, Kl);           /* K*K             */
       t0 = L_abs (t0);                        /* Some case <0 !! */
       t0 = L_sub ((int32_t) 0x7fffffffL, t0);  /* 1 - K*K        */
       L_Extract (t0, &hi, &lo);               /* DPF format      */
       t0 = Mpy_32 (alp_h, alp_l, hi, lo);
       
       /* Normalize Alpha */
       
       j = norm_l (t0);
       t0 = L_shl (t0, j);
       L_Extract (t0, &alp_h, &alp_l);         /* DPF format    */
       alp_exp = add (alp_exp, j);             /* Add normalization to
                                                  alp_exp */
       
       /* A[j] = An[j] */
       
       for (j = 1; j <= i; j++)
       {
          Ah[j] = Anh[j];
          Al[j] = Anl[j];
       }
    }
    
    A[0] = 4096;
    for (i = 1; i <= LPC_ORDER; i++)
    {
       t0 = L_Comp (Ah[i], Al[i]);
       old_A[i] = A[i] = L_round (L_shl (t0, 1));
    }
    
    return 0;
}

/* o:   energy of prediction error   */
short levinson_durbin(	float       *a,     /* o:   LP coefficients (a[0] = 1.0) */
			const float *r,     /* i:   vector of autocorrelations   */
			const short m,      /* i:   order of LP filter           */
			float       epsP[]  /* o:   prediction error energy      */
)
{
    short i, j, l;
    float buf[FFTSIZE*2];
    float *rc;    /* reflection coefficients  0,...,m-1 */
    float s, at, err;
    short flag=0;

    rc = &buf[0];
    rc[0] = (-r[1])/r[0];
    a[0] = 1.0f;
    a[1] = rc[0];
    err = r[0] + r[1]*rc[0];
    if ( epsP != NULL)
    {
        epsP[0] = r[0];
        epsP[1] = err;
    }

    for ( i = 2; i <= m; i++ )
    {
        s = 0.0f;
        for ( j = 0; j < i; j++ )
        {
            s += r[i-j] * a[j];
        }

        rc[i-1]= (-s) / err;

        if (fabs(rc[i-1]) > 0.99945f)
        {
            flag=1;/* Test for unstable filter. If unstable keep old A(z) */
        }

        for ( j = 1; j <= i/2; j++ )
        {
            l = i-j;
            at = a[j] + rc[i-1] * a[l];
            a[l] += rc[i-1] * a[j];
            a[j] = at;
        }

        a[i] = rc[i-1];

        err += rc[i-1] * s;
        if ( err <= 0.0f )
        {
            err = 0.01f;
        }

        if ( epsP != NULL)
        {
            epsP[i] = err;
        }
    }

    return (flag);
}

void autocorr(float32_t   x[],		/* (i) : Input signal         */
	      uint16_t    m,		/* (i) : LPC order            */
              float32_t   r[],		/* (o) : Autocorrelations     */
	      float32_t   window[])	/* (i) : LPC Analysis window  */
{
	float32_t buf[FFTSIZE];
	float32_t a0;
	int i, n;
	
	/* apply analysis window */
	arm_mult_f32(x, window, buf, FFTSIZE);
	
	/* compute autocorrealtion coefficients up to lag order */
	for (i = 0; i <= m; i++)
	{
		a0 = 0.0F;
		for (n = i; n < FFTSIZE; n++)
		{
			a0 += buf[n] * buf [n - i];
		}
		r[i] = a0;
	}
}

#endif