/******************************************************************************************
* Project: 임베디드향 음성 품질 개선 기술 (embedded speech quality improvement technique) *
* Name: Kwangsub Song																	  *
* Institute: ASAP lab., Hanyang univ.													  *
* Version date: 2018. 05. 03															  *
*******************************************************************************************/

/******************************************************************************************
* INPUT: input speech file name                                                           *
* OUTPUT: output speech file name														  *
*******************************************************************************************/

#include "global_define.h"

#ifdef ENABLE_HY_FE

#define GAIN_INDEX	0 //choice of 0,1,2

void Init_window (float *w, short m, short l, short d);
void Apply_window(float *buf, float *w, short size);
int findpeaks(float *SIGNAL, int *pks, int LIMIT);

float32_t preemph_last_FE = 0.0f;
float32_t deemph_last_FE = 0.0f;

// Overlap-add를 위한 변수
float32_t Previn_buf[NS2];
float32_t Prevsynth_buf[NS2];

// LPC를 위한 변수
int16_t old_A[11] = {0,};
int16_t rc[4] = {0,};
int16_t rHigh[11] = {0,}, rLow[11] = {0,};
//int16_t h_lpc[FRONTMARGIN + FE_FRAME_L] = {0,};
int16_t h_lpc[NS2] = {8,78,218,427,705,1050,1460,1934,2470,3066,3718,4425,5183,5989,6840,7731,8660,9621,10612,11627,12663,13714,14777,15847,16919,17989,19052,20103,21139,22154,23145,24106,25035,25926,26777,27583,28341,29048,29700,30296,30832,31306,31716,32061,32339,32548,32688,32758,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32758,32688,32548,32339,32061,31716,31306,30832,30296,29700,29048,28341,27583,26777,25926,25035,24106,23145,22154,21139,20103,19052,17989,16919,15847,14777,13714,12663,11627,10612,9621,8660,7731,6840,5989,5183,4425,3718,3066,2470,1934,1460,1050,705,427,218,78,8};
int16_t x_lpc[FRONTMARGIN + FE_FRAME_L] = {0,};

float32_t fold_A[11] = {0,};
float32_t fft_lpc[NS2*2] = {0,};
float32_t fft_buf[NS2*2] = {0,};
float32_t ifft_buf[NS2*2] = {0,};
float32_t TT[NS2*2] = {0,};
TM_FFT_F32_t FFT;    /*!< FFT structure */

void FEnh_func(int16_t *x){
	int i, j;
	int idx = 0;
	int cnt = 0;
	int formant_L = 0;
	int DC = 0, FO = 0;
	
	float32_t In_buf[NS2] = {0,};
	float32_t Tmp_buf[FE_FRAME_L] = {0,};
	float32_t Out_buf[FE_FRAME_L] = {0,};
	float32_t oFFT_Buf[NS2] = {0,};
	
	int16_t LPC[44] = {0,};
	
	// Windowing 및 FFT 및 IFFT를 위한 변수
	//float32_t h[NS2] = {0,};
	float32_t h[NS2] = {0.000267706288,0.00240763673,0.00667833397,0.0130615104,0.0215298347,0.0320470408,0.0445680916,0.0590393692,0.0753989071,0.0935766548,0.113494776,0.135067970,0.158203870,0.182803378,0.208761156,0.235966086,0.264301658,0.293646485,0.323874980,0.354857683,0.386461884,0.418552250,0.450991452,0.483640522,0.516359568,0.549008548,0.581447780,0.613538146,0.645142436,0.676125050,0.706353605,0.735698342,0.764033973,0.791238904,0.817196667,0.841796219,0.864932060,0.886505187,0.906423330,0.924601078,0.940960646,0.955431938,0.967952967,0.978470147,0.986938477,0.993321657,0.997592330,0.999732256,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,1.00000000,0.999732256,0.997592330,0.993321657,0.986938477,0.978470147,0.967952967,0.955431819,0.940960646,0.924601078,0.906423330,0.886505187,0.864931941,0.841796100,0.817196548,0.791238785,0.764033854,0.735698342,0.706353486,0.676124990,0.645142317,0.613538027,0.581447661,0.549008489,0.516359448,0.483640343,0.450991392,0.418552190,0.386461824,0.354857594,0.323874891,0.293646425,0.264301598,0.235965997,0.208761096,0.182803303,0.158203796,0.135067910,0.113494717,0.0935766026,0.0753988549,0.0590393245,0.0445680544,0.0320470035,0.0215298086,0.0130614899,0.00667831954,0.00240762765,0.000267703232};
	
	float32_t apply_FE_REAL[WINDOW_BY_TWO] = {0,};
	float32_t apply_FE_IMG[WINDOW_BY_TWO] = {0,};
	float32_t spectrum[WINDOW_BY_TWO] = {0,};
	
	float32_t lpc_FE_REAL[WINDOW_BY_TWO] = {0,};
	float32_t lpc_FE_IMG[WINDOW_BY_TWO] = {0,};
	
	float32_t spectrum_lpc[WINDOW_BY_TWO] = {0,};
	float32_t lpc_const = 0.0;
	float32_t formant_3dB_below = 0.0;
	float32_t gain_filter[WINDOW_BY_TWO] = {0,};
	
	float32_t gain = 0.0;
	
	float SKS_FR[128][3] = {
#if 01
		{0.06, 0.30, 0.06}, {0.06, 0.30, 0.06}, {0.36, 0.36, 0.36}, {0.60, 0.74, 0.60}, {0.72, 0.56, 0.72}, {0.94, 0.58, 0.94}, {1.38, 0.60, 1.38}, {0.94, 0.56, 0.94},
		{0.66, 0.56, 0.66}, {0.66, 0.56, 0.66}, {0.56, 0.62, 0.56}, {0.50, 0.66, 0.50}, {0.44, 0.68, 0.44}, {0.42, 0.60, 0.42}, {0.42, 0.52, 0.42}, {0.44, 0.48, 0.44},
		{0.46, 0.50, 0.46}, {0.48, 0.48, 0.48}, {0.48, 0.48, 0.48}, {0.48, 0.54, 0.48}, {0.48, 0.60, 0.48}, {0.48, 0.64, 0.48}, {0.46, 0.64, 0.46}, {0.48, 0.64, 0.48},
		{0.48, 0.64, 0.48}, {0.48, 0.64, 0.48}, {0.44, 0.64, 0.44}, {0.44, 0.68, 0.44}, {0.46, 0.70, 0.46}, {0.46, 0.82, 0.46}, {0.46, 0.84, 0.46}, {0.46, 1.08, 0.46},
		{0.46, 0.90, 0.46}, {0.46, 1.54, 0.46}, {0.48, 0.90, 0.48}, {0.48, 1.18, 0.48}, {0.50, 1.08, 0.50}, {0.52, 0.90, 0.52}, {0.54, 1.00, 0.54}, {0.56, 0.74, 0.56},
		{0.58, 0.72, 0.58}, {0.60, 0.60, 0.60}, {0.62, 0.56, 0.62}, {0.64, 0.50, 0.64}, {0.66, 0.48, 0.66}, {0.70, 0.44, 0.70}, {0.72, 0.44, 0.72}, {0.74, 0.42, 0.74},
		{0.78, 0.44, 0.78}, {0.78, 0.44, 0.78}, {0.82, 0.46, 0.82}, {0.80, 0.46, 0.80}, {0.86, 0.46, 0.86}, {0.86, 0.48, 0.86}, {0.90, 0.50, 0.90}, {0.90, 0.52, 0.90},
		{0.94, 0.52, 0.94}, {0.94, 0.56, 0.94}, {1.00, 0.56, 1.00}, {0.96, 0.60, 0.96}, {1.06, 0.60, 1.06}, {1.02, 0.64, 1.02}, {1.18, 0.66, 1.18}, {1.16, 0.72, 1.16},
		{1.36, 0.70, 1.36}, {1.26, 0.76, 1.26}, {1.38, 0.76, 1.38}, {1.28, 0.86, 1.28}, {1.44, 0.84, 1.44}, {1.40, 0.94, 1.40}, {1.60, 0.90, 1.60}, {1.58, 1.04, 1.58},
		{1.58, 0.98, 1.58}, {1.58, 1.04, 1.58}, {1.54, 1.06, 1.54}, {1.64, 1.12, 1.64}, {1.56, 1.12, 1.56}, {1.58, 1.08, 1.58}, {1.52, 1.28, 1.52}, {1.54, 1.18, 1.54},
		{1.48, 1.34, 1.48}, {1.46, 1.22, 1.46}, {1.48, 1.36, 1.48}, {1.44, 1.16, 1.44}, {1.64, 1.36, 1.64}, {1.54, 1.20, 1.54}, {1.52, 1.38, 1.52}, {1.72, 1.28, 1.72},
		{1.76, 1.34, 1.76}, {1.58, 1.40, 1.58}, {1.62, 1.52, 1.62}, {1.52, 1.52, 1.52}, {2.24, 1.28, 2.24}, {3.16, 1.30, 3.16}, {2.04, 1.40, 2.04}, {1.82, 1.46, 1.82},
		{2.64, 1.50, 2.64}, {1.94, 1.30, 1.94}, {1.64, 1.80, 1.64}, {2.36, 1.58, 2.36}, {2.28, 1.38, 2.28}, {1.78, 1.50, 1.78}, {1.96, 1.38, 1.96}, {2.00, 2.18, 2.00},
		{1.78, 2.64, 1.78}, {1.58, 2.02, 1.58}, {1.82, 1.92, 1.82}, {1.72, 1.70, 1.72}, {1.58, 1.56, 1.58}, {1.68, 1.94, 1.68}, {1.56, 1.90, 1.56}, {1.52, 1.50, 1.52},
		{1.54, 1.56, 1.54}, {1.64, 1.56, 1.64}, {1.62, 1.82, 1.62}, {1.98, 2.44, 1.98}, {1.54, 1.58, 1.54}, {1.82, 2.48, 1.82}, {1.86, 2.26, 1.86}, {1.68, 1.92, 1.68},
		{2.00, 1.64, 2.00}, {2.02, 2.88, 2.02}, {2.72, 1.80, 2.72}, {3.26, 1.88, 3.26}, {1.82, 2.24, 1.82}, {3.10, 1.92, 3.10}, {3.02, 2.04, 3.02}, {2.64, 2.54, 2.64},
#else
	//FE 2018 (중간버전)
		{0.72, 0.76, 0.73}, {0.72, 0.76, 0.73}, {0.72, 0.76, 0.73}, {0.72, 0.76, 0.73}, {0.73, 0.77, 0.73}, {0.73, 0.77, 0.73}, {0.73, 0.77, 0.74}, {0.73, 0.77, 0.74}, 
		{0.59, 0.60, 0.62}, {0.59, 0.60, 0.62}, {0.60, 0.61, 0.62}, {0.60, 0.61, 0.62}, {0.59, 0.57, 0.58}, {0.59, 0.57, 0.58}, {0.59, 0.57, 0.58}, {0.59, 0.57, 0.58},
		{0.63, 0.65, 0.62}, {0.63, 0.65, 0.62}, {0.63, 0.64, 0.61}, {0.63, 0.64, 0.61}, {0.65, 0.71, 0.74}, {0.65, 0.71, 0.74}, {0.64, 0.70, 0.73}, {0.64, 0.70, 0.73}, 
		{0.92, 0.89, 0.87}, {0.92, 0.89, 0.87}, {0.89, 0.87, 0.84}, {0.89, 0.87, 0.84}, {0.87, 0.99, 1.16}, {0.87, 0.99, 1.16}, {0.82, 0.93, 1.09}, {0.82, 0.93, 1.09},
		{0.72, 0.82, 0.96}, {0.72, 0.82, 0.96}, {0.79, 0.85, 0.99}, {0.79, 0.85, 0.99}, {0.88, 0.95, 1.10}, {0.88, 0.95, 1.10}, {0.91, 0.98, 1.15}, {0.91, 0.98, 1.15}, 
		{0.98, 1.03, 1.33}, {0.98, 1.03, 1.33}, {0.99, 1.04, 1.34}, {0.99, 1.04, 1.34}, {0.99, 1.04, 1.34}, {0.99, 1.04, 1.34}, {1.00, 1.12, 1.32}, {1.00, 1.12, 1.32},
		{1.00, 1.11, 1.31}, {1.00, 1.11, 1.31}, {0.99, 1.10, 1.29}, {0.99, 1.10, 1.29}, {0.98, 1.09, 1.28}, {0.98, 1.09, 1.28}, {1.04, 1.06, 1.34}, {1.04, 1.06, 1.34}, 
		{1.03, 1.05, 1.33}, {1.03, 1.05, 1.33}, {1.02, 1.04, 1.32}, {1.02, 1.04, 1.32}, {1.01, 1.03, 1.31}, {1.01, 1.03, 1.31}, {1.00, 1.16, 1.43}, {1.00, 1.16, 1.43},
		{0.99, 1.15, 1.41}, {0.99, 1.15, 1.41}, {0.98, 1.13, 1.40}, {0.98, 1.13, 1.40}, {0.98, 1.13, 1.39}, {0.98, 1.13, 1.39}, {0.98, 1.13, 1.40}, {0.98, 1.13, 1.40}, 
		{1.00, 1.21, 1.45}, {1.00, 1.21, 1.45}, {1.01, 1.22, 1.46}, {1.01, 1.22, 1.46}, {1.01, 1.22, 1.47}, {1.01, 1.22, 1.47}, {1.01, 1.22, 1.47}, {1.01, 1.22, 1.47},
		{1.01, 1.21, 1.46}, {1.01, 1.21, 1.46}, {0.99, 1.20, 1.44}, {0.99, 1.20, 1.44}, {1.02, 1.16, 1.41}, {1.02, 1.16, 1.41}, {0.98, 1.11, 1.35}, {0.98, 1.11, 1.35}, 
		{0.89, 1.01, 1.24}, {0.89, 1.01, 1.24}, {0.72, 0.82, 0.99}, {0.72, 0.82, 0.99}, {0.91, 1.03, 1.26}, {0.91, 1.03, 1.26}, {0.98, 1.11, 1.36}, {0.98, 1.11, 1.36},
		{1.02, 1.15, 1.41}, {1.02, 1.15, 1.41}, {1.06, 1.24, 1.42}, {1.06, 1.24, 1.42}, {1.07, 1.25, 1.43}, {1.07, 1.25, 1.43}, {1.07, 1.25, 1.43}, {1.07, 1.25, 1.43}, 
		{1.07, 1.25, 1.43}, {1.07, 1.25, 1.43}, {1.05, 1.23, 1.41}, {1.05, 1.23, 1.41}, {1.03, 1.20, 1.38}, {1.03, 1.20, 1.38}, {1.00, 1.17, 1.34}, {1.00, 1.17, 1.34},
		{0.96, 1.04, 1.40}, {0.96, 1.04, 1.40}, {0.89, 0.97, 1.30}, {0.89, 0.97, 1.30}, {0.80, 0.88, 1.18}, {0.80, 0.88, 1.18}, {0.84, 0.92, 1.23}, {0.84, 0.92, 1.23}, 
		{0.90, 0.98, 1.31}, {0.90, 0.98, 1.31}, {0.93, 1.02, 1.36}, {0.93, 1.02, 1.36}, {0.95, 1.04, 1.39}, {0.95, 1.04, 1.39}, {0.96, 1.05, 1.40}, {0.96, 1.05, 1.40},
#endif
	};
	
	
	int min, min_i;
	float32_t pre_cal = 0.0;
	uint8_t vuv = 0;
	
	old_A[0] = 4096;
	for(i = 1;i < LPC_ORDER + 1;i++)
	{
		old_A[i] = 0;
	}
	
	// Window generator for speech signal
	//Init_window(h, NS2, FE_FRAME_L, FRONTMARGIN);
	
	// Window generator for LPC
	for(i = 0;i < FRONTMARGIN + FE_FRAME_L;i++)
	{
		if(i < FE_FRAME_L)
			Tmp_buf[i] = (float)x[i];
		
		//h_lpc[i] = (int16_t)(h[i]*32767);
	}
	
	preemphasis(FE_FRAME_L, Tmp_buf, &preemph_last_FE);
	
	// Overlap-add
	for(i = 0;i < NS2;i++)
	{
		if(i<FRONTMARGIN)
		{
			In_buf[i] = Previn_buf[FE_FRAME_L + i];
		}
		else if(i<(FRONTMARGIN + FE_FRAME_L))
		{
			In_buf[i] = Tmp_buf[i - FRONTMARGIN];
		}
		else if(i<NS2)
		{
			In_buf[i] = 0.0;
		}
	}
	
	// Construct buffer for LPC
	for(i = 0;i < FRONTMARGIN + FE_FRAME_L;i++)
		x_lpc[i] = (int16_t)In_buf[i];
	
	//BufferCopy(In_buf, Previn_buf, NS2);
	memcpy(Previn_buf, In_buf, sizeof(float32_t)*NS2);
	Apply_window(In_buf, h, NS2);
	
	float32_t sound_power = 0;
	for(i=0;i<NS2;i+=4)
	{
		sound_power += (In_buf[i] * In_buf[i]);
		sound_power += (In_buf[i+1] * In_buf[i+1]);
		sound_power += (In_buf[i+2] * In_buf[i+2]);
		sound_power += (In_buf[i+3] * In_buf[i+3]);
		if(sound_power >= 3200)
		{
			vuv = 1;
			break;
		}
	}
	
	if( (vuv) && ( g_str_bitflag.b1_fft_enable ) )
	{
		//BufferCopy(In_buf, fft_buf, NS2);
		memcpy(fft_buf, In_buf, sizeof(float32_t)*NS2);
		
		// LPC routine
		Autocorr(x_lpc, LPC_ORDER, rHigh, rLow, h_lpc);
		Levinson(old_A, rHigh, rLow, LPC, rc);
		
		for(i = 0;i < NS2;i++)
		{
			if(i < 11)
				fft_lpc[i] = ((float)LPC[i]*0.000244140625f)*0.0078125f;
			else
				fft_lpc[i] = 0;
			
			fft_buf[i]*=0.0078125;// ÷128
		}
		
		/* Init FFT, FFT_SIZE define is used for FFT_SIZE, samples count is FFT_SIZE * 2, don't use malloc for memory allocation */
		if(TM_FFT_Init_F32(&FFT, NS2, 0, 0) != 0)
			return;
		
		/* We didn't used malloc for allocation, so we have to set pointers ourself */
		/* Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part */
		/* Output buffer must be FFT_SIZE in length */
		memcpy(TT, fft_lpc, sizeof(float32_t)*(NS2*2));
		memset(fft_lpc, 0x00, sizeof(float32_t)*(NS2*2));
		for(idx = 0; idx < NS2; idx++)
		{
			fft_lpc[(idx * 2)] = TT[idx];//real
			fft_lpc[(idx * 2) + 1] = 0;//imaginary
		}
		TM_FFT_SetBuffers_F32(&FFT, fft_lpc, oFFT_Buf);
		
		/* Do FFT on signal, values at each bin and calculate max value and index where max value happened */
		TM_FFT_Process_F32(&FFT);//real:128, imag:128
		
		/* We didn't used malloc for allocation, so we have to set pointers ourself */
		/* Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part */
		/* Output buffer must be FFT_SIZE in length */
		memset(TT, 0x00, sizeof(float32_t)*(NS2*2));
		memcpy(TT, fft_buf, sizeof(float32_t)*(NS2*2));
		memset(fft_buf, 0x00, sizeof(float32_t)*(NS2*2));
		for(idx = 0; idx < NS2; idx++)
		{
			fft_buf[(idx * 2)] = TT[idx];//real
			fft_buf[(idx * 2) + 1] = 0;//imaginary
		}
		TM_FFT_SetBuffers_F32(&FFT, fft_buf, oFFT_Buf);
		
		/* Do FFT on signal, values at each bin and calculate max value and index where max value happened */
		TM_FFT_Process_F32(&FFT);
		
		// Re-arrange buffer of LPC on fft domain
		for(i = 0;i < WINDOW_BY_TWO;i++){
			lpc_FE_REAL[i] = fft_lpc[2*i];
			lpc_FE_IMG[i] = fft_lpc[2*i + 1];
		}
		lpc_FE_REAL[WINDOW_BY_TWO-1] = fft_lpc[2*WINDOW_BY_TWO];
		lpc_FE_IMG[WINDOW_BY_TWO-1] = fft_lpc[2*WINDOW_BY_TWO + 1];	
		
		float32_t deno_real = 0;
		float32_t deno_img = 0;
		// Log power spectrum of LPC
		for(i = 0;i < WINDOW_BY_TWO;i++){
			lpc_const = 1.0/(lpc_FE_REAL[i]*lpc_FE_REAL[i] + lpc_FE_IMG[i]*lpc_FE_IMG[i]);
			deno_real = lpc_const*lpc_FE_REAL[i];
			deno_img = lpc_const*((-1.0f)*lpc_FE_IMG[i]);
			
			pre_cal = sqrtf((deno_real*deno_real) + (deno_img*deno_img));
			//spectrum_lpc[i] = 20*log10(pre_cal);
			spectrum_lpc[i] = 20*log10f(pre_cal);
		}
		
		// Re-arrange buffer of speech on fft domain
		for(i = 0;i < WINDOW_BY_TWO;i++)
		{
			apply_FE_REAL[i] = fft_buf[(i<<1)];
			apply_FE_IMG[i] = fft_buf[(i<<1) + 1];
		}
		
		if( g_str_bitflag.b1_fe_enable )
		{
			int formant_freq[WINDOW_BY_TWO] = {0,};
			int formant_band[WINDOW_BY_TWO][2] = {0,};
			
			// Find formant frequency
			formant_L = findpeaks(spectrum_lpc, formant_freq, 5);
			
			DC = (int)fft_buf[0];
			FO = (int)fft_buf[128];
			
			/*********************************** FE ***********************************/
			// Log power spectrum of speech signal
			spectrum[0] = 20*log10f((float)DC);
			spectrum[WINDOW_BY_TWO - 1] = 20*log10f((float)FO);
			for(i = 1;i < WINDOW_BY_TWO - 1;i++)
			{
				pre_cal = sqrtf((apply_FE_REAL[i]*apply_FE_REAL[i]) + (apply_FE_IMG[i]*apply_FE_IMG[i]));
				spectrum[i] = 20*log10f(pre_cal);
			}
			
			// Find formant bandwidth
			for(i = 0;i < formant_L;i++){
				formant_3dB_below = spectrum_lpc[formant_freq[i]] - 3.0f;
				
				// Left of the formant
				for(j = formant_freq[i];j > 0;j--){
					if(spectrum_lpc[j] - spectrum_lpc[j - 1] < 0){
						formant_band[i][0] = j;
						break;
					}
					if(spectrum_lpc[j] - formant_3dB_below < 0){
						formant_band[i][0] = j;
						break;
					}
					if(j == 0)
						formant_band[i][0] = 0;
				}
				
				// Right of the formant
				for(j = formant_freq[i];j < WINDOW_BY_TWO - 1;j++){
					if(spectrum_lpc[j] - spectrum_lpc[j + 1] < 0){
						formant_band[i][1] = j;
						break;
					}
					if(spectrum_lpc[j] - formant_3dB_below < 0){
						formant_band[i][1] = j;
						break;
					}
					if(j == WINDOW_BY_TWO - 2)
						formant_band[i][1] = WINDOW_BY_TWO - 1;
				}
			}
			
			int harmonics[WINDOW_BY_TWO] = {0,};
			int diff[WINDOW_BY_TWO][2] = {0,};
			
			cnt = 0;
			for(i = 1;i < WINDOW_BY_TWO - 1;i++)
			{
				if(spectrum[i - 1] >= spectrum[i] && spectrum[i] <= spectrum[i + 1])
				{
					harmonics[cnt++] = i;
				}
			}
			
			for(i = 0;i < formant_L;i++)
			{
				for(j = 0;j < cnt;j++)
				{
					diff[j][0] = abs(harmonics[j] - formant_band[i][0]);
					diff[j][1] = abs(harmonics[j] - formant_band[i][1]);
				}
				
				// left
				min = diff[0][0];
				min_i = 0;
				for(j = 1;j < cnt;j++){
					if(diff[j][0] < min){
						min = diff[j][0];
						min_i = j;
					}
				}
				// Except conditional
				if(harmonics[min_i] < formant_freq[i]){
					formant_band[i][0] = harmonics[min_i];
				}
				
				// right
				min = diff[0][1];
				min_i = 0;
				for(j = 1;j < cnt;j++){
					if(diff[j][1] < min){
						min = diff[j][1];
						min_i = j;
					}
				}
				// Except conditional
				if(harmonics[min_i] > formant_freq[i])
					formant_band[i][1] = harmonics[min_i];
			}
			
			int bandwidth = 0;
			short idx1;
			float Hamming_window[WINDOW_BY_TWO] = {0,};

			// Apply formant enhancement
			for(i = 0;i < formant_L;i++){
				if(i == 0 && formant_freq[i] < 3){
					continue;
				}
				
				if(SKS_FR[(int)formant_freq[i]][1] > 3.0f)
					gain = 3;
				else
					gain = SKS_FR[(int)formant_freq[i]][0];
				gain = SKS_FR[(int)formant_freq[i]][GAIN_INDEX];
				
				bandwidth = (formant_band[i][1] - formant_band[i][0] + 1);
				
				for(idx1 = 0;idx1 < bandwidth;idx1++)
				{
					Hamming_window[idx1] = 0.54f - (0.46f*arm_cos_f32((float)((double)(PI2*(float)idx1) / (double)bandwidth)));
					gain_filter[idx1] = powf(((Hamming_window[idx1] - Hamming_window[0]) / (1 - Hamming_window[0])) + 1, gain);
				}
				
				cnt = 0;
				for(j = formant_band[i][0];j < formant_band[i][1] + 1;j++){
					apply_FE_REAL[j] = gain_filter[cnt]*apply_FE_REAL[j];
					apply_FE_IMG[j] = gain_filter[cnt]*apply_FE_IMG[j];
					cnt++;
				}
			}
			/*********************************** FE ***********************************/
		}
		
		if( g_str_bitflag.b1_cgeq_enable )
		{
			/*********************************** CGEQ ***********************************/
			// Apply CGEQ
			for(i = 0;i < BAND_L;i++)
			{
				for(j = 1;j < NS1 - 1;j++)
				{
#if 01
					apply_FE_REAL[j] *= (cgeq_filter[i][j]);
					apply_FE_IMG[j] *= (cgeq_filter[i][j]);
#else
					apply_FE_REAL[j] = (cgeq_filter[i][j])*apply_FE_REAL[j];
					apply_FE_IMG[j] = (cgeq_filter[i][j])*apply_FE_IMG[j];
#endif
				}
			}
			/*********************************** CGEQ ***********************************/
		}
		
		for(i = 0;i < WINDOW_BY_TWO - 1;i++)
		{
			fft_buf[(i<<1)] = apply_FE_REAL[i];
			fft_buf[(i<<1) + 1] = apply_FE_IMG[i];
		}
		
		//sort FFT value (left, right side)
		for(i = 0;i < WINDOW_BY_TWO;i++)
		{	
			fft_buf[(NS2)+(i<<1)] = fft_buf[(NS2)-(i<<1)];
			fft_buf[(NS2+1)+(i<<1)] = -fft_buf[(NS2+1)-(i<<1)];
		}
		
		memcpy(ifft_buf, fft_buf, sizeof(float32_t)*(NS2<<1));
		
		/* Init FFT, FFT_SIZE define is used for FFT_SIZE, samples count is FFT_SIZE * 2, don't use malloc for memory allocation */
		if(TM_FFT_Init_F32(&FFT, NS2, 1, 0) != 0)
			return;
		
		/* We didn't used malloc for allocation, so we have to set pointers ourself */
		/* Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part */
		/* Output buffer must be FFT_SIZE in length */
		TM_FFT_SetBuffers_F32(&FFT, ifft_buf, oFFT_Buf);
		
		/* Do FFT on signal, values at each bin and calculate max value and index where max value happened */
		TM_FFT_Process_F32(&FFT);
		
		memcpy(TT, ifft_buf, sizeof(float32_t)*(NS2*2));
		memset(ifft_buf, 0x00, sizeof(float32_t)*(NS2*2));
		for(idx = 0; idx < NS2; idx++)
		{
			ifft_buf[idx] = TT[(idx * 2)]*128;//64;//real
		}
	}
	else
	{
		memset(ifft_buf, 0x00, sizeof(float32_t)*NS2);
		memcpy(ifft_buf, In_buf, sizeof(float32_t)*NS2);
	}
	
	// Overlap-add
	for (i = 0; i < (NS1 - FE_FRAME_L); i++)
		Out_buf[i] = ifft_buf[i] + Prevsynth_buf[i + FE_FRAME_L];
	for (i = (NS1 - FE_FRAME_L); i < FE_FRAME_L; i++)
		Out_buf[i] = ifft_buf[i];
	
	memcpy(Prevsynth_buf, ifft_buf, sizeof(float32_t)*NS2);
	deemphasis(FE_FRAME_L, Out_buf, &deemph_last_FE);
	
	// Overflow prevention
	for(i = 0;i < FE_FRAME_L;i++)
	{
		if(Out_buf[i] > 32767.0f)
			Out_buf[i] = 32767.0f;
		else if(Out_buf[i] < -32767.0f)
			Out_buf[i] = -32767.0f;
		
		x[i] = (int16_t)Out_buf[i];
	}
}

void Init_window(float *w, short m, short l, short d){
	short	i;
	float	deno, f_tmp;
	
	deno = 0.5f/((float)d);
	
#if 01
	for(i = 0;i < d;i++)
	{
		f_tmp = arm_sin_f32(PI1*((float)i + 0.5f)*deno);
		w[i] = f_tmp*f_tmp;
	}
	for(i = d;i < l;i++)
	{
		w[i] = 1.0f;
	}
	for(i = l;i < (d + l);i++)
	{
		f_tmp = arm_sin_f32(PI1*((float)(i - l + d) + 0.5f)*deno);
		w[i] = f_tmp * f_tmp;
	}
	for(i = (d + l);i < m;i++)
	{
		w[i] = 0.0f;
	}
#else
	for(i = 0;i < d;i++)
	{
		if(i < d)
		{
			f_tmp = arm_sin_f32(PI1*((float)i + 0.5f)*deno);
			w[i] = f_tmp*f_tmp;
		}
		else if(i < l)
		{
			w[i] = 1.0f;
		}
		else if(i < (d + l))
		{
			f_tmp = arm_sin_f32(PI1*((float)(i - l + d) + 0.5f)*deno);
			w[i] = f_tmp * f_tmp;
		}
		else if(i < m)
		{
			w[i] = 0.0f;
		}
	}
#endif
}

void Apply_window(float *buf, float *w, short size){
	short i;
	
	for(i = 0;i < size;i++)
		buf[i] *= w[i];
}

int findpeaks(float *SIGNAL, int *pks, int LIMIT){
	int16_t i, j;
	int16_t sign_f_cnt, sign_r_cnt;
	int16_t cnt = 1;
	int16_t peaks[100];
	int16_t f_range;
	
	float32_t SIGNAL_TMP[WINDOW_BY_TWO + 1];
	float32_t sign_f[WINDOW_BY_TWO];
	float32_t sign_r[WINDOW_BY_TWO];
	
	for(i = 0;i < 100;i++)
		peaks[i] = -1;
	
	for(i = 1;i < WINDOW_BY_TWO + 1;i++)
		SIGNAL_TMP[i] = SIGNAL[i - 1];
	
	for(i = 1;i < WINDOW_BY_TWO - LIMIT + 1;i++){
		if((i - 1) > LIMIT)
			f_range = LIMIT;
		else
			f_range = i - 1;
		
		for(j = 1;j < f_range + 1;j++)
			sign_f[j] = SIGNAL_TMP[i] - SIGNAL[i - j];
		
		for(j = 1;j < LIMIT + 1;j++)
			sign_r[j] = SIGNAL_TMP[i + j] - SIGNAL_TMP[i];
		
		sign_f_cnt = 0;
		for(j = 1;j < f_range + 1;j++){
			if(sign_f[j] < 0)
				sign_f_cnt++;
		}
		
		if(f_range <= 1)
			sign_f_cnt = 1;
		
		sign_r_cnt = 0;
		for(j = 1;j < LIMIT + 1;j++){
			if(sign_r[j] > 0)
				sign_r_cnt++;
		}
		
		if(sign_f_cnt == 0 & sign_r_cnt == 0)
			peaks[cnt++] = i - 1;
	}
	
	for(i = 1;i < cnt;i++)
		pks[i - 1] = peaks[i];
	
	return (cnt - 1);
}

#endif