/*
*****************************************************************************
*
*      GSM AMR-NB speech codec   R98   Version 7.6.0   December 12, 2001
*                                R99   Version 3.3.0                
*                                REL-4 Version 4.1.0                
*
*****************************************************************************
*
*      File             : levinson.c
*      Purpose          : Levinson-Durbin algorithm in double precision.
*                       : To compute the LP filter parameters from the
*                       : speech autocorrelations.
*
*****************************************************************************
*/

/*
*****************************************************************************
*                         MODULE INCLUDE FILE AND VERSION ID
*****************************************************************************
*/

/*
*****************************************************************************
*                         INCLUDE FILES
*****************************************************************************
*/
#include "global_define.h"
#ifdef ENABLE_FE_FPU_TEST
const char levinson_id[] = "@(#)$Id $" levinson_h;

/*************************************************************************
*
*   FUNCTION:  Levinson()
*
*   PURPOSE:  Levinson-Durbin algorithm in double precision. To compute the
*             LP filter parameters from the speech autocorrelations.
*
*   DESCRIPTION:
*       R[i]    autocorrelations.
*       A[i]    filter coefficients.
*       K       reflection coefficients.
*       Alpha   prediction gain.
*
*       Initialisation:
*               A[0] = 1
*               K    = -R[1]/R[0]
*               A[1] = K
*               Alpha = R[0] * (1-K**2]
*
*       Do for  i = 2 to M
*
*            S =  SUM ( R[j]*A[i-j] ,j=1,i-1 ) +  R[i]
*
*            K = -S / Alpha
*
*            An[j] = A[j] + K*A[i-j]   for j=1 to i-1
*                                      where   An[i] = new A[i]
*            An[i]=K
*
*            Alpha=Alpha * (1-K**2)
*
*       END
*
*************************************************************************/
int Levinson(int16_t old_A[],
	     int16_t Rh[],  /* i : Rh[m+1] Vector of autocorrelations (msb) */
	     int16_t Rl[],  /* i : Rl[m+1] Vector of autocorrelations (lsb) */
	     int16_t A[]  /* o : A[m]    LPC coefficients  (m = 10)       */
		     )
{
	int16_t i, j;
	int16_t hi, lo;
	int16_t Kh, Kl; /* reflexion coefficient; hi and lo      */
	int16_t alp_h, alp_l, alp_exp; /* Prediction gain; hi lo and exponent   */
	int16_t Ah[M + 1], Al[M + 1]; /* LPC coef. in double prec.             */
	int16_t Anh[M + 1], Anl[M + 1]; /* LPC coef.for next iteration in double
	prec. */
	int32_t t0, t1, t2; /* temporary variable                    */
	
	/* K = A[1] = -R[1] / R[0] */
	
	t1 = L_Comp(Rh[1], Rl[1]);
	t2 = L_abs(t1); /* abs R[1]         */
	t0 = Div_32(t2, Rh[0], Rl[0]); /* R[1]/R[0]        */
	
	if (t1 > 0)
		t0 = L_negate(t0);
	/* -R[1]/R[0]       */
	L_Extract(t0, &Kh, &Kl); /* K in DPF         */
	
	round_basic_op(t0);
	
	t0 = L_shr(t0, 4); /* A[1] in          */
	L_Extract(t0, &Ah[1], &Al[1]); /* A[1] in DPF      */
	
	/*  Alpha = R[0] * (1-K**2) */
	
	t0 = Mpy_32(Kh, Kl, Kh, Kl); /* K*K             */
	t0 = L_abs(t0); /* Some case <0 !! */
	t0 = L_sub((int32_t)0x7fffffffL, t0); /* 1 - K*K        */
	L_Extract(t0, &hi, &lo); /* DPF format      */
	t0 = Mpy_32(Rh[0], Rl[0], hi, lo); /* Alpha in        */
	
	/* Normalize Alpha */
	
	alp_exp = norm_l(t0);
	t0 = L_shl(t0, alp_exp);
	L_Extract(t0, &alp_h, &alp_l); /* DPF format    */
	
	/*--------------------------------------*
	* ITERATIONS  I=2 to M                 *
	*--------------------------------------*/
	
	for (i = 2; i <= M; i++)
	{
		/* t0 = SUM ( R[j]*A[i-j] ,j=1,i-1 ) +  R[i] */
		
		t0 = 0;
		
		for (j = 1; j < i; j++)
		{
			t0 = L_add(t0, Mpy_32(Rh[j], Rl[j], Ah[i - j], Al[i - j]));
		}
		t0 = L_shl(t0, 4);
		
		t1 = L_Comp(Rh[i], Rl[i]);
		t0 = L_add(t0, t1); /* add R[i]        */
		
		/* K = -t0 / Alpha */
		t1 = L_abs(t0);
		t2 = Div_32(t1, alp_h, alp_l); /* abs(t0)/Alpha              */
		
		if (t0 > 0)
			t2 = L_negate(t2);

		/* K =-t0/Alpha                */
		t2 = L_shl(t2, alp_exp); /* denormalize; compare to Alpha */
		L_Extract(t2, &Kh, &Kl); /* K in DPF                      */
		
		
		if (sub(i, 5) < 0)
		{
			round_basic_op(t2);
		}

		/* Test for unstable filter. If unstable keep old A(z) */
		if (sub(abs_s(Kh), 32750) > 0)
		{
			for (j = 0; j <= M; j++)
			{
				A[j] = old_A[j];
			}
			
			return 0;
		}
		/*------------------------------------------*
		*  Compute new LPC coeff. -> An[i]         *
		*  An[j]= A[j] + K*A[i-j]     , j=1 to i-1 *
		*  An[i]= K                                *
		*------------------------------------------*/
		
		for (j = 1; j < i; j++)
		{
			t0 = Mpy_32(Kh, Kl, Ah[i - j], Al[i - j]);
			t0 = L_add(t0, L_Comp(Ah[j], Al[j]));
			L_Extract(t0, &Anh[j], &Anl[j]);
		}
		t2 = L_shr(t2, 4);
		L_Extract(t2, &Anh[i], &Anl[i]);
		
		/*  Alpha = Alpha * (1-K**2) */
		
		t0 = Mpy_32(Kh, Kl, Kh, Kl); /* K*K             */
		t0 = L_abs(t0); /* Some case <0 !! */
		t0 = L_sub((int32_t)0x7fffffffL, t0); /* 1 - K*K        */
		L_Extract(t0, &hi, &lo); /* DPF format      */
		t0 = Mpy_32(alp_h, alp_l, hi, lo);
		
		/* Normalize Alpha */
		
		j = norm_l(t0);
		t0 = L_shl(t0, j);
		L_Extract(t0, &alp_h, &alp_l); /* DPF format    */
		alp_exp = add(alp_exp, j); /* Add normalization to
		alp_exp */
		
		/* A[j] = An[j] */
		
		for (j = 1; j <= i; j++)
		{
			Ah[j] = Anh[j];
			Al[j] = Anl[j];
		}
	}
	
	A[0] = 4096;
	
	for (i = 1; i <= M; i++)
	{
		t0 = L_Comp(Ah[i], Al[i]);
		old_A[i] = A[i] = round_basic_op(L_shl(t0, 1));
	}
	
	return 0;
}

int16_t Autocorr(int16_t x[],  /* (i)    : Input signal (L_WINDOW)            */
		int16_t m,  /* (i)    : LPC order                          */
		int16_t r_h[],  /* (o)    : Autocorrelations  (msb)            */
		int16_t r_l[],  /* (o)    : Autocorrelations  (lsb)            */
		float wind[] /* (i)    : window for LPC analysis (L_WINDOW) */
			)
{
	int16_t i, j, norm;
	int16_t y[FFTSIZE];
	int32_t sum;
	int16_t overfl, overfl_shft;
	
	/* Windowing of signal */
	for (i = 0; i < FFTSIZE; i++)
	{
		y[i] = mult_r(x[i], (int16_t)(wind[i] * 32767));
	}
	
	/* Compute r[0] and test for overflow */
	
	overfl_shft = 0;

	do
	{
		overfl = 0;
		
		sum = 0L;
		
		for (i = 0; i < FFTSIZE; i++)
		{
			sum = L_mac(sum, y[i], y[i]);
		}
		
		/* If overflow divide y[] by 4 */
		if (L_sub(sum, MAX_32) == 0L)
		{
			overfl_shft = add(overfl_shft, 4);
			overfl = 1;
			/* Set the overflow flag */
			
			for (i = 0; i < FFTSIZE; i++)
			{
				y[i] = shr(y[i], 2);
			}
		}
	} while (overfl != 0);
	
	sum = L_add(sum, 1L); /* Avoid the case of all zeros */
	
	/* Normalization of r[0] */
	
	norm = norm_l(sum);
	sum = L_shl(sum, norm);
	L_Extract(sum, &r_h[0], &r_l[0]); /* Put in DPF format (see oper_32b) */
	
	/* r[1] to r[m] */
	
	for (i = 1; i <= m; i++)
	{
		sum = 0;
		
		for (j = 0; j < FFTSIZE - i; j++)
		{
			sum = L_mac(sum, y[j], y[j + i]);
		}
		
		sum = L_shl(sum, norm);
		L_Extract(sum, &r_h[i], &r_l[i]);
	}
	
	norm = sub(norm, overfl_shft);
	
	return norm;
}

short levinson_durbin(          /* o:   energy of prediction error   */
    float       *a,     /* o:   LP coefficients (a[0] = 1.0) */
    const float *r,     /* i:   vector of autocorrelations   */
    const short m,      /* i:   order of LP filter           */
    float       epsP[]  /* o:   prediction error energy      */
)
{
    short i, j, l;
    float buf[FFTSIZE*2];
    float *rc;    /* reflection coefficients  0,...,m-1 */
    float s, at, err;
    short flag=0;

    rc = &buf[0];
    rc[0] = (-r[1])/r[0];
    a[0] = 1.0f;
    a[1] = rc[0];
    err = r[0] + r[1]*rc[0];
    if ( epsP != NULL)
    {
        epsP[0] = r[0];
        epsP[1] = err;
    }

    for ( i = 2; i <= m; i++ )
    {
        s = 0.0f;
        for ( j = 0; j < i; j++ )
        {
            s += r[i-j] * a[j];
        }

        rc[i-1]= (-s) / err;

        if (fabs(rc[i-1]) > 0.99945f)
        {
            flag=1;/* Test for unstable filter. If unstable keep old A(z) */
        }

        for ( j = 1; j <= i/2; j++ )
        {
            l = i-j;
            at = a[j] + rc[i-1] * a[l];
            a[l] += rc[i-1] * a[j];
            a[j] = at;
        }

        a[i] = rc[i-1];

        err += rc[i-1] * s;
        if ( err <= 0.0f )
        {
            err = 0.01f;
        }

        if ( epsP != NULL)
        {
            epsP[i] = err;
        }
    }

    return (flag);
}

void autocorr(
	     float32_t   x[],		/* (i) : Input signal         */
	     uint16_t    m,		/* (i) : LPC order            */
             float32_t   r[],		/* (o) : Autocorrelations     */
	     float32_t   window[])	/* (i) : LPC Analysis window  */
{
	float32_t buf[FFTSIZE];
	float32_t a0;
	int i, n;
	
	/* apply analysis window */
	arm_mult_f32(x, window, buf, FFTSIZE);
	
	/* compute autocorrealtion coefficients up to lag order */
	for (i = 0; i <= m; i++)
	{
		a0 = 0.0F;
		for (n = i; n < FFTSIZE; n++)
		{
			a0 += buf[n] * buf [n - i];
		}
		r[i] = a0;
	}
}
#endif