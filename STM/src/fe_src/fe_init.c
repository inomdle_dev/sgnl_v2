#include "global_define.h"

#ifdef ENABLE_HY_FE
#define FE_FRAMESIZE 80

//#define FIR_LPF

#define TEST_LENGTH_SAMPLES 		320//160 
#define BLOCK_SIZE			32
#define NUM_TAPS			29
 
/* ------------------------------------------------------------------- 
 * Declare State buffer of size (numTaps + blockSize - 1) 
 * ------------------------------------------------------------------- */ 

static float32_t firStateF32[BLOCK_SIZE + NUM_TAPS - 1]; 
 
/* ---------------------------------------------------------------------- 
** FIR Coefficients buffer generated using fir1() MATLAB function. 
** fir1(28, 6/24)
** ------------------------------------------------------------------- */ 
 
const float32_t firCoeffs32[NUM_TAPS] = { 
-0.0018225230f, -0.0015879294f, +0.0000000000f, +0.0036977508f, +0.0080754303f, +0.0085302217f, -0.0000000000f, -0.0173976984f, 
-0.0341458607f, -0.0333591565f, +0.0000000000f, +0.0676308395f, +0.1522061835f, +0.2229246956f, +0.2504960933f, +0.2229246956f, 
+0.1522061835f, +0.0676308395f, +0.0000000000f, -0.0333591565f, -0.0341458607f, -0.0173976984f, -0.0000000000f, +0.0085302217f, 
+0.0080754303f, +0.0036977508f, +0.0000000000f, -0.0015879294f, -0.0018225230f 
}; 
 
/* ------------------------------------------------------------------ 
 * Global variables for FIR LPF Example 
 * ------------------------------------------------------------------- */ 

uint32_t blockSize = BLOCK_SIZE; 
uint32_t numBlocks = TEST_LENGTH_SAMPLES/BLOCK_SIZE; 
 


#ifdef ENABLE_AUDIO				
#if defined(ENABLE_EQ) | defined(ENABLE_HY_FE)
void fe_init()
{
#if defined(ENABLE_FE_FPU_TEST)
	Init_fe1();
	init_vad1();
	reset_AHDR1();
#elif defined(ENABLE_FE)
	Init_fe();
	init_vad();
	reset_AHDR();
#elif defined(ENABLE_HY_FE)
	memset(Previn_buf, 0x00, sizeof(Previn_buf));
	memset(Prevsynth_buf, 0x00, sizeof(Prevsynth_buf));
#endif
	memset(&g_str_eq, 0, sizeof(eq_struct_t));
	memset(&g_str_fe, 0, sizeof(eq_struct_t));
}

void formant_enhancement( void )
{
	int16_t i16_buf16[FE_FRAMESIZE];
	uint32_t ui32_data = 0;

	if( g_str_bitflag.b1_fe_enable )
	{
		if(g_str_ble_status.audio_type_status == AUDIO_HFP)
		{
			if(g_str_audio.ui16_audio_type == AUDIO_FREQ_8K)
			{
				for (int j = 0; j < FE_FRAMESIZE; j++)
				{
					if(g_str_audio.ui16_data[g_str_eq.ui16_frame_no][j<<1] != 0x0000)
						i16_buf16[j] = (int16_t)g_str_audio.ui16_data[g_str_eq.ui16_frame_no][j<<1];
					else
						i16_buf16[j] = (int16_t)g_str_audio.ui16_data[g_str_eq.ui16_frame_no][(j<<1)+1];
				}
			}
			else if(g_str_audio.ui16_audio_type == AUDIO_FREQ_16K)
			{
#ifdef FIR_LPF
				uint32_t i; 
				arm_fir_instance_f32 S; 
				float32_t  *inputF32, *outputF32; 
				float32_t lpf_input[FE_FRAMESIZE*4] = {0,};
				float32_t lpf_output[FE_FRAMESIZE*4] = {0,};
				for (int j = 0; j < FE_FRAMESIZE*4; ++j)
				{
					lpf_input[j] = (float32_t)g_str_audio.ui16_data[g_str_eq.ui16_frame_no][j];
				}
				
				/* Initialize input and output buffer pointers */ 
				inputF32 = lpf_input;
				outputF32 = lpf_output;
				
				/* Call FIR init function to initialize the instance structure. */
				arm_fir_init_f32(&S, NUM_TAPS, (float32_t *)&firCoeffs32[0], &firStateF32[0], blockSize); 
				
				/* ---------------------------------------------------------------------- 
				** Call the FIR process function for every blockSize samples  
				** ------------------------------------------------------------------- */ 
				for(i=0; i < numBlocks; i++)  
				{	 
					arm_fir_f32(&S, inputF32 + (i * blockSize), outputF32 + (i * blockSize), blockSize);  
				}

				for (int j = 0; j < FE_FRAMESIZE*4; ++j)
				{
					g_str_audio.ui16_data[g_str_eq.ui16_frame_no][j] = (uint16_t)lpf_output[j];
				}
#endif
				for (int j = 0; j < FE_FRAMESIZE; ++j)
				{
					ui32_data = g_str_audio.ui16_data[g_str_eq.ui16_frame_no][j<<2] + g_str_audio.ui16_data[g_str_eq.ui16_frame_no][(j<<2)+1];
					i16_buf16[j] = ui32_data;
				}
			}
			
			FEnh_func(i16_buf16);
			
			for (int j = 0; j < FE_FRAMESIZE; j++)
			{
				g_str_fe.ui16_data[g_str_eq.ui16_frame_no][j<<1] = (uint16_t)i16_buf16[j];
				g_str_fe.ui16_data[g_str_eq.ui16_frame_no][(j<<1)+1] = (uint16_t)i16_buf16[j];
			}
		}
		else
		{
			for (int j = 0; j < FE_FRAMESIZE; j++)
			{
				i16_buf16[j] = (int16_t)g_str_audio.ui16_data[g_str_eq.ui16_frame_no][(uint16_t)(j<<1)];
			}
			
			//FEnh_func(i16_buf16);
			
			for (int j = 0; j < FE_FRAMESIZE; j++)
			{
				g_str_fe.ui16_data[g_str_eq.ui16_frame_no][j<<1] = (uint16_t)i16_buf16[j];
				g_str_fe.ui16_data[g_str_eq.ui16_frame_no][(j<<1)+1] = (uint16_t)i16_buf16[j];
			}
		}
	}
}
#endif
#endif
#endif