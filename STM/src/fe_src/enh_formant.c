#include "global_define.h"

#ifdef ENABLE_FE_FPU_TEST

//#define L4096
#ifdef L4096
float Window[FFTSIZE] = {
	0.000267706288f,    0.00240763673f,    0.00667833537f,    0.0130615123f,
	0.0215298347f,    0.0320470408f,    0.0445680916f,    0.0590393767f,
	0.0753989220f,    0.0935766771f,    0.113494776f,    0.135067970f,
	0.158203870f,    0.182803378f,    0.208761185f,    0.235966116f,
	0.264301658f,    0.293646544f,    0.323875040f,    0.354857683f,
	0.386461884f,    0.418552339f,    0.450991452f,    0.483640522f,
	0.516359627f,    0.549008667f,    0.581447780f,    0.613538206f,
	0.645142436f,    0.676125050f,    0.706353605f,    0.735698462f,
	0.764033973f,    0.791238904f,    0.817196667f,    0.841796219f,
	0.864932060f,    0.886505306f,    0.906423450f,    0.924601078f,
	0.940960646f,    0.955431938f,    0.967952967f,    0.978470147f,
	0.986938477f,    0.993321657f,    0.997592330f,    0.999732256f,
	1.00000000f,    1.00000000f,    1.00000000f,    1.00000000f,
	1.00000000f,    1.00000000f,    1.00000000f,    1.00000000f,
	1.00000000f,    1.00000000f,    1.00000000f,    1.00000000f,
	1.00000000f,    1.00000000f,    1.00000000f,    1.00000000f,
	1.00000000f,    1.00000000f,    1.00000000f,    1.00000000f,
	1.00000000f,    1.00000000f,    1.00000000f,    1.00000000f,
	1.00000000f,    1.00000000f,    1.00000000f,    1.00000000f,
	1.00000000f,    1.00000000f,    1.00000000f,    1.00000000f,
	0.999732256f,    0.997592330f,    0.993321657f,    0.986938477f,
	0.978470147f,    0.967952967f,    0.955431819f,    0.940960526f,
	0.924600959f,    0.906423330f,    0.886505187f,    0.864931941f,
	0.841796100f,    0.817196548f,    0.791238725f,    0.764033794f,
	0.735698223f,    0.706353366f,    0.676124871f,    0.645142198f,
	0.613538027f,    0.581447601f,    0.549008489f,    0.516359389f,
	0.483640343f,    0.450991303f,    0.418552101f,    0.386461735f,
	0.354857534f,    0.323874831f,    0.293646336f,    0.264301509f,
	0.235965937f,    0.208761021f,    0.182803243f,    0.158203721f,
	0.135067865f,    0.113494679f,    0.0935765654f,    0.0753988251f,
	0.0590392910f,    0.0445680171f,    0.0320469774f,    0.0215297826f,
	0.0130614713f,    0.00667830463f,    0.00240761926f,    0.000267700350f
};
#else
float32_t fWindow[FFTSIZE]={
	0.0800000000000000,0.0805628485414398,0.0822500167810610,0.0850573759354599,
	0.0889780559263524,0.0940024621928066,0.100118299170741,0.107310600382232,
	0.115561765060989,0.124851601224384,0.135157375086613,0.146453866692083,
	0.158713431632863,0.171906068699192,0.185999493297458,0.200959216456013,
	0.216748629225465,0.233329092266912,0.250660030408878,0.268699031941561,
	0.287401952405387,0.306723022619904,0.326614960688639,0.347029087705815,
	0.367915446881795,0.389222925795719,0.410899381476157,0.432891768003705,
	0.455146266323232,0.477608415948132,0.500223248234257,0.522935420897402,
	0.545689353445147,0.568429363191639,0.591099801522450,0.613645190076067,
	0.636010356508734,0.658140569510427,0.679981672741539,0.701480217362516,
	0.722583592832128,0.743240155654278,0.763399355758289,0.783011860203399,
	0.802029673904734,0.820406257085326,0.838096639166739,0.855057528819616,
	0.871247419904816,0.886626693045890,0.901157712584333,0.914804918680351,
	0.927534914333745,0.939316547111965,0.950120985385339,0.959921788882895,
	0.968694973396140,0.976419069472435,0.983075174954342,0.988647001236375,
	0.993120913125941,0.996485962210947,0.998733913652397,0.999859266336418,
	0.999859266336418,0.998733913652397,0.996485962210947,0.993120913125941,
	0.988647001236375,0.983075174954342,0.976419069472435,0.968694973396140,
	0.959921788882895,0.950120985385339,0.939316547111965,0.927534914333745,
	0.914804918680351,0.901157712584333,0.886626693045890,0.871247419904816,
	0.855057528819616,0.838096639166739,0.820406257085326,0.802029673904734,
	0.783011860203399,0.763399355758289,0.743240155654278,0.722583592832128,
	0.701480217362516,0.679981672741539,0.658140569510427,0.636010356508734,
	0.613645190076067,0.591099801522450,0.568429363191639,0.545689353445147,
	0.522935420897402,0.500223248234257,0.477608415948132,0.455146266323232,
	0.432891768003705,0.410899381476157,0.389222925795719,0.367915446881795,
	0.347029087705815,0.326614960688639,0.306723022619904,0.287401952405387,
	0.268699031941561,0.250660030408878,0.233329092266912,0.216748629225465,
	0.200959216456013,0.185999493297458,0.171906068699192,0.158713431632863,
	0.146453866692083,0.135157375086613,0.124851601224384,0.115561765060989,
	0.107310600382232,0.100118299170741,0.0940024621928066,0.0889780559263524,
	0.0850573759354599,0.0822500167810610,0.0805628485414398,0.0800000000000000};
#endif

float32_t Previn_Buf[FFTSIZE];
float32_t Prevsynth_Buf[FFTSIZE];
float32_t preemph_last_FE;
float32_t deemph_last_FE;

float32_t *pOverlap;
int16_t old_AA[11];
float32_t fold_A[11];

#ifdef AMR_NB
float32_t FFT_Buf[FFTSIZE];
#else
float32_t FFT_Buf[FFTSIZE*2];
#endif

void Init_fe1(void)
{
	pOverlap = FFT_Buf + FRONTMARGIN;
	
#ifdef L4096
	memset(old_AA, 0x00, sizeof(old_AA));
	// For LPC coefficient
	old_AA[0] = 4096;
#else
	memset(fold_A, 0x00, sizeof(fold_A));
	// For LPC coefficient
	fold_A[0] = 1.0f;
#endif
	
	preemph_last_FE = 0.f;
	deemph_last_FE = 0.f;
	
	memset(Previn_Buf, 0, sizeof(Previn_Buf));
	memset(Prevsynth_Buf, 0, sizeof(Prevsynth_Buf));
}

void preemphasis(int len, float *buf, float *prev)
{
    short k;
    float temp;

    for (k = 0; k < len; k++)
    {
        temp = buf[k];
        buf[k] -= (ALPHA **prev);
        *prev = temp;
    }
}

void deemphasis(int len, float *buf, float *prev)
{
    short k;

    for (k = 0; k < len; k++)
    {
        buf[k] += (ALPHA **prev);
        *prev = buf[k];
    }
}

int compare(void *first, void *second)
{
	if (*(float *)first > *(float *)second)
		return 1;
	else if (*(float *)first < *(float *)second)
		return -1;
	else
		return 0;
}

void FormantEnhancement1(int16_t *Signal, int Mode)
{
	int16_t Signal_AC[FRONTMARGIN + FRAMESIZE];
	int16_t SignalVAD[FRAMESIZE] = {0,};
	//int16_t rc[4];
	int16_t LPC[44];
#ifdef L4096
	int16_t rHigh[11], rLow[11];
#endif

	int Final, Apt;
	int idx, idx_1, idx_2, cnt = 0, cnt2 = 0, cnt3 = 0, idx_3, idx_4, flag = 0;
	int Formant[M];
	int Valley_buf[10];
	int Size;
	int MinimumGap_idx;
	int MinimumGap;
	int HarValley[WINDOW_BY_TWO], HarSize;
	int Gap[M];
	int ApplyRange[M][2];
	int Detect;
	
	float32_t Out_Buf[FRAMESIZE];
	float32_t FFTabs[WINDOW_BY_TWO];
#ifdef AMR_NB
	float32_t FFT_LPC[FFTSIZE];
#else
	float32_t FFT_LPC[FFTSIZE*2];
	float32_t oFFT_Buf[FFTSIZE];
	float32_t TT[FFTSIZE];
#endif
	float32_t FE_SignalReal[WINDOW_BY_TWO];
	float32_t FE_SignalIMG[WINDOW_BY_TWO];
	float32_t Norm[WINDOW_BY_TWO];
	float32_t Roots_Positive[10][2];
	float32_t angz[10];
	float32_t Eqn[EQNSIZE + 1];
	float32_t Roots[EQNSIZE] = {0,};
	float32_t Roots_I[EQNSIZE][2] = {0,};
	float32_t BandWidth[M];
	float32_t Formants[M];
	float32_t FormantsNew[M], BandWidthNew[M];
	float32_t FF_BW[M][2];
	
	//float32_t GainSize_One = 0;
	float32_t Valley = 0;
	float32_t GainSize = 0;
	float32_t DC, FO;
	
	float32_t *pSynth_Buf;
	float32_t *pGainFilter;
	
	TM_FFT_F32_t FFT;    /*!< FFT structure */
	
	// 송광섭 학생의 Frequency Response를 고려한 Formant Enhancement를 위한 Table
	float SKS_FR[FFTSIZE_BY_TWO][3] = {
		{ 1.15f, 0.83f, 0.78f }, { 1.03f, 0.85f, 0.76f }, { 0.80f, 0.80f, 0.78f }, { 0.87f, 0.71f, 0.66f },
		{ 0.87f, 0.64f, 0.63f }, { 0.62f, 0.60f, 0.57f }, { 0.62f, 0.58f, 0.58f }, { 0.69f, 0.60f, 0.60f },
		{ 0.64f, 0.62f, 0.61f }, { 0.65f, 0.65f, 0.63f }, { 0.78f, 0.71f, 0.63f }, { 0.76f, 0.75f, 0.63f },
		{ 0.98f, 0.79f, 0.64f }, { 1.01f, 0.79f, 0.67f }, { 0.97f, 0.94f, 0.88f }, { 1.03f, 0.87f, 0.86f },
		{ 0.96f, 0.83f, 0.81f }, { 0.89f, 0.82f, 0.80f }, { 1.01f, 0.96f, 0.76f }, { 1.23f, 1.11f, 0.96f },
		{ 1.24f, 1.08f, 1.01f }, { 1.15f, 1.01f, 0.99f }, { 1.24f, 1.15f, 1.07f }, { 1.26f, 1.16f, 1.09f },
		{ 1.30f, 1.06f, 1.05f }, { 1.43f, 1.08f, 1.07f }, { 1.25f, 1.07f, 1.05f }, { 1.29f, 1.18f, 1.06f },
		{ 1.40f, 1.12f, 1.09f }, { 1.43f, 1.11f, 1.00f }, { 1.45f, 1.19f, 1.09f }, { 1.37f, 1.10f, 1.00f },
		{ 1.92f, 1.11f, 0.98f }, { 1.30f, 1.07f, 1.06f }, { 1.72f, 1.10f, 1.08f }, { 1.81f, 1.29f, 1.09f },
		{ 1.44f, 1.10f, 0.93f }, { 2.15f, 1.11f, 1.11f }, { 1.61f, 1.39f, 1.10f }, { 1.45f, 1.15f, 1.02f },
		{ 1.33f, 1.08f, 1.08f }, { 1.35f, 1.23f, 1.11f }, { 1.29f, 1.14f, 1.11f }, { 1.46f, 1.05f, 0.98f },
		{ 1.13f, 0.98f, 0.94f }, { 1.07f, 0.72f, 0.64f }, { 1.61f, 0.94f, 0.94f }, { 2.19f, 1.09f, 1.00f },
		{ 1.33f, 1.17f, 1.11f }, { 1.55f, 1.15f, 1.14f }, { 1.31f, 1.19f, 1.18f }, { 1.30f, 1.21f, 1.13f },
		{ 1.30f, 1.29f, 1.13f }, { 1.49f, 1.40f, 1.08f }, { 1.63f, 1.10f, 1.07f }, { 1.39f, 1.10f, 1.09f },
		{ 1.31f, 1.17f, 1.05f }, { 1.08f, 1.07f, 1.03f }, { 1.50f, 0.80f, 0.76f }, { 0.99f, 0.95f, 0.91f },
		{ 1.33f, 1.07f, 0.95f }, { 1.83f, 1.05f, 1.00f }, { 1.13f, 1.11f, 1.00f }, { 1.58f, 1.04f, 0.97f }
	};
	
	pGainFilter = FFT_LPC;

	for (idx_1 = 0; idx_1 < M; idx_1++)
	{
		for (idx_2 = 0; idx_2 < 2; idx_2++)
			Roots_Positive[idx_1][idx_2] = 0;
		
		angz[idx_1] = 0;
		BandWidth[idx_1] =  - 10;
		Formants[idx_1] =  - 10;
		Formant[idx_1] =  - 10;
		
		FF_BW[idx_1][0] =  - 10;
		FF_BW[idx_1][1] =  - 10;
		
		Valley_buf[idx_1] =  - 10;
		
		ApplyRange[idx_1][0] =  - 10;
		ApplyRange[idx_1][1] =  - 10;
	}
	
	// LPC 버퍼 초기화
	memset(LPC, 0x00, sizeof(LPC));
	memset(Eqn, 0x00, sizeof(Eqn));
	
	// LPC의 FFT를 위한 버퍼 초기화
	memset(LPC, 0x00, sizeof(LPC));
	memset(FFT_LPC, 0x00, sizeof(FFT_LPC));
	
	memset(FFT_Buf, 0x00, sizeof(FFT_Buf));
	
	// Formant Enhancement를 하기전에 Overlap-add를 위해 Frame 설정
	for (idx = 0; idx < FRAMESIZE; idx++)
		pOverlap[idx] = (float32_t)Signal[idx];
	
	preemphasis(FRAMESIZE, pOverlap, &preemph_last_FE);
	
	BufferCopy(Previn_Buf + FRAMESIZE, FFT_Buf, FRONTMARGIN);
	
	// Autocorrelation 및 VAD를 위한 신호를 각각의 버퍼에 저장
	for (idx = 0; idx < FRONTMARGIN + FRAMESIZE; idx++)
	{
		Signal_AC[idx] = (int16_t)FFT_Buf[idx];
		if (idx >= FRONTMARGIN && idx < FRONTMARGIN + FRAMESIZE)
			SignalVAD[idx - FRONTMARGIN] = (int16_t)(FFT_Buf[idx]);
	}
	
	// 다음 Overlap-add를 위해 FFT_Buf의 내용을 Previn_Buf에 저장
	BufferCopy(FFT_Buf, Previn_Buf, FFTSIZE);

#ifdef L4096
	// Window를 적용
	Apply_Window(FFT_Buf, Window, FFTSIZE);
#else
	Apply_Window(FFT_Buf, fWindow, FFTSIZE);
#endif	
	
	// VAD 루틴
	// 음성이 존재하는 구간만 Formant Enhancement
	if ( vad1(SignalVAD) )
	{
		/* Init FFT, FFT_SIZE define is used for FFT_SIZE, samples count is FFT_SIZE * 2, don't use malloc for memory allocation */
		TM_FFT_Init_F32(&FFT, FFTSIZE, 0, 0);
		
#ifdef L4096
		// LPC를 구하기 위한 루틴
		Autocorr(Signal_AC, M, rHigh, rLow, Window);
		Levinson(old_AA, rHigh, rLow, LPC);
		memset(FFT_LPC, 0x00, sizeof(FFT_LPC));
		for (idx = 0; idx < 11; idx++)
		{
			Eqn[idx] = FFT_LPC[idx] = (float)LPC[idx] * 0.000244140625f; // ( LPC[idx] / 4096 )
		}
#else
		float32_t fSignal_AC[128];
		float32_t fLPC[44];
		float32_t autocorr_co[11];

		int i = 0;
		for(i=0;i<128;i++)
		{
			fSignal_AC[i] = (float32_t)Signal_AC[i];
		}
		for(i=0;i<44;i++)
		{
			fLPC[i] = (float32_t)LPC[i];
		}
		autocorr(fSignal_AC, M, autocorr_co, fWindow);
		levinson_durbin(fLPC, autocorr_co, M, fold_A);
		memset(FFT_LPC, 0x00, sizeof(FFT_LPC));
		for (idx = 0; idx < 11; idx++)
		{
			Eqn[idx] = FFT_LPC[idx] = fLPC[idx]; // ( LPC[idx] / 4096 )
		}
#endif

		// LPC Coefficient로 구성된 다항식의 실근 혹은 복소수근을 구하는 Bairstow Method 루틴
		// 여기서 pow(0.1, 10)가 정확도를 의미.
		bairstow(Eqn, Roots, Roots_I, (float32_t)pow(0.1, 2));
		
		for (idx = 0; idx < M; idx++)
		{
			if (fabs(Roots_I[idx][0]) > 1.0 || fabs(Roots_I[idx][1]) > 1.0)
			{
				GPIOA->BSRRH = GPIO_Pin_2;//low
				Roots_I[idx][0] = Roots_I[idx][1];
				//bairstow(Eqn, Roots, Roots_I, (float32_t)pow(0.1, 2));
				goto unvoiced;
			}
		}
		
		// 실근과 복소수근을 하나로 통일하기 위한 루틴
		for (idx_1 = 0; idx_1 < M; idx_1++)
		{
			if (Roots[idx_1] != 0)
			{
				for (idx_2 = 0; idx_2 < M; idx_2++)
				{
					if (Roots_I[idx_2][0] == 0 && Roots_I[idx_2][1] == 0)
					{
						Roots_I[idx_2][0] = Roots[idx_1];
						break;
					}
				}
			}
		}
		
		// 위에서 구한 근을 오름차순으로 정렬하는 루틴
		SortArray(Roots_I);
		
		// 복소수근의 허수가 양수인 값만 이용하므로, 해당 값만 저장하는 루틴
		for (idx = 0; idx < M; idx++)
		{
			if (Roots_I[idx][1] >= 0 || (Roots_I[idx][0] != 0 && Roots_I[idx][1] == 0))
			{
				Roots_Positive[cnt2][0] = Roots_I[idx][0];
				Roots_Positive[cnt2][1] = Roots_I[idx][1];
				cnt2++;
			}
		}
		
		if (cnt2 == 11)
			Roots_Positive[cnt2][0] = 100;
		
		// Formant Location을 위한 루틴으로 Formant 및 Formant의 Bandwidth를 구하는 루틴
		for (idx = 0; idx < M; idx++)
		{
			if (Roots_Positive[idx][0] == 0 && Roots_Positive[idx][1] == 0)
				break;
			
			angz[idx] = (float32_t)atan2(Roots_Positive[idx][1], Roots_Positive[idx][0]);
			float32_t fval = Roots_Positive[idx][1] *Roots_Positive[idx][1] + Roots_Positive[idx][0] *Roots_Positive[idx][0];
			BandWidth[idx] = (float32_t)floor((( - 0.5f *((float32_t)Fs / PI1) * log(fval)) / Hz ) + 0.5f);
			
			if ((int)BandWidth[idx] % 2 == 0)
			{
				BandWidth[idx] -= 1;
			}
			
			Formants[idx] = (float32_t)floor(((angz[idx]*(Fs / PI2)) / Hz) + 0.5f);
		}
		
		// Formant Location을 통해 구한 Formant 및 Formant Bandwidth를 오름차순으로 정렬하는 루틴
#if 0
		SortArraySingle(Formants, BandWidth, M);
#else
		qsort(Formants, sizeof(Formants) / sizeof(float), sizeof(float), (_Cmpfun *)compare);
		qsort(BandWidth, sizeof(BandWidth) / sizeof(float), sizeof(float), (_Cmpfun *)compare);
#endif
		cnt = 0;
		for (idx = 0; idx < M; idx++)
		{
			if (Formants[idx] !=  - 10)
				cnt++;
		}
		
		for (idx = 0; idx < cnt; idx++)
		{
			FormantsNew[cnt - idx - 1] = Formants[M - idx - 1];
			BandWidthNew[cnt - idx - 1] = BandWidth[M - idx - 1];
		}
		Size = cnt;
		//GPIOA->BSRRH = GPIO_Pin_2;//low
		///////////////
#ifdef AMR_NB
		// LPC Spectrum을 구하기 위해 LPC를 FFT하는 루틴
		for (idx = 0; idx < FFTSIZE; idx++)
		{
			FFT_LPC[idx] = (float32_t)((float32_t)FFT_LPC[idx] * 8388607.f);
		}
		r_fft(FFT_LPC, 1);

		FE_SignalReal[0] = FFT_LPC[0] / 8388607.f;
		FE_SignalIMG[0] = 0;
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			FE_SignalReal[idx] = FFT_LPC[(2 * idx)] / 8388607.f;
			FE_SignalIMG[idx]  = FFT_LPC[(2 * idx) + 1] / 8388607.f;
		}
		FE_SignalReal[WINDOW_BY_TWO - 1] = FFT_LPC[1] / 8388607.f;
		FE_SignalIMG[WINDOW_BY_TWO - 1] = 0;

		// LPC Spectrum을 구하기 위한 루틴
		for (idx = 0; idx < WINDOW_BY_TWO; idx++)
		{
			Norm[idx] = (float)log(FE_SignalReal[idx] * FE_SignalReal[idx] + FE_SignalIMG[idx] * FE_SignalIMG[idx]) * (-0.5f);// lpc filter
		}
		
		// 음성신호를 FFT하는 루틴
		r_fft(FFT_Buf, 1);
		
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			FE_SignalReal[idx] = FFT_Buf[2 * idx];
			FE_SignalIMG[idx] = FFT_Buf[2 * idx + 1];
		}
		
		// Search Harmonic Valley
		cnt = 0;
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			FFTabs[idx] = (float)log(sqrt(FE_SignalReal[idx] * FE_SignalReal[idx] + FE_SignalIMG[idx] * FE_SignalIMG[idx]));
		}
		
		for (idx = 1; idx < WINDOW_BY_TWO - 2; idx++)
			if (idx > 1 && (FFTabs[idx - 1] > FFTabs[idx]) && FFTabs[idx] < FFTabs[idx + 1])
				cnt++;
		
		HarSize = cnt;
		cnt = 0;
		for (idx = 1; idx < WINDOW_BY_TWO - 2; idx++)
		{
			if (idx > 1 && (FFTabs[idx - 1] > FFTabs[idx]) && FFTabs[idx] < FFTabs[idx + 1])
			{
				HarValley[cnt] = idx;
				cnt++;
			}
		}
		
#else
		/* We didn't used malloc for allocation, so we have to set pointers ourself */
		/* Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part */
		/* Output buffer must be FFT_SIZE in length */
		memcpy(TT, FFT_LPC, sizeof(TT));
		memset(FFT_LPC, 0x00, sizeof(FFT_LPC));
		for(idx = 0; idx < FFTSIZE; idx++)
		{
			FFT_LPC[(idx * 2)] = TT[idx];//real
			FFT_LPC[(idx * 2) + 1] = 0;//imaginary
		}
		TM_FFT_SetBuffers_F32(&FFT, FFT_LPC, oFFT_Buf);
	
		/* Do FFT on signal, values at each bin and calculate max value and index where max value happened */
		//GPIOA->BSRRL = GPIO_Pin_2;//high
		TM_FFT_Process_F32(&FFT);
		//GPIOA->BSRRH = GPIO_Pin_2;//low

		FE_SignalReal[0] = FFT_LPC[0];
		FE_SignalIMG[0] = 0;
		Norm[0] = (float32_t)log(FE_SignalReal[0] * FE_SignalReal[0]) * (-0.5f);
		for (idx = 1; idx < WINDOW_BY_TWO; idx++)
		{
			FE_SignalReal[idx] = FFT_LPC[(idx * 2)];
			FE_SignalIMG[idx]  = FFT_LPC[(idx * 2) + 1];

			// LPC Spectrum을 구하기 위한 루틴
			Norm[idx] = (float32_t)log(FE_SignalReal[idx] * FE_SignalReal[idx] + FE_SignalIMG[idx] * FE_SignalIMG[idx]) * (-0.5f);// lpc filter
		}
		FE_SignalReal[WINDOW_BY_TWO - 1] = FFT_LPC[1];
		FE_SignalIMG[WINDOW_BY_TWO - 1] = 0;
		
		/* We didn't used malloc for allocation, so we have to set pointers ourself */
		/* Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part */
		/* Output buffer must be FFT_SIZE in length */
		memcpy(TT, FFT_Buf, sizeof(TT));
		memset(FFT_Buf, 0x00, sizeof(FFT_Buf));
		for(idx = 0; idx < FFTSIZE; idx++)
		{
			FFT_Buf[(idx * 2)] = TT[idx];//real
			FFT_Buf[(idx * 2) + 1] = 0;//imaginary
		}
		TM_FFT_SetBuffers_F32(&FFT, FFT_Buf, oFFT_Buf);
		
		//GPIOA->BSRRL = GPIO_Pin_2;//high
		/* Do FFT on signal, values at each bin and calculate max value and index where max value happened */
		TM_FFT_Process_F32(&FFT);
		//GPIOA->BSRRH = GPIO_Pin_2;//low
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			FE_SignalReal[idx] = FFT_Buf[(idx * 2)];
			FE_SignalIMG[idx]  = FFT_Buf[(idx * 2) + 1];

			// Search Harmonic Valley
			FFTabs[idx] = (float32_t)log(FE_SignalReal[idx] * FE_SignalReal[idx] + FE_SignalIMG[idx] * FE_SignalIMG[idx]) * (0.5f);// lpc filter
		}
#endif
		// 구해진 LPC Spectrum을 통해 Formant Frequency를 구하는 루틴
		cnt = 0;
		cnt2 = 0;
		cnt3 = 0;
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			if (Norm[0] > Norm[1] && idx == 1)
				Formant[cnt++] = 0;
			
			if ((Norm[idx - 1] < Norm[idx]) && (Norm[idx] > Norm[idx + 1]))
				Formant[cnt++] = idx;
			
			if ((Norm[idx - 1] > Norm[idx]) && (Norm[idx] < Norm[idx + 1]))
				Valley_buf[cnt2++] = idx;

			if ((idx > 1) && (idx < WINDOW_BY_TWO - 2) && (FFTabs[idx] < FFTabs[idx - 1]) && (FFTabs[idx] < FFTabs[idx + 1]))//find harmonic valley
				HarValley[cnt3++] = idx;
		}
		HarSize = cnt3;
		
		// Formant Frequency 및 Bandwidth를 결정하는 루틴
		for (idx = 0; idx < M; idx++)
		{
			if (Formant[idx] ==  - 10)
				break;
			for (idx_1 = 0; idx_1 < Size; idx_1++)
			{
				Gap[idx_1] = abs(Formant[idx] - (int)FormantsNew[idx_1]);
			}
			MinimumGap = Gap[0];
			MinimumGap_idx = 0;
			
			for (idx_2 = 1; idx_2 < Size; idx_2++)
			{
				if (MinimumGap > Gap[idx_2])
				{
					MinimumGap = Gap[idx_2];
					MinimumGap_idx = idx_2;
				}
			}
			
			if (idx == 0 && FormantsNew[0] == 0 && Valley_buf[0] - Formant[0] < (int)(floor(BandWidthNew[MinimumGap_idx] / (float32_t)2.0)))
			{
				for (idx_3 = 0; idx_3 < Size - 1; idx_3++)
				{
					FormantsNew[idx_3] = FormantsNew[idx_3 + 1];
					BandWidthNew[idx_3] = BandWidthNew[idx_3 + 1];
				}
				FormantsNew[idx_3] =  - 10;
				BandWidthNew[idx_3] =  - 10;
				
				idx--;
				continue;
			}
			
			for (idx_3 = 0; idx_3 < cnt2; idx_3++)
			{
				if (Valley_buf[idx_3] < FormantsNew[MinimumGap_idx] && Valley_buf[idx_3 + 1] > FormantsNew[MinimumGap_idx])
				{
					if ((Valley_buf[idx_3 + 1] - Valley_buf[idx_3] + 1) < BandWidthNew[MinimumGap_idx] && BandWidthNew[MinimumGap_idx] > FFTSIZE_BY_TWO)
					{
						for (idx_4 = MinimumGap_idx; idx_4 < Size - 1; idx_4++)
						{
							FormantsNew[idx_4] = FormantsNew[idx_4 + 1];
							BandWidthNew[idx_4] = BandWidthNew[idx_4 + 1];
						}
						FormantsNew[idx_4] =  - 10;
						BandWidthNew[idx_4] =  - 10;
						
						flag = 1;
					}
				}
			}
			if (flag == 1)
			{
				flag = 0;
				idx--;
				continue;
			}
			
			FF_BW[idx][0] = (float32_t)Formant[idx];
			FF_BW[idx][1] = BandWidthNew[MinimumGap_idx];
			
		}
		Final = idx;
		
		if ((int)FF_BW[idx - 1][0] > Valley_buf[cnt2 - 1] && ((int)FF_BW[idx - 1][0] - Valley_buf[cnt2 - 1]) < (int)floor(FF_BW[idx - 1][1] / (float32_t)2.0))
		{
			FF_BW[idx - 1][0] =  - 10;
			FF_BW[idx - 1][1] =  - 10;
		}
		
		// 첫번째 Formant의 오류를 검사하고 오류라면 없애는 루틴
		if (FF_BW[0][0] == 0)
		{
			for (idx = 0; idx < FFTSIZE_BY_TWO-1; idx++)
			{
				if ((Norm[idx] - Norm[idx + 1]) < 0)
				{
					Valley = (float32_t)idx;
					break;
				}
			}
			
			if (Valley - FF_BW[0][0] < floor(FF_BW[0][1] * 0.5f))
			{
				FF_BW[0][0] =  - 10.0;
				FF_BW[0][1] =  - 10.0;
			}
		}
		
		for (idx = 0; idx < M; idx++)
		{
			if (FF_BW[idx][1] == 1)
			{
				FF_BW[idx][1] = 3.0;
			}
		}
		
#if 0
		// 결정된 Formant Frequency 및 Formant Bandwidth를 개선시키는 루틴.
		// 추정된 Formant Bandwidth 인근에 있는 Harmonics Valley를 찾아서 Bandwidth를 더욱 정확하게 추정한다.
		for (idx = 0; idx < M; idx++)
		{
#if 01
			if (FF_BW[idx][0] > 32)// 8000:128, 2000:32, 1000:16
			{
				break;
			}
#endif
			if (((int)FF_BW[idx][0] < 0 || (int)FF_BW[idx][1] < 0) || (isnan(FF_BW[idx][1]) == 1) )// || _fpclass(FF_BW[idx][1]) == _FPCLASS_PINF)
				//if (((int)FF_BW[idx][0] < 0 || (int)FF_BW[idx][1] < 0) || (isnan(FF_BW[idx][1]) == 1) || _fpclass(FF_BW[idx][1]) == 0x0200)
				continue;
			
			Detect = (int)FF_BW[idx][0];
			
			switch (Detect)
			{
			case 0:
				if (FF_BW[idx][0] == (float32_t)0.0)
				{
					for (idx_1 = 0; idx_1 < (int)FF_BW[idx + 1][0]; idx_1++)
						if (Norm[idx_1] < Norm[idx_1 + 1])
							break;
					
					for (idx_2 = 0; idx_2 < HarSize; idx_2++)
						if ((int)floor(FF_BW[idx][1] * 0.5f) < HarValley[idx_2] && HarValley[idx_2] < idx_1)
							break;
					
					FF_BW[idx][1] = (float32_t)(2 *HarValley[idx_2] + 1);
					ApplyRange[idx][0] = 0;
					ApplyRange[idx][1] = HarValley[idx_2];
				}
				else
				{
					for (idx_1 = HarSize; idx_1 != 0; idx_1--)
					{
						if ((int)Valley < HarValley[idx_1 - 1] && HarValley[idx_1 - 1] < (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] * 0.5f)))
						{
							ApplyRange[idx][0] = HarValley[idx_1 - 1];
							break;
						}
					}
					
					for (idx_1 = 0; idx_1 < cnt2; idx_1++)
						if ((int)FF_BW[idx][0] < Valley_buf[idx_1] && Valley_buf[idx_1] < (int)FF_BW[idx + 1][0])
							break;
					
					for (idx_2 = 0; idx_2 < HarSize; idx_2++)
					{
						if ((int)(FF_BW[idx][0] + floor(FF_BW[idx][1] * 0.5f)) < HarValley[idx_2] && HarValley[idx_2] < Valley_buf[idx_1])
						{
							ApplyRange[idx][1] = HarValley[idx_2];
							break;
						}
					}
					
					if (ApplyRange[idx][0] ==  - 10)
					{
						ApplyRange[idx][0] = (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] * 0.5f + 0.5f));
						
						for (idx_2 = 0; idx_2 < HarSize; idx_2++)
						{
							if (ApplyRange[idx][0] < HarValley[idx_2] && HarValley[idx_2] < FF_BW[idx][0])
							{
								ApplyRange[idx][0] = HarValley[idx_2];
								break;
							}
						}
					}
					
					if (ApplyRange[idx][1] ==  - 10)
					{
						ApplyRange[idx][1] = (int)(FF_BW[idx][0] + floor(FF_BW[idx][1] * 0.5f + 0.5f));
						
						for (idx_2 = HarSize; idx_2 != 0; idx_2--)
						{
							if (FF_BW[idx][0] < HarValley[idx_2 - 1] && HarValley[idx_2 - 1] < ApplyRange[idx][1])
							{
								ApplyRange[idx][1] = HarValley[idx_2 - 1];
								break;
							}
						}
					}
					
					if (ApplyRange[idx][0] < 0)
						ApplyRange[idx][0] = 0;
					
					FF_BW[idx][1] = 2 *((float32_t)ApplyRange[idx][1] - FF_BW[idx][0]) + 1;
					
				}
				
				continue;
			default:
				if ((idx != Final - 1) || (idx == Final - 1 && Valley_buf[cnt2 - 1] > (int)FF_BW[idx][0]))
				{
					for (idx_1 = 0; idx_1 < cnt2; idx_1++)
						if (FF_BW[idx - 1][0] < Valley_buf[idx_1] && Valley_buf[idx_1] < FF_BW[idx][0])
							break;
					
					for (idx_2 = HarSize; idx_2 != 0; idx_2--)
					{
						if (Valley_buf[idx_1] < HarValley[idx_2 - 1] && HarValley[idx_2 - 1] < (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] * 0.5f)))
						{
							ApplyRange[idx][0] = HarValley[idx_2 - 1];
							break;
						}
					}
					
					for (idx_1 = 0; idx_1 < cnt2; idx_1++)
						if ((int)FF_BW[idx][0] < Valley_buf[idx_1] && Valley_buf[idx_1] < (int)FF_BW[idx + 1][0])
							break;
					
					for (idx_2 = 0; idx_2 < HarSize; idx_2++)
					{
						if ((int)(FF_BW[idx][0] + floor(FF_BW[idx][1] * 0.5f)) < HarValley[idx_2] && HarValley[idx_2] < Valley_buf[idx_1])
						{
							ApplyRange[idx][1] = HarValley[idx_2];
							break;
						}
					}
					
					if (ApplyRange[idx][0] ==  - 10)
					{
						ApplyRange[idx][0] = (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] * 0.5f + 0.5f));
						
						for (idx_2 = 0; idx_2 < HarSize; idx_2++)
						{
							if (ApplyRange[idx][0] < HarValley[idx_2] && HarValley[idx_2] < FF_BW[idx][0])
							{
								ApplyRange[idx][0] = HarValley[idx_2];
								break;
							}
						}
					}
					
					if (ApplyRange[idx][1] ==  - 10)
					{
						ApplyRange[idx][1] = (int)(FF_BW[idx][0] + floor(FF_BW[idx][1] *0.5f + 0.5f));
						
						for (idx_2 = HarSize; idx_2 != 0; idx_2--)
						{
							if (FF_BW[idx][0] < HarValley[idx_2 - 1] && HarValley[idx_2 - 1] < ApplyRange[idx][1])
							{
								ApplyRange[idx][1] = HarValley[idx_2 - 1];
								break;
							}
						}
					}
					
					if ((int)FF_BW[idx][0] - ApplyRange[idx][0] < ApplyRange[idx][1] - (int)FF_BW[idx][0])
						FF_BW[idx][1] = 2 *((float32_t)ApplyRange[idx][1] - FF_BW[idx][0]) + 1;
					else
					{
						FF_BW[idx][1] = 2 *(FF_BW[idx][0] - (float32_t)ApplyRange[idx][0]) + 1;
					}
				}
				else if (idx == Final - 1 && Valley_buf[cnt2 - 1] < (int)FF_BW[idx][0])
				{
					for (idx_2 = HarSize; idx_2 != 0; idx_2--)
					{
						if (Valley_buf[cnt2 - 1] < HarValley[idx_2 - 1] && HarValley[idx_2 - 1] < (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] * 0.5f)))
						{
							ApplyRange[idx][0] = HarValley[idx_2 - 1];
							break;
						}
					}
					
					for (idx_2 = 0; idx_2 < HarSize; idx_2++)
					{
						if ((int)(FF_BW[idx][0] + floor(FF_BW[idx][1] * 0.5f)) < HarValley[idx_2] && HarValley[idx_2] < FFTSIZE_BY_TWO)
						{
							ApplyRange[idx][1] = HarValley[idx_2];
							break;
						}
					}
					
					if (ApplyRange[idx][0] ==  - 10)
					{
						ApplyRange[idx][0] = (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] * 0.5f + 0.5f));
						
						for (idx_2 = 0; idx_2 < HarSize; idx_2++)
						{
							if (ApplyRange[idx][0] < HarValley[idx_2] && HarValley[idx_2] < FF_BW[idx][0])
							{
								ApplyRange[idx][0] = HarValley[idx_2];
								break;
							}
						}
					}
					
					if (ApplyRange[idx][1] ==  - 10)
					{
						ApplyRange[idx][1] = (int)(FF_BW[idx][0] + floor(FF_BW[idx][1] * 0.5f + 0.5f));
						
						for (idx_2 = HarSize; idx_2 != 0; idx_2--)
						{
							if (FF_BW[idx][0] < HarValley[idx_2 - 1] && HarValley[idx_2 - 1] < ApplyRange[idx][1])
							{
								ApplyRange[idx][1] = HarValley[idx_2 - 1];
								break;
							}
						}
					}
					
					if ((int)FF_BW[idx][0] - ApplyRange[idx][0] < ApplyRange[idx][1] - (int)FF_BW[idx][0])
						FF_BW[idx][1] = 2 *((float32_t)ApplyRange[idx][1] - FF_BW[idx][0]) + 1;
					else
					{
						FF_BW[idx][1] = 2 *(FF_BW[idx][0] - (float32_t)ApplyRange[idx][0]) + 1;
						
					}
				}
			}
		}
#endif
		
		// Formant Enhancement를 적용하는 루틴
		DC = FE_SignalReal[0];
		FO = FE_SignalIMG[0];
		
		for (idx_1 = 0; idx_1 < FORMANTNUM; idx_1++)
		{
			if (((int)FF_BW[idx_1][0] < 0 || (int)FF_BW[idx_1][1] < 0) || (isnan(FF_BW[idx_1][1]) == 1) )//|| _fpclass(FF_BW[idx_1][1]) == _FPCLASS_PINF)
				//if (((int)FF_BW[idx_1][0] < 0 || (int)FF_BW[idx_1][1] < 0) || (isnan(FF_BW[idx_1][1]) == 1) || fpclass(FF_BW[idx_1][1]) == 0x0200)
				continue;

#if 01
			if (FF_BW[idx_1][0] > 32)// 8000:128, 2000:32, 1000:16
			{
				break;
			}
#endif
			
#if 0
			if (SKS_FR[(int)FF_BW[idx_1][0]][Mode] > 3)
				GainSize = 3;
			else
#endif
				GainSize = SKS_FR[(int)FF_BW[idx_1][0]][Mode];
			
			Apt = ApplyRange[idx_1][1] - ApplyRange[idx_1][0] + 1;
			
			// Gain Window를 생성하는 루틴
			HammingGainFilter(Apt, pGainFilter, GainSize);
			
			if ( ( (ApplyRange[idx_1][0] > 63) || (ApplyRange[idx_1][1] > 63) ) //63 -> (FFTSIZE_BY_TWO - 1)
			    || ( (ApplyRange[idx_1][0] < 0) || (ApplyRange[idx_1][1] < 0) ) )
				continue;

			idx = ApplyRange[idx_1][0];
			arm_mult_f32(FE_SignalReal + idx, pGainFilter, FE_SignalReal + idx, Apt);
			arm_mult_f32(FE_SignalIMG + idx,  pGainFilter, FE_SignalIMG + idx,  Apt);
		}
		
		FE_SignalReal[0] = DC;
		FE_SignalIMG[0] = FO;
		
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			FFT_Buf[(idx * 2)] = FE_SignalReal[idx];
			FFT_Buf[(idx * 2) + 1] = FE_SignalIMG[idx];
		}

#ifdef AMR_NB
		r_fft(FFT_Buf, -1);
#else
		/* Init FFT, FFT_SIZE define is used for FFT_SIZE, samples count is FFT_SIZE * 2, don't use malloc for memory allocation */
		TM_FFT_Init_F32(&FFT, FFTSIZE, 1, 0);
		
		/* We didn't used malloc for allocation, so we have to set pointers ourself */
		/* Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part */
		/* Output buffer must be FFT_SIZE in length */
		TM_FFT_SetBuffers_F32(&FFT, FFT_Buf, oFFT_Buf);
		
		//GPIOA->BSRRL = GPIO_Pin_2;//high
		/* Do FFT on signal, values at each bin and calculate max value and index where max value happened */
		TM_FFT_Process_F32(&FFT);
		//GPIOA->BSRRH = GPIO_Pin_2;//low

		memcpy(TT, FFT_Buf, sizeof(FFT_Buf));
		memset(FFT_Buf, 0x00, sizeof(FFT_Buf));
		for(idx = 0; idx < FFTSIZE; idx++)
		{
			FFT_Buf[idx] = TT[(idx * 2)];//real
		}
		GPIOA->BSRRH = GPIO_Pin_2;//low
#endif
	}

unvoiced:
	pSynth_Buf = FFT_Buf;
	
	int overlap = FFTSIZE - FRAMESIZE;
	// Overlap-add를 위한 루틴
	for (idx = 0; idx < overlap; idx++)
	{
		Out_Buf[idx] = pSynth_Buf[idx] + Prevsynth_Buf[idx + FRAMESIZE];
	}

	memcpy(Out_Buf+overlap, pSynth_Buf+overlap, (FRAMESIZE-overlap)*sizeof(float32_t));
	
	BufferCopy(pSynth_Buf, Prevsynth_Buf, FFTSIZE);
	
	deemphasis(FRAMESIZE, Out_Buf, &deemph_last_FE);
	
	// Overflow가 되면 최대치로 고정시켜주는 루틴
	for (idx = 0; idx < FRAMESIZE; idx++)
	{
		if (Out_Buf[idx] > MAX_16)
			Signal[idx] = MAX_16;
		else if (Out_Buf[idx] <  MIN_16)
			Signal[idx] = MIN_16;
		else
			Signal[idx] = (int16_t)(Out_Buf[idx] + 0.5f);
	}
	
	return;
}

void HammingGainFilter(int16_t BW, float32_t *GainFilter, float32_t GainSize)
{
	short idx1;
	float64_t val0, val1;
	float32_t ttt;
	
#if 0
	val1 = (float32_t)(0.54 - 0.46 * cosf(PI2 *(1 / (BW + 1))));
	for (idx1 = 0; idx1 < BW; idx1++)
	{
		val0 = (float32_t)(0.54 - 0.46 * cosf(PI2 *((idx1 + 1) / (BW + 1))));
		GainFilter[idx1] = (float32_t)pow((val0 - val1) / (1 - val1) + 1, GainSize);
	}
#else
	val1 = (float32_t)(0.54f - 0.46f * cosf(PI2 *(1.0f / ((float)BW + 1.0f))));
	val1 = (float32_t)(0.54f - 0.46f * arm_cos_f32(PI2 *(1.0f / ((float)BW + 1.0f))));
	for (idx1 = 0; idx1 < BW; idx1++)
	{
		val0 = (float32_t)(0.54f - 0.46f * cosf(PI2 *(((float)idx1 + 1.0f) / ((float)BW + 1.0f))));
		val0 = (float64_t)(0.54f - 0.46f * arm_cos_f32(PI2 *((idx1 + 1.0f) / ((float)BW + 1.0f))));
		ttt = (float32_t)pow(((val0 - val1) / (1.0 - val1)) + 1.0, GainSize);
		GainFilter[idx1] = ttt;
	}
#endif
	return;
}

void bairstow(float32_t *equ, float32_t *arroot_R, float32_t arroot_I[][2], float32_t Accuracy)
#if 01
{
	float32_t r, s;
	float32_t r_last, s_last;
	float32_t dr, ds;
	float32_t b[EQNSIZE + 1];
	float32_t c[EQNSIZE + 1];
	float32_t buffer_b[EQNSIZE + 1];
	int i, j, k;
	int n, m, o;
	float32_t sqrt_out;
	
	memcpy(buffer_b, equ, (EQNSIZE + 1) *sizeof(float32_t));
	
	for (i = 0; i <= EQNSIZE; i++)
	{
		if (buffer_b[i] == 0)
		{
			//buffer_b[i] = 0.00000001f;
			buffer_b[i] = Accuracy;
		}
	}
	
	n = EQNSIZE;
	m = EQNSIZE;
	o = (n >> 1);
	float32_t pre_cal = 0.0;
	for (j = 0; j < o; j++)
	{
		float32_t val0;
		
		r_last = (-1)*(buffer_b[1]) / (buffer_b[0]);
		s_last = (-1)*(buffer_b[2]) / (buffer_b[0]);
		
		if (m == 2 && j == 0)
		{
			r = r_last;
			s = s_last;
			
			pre_cal = (float32_t)(r *r + 4 * s);
			val0 = r * 0.5f;
			if (pre_cal > 0)
			{
#if 01
				arm_sqrt_f32(pre_cal,&sqrt_out);
				arroot_R[(j * 2)] = val0 + (sqrt_out * 0.5f);
				arroot_R[(j * 2) + 1] = val0 - (sqrt_out * 0.5f);
#else
				arroot_R[2 * j] = val0 + ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
				arroot_R[2 * j + 1] = val0 - ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
#endif
			}
			else if (pre_cal == 0)
			{
				arroot_R[(j * 2)] = val0;
				arroot_R[(j * 2) + 1] = val0;
			}
			else if (pre_cal < 0)
			{
#if 01
				arm_sqrt_f32((-1)*pre_cal,&sqrt_out);
				arroot_I[(j * 2)][0] = val0;
				arroot_I[(j * 2)][1] = sqrt_out * (-0.5f);
				arroot_I[(j * 2) + 1][0] = val0;
				arroot_I[(j * 2) + 1][1] = sqrt_out * 0.5f;
#else
				arroot_I[2 * j][0] = val0;
				arroot_I[2 * j][1] = (-1)*((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
				arroot_I[2 * j + 1][0] = val0;
				arroot_I[2 * j + 1][1] = ((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
#endif
			}
		}
		else if (m == 2 && j != 0)
		{
			r = (-1)*(b[1]) / (b[0]);
			s = (-1)*(b[2]) / (b[0]);
			
			val0 = r * 0.5f;
			pre_cal = (float32_t)(r *r + 4 * s);
			if (pre_cal > 0)
			{
#if 01
				arm_sqrt_f32(pre_cal,&sqrt_out);
				arroot_R[(j * 2)] = val0 + (sqrt_out * 0.5f);
				arroot_R[(j * 2) + 1] = val0 - (sqrt_out * 0.5f);
#else
				arroot_R[2 * j] = val0 + ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
				arroot_R[2 * j + 1] = val0 - ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
#endif
			}
			else if (pre_cal == 0)
			{
				arroot_R[(j * 2)] = val0;
				arroot_R[(j * 2) + 1] = val0;
			}
			else if (pre_cal < 0)
			{
#if 01
				arm_sqrt_f32((-1)*pre_cal,&sqrt_out);
				arroot_I[(j * 2)][0] = val0;
				arroot_I[(j * 2)][1] = sqrt_out * (-0.5f);
				arroot_I[(j * 2) + 1][0] = val0;
				arroot_I[(j * 2) + 1][1] = sqrt_out * 0.5f;
#else
				arroot_I[2 * j][0] = val0;
				arroot_I[2 * j][1] = (-1)*((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
				arroot_I[2 * j + 1][0] = val0;
				arroot_I[2 * j + 1][1] = ((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
#endif
			}
			
		}
		
		for (k = 0; k <= LOOPMAX; k++)
		{
			for (i = 0; i <= m; i++)
			{
				if (i == 0)
				{
					b[i] = buffer_b[i];
					c[i] = b[i];
				}
				else if (i == 1)
				{
					b[i] = buffer_b[i] + (r_last)*(b[i - 1]);
					c[i] = b[i] + (r_last)*(c[i - 1]);
				}
				else
				{
					b[i] = buffer_b[i] + (r_last)*(b[i - 1]) + (s_last)*(b[i - 2]);
					c[i] = b[i] + (r_last)*(c[i - 1]) + (s_last)*(c[i - 2]);
				}
				
			}
			
			float32_t fdeno = (c[m - 1]*c[m - 3]) - (c[m - 2]*c[m - 2]);
			if (fdeno == 0)
			{
				dr = 0;
				ds = 0;
			}
			else
			{
				dr = ((b[m - 1])*(c[m - 2]) - (b[m])*(c[m - 3])) / fdeno;
				ds = ((b[m])*(c[m - 2]) - (b[m - 1])*(c[m - 1])) / fdeno;
			}
			
			if (isnan(dr) == 1 && isnan(ds) == 1)
			{
				break;
			}
			
			r = r_last + dr;
			s = s_last + ds;
			
			if ((fabs((ds / s)) < Accuracy) && (fabs((dr / r)) < Accuracy))
			{
				break;
			}
			
			r_last = r;
			s_last = s;
			
		}
		
		val0 = r * 0.5f;
		pre_cal = (float32_t)(r *r + 4 * s);
		if (pre_cal > 0)
		{
#if 01
			arm_sqrt_f32(pre_cal,&sqrt_out);
			arroot_R[(j * 2)] = val0 + (sqrt_out * 0.5f);
			arroot_R[(j * 2) + 1] = val0 - (sqrt_out * 0.5f);
#else
			arroot_R[2 * j] = val0 + ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
			arroot_R[2 * j + 1] = val0 - ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
#endif
		}
		else if (pre_cal == 0)
		{
			arroot_R[(j * 2)] = val0;
			arroot_R[(j * 2) + 1] = val0;
		}
		else if (pre_cal < 0)
		{
#if 01
			arm_sqrt_f32((-1)*pre_cal,&sqrt_out);
			arroot_I[(j * 2)][0] = val0;
			arroot_I[(j * 2)][1] = sqrt_out * (-0.5f);
			arroot_I[(j * 2) + 1][0] = val0;
			arroot_I[(j * 2) + 1][1] = sqrt_out * 0.5f;
#else
			arroot_I[2 * j][0] = val0;
			arroot_I[2 * j][1] = (-1)*((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
			arroot_I[2 * j + 1][0] = val0;
			arroot_I[2 * j + 1][1] = ((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
#endif
		}
		
		m = m - 2;
		memcpy(buffer_b, b, (m + 1) *sizeof(float32_t));
	}
	
	if (n % 2 == 1)
	{
		arroot_R[n - 1] = (-1)*(b[1]) / (b[0]);
	}
	
	return;
}
#else
{
	uint16_t iter = 1;
	uint8_t i = 0, k = 0, n = EQNSIZE;
#if 01
	float32_t u = 1;
	float32_t v = 1;
	float32_t st = 1;
	float32_t tol = Accuracy;
	float32_t a[EQNSIZE + 1] = { 0, };
	float32_t b[EQNSIZE + 1] = { 0, };
	float32_t c[EQNSIZE] = { 0, };
	float32_t c1, b1, cb, c2, bc = 0.0f;
	float32_t dn, du, dv = 0.0f;
	float32_t d, im1, r1, r2, im2 = 0.0f;
	float32_t rootr[EQNSIZE];
	float32_t rooti[EQNSIZE][2];
	float32_t ddu, ddv = 0.0f;
#else
	float64_t u = 1;
	float64_t v = 1;
	float64_t st = 1;
	float64_t tol = Accuracy;
	float64_t a[EQNSIZE + 1] = { 0, };
	float64_t b[EQNSIZE + 1] = { 0, };
	float64_t c[EQNSIZE] = { 0, };
	float64_t c1, b1, cb, c2, bc = 0.0f;
	float64_t dn, du, dv = 0.0f;
	float64_t d, im1, r1, r2, im2 = 0.0f;
	float64_t rootr[EQNSIZE];
	float64_t rooti[EQNSIZE][2];
	float64_t ddu, ddv = 0.0f;
#endif

	for(i=0;i<EQNSIZE;i++)
	{
		a[i] = equ[i+1];
	}

	iter = 1;
	while (n>2)
	{
		u = 1;
		v = 1;
		st = 1;
		while (st>tol)
		{
			b[0] = a[0] - u;
			b[1] = a[1] - b[0] * u - v;
			for (k = 2; k<n; k++)
			{
				b[k] = a[k] - b[k - 1] * u - b[k - 2] * v;
			}
			c[0] = b[0] - u;
			c[1] = b[1] - c[0] * u - v;
			for (k = 2; k<n - 1; k++)
			{
				c[k] = b[k] - c[k - 1] * u - c[k - 2] * v;
			}

			//calculate change in u and v
			c1 = c[n - 2];
			b1 = b[n - 1];
			cb = c[n - 2] * b[n - 2];
			c2 = c[n - 3] * c[n - 3];
			bc = b[n - 2] * c[n - 3];
			if (n>3)
			{
				c1 *= c[n - 4];
				b1 *= c[n - 4];
			}
			dn = c1 - c2;

			du = (b1 - bc) / dn;
			dv = (cb - c[n - 3] * b[n - 1]) / dn;
			
#if 0
			if (isnan(du) == 1 && isnan(dv) == 1)
			{
				break;
			}
			
			if(iter == 30000)
			{
				arroot_I[0][0] = 1.1f;
				arroot_I[0][1] = 1.1f;
				break;
			}

			if ((fabs((du / u)) < Accuracy) && (fabs((dv / v)) < Accuracy))
			{
				break;
			}
#endif

			u = u + du;
			v = v + dv;
			ddu=(du*du);
			ddv=(dv*dv);
			//arm_sqrt_f32(ddu + ddv,&st);
			st = (float32_t)sqrt((double)(ddu + ddv));
			iter++;
		}

		if(n == 0)
		{
			r1 = -a[0];
			im1 = 0.0;
			r2 = 0.0;
			im2 = 0.0;
		}
		else
		{
			d = u*u - 4 * v;
			if (d<0)
			{
				d = -d;
				im1 = sqrt(d) / 2;
				r1 = -u / 2;
				r2 = r1;
				im2 = -im1;
			}
			else if (d>0)
			{
				r1 = (-u + sqrt(d)) / 2;
				im1 = 0;
				r2 = (-u - sqrt(d)) / 2;
				im2 = 0;
			}
			else
			{
				r1 = -u / 2;
				im1 = 0;
				r2 = -u / 2;
				im2 = 0;
			}
		}

		rooti[i][0] = r1;
		rooti[i][1] = im1;
		rooti[i + 1][0] = r2;
		rooti[i + 1][1] = im2;
		i = i + 2;
		n = n - 2;
		for (k = 0; k<n; k++)
			a[k] = b[k];

	}
	//Solve last quadratic or linear equation
	u = a[0];
	v = a[1];

	if (n == 0)
	{
		r1 = -a[0];
		im1 = 0.0;
		r2 = 0.0;
		im2 = 0.0;
	}
	else
	{
		d = u*u - 4 * v;
		if (d<0)
		{
			d = -d;
			im1 = sqrt(d) / 2;
			r1 = -u / 2;
			r2 = r1;
			im2 = -im1;
		}
		else if (d>0)
		{
			r1 = (-u + sqrt(d)) / 2;
			im1 = 0;
			r2 = (-u - sqrt(d)) / 2;
			im2 = 0;
		}
		else
		{
			r1 = -u / 2;
			im1 = 0;
			r2 = -u / 2;
			im2 = 0;
		}
	}


	rooti[i][0] = r1;
	rooti[i][1] = im1;
	if (n == 2)
	{
		rooti[i+1][0] = r2;
		rooti[i+1][1] = im2;
	}
	
	for (i = 0; i < EQNSIZE; i++)
	{
		if (rooti[i] == 0)
		{
			arroot_R[i] = (float32_t)rooti[i][0];
			arroot_I[i][0] = 0.0f;
			arroot_I[i][1] = 0.0f;
		}
		else
		{
			arroot_R[i] = 0;
			arroot_I[i][0] = (float32_t)rooti[i][0];
			arroot_I[i][1] = (float32_t)rooti[i][1];
		}
	}

}
#endif
void Apply_Window(float32_t *buf, float32_t *w, short size)
{
	arm_mult_f32(buf, w, buf, size);
}


void SortArray(float32_t arroot_I[][2])
{
	int i, j;
	float32_t tmp;
	
	for (i = 0; i < M; i++)
	{
		for (j = (i + 1); j < M; j++)
		{
			if (arroot_I[i][0] > arroot_I[j][0])
			{
				tmp = arroot_I[i][0];
				arroot_I[i][0] = arroot_I[j][0];
				arroot_I[j][0] = tmp;
				
				tmp = arroot_I[i][1];
				arroot_I[i][1] = arroot_I[j][1];
				arroot_I[j][1] = tmp;
			}
		}
	}
}

void SortArraySingle(float32_t *Formants, float32_t *BandWidth, int Size)
{
	int i, j;
	float32_t tmp;
	
	for (i = 0; i < Size; i++)
	{
		for (j = (i + 1); j < Size; j++)
		{
			if (Formants[i] > Formants[j])
			{
				tmp = Formants[i];
				Formants[i] = Formants[j];
				Formants[j] = tmp;
				
				tmp = BandWidth[i];
				BandWidth[i] = BandWidth[j];
				BandWidth[j] = tmp;
			}
		}
	}
}
#endif