/*
 * app_status_manager.h
 *
 *  Created on: 2017. 8. 31.
 *      Author: sheum
 */

#ifndef APPS_HCI_SERIAL_GATT_SERVICE_APP_STATUS_MANAGER_H_
#define APPS_HCI_SERIAL_GATT_SERVICE_APP_STATUS_MANAGER_H_


void app_status_read( void );
void app_status_write( void );

#endif /* APPS_HCI_SERIAL_GATT_SERVICE_APP_STATUS_MANAGER_H_ */
