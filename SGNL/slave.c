
#include "sparcommon.h"
#include "wiced_bt_dev.h"
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_cfg.h"
#include "wiced_hal_gpio.h"
#include "wiced_bt_uuid.h"
#include "wiced_bt_app_common.h"
#include "wiced_bt_app_hal_common.h"
#include "wiced_bt_trace.h"
#include "wiced_hal_nvram.h"
#include "wiced_result.h"
//#include "wiced_hci.h"
#include "wiced_hal_platform.h"
#include "slave.h"
#include "SGNL_db.h"
#include "ancs.h"
#include "global_define.h"
#include "uart.h"


static void init_next_slave(void);

#pragma pack(1)
// host information for NVRAM
typedef PACKED struct
{
	// BD address of the bonded host
	BD_ADDR  bdaddr;

	// Current value of the client configuration descriptor for characteristic 'Report'
	uint16_t ancs_s_handle;
	uint16_t ancs_e_handle;
}  SLAVEINFO;

#pragma pack()

// NVRAM save area
SLAVEINFO le_slave_info;

le_slave_conn_t le_slave_conn;

void slave_init(void)
{
	memset(&le_slave_info, 0, sizeof(le_slave_info));
	memset(&le_slave_conn, 0, sizeof(le_slave_conn));

	//Creating a buffer pool for holding the peer devices's key info
	ancs_client_event_pool = (uint8_t *)wiced_bt_create_pool(sizeof(ancs_queued_event_t), ANCS_MAX_QUEUED_NOTIFICATIONS);

#if 0
	wiced_init_timer(&le_slave_conn.timer, slave_connection_timeout, 0, WICED_SECONDS_TIMER );
#endif    
}

void slave_connect_up(wiced_bt_gatt_connection_status_t *p_conn_status)
{
	le_slave_conn.conn_id = p_conn_status->conn_id;

	// save address of the connected device and print it out.
	memcpy(le_slave_conn.remote_addr, p_conn_status->bd_addr, sizeof(le_slave_conn.remote_addr));
	le_slave_conn.addr_type = p_conn_status->addr_type;
	le_slave_conn.transport = p_conn_status->transport;

	ancs_connection_up(p_conn_status);

	/* Connected as Slave. Start discovery in couple of seconds to give time to the peer device
	 * to find/configure our services */
#if 0
	wiced_start_timer(&le_slave_conn.timer, 2);
#endif    
}

// This function will be called when connection goes down
void slave_connect_down(wiced_bt_gatt_connection_status_t *p_conn_status)
{
	le_slave_conn.conn_id = 0;
	le_slave_conn.encrypted = WICED_FALSE;
	memset(&le_slave_info, 0, sizeof(le_slave_info));

#if 0
	wiced_stop_timer(&le_slave_conn.timer);
#endif    

	ancs_connection_down(p_conn_status);
}


// Process encryption status changed notification from the stack
void slave_encryption_status_changed(wiced_bt_dev_encryption_status_t *p_status)
{
	wiced_result_t result;
	uint8_t role;

	/* Ignore event if Encryption failed */
	if (p_status->result != WICED_BT_SUCCESS)
		return;

	/* Check if it's a Slave/Client device */
	if (memcmp(le_slave_conn.remote_addr, p_status->bd_addr, sizeof(le_slave_conn.remote_addr)))
	{
		/* Handle Race condition with already paired iPhone. In this case,
		 * BTM_ENCRYPTION_STATUS_EVT is received before GATT_CONNECTION_STATUS_EVT
		 */
		result = wiced_bt_dev_get_role(p_status->bd_addr, &role, BT_TRANSPORT_LE);
		if ((result != WICED_BT_SUCCESS) || (role != HCI_ROLE_SLAVE))
		{
			/* This is, definitely, not a Slave LE connection. Ignore it. */
			return;
		}
	}

	le_slave_conn.encrypted = WICED_TRUE;
	WICED_BT_TRACE("Client Link is Encrypted\n");

	/* Handle race connection again. If GATT_CONNECTION_STATUS_EVT not yet received, we don't
	 * know the Connection Id. We need to wait for the GATT_CONNECTION_STATUS_EVT event. */
	if (le_slave_conn.conn_id == 0)
	{
		WICED_BT_TRACE("ConnId not yet known. Wait.\n");
		return;
	}

	/* If at ANCS Service already found */
	if ( (le_slave_info.ancs_s_handle && le_slave_info.ancs_e_handle) )
	{
		/* Link is encrypted => Start Service configuration */
		le_slave_conn.init_state = SLAVE_INIT_STATE_NONE;
		init_next_slave();
	}
}

void ancs_initialization_callback(int result)
{
	init_next_slave();
}

void ancs_message_received_callback(ancs_event_t *p_ancs_event)
{
	uint8_t mcu_send_data[3] = {0,};
	if(!g_str_bit.b1_ota_fw_upgrade)
	{
		WICED_BT_TRACE("ANCS notification UID:%d command:%d category:%d flags:%04x\n", p_ancs_event->notification_uid, p_ancs_event->command, p_ancs_event->category, p_ancs_event->flags);

		//command 0 add, 1 modified, 2 removed
		//category : 1 incoming, 2 missed, 4 social

		strncpy((char*)g_str_ancs.ui8_identifier, (char*)p_ancs_event->identifier, sizeof(p_ancs_event->identifier));
		g_str_ancs.ui8_id_len = sizeof(p_ancs_event->identifier);

		if( strcmp((char*)g_str_ancs.ui8_identifier,"com.apple.MobileSMS") )//the others
		{
			strncpy((char*)g_str_ancs.ui8_title, (char*)p_ancs_event->title, sizeof(p_ancs_event->title));
			//variable size (X) -> received title length (O)
			g_str_ancs.ui8_title_len = strlen(p_ancs_event->title);//sizeof(p_ancs_event->title);
		}
		else//SMS
		{
			strncpy((char*)g_str_ancs.ui8_title, (char*)"Message", 7);
			g_str_ancs.ui8_title_len = 7;
		}

		if( p_ancs_event->command == BLE_ANCS_EVENT_ID_NOTIFICATION_ADDED )
		{
			WICED_BT_TRACE("T A:%s T:%s \n", p_ancs_event->identifier, p_ancs_event->title);//, p_ancs_event->sub_title);
			g_str_ancs.ui8_category_cnt++;

			switch( p_ancs_event->category )
			{
				case BLE_ANCS_CATEGORY_ID_INCOMING_CALL :
					{
#if 0
#ifdef ENABLE_FAVORITE_MANAGER
						//bt_incoming_call();
						// check caller name in flash : g_str_ancs.ui8_title, g_str_ancs.ui8_title_len
						if( favorite_name_find( (uint8_t *)g_str_ancs.ui8_title, g_str_ancs.ui8_title_len ) )
						{
							g_str_call_bit.b4_call_status |= CALL_STATUS_FAVORITE;

							WICED_BT_TRACE("call_incoming_favorite\n");                
							mcu_send_data[1] = g_str_favo_mgr.ui8_index + 1;
							g_str_bit.b2_incoming_call = 2;
						}
						else
#endif //ENABLE_FAVORITE_MANAGER
						{
							//call_incoming_call();
							WICED_BT_TRACE("call_incoming_call\n");
							mcu_send_data[1] = 0;
							g_str_bit.b2_incoming_call = 1;
						}

						g_str_call_bit.b4_call_status |= CALL_STATUS_INCOMING;
						mcu_send_data[0] = g_str_call_bit.b4_call_status;
						mcu_send_data[2] = (g_str_app_status.ui8_dnd_status == 1)? HAPTIC_OFF : HAPTIC_ON;
						send_mcu_msg(CMD_CALL_STATUS, UART_ID_CYW, 3, mcu_send_data);
#endif
					}
					break;
				case BLE_ANCS_CATEGORY_ID_MISSED_CALL :
#ifdef ENABLE_FAVORITE_MANAGER
					// check caller name in flash : g_str_ancs.ui8_title, g_str_ancs.ui8_title_len
					if( favorite_name_find( (uint8_t *)g_str_ancs.ui8_title, g_str_ancs.ui8_title_len ) )
					{
						static uint8_t last_date[32] = "";
						//if( strncmp((char *)g_str_ancs.ui8_date, (char *)last_date, g_str_ancs.ui8_date_len) )
						{
							g_str_favo_log.ui16_missed_call++;
							favorite_log_write( g_str_favo_log.ui8_index );
						}

						//strncpy((char *)last_date, (char *)g_str_ancs.ui8_date, g_str_ancs.ui8_date_len);

						g_str_call_bit.b4_call_status &= ~(CALL_STATUS_FAVORITE);
						WICED_BT_TRACE("favorite_");
					}
#endif
					g_str_bit.b2_incoming_call = 0;
					WICED_BT_TRACE("missed_call\n");
					break;
				default :
					user_str_lower((uint8_t *)g_str_ancs.ui8_identifier, g_str_ancs.ui8_id_len);
					package_parser((uint8_t *)g_str_ancs.ui8_identifier, g_str_ancs.ui8_id_len);
					break;
			}


		}
		else if( p_ancs_event->command == BLE_ANCS_EVENT_ID_NOTIFICATION_REMOVED )
		{
			if( p_ancs_event->category == BLE_ANCS_CATEGORY_ID_INCOMING_CALL)
			{
				if(g_str_call_bit.b4_call_status & CALL_STATUS_CALLING);
				else
				{
#if 0
					g_str_bit.b2_incoming_call = 0;
					g_str_call_bit.b1_favorite_enable = 0;
					g_str_call_bit.b4_call_status |= CALL_STATUS_CALLING;
					mcu_send_data[0] = g_str_call_bit.b4_call_status;
					mcu_send_data[1] = 0;
					send_mcu_msg(CMD_CALL_STATUS, UART_ID_CYW, 2, mcu_send_data);
					WICED_BT_TRACE("Receiving call using ancs");
#endif
				}
				//g_str_call_bit.b4_call_status &= ~(CALL_STATUS_INCOMING);
				//WICED_BT_TRACE("end_call\n");
			}

			if( g_str_ancs.ui8_category_cnt )
			{
				g_str_ancs.ui8_category_cnt--;
			}
		}

		wiced_bt_free_buffer(p_ancs_event);

	}
}

void slave_connection_timeout(uint32_t arg)
{
#if 0
	/* If Pairing is not allowed AND peer device not yet Paired */
	if ( find_nvram_id(le_slave_conn.remote_addr, BD_ADDR_LEN) == NVRAM_INVALID_NVRAM_ID )
	{
		WICED_BT_TRACE("Slave timeout. Pairing not allowed and Device not Paired. Do nothing\n");
		return;
	}
#endif
	WICED_BT_TRACE("Slave timeout. Starting Service Search\n");


	// perform primary service search
	le_slave_conn.init_state = SLAVE_INIT_STATE_NONE;

	le_slave_info.ancs_s_handle = 0;
	le_slave_info.ancs_e_handle = 0;

	send_discover(le_slave_conn.conn_id, GATT_DISCOVER_SERVICES_ALL, UUID_ATTRIBUTE_PRIMARY_SERVICE, 1, 0xffff);
}

void init_next_slave(void)
{
	uint8_t  writtenbyte;

	WICED_BT_TRACE("%s state:%d", __FUNCTION__, le_slave_conn.init_state);

	switch (le_slave_conn.init_state)
	{
		case SLAVE_INIT_STATE_NONE:
			le_slave_conn.init_state = SLAVE_INIT_STATE_ANCS;
			if (ancs_initialize(le_slave_conn.conn_id, le_slave_info.ancs_s_handle, le_slave_info.ancs_e_handle,
						&ancs_initialization_callback, &ancs_message_received_callback))
				break;
			/* No break on purpose (if not ANCS Service found) */

		case SLAVE_INIT_STATE_ANCS:
			le_slave_conn.init_state = SLAVE_INIT_STATE_NONE;
		default:
			break;
	}
}

wiced_bt_gatt_status_t set_config_descriptor(uint16_t conn_id, uint16_t handle, uint16_t value)
{
	wiced_bt_gatt_status_t status = WICED_BT_GATT_SUCCESS;
	uint8_t                buf[sizeof(wiced_bt_gatt_value_t) + 1];
	wiced_bt_gatt_value_t *p_write = ( wiced_bt_gatt_value_t* )buf;
	uint16_t               u16 = value;

	// Allocating a buffer to send the write request
	memset(buf, 0, sizeof(buf));

	p_write->handle   = handle;
	p_write->offset   = 0;
	p_write->len      = 2;
	p_write->auth_req = GATT_AUTH_REQ_NONE;
	p_write->value[0] = u16 & 0xff;
	p_write->value[1] = (u16 >> 8) & 0xff;

	// Register with the server to receive notification
	status = wiced_bt_gatt_send_write ( le_slave_conn.conn_id, GATT_WRITE, p_write );

	WICED_BT_TRACE("wiced_bt_gatt_send_write %d\n", status);
	return status;
}

void send_discover(uint16_t conn_id, wiced_bt_gatt_discovery_type_t type, uint16_t uuid, uint16_t s_handle, uint16_t e_handle)
{
	wiced_bt_gatt_discovery_param_t param;
	wiced_bt_gatt_status_t          status;

	memset(&param, 0, sizeof(param));
	if (uuid != 0)
	{
		param.uuid.len = LEN_UUID_16;
		param.uuid.uu.uuid16 = uuid;
	}
	param.s_handle = s_handle;
	param.e_handle = e_handle;

	status = wiced_bt_gatt_send_discover(conn_id, type, &param);

	WICED_BT_TRACE("wiced_bt_gatt_send_discover %d\n", status);
}

wiced_bt_gatt_status_t slave_gatt_operation_complete_callback(wiced_bt_gatt_operation_complete_t *p_complete)
{
	WICED_BT_TRACE("[%s] \n", __FUNCTION__);
	uint16_t conn_id = p_complete->conn_id;

	switch (p_complete->op)
	{
		case GATTC_OPTYPE_DISCOVERY:
			WICED_BT_TRACE( "!!! Disc compl conn_id:%d \n", conn_id  );
			break;
		case GATTC_OPTYPE_READ:
			WICED_BT_TRACE("read response handle:%04x\n", p_complete->response_data.att_value.handle);
			if ((p_complete->response_data.att_value.handle >= le_slave_info.ancs_s_handle) &&
					(p_complete->response_data.att_value.handle <= le_slave_info.ancs_e_handle))
			{
				ancs_operation_read_response_handler(p_complete);
			}
			break;

		case GATTC_OPTYPE_WRITE:
		case GATTC_OPTYPE_EXE_WRITE:
			WICED_BT_TRACE("write response handle:%04x\n", p_complete->response_data.handle);

			// Check the handle to figure out which client this answer belongs to
			if ((p_complete->response_data.handle >= le_slave_info.ancs_s_handle) &&
					(p_complete->response_data.handle <= le_slave_info.ancs_e_handle))
			{
				ancs_operation_write_response_handler(p_complete);
			}
			break;

		case GATTC_OPTYPE_CONFIG:
			WICED_BT_TRACE("peer mtu:%d\n", p_complete->response_data.mtu);
			break;

		case GATTC_OPTYPE_NOTIFICATION:
			WICED_BT_TRACE("notification handle:%04x\n", p_complete->response_data.att_value.handle);

			// Check the handle to figure out which client this answer belongs to
			if ((p_complete->response_data.att_value.handle >= le_slave_info.ancs_s_handle) &&
					(p_complete->response_data.att_value.handle <=le_slave_info.ancs_e_handle))
			{
				ancs_operation_notification_handler(p_complete);
			}
			break;

		case GATTC_OPTYPE_INDICATION:
			// remember GATT service start and end handles
			if ((p_complete->response_data.att_value.handle >= le_slave_info.ancs_s_handle) &&
					(p_complete->response_data.att_value.handle <=le_slave_info.ancs_e_handle))
			{
				ancs_operation_indication_handler(p_complete);
			}
			break;
	}
	return WICED_BT_GATT_SUCCESS;

}

wiced_bt_gatt_status_t slave_gatt_discovery_result_callback(wiced_bt_gatt_discovery_result_t *p_result)
{
	WICED_BT_TRACE("[%s] conn %d type %d state 0x%02x\n", __FUNCTION__, p_result->conn_id, p_result->discovery_type, le_slave_conn.init_state);

	switch (le_slave_conn.init_state)
	{
		case SLAVE_INIT_STATE_ANCS:
			ancs_discovery_result_handler(p_result);
			break;
		default:
			if (p_result->discovery_type  == GATT_DISCOVER_SERVICES_ALL)
			{
				if (p_result->discovery_data.group_value.service_type.len == 16)
				{
					WICED_BT_TRACE("%04x e:%04x uuid\n", p_result->discovery_data.group_value.s_handle, p_result->discovery_data.group_value.e_handle);
					if (memcmp(p_result->discovery_data.group_value.service_type.uu.uuid128, ANCS_SERVICE, 16) == 0)
					{
						WICED_BT_TRACE("ANCS Service found s:%04x e:%04x\n", p_result->discovery_data.group_value.s_handle, p_result->discovery_data.group_value.e_handle);
						le_slave_info.ancs_s_handle = p_result->discovery_data.group_value.s_handle;
						le_slave_info.ancs_e_handle = p_result->discovery_data.group_value.e_handle;
					}
				}
			}
			else
			{
				WICED_BT_TRACE("!!!! invalid op:%d\n", p_result->discovery_type);
			}
			break;
	}
	return WICED_BT_GATT_SUCCESS;

}

wiced_bt_gatt_status_t slave_gatt_discovery_complete_callback(wiced_bt_gatt_discovery_complete_t *p_complete)
{
	wiced_result_t result;

	WICED_BT_TRACE("[%s] conn %d type %d state %d\n", __FUNCTION__, p_complete->conn_id, p_complete->disc_type, le_slave_conn.init_state);

	switch (le_slave_conn.init_state)
	{
		case SLAVE_INIT_STATE_ANCS:
			ancs_discovery_complete_handler(p_complete);
			break;
		default:
			if (p_complete->disc_type == GATT_DISCOVER_SERVICES_ALL)
			{
				WICED_BT_TRACE("ANCS:%04x-%04x \n", le_slave_info.ancs_s_handle, le_slave_info.ancs_e_handle);

				/* If at ANCS or AMS Service found */
				if ( le_slave_info.ancs_s_handle && le_slave_info.ancs_e_handle )
				{
					/* These Services require Authentication/Encryption */
					if (le_slave_conn.encrypted == WICED_FALSE)
					{
						WICED_BT_TRACE( "Start Authentication\n");
						/* Link is Not encrypted => Initiate Authorization */
						result = wiced_bt_dev_sec_bond(le_slave_conn.remote_addr,
								le_slave_conn.addr_type, le_slave_conn.transport, 0, NULL);
						WICED_BT_TRACE( "wiced_bt_dev_sec_bond returns:%d\n", result);
						// If call to the Bond returns success, device is bonded, and we just need
						// to setup encryption
						if( result == WICED_BT_SUCCESS )
						{
							WICED_BT_TRACE( "starting encryption\n" );
							wiced_bt_dev_set_encryption(le_slave_conn.remote_addr, BT_TRANSPORT_LE, NULL );
						}
					}
					else
					{
						WICED_BT_TRACE( "LE Slave Link encrypted. Let's start LE Services config\n");
						/* Link is encrypted => Start Service configuration */
						le_slave_conn.init_state = SLAVE_INIT_STATE_NONE;
						init_next_slave();
					}
				}
			}
			else
			{
				WICED_BT_TRACE("!!!! invalid op:%d\n", p_complete->disc_type);
			}
	}
	return WICED_BT_GATT_SUCCESS;
}

