#include "global_define.h"
#include "wiced_bt_trace.h"

#ifdef ENABLE_FUEL_GAUGE

#define EMPTY_SOC   3
#define FULL_SOC    97

void fuel_gauge_write(uint16_t reg, uint32_t length, uint8_t *data)
{
	i2c_write_multi_byte(FUEL_GAUGE_ADDRESS, (uint8_t)reg, (uint8_t *)data, (uint8_t)length);
}

void fuel_gauge_read(uint16_t reg, uint32_t length, uint8_t *data)
{
    	i2c_read_multi_byte(FUEL_GAUGE_ADDRESS, (uint8_t)reg, (uint8_t *)data, (uint8_t)length);
}

void fuel_gauge_condition(fuel_gauge_condition_e condition)
{
	uint8_t write_data[2] = {0,};
	
	write_data[0] = condition;
	
	fuel_gauge_write(FUEL_GAUGE_CONDITION, 2, write_data);
	Delay_us(1000);
}

void fuel_gauge_init(void)
{
	fuel_gauge_condition(ACTIVE_MODE);
	
	fuel_gauge_communication(VOLTAGE);
	if( g_str_batt.ui16_batt_level > BATT_LEVEL_3_0 )
	{
		fuel_gauge_communication(BATTERY_STATUS);
		//fuel_gauge_communication(ABSOLUTE_STATE_CHARGE);
		fuel_gauge_communication(RELATIVE_STATE_CHARGE);
	}
	
	fuel_gauge_condition(STANDBY_MODE);
}

void fuel_gauge_communication(fuel_gauge_command_e cmd)
{
	uint8_t read_data[8] = {0,};
	uint32_t data = 0;
	
	fuel_gauge_read(cmd, 2, read_data);
	data = ((read_data[1] << 8) | read_data[0]);
	
	switch(cmd)
	{
	case TEMPERATURE:
		{
			uint16_t remainder = 0;
			
			if( (read_data[0] == 0xFF) && (read_data[1] == 0xFF) )
			{
				fuel_gauge_read(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			
			remainder = data % 10;//0.x℃ 값 받기
			if(remainder >= 5)//반올림
			{
				data += 10;
			}
			g_str_batt.i8_batt_temperature = (data - remainder)/10;
			break;
		}
		
	case VOLTAGE:
		{
			if(data > 4350)
			{
				fuel_gauge_read(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}

			if(data >= 4400)    	g_str_batt.ui16_batt_level = BATT_LEVEL_ERR;
            else if(data >= 4200)	g_str_batt.ui16_batt_level = BATT_LEVEL_4_2;
			else if(data >= 4100)	g_str_batt.ui16_batt_level = BATT_LEVEL_4_1;
			else if(data >= 4000)	g_str_batt.ui16_batt_level = BATT_LEVEL_4_0;
			else if(data >= 3900)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_9;
			else if(data >= 3800)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_8;
			else if(data >= 3700)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_7;
			else if(data >= 3600)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_6;
			else if(data >= 3500)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_5;
			else if(data >= 3400)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_4;
			else if(data >= 3300)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_3;
			else if(data >= 3200)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_2;
			else if(data >= 3100)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_1;
			else if(data >= 3000)	g_str_batt.ui16_batt_level = BATT_LEVEL_3_0;
            else                    g_str_batt.ui16_batt_level = BATT_LEVEL_ERR;

            if( g_str_batt.ui16_batt_level != BATT_LEVEL_ERR )
            {
                WICED_BT_TRACE("voltage : %d", data);
            }
            else
            {
                WICED_BT_TRACE("voltage err : %d", data);
            }
                
			break;
		}
		
	case USING_CURRENT:
		{
			if(data == 0xFFFF)
			{
				fuel_gauge_read(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.i16_batt_using_current = data;
			break;
		}
		
	case AVERAGE_CURRENT:
		{
			if(data == 0xFFFF)
			{
				fuel_gauge_read(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.i16_batt_avg_current = data;
			break;
		}
		
	case RELATIVE_STATE_CHARGE:
		{
            if( g_str_batt.ui16_batt_level != BATT_LEVEL_ERR )
            {
          
    			if(data == 0xFFFF)
    			{
    				fuel_gauge_read(cmd, 2, read_data);
    				data = ((read_data[1] << 8) | read_data[0]);
    			}
    			
    			if( !data )
    			{
    				if( g_str_batt.ui16_batt_level == BATT_LEVEL_4_2 )
    				{
    					data = 25600;
    				}
    			}

    			//under 5% (5%->1280, 2%->512)
    			if( data <= 1280 )
    			{
    				WICED_BT_TRACE("before data:%d %d%%",data,(data>>8));
    				if(data >= 512)
    				{
    					data = ((data - 512) * 5) / 3;
    					WICED_BT_TRACE("after data:%d %d%%",data,(data>>8));
    				}
    				else
    					data = 0;
    			}

#if 1 //BATT_ADJUST
                uint32_t tmp_soc = (data>>8);
                uint32_t tmp_percent = 0;

                if( tmp_soc > EMPTY_SOC )
                {
                    if( tmp_soc < 0x80 )
                    {
                        tmp_percent = (uint32_t)((tmp_soc - EMPTY_SOC ) * 100 / ( FULL_SOC - EMPTY_SOC ) );
                    }
                    else
                    {
                        tmp_percent= 0;
                    }
                }
                else
                {
                    tmp_percent = 0;
                }

                if( tmp_percent <= 100 )
                {
                    g_str_batt.ui32_relative_state_charge = tmp_percent;
                }
                else
                {
                    g_str_batt.ui32_relative_state_charge = 100;
                }
#else
    			g_str_batt.ui32_relative_state_charge = (data>>8);
#endif


    			if( g_str_batt.ui32_relative_state_charge <= 100 )
    			{
    				g_str_batt.ui8_previous_percent = g_str_batt.ui8_percent;
    				if( g_str_batt.ui8_buf_per[7] == 0 )
    				{
    					int i = 0;
    					for( i = 0; i < 8; i++ )
    					{
                            if( g_str_batt.ui8_percent )
                            {
                                g_str_batt.ui8_buf_per[i] = (uint8_t)(g_str_batt.ui8_percent);
                            }
                            else
                            {
        						g_str_batt.ui8_buf_per[i] = (uint8_t)(g_str_batt.ui32_relative_state_charge);
                            }
    					}
    				}
    				else
    				{
    					g_str_batt.ui8_buf_per[7] = g_str_batt.ui8_buf_per[6];
    					g_str_batt.ui8_buf_per[6] = g_str_batt.ui8_buf_per[5];
    					g_str_batt.ui8_buf_per[5] = g_str_batt.ui8_buf_per[4];
    					g_str_batt.ui8_buf_per[4] = g_str_batt.ui8_buf_per[3];
    					g_str_batt.ui8_buf_per[3] = g_str_batt.ui8_buf_per[2];
    					g_str_batt.ui8_buf_per[2] = g_str_batt.ui8_buf_per[1];
    					g_str_batt.ui8_buf_per[1] = g_str_batt.ui8_buf_per[0];
    					g_str_batt.ui8_buf_per[0] = (uint8_t)(g_str_batt.ui32_relative_state_charge);
    				}

    				uint16_t batt_sum = g_str_batt.ui8_buf_per[0] + g_str_batt.ui8_buf_per[1]
    				+ g_str_batt.ui8_buf_per[2] + g_str_batt.ui8_buf_per[3]
    				+ g_str_batt.ui8_buf_per[4] + g_str_batt.ui8_buf_per[5]
    				+ g_str_batt.ui8_buf_per[6] + g_str_batt.ui8_buf_per[7];

    				if(batt_sum >= 792)//99*8 = 792
    				{
    					WICED_BT_TRACE("100%% compensation!!\n");
    					g_str_batt.ui8_percent = 100;
    				}
    				else
    				{
    					WICED_BT_TRACE("normal battery check!!\n");
#if 1
    					g_str_batt.ui8_percent = (batt_sum >> 3);
#else
    					if( (batt_sum >> 3) > g_str_batt.ui32_absolute_state_charge )//relative > absolute
    					{
    						WICED_BT_TRACE("relative > absolute\n");
    						g_str_batt.ui8_percent = (batt_sum >> 3);
    					}
    					else //relative <= absolute
    					{
    						WICED_BT_TRACE("relative <= absolute\n");
    						g_str_batt.ui8_percent = g_str_batt.ui32_absolute_state_charge;
    					}
#endif
    				}

    				g_str_batt.ui16_level = (g_str_batt.ui8_percent / 20);

    			}
				WICED_BT_TRACE("check percent:%d%%",g_str_batt.ui8_percent);

    			batt_charger_status_send();
            }

			break;
		}
		
	case ABSOLUTE_STATE_CHARGE:
		{
			if(data == 0xFFFF)
			{
				fuel_gauge_read(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}

			WICED_BT_TRACE("data:%d\n",data);
			g_str_batt.ui32_absolute_state_charge = (data>>8);

			if( g_str_batt.ui32_absolute_state_charge <= 100 )
			{
				if( g_str_batt.ui8_abs_buf_per[7] == 0 )
				{
					int i = 0;
					for( i = 0; i < 8; i++ )
					{
						g_str_batt.ui8_abs_buf_per[i] = (uint8_t)(g_str_batt.ui32_absolute_state_charge);
					}
				}
				else
				{
					g_str_batt.ui8_abs_buf_per[7] = g_str_batt.ui8_abs_buf_per[6];
					g_str_batt.ui8_abs_buf_per[6] = g_str_batt.ui8_abs_buf_per[5];
					g_str_batt.ui8_abs_buf_per[5] = g_str_batt.ui8_abs_buf_per[4];
					g_str_batt.ui8_abs_buf_per[4] = g_str_batt.ui8_abs_buf_per[3];
					g_str_batt.ui8_abs_buf_per[3] = g_str_batt.ui8_abs_buf_per[2];
					g_str_batt.ui8_abs_buf_per[2] = g_str_batt.ui8_abs_buf_per[1];
					g_str_batt.ui8_abs_buf_per[1] = g_str_batt.ui8_abs_buf_per[0];
					g_str_batt.ui8_abs_buf_per[0] = (uint8_t)(g_str_batt.ui32_absolute_state_charge);
				}
			}

			uint16_t batt_sum = g_str_batt.ui8_abs_buf_per[0] + g_str_batt.ui8_abs_buf_per[1]
			+ g_str_batt.ui8_abs_buf_per[2] + g_str_batt.ui8_abs_buf_per[3]
			+ g_str_batt.ui8_abs_buf_per[4] + g_str_batt.ui8_abs_buf_per[5]
			+ g_str_batt.ui8_abs_buf_per[6] + g_str_batt.ui8_abs_buf_per[7];

			g_str_batt.ui32_absolute_state_charge = batt_sum>>3;
			//g_str_batt.ui8_percent = (uint8_t)(g_str_batt.f32_absolute_state_charge);
			WICED_BT_TRACE("abs per: %d%", g_str_batt.ui32_absolute_state_charge );
			break;
		}
		
	case USABLE_CAPACITY:
		{
			if(data >= BATT_FULL_CAPACITY)
			{
				fuel_gauge_read(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.ui16_batt_usable_capacity = data;
			break;
		}
		
	case REMAINING_CAPACITY:
		{
			if(data >= BATT_FULL_CAPACITY)
			{
				fuel_gauge_read(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.ui16_batt_remain_capacity = data;
			break;
		}
		
	case FULL_CHARGE_CAPACITY:
		{
			if(data > BATT_FULL_CAPACITY)
			{
				fuel_gauge_read(cmd, 2, read_data);
				data = ((read_data[1] << 8) | read_data[0]);
			}
			g_str_batt.ui16_batt_full_charge_capacity = data;
			break;
		}
	case BATTERY_STATUS:
		{
			if(data == 0xFFFF)
			{
				memset(read_data, 0x00, sizeof(read_data));
				fuel_gauge_read(cmd, 2, read_data);
			}
			g_str_fuel_gauge_batt.b1_battery_degradation_alert = (read_data[0] & 0x80) >> 7;
			g_str_fuel_gauge_batt.b1_usable_capacity_low_alarm = (read_data[0] & 0x40) >> 6;
			g_str_fuel_gauge_batt.b1_remaining_run_time_alarm = (read_data[0] & 0x20) >> 5;
			g_str_fuel_gauge_batt.b1_discharge = (read_data[0] & 0x10) >> 4;
			
			if(g_str_fuel_gauge.ui16_firmware_version >= 0x0703)
			{
				g_str_fuel_gauge_batt.b1_full_charge = (read_data[0] & 0x08) >> 3;
				g_str_fuel_gauge_batt.b1_full_discharge = (read_data[0] & 0x04) >> 2;
			}
			
			g_str_fuel_gauge_batt.b1_soc_high_detection = (read_data[0] & 0x02) >> 1;
			g_str_fuel_gauge_batt.b1_soc_low_detection = read_data[0] & 0x01;
			
			g_str_fuel_gauge_batt.b1_over_charge = (read_data[1] & 0x80) >> 7;
			g_str_fuel_gauge_batt.b1_over_discharge = (read_data[1] & 0x40) >> 6;
			g_str_fuel_gauge_batt.b1_charge_over_current = (read_data[1] & 0x20) >> 5;
			g_str_fuel_gauge_batt.b1_discharge_over_current = (read_data[1] & 0x10) >> 4;
			g_str_fuel_gauge_batt.b1_over_temperature = (read_data[1] & 0x08) >> 3;
			g_str_fuel_gauge_batt.b1_under_temperature = (read_data[1] & 0x04) >> 2;
			break;
		}
		
	case FUEL_GAUGE_STATUS:
		{
			g_str_fuel_gauge.b1_battery_alert = (read_data[0] & 0x80) >> 7;
			if(g_str_fuel_gauge.b1_battery_alert)
			{
				;//When abnormal condition or alert of Battery Status
				// command (0x16) (any bits of Byte0 or bit7 of Byte1 are set)
				// was detected, Battery Alert flag is set.
			}
			
			g_str_fuel_gauge.b2_system_failure_alarm = (read_data[0] & 0x60) >> 5;
			switch(g_str_fuel_gauge.b2_system_failure_alarm)
			{
			case 0://fuel gauge is normal operation
				break;
			case 1://fuel gauge is simple operation
				break;
			case 2://fuel gauge isn't operational
				break;
			}
			
			g_str_fuel_gauge.b1_alert_status = (read_data[0] & 0x10) >> 4;
			if(g_str_fuel_gauge.b1_alert_status)
			{
				;//This bit hold the state of the alert interrupt signal.
			}
			
			g_str_fuel_gauge.b4_command_response_status = (read_data[0] & 0x0f);
			
			g_str_fuel_gauge.b1_data_not_ready = (read_data[1] & 0x80) >> 7;
			g_str_fuel_gauge.b1_detect_stable_current = (read_data[1] & 0x10) >> 4;
			g_str_fuel_gauge.b1_update_battery_capacity = (read_data[1] & 0x04) >> 2;
			g_str_fuel_gauge.b1_correct_remaining_capacity = (read_data[1] & 0x02) >> 1;
			if(g_str_fuel_gauge.b1_data_not_ready)
			{
				;//FG data of SOC etc. cannot yet be obtained
			}
			break;
		}
		
	case IDENTIFY:
		{
			g_str_fuel_gauge.ui16_firmware_version = ((read_data[1] << 8) | read_data[0]);
			break;
		}
		
	case AVERAGE_TIME_EMPTY:
		break;
	case AVERAGE_TIME_FULL:
		break;
	case CYCLE_COUNT:
		break;
	case STATE_BATTERY_CONDITION:
		break;
	case INTERNAL_TEMPERATURE:
		break;
	case AT_RATE_TIME_EMPTY:
		break;
	case AT_RATE_TIME_FULL:
		break;
	case FUEL_GAUGE_PARAMETER_OFFSET:
		break;
	case FUEL_GAUGE_PARAMETER_DATA:
		break;
	case PRODUCT_INFORMATION:
		break;
	case ID_INFORMATION:
		break;
	case BATTERY_PACK_INFORMATION:
		break;
	case DESIGN_CAPACITY:
		break;
	case DESIGN_VOLTAGE:
		break;
	case NVM_PARAMTER_OFFSET:
		break;
	case NVM_PARAMTER_DATA:
		break;
	case MON_STATUS:
		break;
	case FUEL_GAUGE_RESET:
		break;
	case READ_NVM:
		break;
	case ERASE_NVM:
		break;
	case WRITE_NVM:
		break;
	default : break;
	}
	
	Delay_us(1000);
}

#endif
