#ifndef _FAVORITE_CALL_LOG_H_
#define _FAVORITE_CALL_LOG_H_

extern void favorite_log_read( uint8_t u8_index );
extern void favorite_log_write( uint8_t u8_index );
extern void favorite_data_sync( uint8_t u8_index );
extern void favorite_log_process( void );

extern void favorite_log_delete( uint8_t u8_index );
extern void favorite_log_init( void );

#endif //_FAVORITE_CALL_LOG_H_

