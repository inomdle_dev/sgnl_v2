#ifndef _IO_EXPANDER_H_
#define _IO_EXPANDER_H_

#define PCA9537_I2C_ADDR        (0x49U<<1)

#define PCA9537_IN_PORT_REG     0
#define PCA9537_OUT_PORT_REG    1
#define PCA9537_POL_INV_REG     2
#define PCA9537_CONFIG_REG      3

void io_expander_init(void);
void io_expander_reset_int(void);

#endif //_IO_EXPANDER_H_

