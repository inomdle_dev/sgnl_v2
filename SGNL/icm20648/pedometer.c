//_PEDOMETER_C_
#include "inv_defines.h"
#include "wiced_bt_trace.h"
#include "uart.h"

#ifdef ENABLE_PEDOMETER

#define PEDOMETER_BACKUP_PERIOD			360	//15*24 15 days
#define PEDOMETER_BACKUP_SPARE_PERIOD	120	// 5*24  buffer 5 days

typedef enum BAC_Packet_e_
{
	BAC_DRIVE = (1 << 0),
	BAC_WALK = (1 << 1),
	BAC_RUN = (1 << 2),
	BAC_BIKE = (1 << 3),
	BAC_TILT = (1 << 4),
	BAC_STILL = (1 << 5),
}BAC_Packet_e;

pedometer_struct_t g_str_pedo;
pedo_back_struct_t g_str_pedo_back;

uint8_t now_state = 0;

uint32_t g_ui32_last_pedo_steps = 0;
uint32_t g_ui32_cur_pedo_steps = 0;
uint32_t g_ui32_pedometer_steps = 0;
uint32_t g_ui32_pedometer_times = 0;

void pedometer_data_update( void )
{
	uint32_t bac_ts = 0;
	uint32_t timestamp = 0;

	dmp_get_bac_ts((unsigned long *)&bac_ts); //10ms check

	timestamp = (bac_ts / 100);// unit : 100ms

#ifdef ENABLE_PEDOMETER_DEBUG

	static uint32_t prev_status = 0;

	if( prev_status != now_state )
	{
		WICED_BT_TRACE("timestamp: %d, back_walk : %d, back_run : %d, status : %04x \n", timestamp, g_str_pedo.ui16_walk_time, g_str_pedo.ui16_run_time, now_state );
	}

	prev_status = now_state;
#endif
	switch(now_state)
	{
		case BAC_STILL:
			g_str_pedo.ui16_total_still_time += (timestamp - g_str_pedo_back.ui16_still_time);
			g_str_pedo_back.ui16_still_time = timestamp;
			break;

		case BAC_DRIVE:
			if( g_ui32_pedometer_steps > g_str_pedo_back.ui32_drive_step )
			{
				g_str_pedo.ui32_total_drive_step += (g_ui32_pedometer_steps - g_str_pedo_back.ui32_drive_step );
			}

			if( timestamp > g_str_pedo_back.ui16_drive_time )
			{
				g_str_pedo.ui16_total_drive_time += (timestamp - g_str_pedo_back.ui16_drive_time);
			}
			g_str_pedo_back.ui32_drive_step = g_ui32_pedometer_steps;
			g_str_pedo_back.ui16_drive_time = timestamp;
			break;
		case BAC_BIKE:
			if( g_ui32_pedometer_steps > g_str_pedo_back.ui32_bike_step )
			{
				g_str_pedo.ui32_total_bike_step += (g_ui32_pedometer_steps - g_str_pedo_back.ui32_bike_step );
			}

			if( timestamp > g_str_pedo_back.ui16_bike_time )
			{
				g_str_pedo.ui16_total_bike_time += (timestamp - g_str_pedo_back.ui16_bike_time);
			}
			g_str_pedo_back.ui32_bike_step = g_ui32_pedometer_steps;
			g_str_pedo_back.ui16_bike_time = timestamp;
			break;
		case BAC_WALK:
			if( g_ui32_pedometer_steps > g_str_pedo_back.ui32_walk_step )
			{
				g_str_pedo.ui32_total_walk_step += (g_ui32_pedometer_steps - g_str_pedo_back.ui32_walk_step);
			}

			if( timestamp > g_str_pedo_back.ui16_walk_time )
			{
				g_str_pedo.ui16_total_walk_time += (timestamp - g_str_pedo_back.ui16_walk_time);
			}
			g_str_pedo_back.ui32_walk_step = g_ui32_pedometer_steps;
			g_str_pedo_back.ui16_walk_time = timestamp;
			break;

		case BAC_RUN:
			if( g_ui32_pedometer_steps > g_str_pedo_back.ui32_run_step )
			{
				g_str_pedo.ui32_total_run_step += (g_ui32_pedometer_steps - g_str_pedo_back.ui32_run_step);
			}

			if( timestamp > g_str_pedo_back.ui16_run_time )
			{
				g_str_pedo.ui16_total_run_time += (timestamp - g_str_pedo_back.ui16_run_time);
			}
			g_str_pedo_back.ui32_run_step = g_ui32_pedometer_steps;
			g_str_pedo_back.ui16_run_time = timestamp;
			break;
	}
}

void pedometer_data_transfer(uint16_t conn_id)
{

	pedometer_data_update();

	uint8_t byte[20] = "";
	uint8_t nus_index = 0;

	byte[++nus_index] = REPORT_PEDOMETER;
	byte[++nus_index] = REP_PEDO_LIVE_DATA;
	byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_app_date & 0xff0000 ) >> 16 );
	byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_app_date & 0xff00 ) >> 8 );
	byte[++nus_index] = ( uint8_t )( g_str_time.ui32_app_date & 0xff );
	byte[++nus_index] = ( uint8_t )( g_str_time.ui32_app_hour );
	byte[++nus_index] = ( uint8_t )((g_ui32_pedometer_steps&0xff000000)>>24);
	byte[++nus_index] = ( uint8_t )((g_ui32_pedometer_steps&0x00ff0000)>>16);
	byte[++nus_index] = ( uint8_t )((g_ui32_pedometer_steps&0x0000ff00)>>8);
	byte[++nus_index] = ( uint8_t )(g_ui32_pedometer_steps&0x000000ff);
	byte[++nus_index] = ( uint8_t )((g_str_pedo.ui16_total_walk_time&0xff00)>>8);
	byte[++nus_index] = ( uint8_t )(g_str_pedo.ui16_total_walk_time&0x00ff);
	byte[++nus_index] = ( uint8_t )(0);
	byte[++nus_index] = ( uint8_t )(0);
	byte[++nus_index] = ( uint8_t )(0);
	byte[++nus_index] = ( uint8_t )(0);
	byte[++nus_index] = ( uint8_t )((g_str_pedo.ui16_total_run_time&0xff00)>>8);
	byte[++nus_index] = ( uint8_t )(g_str_pedo.ui16_total_run_time&0x00ff);

	byte[0] = ++nus_index;

	send_byte_gatt_connid(conn_id, byte, byte[0]);
}

static uint8_t state_high = 0;
static uint8_t state_low = 0;

static uint32_t data_left_in_fifo = 0;
static uint16_t bac_state = 0;
static uint32_t bac_ts = 0;
static uint32_t timestamp = 0;

static uint16_t header = 0;
static uint16_t header2 = 0;

/**
 * @brief processing pedometer data of mpu.
 * @par Parameters None
 * @retval void None
 * @par Required preconditions: None
 */
void pedometer_check(void)
{
	static uint16_t intr_status = 0;
	inv_identify_interrupt(&intr_status);

	if(intr_status & BIT_MSG_DMP_INT)//dmp int
	{
		//WICED_BT_TRACE("dmp interrupt\n");
		// data main loop for header packet from fifo
		do
		{
			// Read FIFO contents and parse it.
			dmp_process_fifo((unsigned int *)&data_left_in_fifo, (unsigned short *)&header, (unsigned short *)&header2);

			// Activity recognition sample available from DMP FIFO
			if (header2 & ACT_RECOG_SET)
			{
				// Read activity type and associated timestamp out of DMP FIFO
				// activity type is a set of 2 bytes :
				// - high byte indicates activity start
				// - low byte indicates activity end 
				dmp_get_bac_state(&bac_state);

				dmp_get_bac_ts((unsigned long *)&bac_ts); //10ms check
				timestamp = (bac_ts / 100);//unit 10:100ms // unit 100 : 1s
				// Tilt information is inside BAC, so extract it out 
				state_high = (bac_state >> 8);
				state_low = bac_state;

				if( (state_low & BAC_WALK) ) // walk end
				{
					if ((g_str_pedo_back.ui32_walk_step < g_ui32_pedometer_steps))
					{
						g_str_pedo.ui32_walk_step = g_ui32_pedometer_steps - g_str_pedo_back.ui32_walk_step;
						g_str_pedo.ui32_total_walk_step += g_str_pedo.ui32_walk_step;
					}
					if( g_str_pedo_back.ui16_walk_time < timestamp )
					{

						g_str_pedo.ui16_walk_time = timestamp - g_str_pedo_back.ui16_walk_time;
						g_str_pedo.ui16_total_walk_time += g_str_pedo.ui16_walk_time;
					}

					g_str_pedo_back.ui32_walk_step = g_ui32_pedometer_steps;
					g_str_pedo_back.ui16_walk_time = timestamp;

					now_state &= ~(BAC_WALK);
				}

				if(state_low & BAC_RUN) // run end
				{
					if ((g_str_pedo_back.ui32_run_step < g_ui32_pedometer_steps))
					{
						g_str_pedo.ui32_run_step = g_ui32_pedometer_steps - g_str_pedo_back.ui32_run_step;
						g_str_pedo.ui32_total_run_step += g_str_pedo.ui32_run_step;
					}
					if( g_str_pedo_back.ui16_run_time < timestamp )
					{

						g_str_pedo.ui16_run_time = timestamp - g_str_pedo_back.ui16_run_time;
						g_str_pedo.ui16_total_run_time += g_str_pedo.ui16_run_time;
					}

					g_str_pedo_back.ui32_run_step = g_ui32_pedometer_steps;
					g_str_pedo_back.ui16_run_time = timestamp;

					now_state &= ~(BAC_WALK);
				}

				if( (state_low & BAC_BIKE) ) // bike end
				{
					if ((g_str_pedo_back.ui32_bike_step < g_ui32_pedometer_steps))
					{
						g_str_pedo.ui32_bike_step = g_ui32_pedometer_steps - g_str_pedo_back.ui32_bike_step;
						g_str_pedo.ui32_total_bike_step += g_str_pedo.ui32_bike_step;
					}
					if( g_str_pedo_back.ui16_bike_time < timestamp )
					{
						g_str_pedo.ui16_bike_time = timestamp - g_str_pedo_back.ui16_bike_time;
						g_str_pedo.ui16_total_bike_time += g_str_pedo.ui16_bike_time;
					}

					g_str_pedo_back.ui32_bike_step = g_ui32_pedometer_steps;
					g_str_pedo_back.ui16_bike_time = timestamp;
					now_state &= ~(BAC_BIKE);
				}

				if( (state_low & BAC_DRIVE) ) // drive end
				{
					if ((g_str_pedo_back.ui32_drive_step < g_ui32_pedometer_steps))
					{
						g_str_pedo.ui32_drive_step = g_ui32_pedometer_steps - g_str_pedo_back.ui32_drive_step;
						g_str_pedo.ui32_total_drive_step += g_str_pedo.ui32_drive_step;
					}
					if( g_str_pedo_back.ui16_drive_time < timestamp )
					{
						g_str_pedo.ui16_drive_time = timestamp - g_str_pedo_back.ui16_drive_time;
						g_str_pedo.ui16_total_drive_time += g_str_pedo.ui16_drive_time;
					}

					g_str_pedo_back.ui32_drive_step = g_ui32_pedometer_steps;
					g_str_pedo_back.ui16_drive_time = timestamp;
					now_state &= ~(BAC_DRIVE);
				}

				if( (state_low & BAC_TILT) ) // tilt end
				{
					now_state &= ~(BAC_TILT);
				}

				if ( (state_low & BAC_STILL) ) //still end
				{
					if(now_state == BAC_STILL)
					{
						g_str_pedo.ui16_still_time = timestamp - g_str_pedo_back.ui16_still_time;
						g_str_pedo.ui16_total_still_time += g_str_pedo.ui16_still_time;

						g_str_pedo_back.ui16_still_time = timestamp;
					}
					now_state &= ~(BAC_STILL);
					g_str_bit.b2_pedo_mode = 1;
				}

				if (state_high & BAC_STILL) // still start
				{
					g_str_pedo_back.ui16_still_time = timestamp;
					now_state = BAC_STILL;

					g_str_bit.b2_pedo_mode = 0;
				}

				if (state_high & BAC_WALK) // walk start
				{
					g_str_pedo_back.ui32_walk_step = g_ui32_pedometer_steps;
					g_str_pedo_back.ui16_walk_time = timestamp;
					now_state = BAC_WALK;

					g_str_bit.b2_pedo_mode = 1;
				}

				if (state_high & BAC_RUN) // run start
				{
					g_str_pedo_back.ui32_run_step = g_ui32_pedometer_steps;
					g_str_pedo_back.ui16_run_time = timestamp;
					now_state = BAC_RUN;

					g_str_bit.b2_pedo_mode = 2;
				}

				if (state_high & BAC_BIKE) // bike start
				{
					g_str_pedo_back.ui32_bike_step = g_ui32_pedometer_steps;
					g_str_pedo_back.ui16_bike_time = timestamp;
					now_state = BAC_BIKE;

					g_str_bit.b2_pedo_mode = 1;
				}

				if (state_high & BAC_DRIVE) // drive start
				{
					g_str_pedo_back.ui32_drive_step = g_ui32_pedometer_steps;
					g_str_pedo_back.ui16_drive_time = timestamp;
					now_state = BAC_DRIVE;

					g_str_bit.b2_pedo_mode = 1;
				}

				if (state_high & BAC_TILT) // tilt start
				{
					now_state = BAC_TILT;
				}
			}
			//refresh activate time
			g_ui32_pedometer_times = g_str_pedo.ui16_total_walk_time + g_str_pedo.ui16_total_run_time;

			// Step detector available from DMP FIFO and step counter sensor is enabled
			if (header & PED_STEPDET_SET)
			{
				if ( g_str_bit.b1_inspect_mode )
					g_str_bit.b1_motion_chk = 1;
				
				g_str_timer.ui16_stay_hour = 0;

				//dmp_get_pedometer_num_of_steps(&g_ui32_pedometer_steps);
				dmp_get_pedometer_num_of_steps(&g_ui32_cur_pedo_steps);

				g_ui32_pedometer_steps = g_ui32_last_pedo_steps + g_ui32_cur_pedo_steps;

				if( g_str_app_status.ui8_step_status )
				{
					if( g_str_bit.b1_daily_step_noti )
					{
						//step goal achievement
						if( g_ui32_pedometer_steps > ( g_str_app_status.ui16_target_steps * 1000 ) )
						{
							// noti steps
							if( ( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING ) || ( g_str_call_bit.b1_favorite_enable ) )
							{
								if( !g_str_app_status.ui8_dnd_status )
								{
									WICED_BT_TRACE("call_step_noti\n");
									uint8_t mcu_send_data[2] = {0,};
									mcu_send_data[0] = WICED_TRUE;
									mcu_send_data[1] = (g_str_app_status.ui8_dnd_status == 1)? HAPTIC_OFF : HAPTIC_ON;
									send_mcu_msg(CMD_APP_NOTI_STATUS, UART_ID_CYW, 2, mcu_send_data);
									g_str_bit.b1_daily_step_noti = 0;
								}
							}
							else
							{
								WICED_BT_TRACE("call_step_noti\n");
								uint8_t mcu_send_data[2] = {0,};
								mcu_send_data[0] = WICED_TRUE;
								mcu_send_data[1] = (g_str_app_status.ui8_dnd_status == 1)? HAPTIC_OFF : HAPTIC_ON;
								send_mcu_msg(CMD_APP_NOTI_STATUS, UART_ID_CYW, 2, mcu_send_data);
								g_str_bit.b1_daily_step_noti = 0;
							}
						}
					}
				}
			}
			//if(!data_left_in_fifo)
			//	g_str_bit.b1_motion_chk = 0;

		}while (data_left_in_fifo);
	}

}
#endif
