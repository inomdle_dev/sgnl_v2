/*
 * inv_mems_drv_hook.c
 *
 *  Created on: 2017. 9. 7.
 *      Author: sheum
 */

#include "types.h"
#include "inv_defines.h"
#include "i2c_event.h"

int inv_serial_interface_write_hook(uint16_t reg, uint32_t length, const uint8_t *data)
{
	i2c_write_multi_byte(I2C_SLAVE_ADDRESS, (uint8_t)reg, (uint8_t *)data, (uint8_t)length);
	return 0;
}

int inv_serial_interface_read_hook(uint16_t reg, uint32_t length, uint8_t *data)
{
    	i2c_read_multi_byte(I2C_SLAVE_ADDRESS, (uint8_t)reg, (uint8_t *)data, (uint8_t)length);
	return 0;
}



