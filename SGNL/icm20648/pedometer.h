#ifndef _PEDOMETER_H_
#define _PEDOMETER_H_

#define ACCEL_ON        (0x01)
#define GYRO_ON         (0x02)
#define DEFAULT_MPU_HZ  (20)

#define TEMP_READ_MS    (500)
#define PEDO_READ_MS    (1000)
#define COMPASS_READ_MS (100)

#define PRINT_ACCEL     (0x01)
#define PRINT_GYRO      (0x02)
#define PRINT_QUAT      (0x04)
#define PRINT_COMPASS   (0x08)
#define PRINT_EULER     (0x10)
#define PRINT_ROT_MAT   (0x20)
#define PRINT_HEADING   (0x40)
#define PRINT_PEDO      (0x80)
#define PRINT_LINEAR_ACCEL (0x100)
#define PRINT_GRAVITY_VECTOR (0x200)

#define PEDOMETER_PERIOD        6000*60

extern uint32_t g_ui32_last_pedo_steps;
extern uint32_t g_ui32_cur_pedo_steps;
extern uint32_t g_ui32_pedometer_steps;
extern uint32_t g_ui32_pedometer_times;

extern unsigned char now_state;

void pedometer_data_update( void );
void pedometer(void);
void pedometer_init(void);
void reset_pedometer(void);
void pedometer_data_transfer(uint16_t conn_id);
void pedometer_check(void);

#endif  //_PEDOMETER_H_
