/*
 * timer_event.c
 *
 *  Created on: 2017. 9. 7.
 *      Author: sheum
 */
#include "global_define.h"
#include "wiced_bt_trace.h"
#include "uart.h"

void day_of_week( void )
{
	uint8_t year    = (uint8_t)(g_str_time.ui32_date>>16);
	uint8_t month   = (uint8_t)((g_str_time.ui32_date&0xff00)>>8);
	uint8_t days    = (uint8_t)(g_str_time.ui32_date & 0xff);

	int total = (year - 1) * 365 + (((year-1) / 4) - ((year-1) / 100) + ((year-1) / 400));
	int m[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};     // last day of month
	m[1] =  year % 4 == 0 && year % 100 != 0 || year % 400 == 0 ? 29 : 28;  // check leap year

	int i = 0;

	for(i=1; i <month; i++)
	{
		total += m[i-1];
	}
	total += days;

	g_str_time.ui8_last_day_of_week = g_str_time.ui8_day_of_week;
	g_str_time.ui8_day_of_week = total % 7;

	if( !g_str_bit.b1_por_status )
	{
#ifdef ENABLE_PEDOMETER
		g_ui32_pedometer_times = 0;
		memset(&g_str_pedo, 0, sizeof(pedometer_struct_t));
		app_status_read();
		reset_steps();
#endif
	}


	if( g_str_app_status.ui8_step_status )
	{
		g_str_bit.b1_daily_step_noti = 1;
	}

	if( g_str_app_status.ui8_activity_status )
	{
		g_str_bit.b1_daily_active_noti = 1;
	}
}

void time_update( void )
{
	uint32_t year = 0;
	uint32_t month = 0;
	uint32_t days = 0;

	if(++g_str_time.ui32_hour >= 24)// When it is 24 o'clock
	{
		g_str_time.ui32_hour = g_str_time.ui32_hour % 24;
		year = (g_str_time.ui32_date&0xff0000)>>16;
		month = (g_str_time.ui32_date&0xff00)>>8;
		days = (g_str_time.ui32_date & 0xff);

		if( ( (month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12) ) && (days == 31) )
		{
			month++;
			days = 1;
			if(month == 13)
			{
				year++;
				month = 1;
			}
		}
		else if( ( (month == 4) || (month == 6) || (month == 9) || (month == 11) ) && (days == 30) )
		{
			month++;
			days = 1;
		}
		else if( (month == 2) && ( ( ( (year % 4) == 0 ) && (days == 29) ) || ( ( (year % 4) != 0 ) && (days == 28) ) ) )//leap year
		{
			month++;
			days = 1;
		}
		else
		{
			days++;
		}
#ifdef ENABLE_PEDOMETER
		if( g_ui32_flash_hour_write_cnt )
		{
			pedo_flash_daily_write();
		}
#endif
		g_str_time.ui32_date = (year<<16) | (month<<8) | days;

		day_of_week();
	}

}

void one_minute_timer( void )
{
	if( g_str_time.ui8_minute < 59 )
	{
		g_str_time.ui8_minute++;
	}
	else
	{
#ifdef ENABLE_PEDOMETER
		pedo_flash_hour_write();
#endif
		g_str_time.ui8_minute = 0;
		time_update();
	}

#ifdef ENABLE_PEDOMETER
	if( g_str_time.ui32_date )
	{
		uint32_t cur_time = (g_str_time.ui32_hour&0xFF)<<8;
		cur_time |= (g_str_time.ui8_minute&0xFF);

#ifdef ENABLE_PEDOMETER_DEBUG
        WICED_BT_TRACE("date %04x, hour %02x, min %02x cur time %04x stay h %d\n", g_str_time.ui32_date, g_str_time.ui32_hour, g_str_time.ui8_minute, cur_time, g_str_timer.ui16_stay_hour);
#endif

		if( ( g_str_app_status.ui8_longsit_status ) &&
				( g_str_app_status.ui16_longsit_start_time < cur_time ) &&
				( g_str_app_status.ui16_longsit_end_time > cur_time ) )
		{
			if( ++g_str_timer.ui16_stay_hour >= 60 )
			{
				uint8_t byte[3];
				uint8_t nus_index = 0;

				byte[++nus_index] = REPORT_APP_CONFIG;
				byte[++nus_index] = REP_CONFIG_LONGSIT_STATUS;
				byte[0] = ++nus_index;
				send_byte_gatt(byte, byte[0]);

				g_str_timer.ui16_stay_hour = 0;

				WICED_BT_TRACE("Long sit noti\n");
				//call_longsit_noti();
				uint8_t mcu_send_data[2] = {0,};
				mcu_send_data[0] = WICED_TRUE;
				mcu_send_data[1] = (g_str_app_status.ui8_dnd_status == 1)? HAPTIC_OFF : HAPTIC_ON;
				send_mcu_msg(CMD_APP_NOTI_STATUS, UART_ID_CYW, 2, mcu_send_data);
			}
		}
	}

	if( g_str_bit.b2_pedo_mode )
	{
		if( g_str_app_status.ui8_activity_status )
		{
			if( g_str_bit.b1_daily_active_noti )
			{
				if( g_ui32_pedometer_times > ( g_str_app_status.ui16_target_active_time * 60 ) )
				{
					// noti times
					if( ( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING ) || ( g_str_call_bit.b1_favorite_enable ) )
					{
						if( !g_str_app_status.ui8_dnd_status )
						{
							//call_sub_activity_noti();
							WICED_BT_TRACE("sub active noti\n");
							uint8_t mcu_send_data[2] = {0,};
							mcu_send_data[0] = WICED_TRUE;
							mcu_send_data[1] = (g_str_app_status.ui8_dnd_status == 1)? HAPTIC_OFF : HAPTIC_ON;
							send_mcu_msg(CMD_APP_NOTI_STATUS, UART_ID_CYW, 2, mcu_send_data);
							g_str_bit.b1_daily_active_noti = 0;
						}
					}
					else
					{
						WICED_BT_TRACE("active noti\n");
						uint8_t mcu_send_data[2] = {0,};
						mcu_send_data[0] = WICED_TRUE;
						mcu_send_data[1] = (g_str_app_status.ui8_dnd_status == 1)? HAPTIC_OFF : HAPTIC_ON;
						send_mcu_msg(CMD_APP_NOTI_STATUS, UART_ID_CYW, 2, mcu_send_data);
						g_str_bit.b1_daily_active_noti = 0;
					}
				}
			}
		}
    }
#endif //ENABLE_PEDOMETER

}



