/*
 * This file has been automatically generated by the WICED 20706-A2 Designer.
 * SDP database for Single Mode BR/EDR or Dual Mode device.
 *
 */

// SGNL_sdp_db.c

#include "wiced_bt_uuid.h"
#include "wiced_bt_sdp.h"
#if 0
#include "SGNL_db.h"
#endif
#include "SGNL_sdp_db.h"

const uint8_t sdp_database[] = // Define SDP database
{

#if 1
#if defined(SGNL_SUPPORT_HFP) && defined(SGNL_SUPPORT_A2DP) && defined(SGNL_SUPPORT_AVRC)
	SDP_ATTR_SEQUENCE_2( (72 + 2) + (73 + 2) + ( 56 + 2 + 59 + 2) ),
#elif defined(SGNL_SUPPORT_HFP) && defined(SGNL_SUPPORT_A2DP)
	SDP_ATTR_SEQUENCE_2( (72 + 2) + (73 + 2) ),
#elif defined(SGNL_SUPPORT_HFP) 
	SDP_ATTR_SEQUENCE_2( (72 + 2) ),
#endif

#ifdef SGNL_SUPPORT_A2DP
    // SDP Record for A2DP Sink
    SDP_ATTR_SEQUENCE_1(73),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_A2DP_SINK),								// 8
        SDP_ATTR_CLASS_ID(UUID_SERVCLASS_AUDIO_SINK),							// 8
        SDP_ATTR_ID(ATTR_ID_PROTOCOL_DESC_LIST), SDP_ATTR_SEQUENCE_1(16),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_L2CAP),							// 3
                SDP_ATTR_VALUE_UINT2(BT_PSM_AVDTP),							// 3
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_AVDTP),							// 3
                SDP_ATTR_VALUE_UINT2(0x100),									// 3
        SDP_ATTR_ID(ATTR_ID_BT_PROFILE_DESC_LIST), SDP_ATTR_SEQUENCE_1(8),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_ADV_AUDIO_DISTRIBUTION),		// 3
                SDP_ATTR_VALUE_UINT2(0x100),									// 3
        SDP_ATTR_UINT2(ATTR_ID_SUPPORTED_FEATURES, 0x000B),					// 6
        SDP_ATTR_SERVICE_NAME(12),											// 5
            'S', 'g', 'n', 'l', ' ', 'I', 'M', '-', 'S', '0', '0', '1',	// 12
#endif

#ifdef SGNL_SUPPORT_AVRC
    // SDP Record for AVRC Target
    SDP_ATTR_SEQUENCE_1(56),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_AVRC_TARGET),							// 8
        SDP_ATTR_ID(ATTR_ID_SERVICE_CLASS_ID_LIST),							// 3
            SDP_ATTR_SEQUENCE_1(3),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_AV_REM_CTRL_TARGET),			// 3
        SDP_ATTR_ID(ATTR_ID_PROTOCOL_DESC_LIST), SDP_ATTR_SEQUENCE_1(16),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_L2CAP),							// 3
                SDP_ATTR_VALUE_UINT2(BT_PSM_AVCTP),							// 3
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_AVCTP),							// 3
                SDP_ATTR_VALUE_UINT2(0x0104),								// 3
        SDP_ATTR_ID(ATTR_ID_BT_PROFILE_DESC_LIST), SDP_ATTR_SEQUENCE_1(8),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_AV_REMOTE_CONTROL),			// 3
                SDP_ATTR_VALUE_UINT2(AVRC_REV_1_5),							// 3
        SDP_ATTR_UINT2(ATTR_ID_SUPPORTED_FEATURES, AVRC_SUPF_TG_CAT2),		// 6

    // SDP Record for AVRC Controller
    SDP_ATTR_SEQUENCE_1(59),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_AVRC_CONTROLLER),						// 8
        SDP_ATTR_ID(ATTR_ID_SERVICE_CLASS_ID_LIST),							// 3
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_AV_REMOTE_CONTROL),			// 3
                SDP_ATTR_UUID16(UUID_SERVCLASS_AV_REM_CTRL_CONTROL),			// 3
        SDP_ATTR_ID(ATTR_ID_PROTOCOL_DESC_LIST), SDP_ATTR_SEQUENCE_1(16),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_L2CAP),							// 3
                SDP_ATTR_VALUE_UINT2(BT_PSM_AVCTP),							// 3
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_AVCTP),							// 3
                SDP_ATTR_VALUE_UINT2(0x104),									// 3
        SDP_ATTR_ID(ATTR_ID_BT_PROFILE_DESC_LIST), SDP_ATTR_SEQUENCE_1(8),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_AV_REMOTE_CONTROL),			// 3
                SDP_ATTR_VALUE_UINT2(AVRC_REV_1_3),							// 3
        SDP_ATTR_UINT2(ATTR_ID_SUPPORTED_FEATURES, AVRC_SUPF_CT_CAT1),		// 6
#endif 

#ifdef SGNL_SUPPORT_HFP
    // SDP Record for Hands-Free Unit
    SDP_ATTR_SEQUENCE_1(72),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_HANDS_FREE_UNIT),						// 8
        SDP_ATTR_ID(ATTR_ID_SERVICE_CLASS_ID_LIST), SDP_ATTR_SEQUENCE_1(6),	// 5
            SDP_ATTR_UUID16(UUID_SERVCLASS_HF_HANDSFREE),						// 3
            SDP_ATTR_UUID16(UUID_SERVCLASS_GENERIC_AUDIO),					// 3
        SDP_ATTR_RFCOMM_PROTOCOL_DESC_LIST(HANDS_FREE_SCN),					// 17
        SDP_ATTR_ID(ATTR_ID_BT_PROFILE_DESC_LIST), SDP_ATTR_SEQUENCE_1(8), // 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_HF_HANDSFREE),					// 3
                SDP_ATTR_VALUE_UINT2(0x107),									// 3
        SDP_ATTR_SERVICE_NAME(12),											// 5
            'S', 'g', 'n', 'l', ' ', 'I', 'M', '-', 'S', '0', '0', '1',	// 12
        SDP_ATTR_UINT2(ATTR_ID_SUPPORTED_FEATURES, 0x0016),					// 6
#endif

#endif

#if 0
#if defined(SGNL_SUPPORT_HFP) && defined(SGNL_SUPPORT_A2DP) && defined(SGNL_SUPPORT_AVRC)
	SDP_ATTR_SEQUENCE_2( (40 + 2 + 40 + 2 + 40 + 2) + (72 + 2) + (73 + 2) + ( 56 + 2 + 59 + 2) ),
#elif defined(SGNL_SUPPORT_HFP) && defined(SGNL_SUPPORT_A2DP)
	SDP_ATTR_SEQUENCE_2( (40 + 2 + 40 + 2 + 40 + 2) + (72 + 2) + (73 + 2) ),
#elif defined(SGNL_SUPPORT_HFP) 
	SDP_ATTR_SEQUENCE_2( (40 + 2 + 40 + 2 + 40 + 2) + (72 + 2) ),
#endif

    // SDP Record for GATT (Generic Attribute)
    SDP_ATTR_SEQUENCE_1(40),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_GATT_GENERIC_ATTRIBUTE),					// 8
        SDP_ATTR_CLASS_ID(UUID_SERVICE_GATT),									// 8
        SDP_ATTR_ID(ATTR_ID_PROTOCOL_DESC_LIST), SDP_ATTR_SEQUENCE_1(19),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_L2CAP),							// 3
                SDP_ATTR_VALUE_UINT2(BT_PSM_ATT),								// 3
            SDP_ATTR_SEQUENCE_1(9),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_ATT),							// 3
                SDP_ATTR_VALUE_UINT2(HDLS_GENERIC_ATTRIBUTE),					// 3
                SDP_ATTR_VALUE_UINT2(HDLS_GENERIC_ATTRIBUTE),					// 3

    // SDP Record for GATT (Generic Access)
    SDP_ATTR_SEQUENCE_1(40),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_GATT_GENERIC_ACCESS),					// 8
        SDP_ATTR_CLASS_ID(UUID_SERVICE_GAP),									// 8
        SDP_ATTR_ID(ATTR_ID_PROTOCOL_DESC_LIST), SDP_ATTR_SEQUENCE_1(19),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_L2CAP),							// 3
                SDP_ATTR_VALUE_UINT2(BT_PSM_ATT),								// 3
            SDP_ATTR_SEQUENCE_1(9),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_ATT),							// 3
                SDP_ATTR_VALUE_UINT2(HDLS_GENERIC_ACCESS),					// 3
                SDP_ATTR_VALUE_UINT2(HDLC_GENERIC_ACCESS_APPEARANCE_VALUE),	// 3

#ifdef SGNL_SUPPORT_A2DP
    // SDP Record for A2DP Sink
    SDP_ATTR_SEQUENCE_1(73),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_A2DP_SINK),								// 8
        SDP_ATTR_CLASS_ID(UUID_SERVCLASS_AUDIO_SINK),							// 8
        SDP_ATTR_ID(ATTR_ID_PROTOCOL_DESC_LIST), SDP_ATTR_SEQUENCE_1(16),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_L2CAP),							// 3
                SDP_ATTR_VALUE_UINT2(BT_PSM_AVDTP),							// 3
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_AVDTP),							// 3
                SDP_ATTR_VALUE_UINT2(0x100),									// 3
        SDP_ATTR_ID(ATTR_ID_BT_PROFILE_DESC_LIST), SDP_ATTR_SEQUENCE_1(8),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_ADV_AUDIO_DISTRIBUTION),		// 3
                SDP_ATTR_VALUE_UINT2(0x100),									// 3
        SDP_ATTR_UINT2(ATTR_ID_SUPPORTED_FEATURES, 0x000B),					// 6
        SDP_ATTR_SERVICE_NAME(12),											// 5
            'S', 'g', 'n', 'l', ' ', 'I', 'M', '-', 'S', '0', '0', '1',	// 12
#endif

#ifdef SGNL_SUPPORT_AVRC
    // SDP Record for AVRC Target
    SDP_ATTR_SEQUENCE_1(56),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_AVRC_TARGET),							// 8
        SDP_ATTR_ID(ATTR_ID_SERVICE_CLASS_ID_LIST),							// 3
            SDP_ATTR_SEQUENCE_1(3),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_AV_REM_CTRL_TARGET),			// 3
        SDP_ATTR_ID(ATTR_ID_PROTOCOL_DESC_LIST), SDP_ATTR_SEQUENCE_1(16),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_L2CAP),							// 3
                SDP_ATTR_VALUE_UINT2(BT_PSM_AVCTP),							// 3
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_AVCTP),							// 3
                SDP_ATTR_VALUE_UINT2(0x0104),								// 3
        SDP_ATTR_ID(ATTR_ID_BT_PROFILE_DESC_LIST), SDP_ATTR_SEQUENCE_1(8),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_AV_REMOTE_CONTROL),			// 3
                SDP_ATTR_VALUE_UINT2(AVRC_REV_1_5),							// 3
        SDP_ATTR_UINT2(ATTR_ID_SUPPORTED_FEATURES, AVRC_SUPF_TG_CAT2),		// 6

    // SDP Record for AVRC Controller
    SDP_ATTR_SEQUENCE_1(59),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_AVRC_CONTROLLER),						// 8
        SDP_ATTR_ID(ATTR_ID_SERVICE_CLASS_ID_LIST),							// 3
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_AV_REMOTE_CONTROL),			// 3
                SDP_ATTR_UUID16(UUID_SERVCLASS_AV_REM_CTRL_CONTROL),			// 3
        SDP_ATTR_ID(ATTR_ID_PROTOCOL_DESC_LIST), SDP_ATTR_SEQUENCE_1(16),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_L2CAP),							// 3
                SDP_ATTR_VALUE_UINT2(BT_PSM_AVCTP),							// 3
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_AVCTP),							// 3
                SDP_ATTR_VALUE_UINT2(0x104),									// 3
        SDP_ATTR_ID(ATTR_ID_BT_PROFILE_DESC_LIST), SDP_ATTR_SEQUENCE_1(8),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_AV_REMOTE_CONTROL),			// 3
                SDP_ATTR_VALUE_UINT2(AVRC_REV_1_3),							// 3
        SDP_ATTR_UINT2(ATTR_ID_SUPPORTED_FEATURES, AVRC_SUPF_CT_CAT1),		// 6
#endif 

#ifdef SGNL_SUPPORT_HFP
    // SDP Record for Hands-Free Unit
    SDP_ATTR_SEQUENCE_1(72),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_HANDS_FREE_UNIT),						// 8
        SDP_ATTR_ID(ATTR_ID_SERVICE_CLASS_ID_LIST), SDP_ATTR_SEQUENCE_1(6),	// 5
            SDP_ATTR_UUID16(UUID_SERVCLASS_HF_HANDSFREE),						// 3
            SDP_ATTR_UUID16(UUID_SERVCLASS_GENERIC_AUDIO),					// 3
        SDP_ATTR_RFCOMM_PROTOCOL_DESC_LIST(HANDS_FREE_SCN),					// 17
        SDP_ATTR_ID(ATTR_ID_BT_PROFILE_DESC_LIST), SDP_ATTR_SEQUENCE_1(8), // 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_SERVCLASS_HF_HANDSFREE),					// 3
                SDP_ATTR_VALUE_UINT2(0x106),									// 3
        SDP_ATTR_SERVICE_NAME(12),											// 5
            'S', 'g', 'n', 'l', ' ', 'I', 'M', '-', 'S', '0', '0', '1',	// 12
        SDP_ATTR_UINT2(ATTR_ID_SUPPORTED_FEATURES, 0x0016),					// 6
#endif        

    // SDP Record for GATT (Device Information)
    SDP_ATTR_SEQUENCE_1(40),													// 2
        SDP_ATTR_RECORD_HANDLE(HDLR_GATT_DEVICE_INFORMATION),				// 8
        SDP_ATTR_CLASS_ID(UUID_SERVICE_DEVICE_INFORMATION),					// 8
        SDP_ATTR_ID(ATTR_ID_PROTOCOL_DESC_LIST), SDP_ATTR_SEQUENCE_1(19),	// 5
            SDP_ATTR_SEQUENCE_1(6),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_L2CAP),							// 3
                SDP_ATTR_VALUE_UINT2(BT_PSM_ATT),								// 3
            SDP_ATTR_SEQUENCE_1(9),											// 2
                SDP_ATTR_UUID16(UUID_PROTOCOL_ATT),							// 3
                SDP_ATTR_VALUE_UINT2(HDLS_DEVICE_INFORMATION),				// 3
                SDP_ATTR_VALUE_UINT2(HDLC_DEVICE_INFORMATION_MODEL_NUMBER_STRING_VALUE),	// 3
#endif        
};

// Length of the SDP database
const uint16_t sdp_database_len = sizeof(sdp_database);
