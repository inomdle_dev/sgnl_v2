/*
 * Cypress Semiconductor Proprietary and Confidential. ?2016 Cypress Semiconductor.  All rights reserved.
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Cypress Semiconductor.
 */

/** @file
 *
 * This file implements audio application controlled over UART.
 *
 */
#include "wiced_bt_a2d.h"
#include "wiced_bt_a2d_sbc.h"
#include "wiced_bt_a2dp_sink.h"
#include "wiced_bt_trace.h"
#include "handsfree.h"
#include "handsfree_av.h"

#include "uart.h"
#include "sgnl_struct.h"

/******************************************************************************
 *                         Type Definitions
 ******************************************************************************/

/******************************************************************************
 *                         Structures
 ******************************************************************************/

typedef struct
{
    wiced_bt_device_address_t       peerBda;             /* Peer bd address */
    AV_STATE                        state;               /* AVDT State machine state */
    AV_STREAM_STATE                 audio_stream_state;  /* Audio Streaming to host state */
    wiced_bt_a2dp_codec_info_t      codec_config;       /* Codec configuration information */
    uint16_t                        handle;             /* connection handle */
} tAV_APP_CB;

/******************************************************************************
 *                         Variable Definitions
 ******************************************************************************/

/* A2DP module control block */
static tAV_APP_CB                   av_app_cb;
extern wiced_bt_a2dp_config_data_t  bt_audio_config;

static void a2dp_sink_control_cback(wiced_bt_a2dp_sink_event_t event, wiced_bt_a2dp_sink_event_data_t* p_data);

/******************************************************************************
 *                          Function Definitions
 ******************************************************************************/
static const char *dump_control_event_name(wiced_bt_a2dp_sink_event_t event)
{
    switch ((int)event)
    {
        CASE_RETURN_STR(WICED_BT_A2DP_SINK_CONNECT_EVT)
        CASE_RETURN_STR(WICED_BT_A2DP_SINK_DISCONNECT_EVT)
        CASE_RETURN_STR(WICED_BT_A2DP_SINK_START_IND_EVT)
        CASE_RETURN_STR(WICED_BT_A2DP_SINK_START_CFM_EVT)
        CASE_RETURN_STR(WICED_BT_A2DP_SINK_SUSPEND_EVT)
        CASE_RETURN_STR(WICED_BT_A2DP_SINK_CODEC_CONFIG_EVT)
    }

    return NULL;
}

static const char *dump_state_name(AV_STATE state)
{
    switch ((int)state)
    {
        CASE_RETURN_STR(AV_STATE_IDLE)
        CASE_RETURN_STR(AV_STATE_CONFIGURED)
        CASE_RETURN_STR(AV_STATE_CONNECTED)
        CASE_RETURN_STR(AV_STATE_STARTED)
    }

    return NULL;
}

/*******************************************************************************
 * A2DP Application HCI Control handlers
 *******************************************************************************/
/*
 * Handles the audio connect command
 */
wiced_result_t handsfree_av_connect(wiced_bt_device_address_t bd_addr)
{
    wiced_result_t status = WICED_ERROR;

    WICED_BT_TRACE("[%s]  <%B>\n", __FUNCTION__, bd_addr);

    /* First make sure that there is not already a connection */
    if (av_app_cb.state == AV_STATE_IDLE)
    {
        status = wiced_bt_a2dp_sink_connect(bd_addr);
    }
    else
    {
        WICED_BT_TRACE("[%s]: Bad state: (%d) %s", __FUNCTION__, av_app_cb.state, dump_state_name(av_app_cb.state));
    }
    return status;
}

/*
 * Handles the audio disconnect
 */
wiced_result_t handsfree_av_disconnect(uint16_t handle)
{
    wiced_result_t status = WICED_ERROR;

    WICED_BT_TRACE("[%s] Handle %d\n\r", __FUNCTION__, handle);

    /* if already idle there is no connection to disconnect */
    /* TODO: Bad use of an enumerated type as an integer. Fix later */
    if (av_app_cb.state >= AV_STATE_CONNECTED)
    {
        if (WICED_BT_PENDING == wiced_bt_dev_cancel_sniff_mode (av_app_cb.peerBda))
        {
            WICED_BT_TRACE("[%s] sniff canceling for <%B>\n", __FUNCTION__, av_app_cb.peerBda);
        }

        /* attempt to disconnect directly */
        status = wiced_bt_a2dp_sink_disconnect(handle);
    }
    else
    {
        WICED_BT_TRACE("[%s]: Bad state: (%d) %s", __FUNCTION__, av_app_cb.state, dump_state_name(av_app_cb.state));
    }
    return status;
}


/* ****************************************************************************
 * Function: a2dp_sink_control_cback
 *
 * Parameters:
 *          event - control event called back
 *          p_data - event data
 *
 * Description:
 *          Control callback supplied by  the a2dp sink profile code.
 * ***************************************************************************/

void a2dp_sink_control_cback(wiced_bt_a2dp_sink_event_t event,  wiced_bt_a2dp_sink_event_data_t* p_data)
{
	uint16_t idx = 0xFFFF;
	uint8_t data[2] = {0,};
	static wiced_bt_a2dp_codec_info_t   parsed_codec;
	wiced_bt_a2dp_sink_route_config route_config;

	WICED_BT_TRACE("[%s] Event: (%d) %s\n\r", __FUNCTION__, event, dump_control_event_name(event));

	switch (event)
	{
		case WICED_BT_A2DP_SINK_CODEC_CONFIG_EVT: /**< Codec config event, received when codec config for a streaming session is updated */
    	    /* Save the configuration to setup the decoder if necessary. */
	        memcpy(&parsed_codec, &p_data->codec_config, sizeof(wiced_bt_a2dp_codec_info_t));

#ifdef I2S_SLAVE_MODE
			route_config.route = AUDIO_ROUTE_I2S;
			//route_config.data_cb = NULL;
			route_config.is_master = WICED_FALSE;
			wiced_bt_a2dp_sink_update_route_config(p_data->codec_config.handle, &route_config);
#endif
			WICED_BT_TRACE(" a2dp sink codec configuration done\n\r");
			break;

		case WICED_BT_A2DP_SINK_CONNECT_EVT:      /**< Connected event, received on establishing connection to a peer device. Ready to stream. */
			if (p_data->connect.result == WICED_SUCCESS)
			{
				/* Save the address of the remote device on remote connection */
				memcpy(av_app_cb.peerBda, p_data->connect.bd_addr, sizeof(wiced_bt_device_address_t));
				av_app_cb.handle = p_data->connect.handle;
				av_app_cb.state  = AV_STATE_CONNECTED;

#ifdef WICED_V6_1
				BTM_SwitchRole(p_data->connect.bd_addr, HCI_ROLE_SLAVE, NULL);
#endif

				WICED_BT_TRACE("[%s] A2DP sink connected to addr: <%B> Handle:%d\n\r", __FUNCTION__, p_data->connect.bd_addr, p_data->connect.handle);

#ifdef SGNL_SUPPORT_AVRC
				if (handsfree_app_state.is_originator)
				{
					wiced_bt_avrc_ct_connect(av_app_cb.peerBda);
				}
#endif			
			}
			else
			{
				WICED_BT_TRACE(" a2dp sink connection failed %d \n", p_data->connect.result);
			}
			break;

		case WICED_BT_A2DP_SINK_DISCONNECT_EVT:   /**< Disconnected event, received on disconnection from a peer device */
			av_app_cb.state = AV_STATE_IDLE;
			WICED_BT_TRACE(" a2dp sink disconnected handle:%d\n\r", p_data->disconnect.handle);
			break;

		case WICED_BT_A2DP_SINK_START_IND_EVT:        /**< Start stream event, received when audio streaming is about to start */
			g_str_bit.b1_a2dp_start = 1;
			data[0] = 0x01;
			send_mcu_msg(CMD_HFP_A2DP, UART_ID_CYW, 1, data);
			WICED_BT_TRACE(" WICED_BT_A2DP_SINK_START_IND_EVT handle %x label:%x\n\r", p_data->start_ind.handle, p_data->start_ind.label);
			wiced_bt_a2dp_sink_send_start_response(p_data->start_ind.handle, p_data->start_ind.label, AVDT_SUCCESS);

			av_app_cb.state = AV_STATE_STARTED;
			data[0] = g_str_app_status.i8_call_volume;
			data[1] = 0x00;
			data[2] = g_str_app_status.ui8_max_volume;
			send_mcu_msg(CMD_VOLUME_CONTROL, UART_ID_CYW, 3, data);
#ifdef NEEDLESS_SLEEP_CONTROL
#ifdef FUNC_PP3_SLEEP_MODE
#ifdef ENABLE_POWER_SAVE_MODE
			set_app_sleep_allowed(FALSE);
#else
			wiced_sleep_config(FALSE, 0x01, 0x00);
#endif
#endif
#endif
			batt_check();
			break;

		case WICED_BT_A2DP_SINK_START_CFM_EVT:        /**< Start stream event, received when audio streaming is about to start */
			av_app_cb.state = AV_STATE_STARTED;
			WICED_BT_TRACE("WICED_BT_A2DP_SINK_START_CFM_EVT handle: %x\n\r", p_data->start_cfm.handle);
			break;

		case WICED_BT_A2DP_SINK_SUSPEND_EVT:      /**< Suspend stream event, received when audio streaming is suspended */
			app_status_write();
			av_app_cb.state = AV_STATE_CONNECTED;
			WICED_BT_TRACE(" a2dp sink streaming suspended Handle:%d\n\r", p_data->suspend.handle);
			g_str_bit.b1_a2dp_start = 0;
			data[0] = 0x00;
			send_mcu_msg(CMD_HFP_A2DP, UART_ID_CYW, 1, data);

#ifdef NEEDLESS_SLEEP_CONTROL
#ifdef FUNC_PP3_SLEEP_MODE
#ifdef ENABLE_POWER_SAVE_MODE
			set_app_sleep_allowed(TRUE);
#else
			wiced_sleep_config(TRUE, 0x01, 0x00);
#endif
#endif
#endif
			batt_check();
			break;

		default:
			break;
	}
}

/******************************************************************************
 *                     Application Initialization
 ******************************************************************************/

wiced_result_t handsfree_av_init(void)
{
    wiced_result_t result;

    memset(av_app_cb, 0, sizeof(av_app_cb));

    /* Register with the A2DP sink profile code */
    result = wiced_bt_a2dp_sink_init(&bt_audio_config, a2dp_sink_control_cback);
    WICED_BT_TRACE("[%s] A2DP sink initialized result:%x\n\r", __FUNCTION__, result);
    return result;
}

