/*
 * app_status_manager.c
 *
 *  Created on: 2017. 8. 31.
 *      Author: sheum
 */

#include "string.h"
#include "wiced_bt_trace.h"

#include "sgnl_struct.h"
#include "app_status_manager.h"
#include "flash_event.h"

void app_status_read( void )
{
    WICED_BT_TRACE( "app_status read\n" );
    user_flash_read( USER_FLASH_APP_CONFIG,  (uint32_t *)&g_str_app_status, 0, (uint32_t)sizeof(g_str_app_status));
}

void app_status_write( void )
{
    WICED_BT_TRACE( "app_status write\n" );


    if( g_str_app_status.ui8_app_noti_status ) WICED_BT_TRACE( "app_noti ON\n" );
    if( g_str_app_status.ui8_dnd_status )   WICED_BT_TRACE( "DND ON\n" );
    else                                    WICED_BT_TRACE( "DND OFF\n" );

    //if (g_str_app_status.ui8_host_device ) WICED_BT_TRACE( "host iOS\n" );
    //if( g_str_app_status.ui8_audio_bt_paired ) WICED_BT_TRACE( "BT paired\n" );
    if( g_str_app_status.ui8_ble_bonded ) WICED_BT_TRACE( "BLE bonded\n" );


    if( g_str_app_status.ui8_longsit_status )
    {
        WICED_BT_TRACE( "long_sit ON : %d:%d - %d:%d \n", g_str_app_status.ui16_longsit_start_time>>8, g_str_app_status.ui16_longsit_start_time&0xFF, g_str_app_status.ui16_longsit_end_time>>8, g_str_app_status.ui16_longsit_end_time&0xFF );

    }

    if( g_str_app_status.ui8_step_status )
    {
        WICED_BT_TRACE( "step status ON : %d\n", g_str_app_status.ui16_target_steps );
    }


    if( g_str_app_status.ui8_activity_status )
    {
        WICED_BT_TRACE( "activity ON : %d\n", g_str_app_status.ui16_target_active_time );
    }

    if( g_str_app_status.i8_call_volume )
    {
	    WICED_BT_TRACE( "Call volume level : %d\n", g_str_app_status.i8_call_volume );
    }

    if( g_str_app_status.ui8_max_volume )
    {
	    WICED_BT_TRACE( "Max volume level : %d\n", g_str_app_status.ui8_max_volume );
    }

    if( g_str_app_status.ui8_cgeq_status )
    {
	    WICED_BT_TRACE( "cgeq status : %d\n", g_str_app_status.ui8_cgeq_status );
    }

    user_flash_erase( USER_FLASH_APP_CONFIG );

    user_flash_write( USER_FLASH_APP_CONFIG,  (uint32_t *)&g_str_app_status, 0, (uint32_t)sizeof(g_str_app_status));
}



