/*
 * serial_comm_ble.h
 *
 *  Created on: 2017. 8. 31.
 *      Author: sheum
 */

#ifndef APPS_HCI_SERIAL_GATT_SERVICE_SERIAL_COMM_BLE_H_
#define APPS_HCI_SERIAL_GATT_SERVICE_SERIAL_COMM_BLE_H_

#include "hci_control_api.h"
#include "SGNL_DB.h"

void send_byte_gatt( uint8_t * ui8_param, uint8_t ui8_param_len );

void send_byte_gatt_connid( uint16_t conn_id, uint8_t * ui8_param, uint8_t ui8_param_len );


#endif /* APPS_HCI_SERIAL_GATT_SERVICE_SERIAL_COMM_BLE_H_ */
