/*
 * global_define.h
 *
 *  Created on: 2017. 9. 6.
 *      Author: sheum
 */

#ifndef APPS_SGNL_GLOBAL_DEFINE_H_
#define APPS_SGNL_GLOBAL_DEFINE_H_

#define ENABLE_IO_EXPANDER

#define ENABLE_BATT
#ifdef HW_PP4
#undef ENABLE_FUEL_GAUGE
#else
#define ENABLE_FUEL_GAUGE
#endif

#define ENABLE_FAVORITE_MANAGER
#define ENABLE_ANCS_PARSER

#define ENABLE_SERIAL_FLASH

#define ENABLE_POWER_OFF_TIMER

//#define ENABLE_SFLASH_READ

#define ENABLE_MCU_MSG_QUEUE

#define ENABLE_HCI_USER_CONTROL

#define ENABLE_GUARANTEE_BOOT_TIME
#define ENABLE_HFP_VOL_LEVEL_SYNC
#define ENABLE_INQUIRY_DEV
#define ENABLE_BT_FIRST
#define DISABLE_BT_DISCONN_BLE
//#define ENABLE_LED

//#define ENABLE_LE_NAME
#define WICED_V6_1
#define I2S_SLAVE_MODE
#define ENABLE_STOP_PEDOMETER
#define ENABLE_PEDO_RESET_TIMER

/**********************************/
/* certification                  */
/**********************************/
//#define ENABLE_CERTIFICATION_MODE
/**********************************/

/**********************************/
/* inspection                     */
/**********************************/
//#define ENABLE_INSPECTION
/**********************************/

#include "sgnl_struct.h"
#include "ble_protocol.h"
#include "timer_event.h"

#ifdef ENABLE_SERIAL_FLASH
#include "flash_event.h"
#endif

#ifdef ENABLE_ANCS_PARSER
#include "ancs_parser.h"
#endif

#ifdef ENABLE_IO_EXPANDER
#include "io_expander.h"
#endif

#ifdef ENABLE_PEDOMETER
#include "./icm20648/pedometer.h"
#include "./icm20648/pedo_flash_manager.h"
#endif

#ifdef ENABLE_FUEL_GAUGE
#include "fuel_gauge.h"
#endif

#ifdef ENABLE_OTA_FWUPGRADE
#include "wiced_bt_firmware_upgrade.h"
#include "wiced_bt_fw_upgrade.h"
#endif

#ifdef ENABLE_MCU_MSG_QUEUE
#include "mcu_msg_queue.h"
#endif

#ifdef ENABLE_HCI_USER_CONTROL
    #define HCI_CONTROL_GROUP_USER                  0xAF
    #define HCI_CONTROL_USER_SERIAL_FLASH_ERASE     ( ( HCI_CONTROL_GROUP_USER << 8 ) | 0x01 )
#endif

#ifndef ON
#define ON		(1)
#endif
#ifndef OFF
#define OFF	(0)
#endif

#ifndef UP
#define UP		(1)
#endif
#ifndef DN
#define DN		(0)
#endif

#ifndef HIGH
#define HIGH	(1)
#endif
#ifndef LOW
#define LOW	(0)
#endif

#ifndef YES
#define YES	(1)
#endif
#ifndef NO
#define NO 	(0)
#endif

//#define APP_TEST
//#define DEV_NAME
#define PP_NUMBERING

#if defined(APP_TEST)
#define BT_DEVICE_NAME	"JBY_TEST"
#elif defined (DEV_NAME)
#define BT_DEVICE_NAME	"TEST_NAME"
#elif defined (SMT_CHECK)
#define BT_DEVICE_NAME	"Sgnl_02"
#elif defined (PP_NUMBERING)
#define BT_DEVICE_NAME	"Sgnl"
#else
#define BT_DEVICE_NAME	"Sgnl"
#endif


#endif /* APPS_SGNL_GLOBAL_DEFINE_H_ */
