#include "global_define.h"

#define FAVORITE_LOG_INDEX		5
#define FAVORITE_LOG_SIZE		4

#define FAVORITE_LOG_COMP		0
#define FAVORITE_LOG_CNT		1
#define FAVORITE_LOG_IN_TIME	2
#define FAVORITE_LOG_OUT_TIME	3

#ifdef ENABLE_FAVORITE_MANAGER
void favorite_log_read( uint8_t u8_index )
{
	static uint32_t flash_log_read[FAVORITE_LOG_SIZE];

	memset(&g_str_favo_log, 0, sizeof(favo_log_struct_t));
	user_flash_read( USER_FLASH_FAVORITE_LOG, flash_log_read, u8_index, (uint32_t)sizeof(flash_log_read));
	
	g_str_favo_log.ui8_index = u8_index;
	
	if( ( flash_log_read[FAVORITE_LOG_COMP] & 0x00ff ) == (uint8_t)('L') )
	{
		g_str_favo_log.ui16_missed_call = ( uint16_t )( ( flash_log_read[FAVORITE_LOG_COMP] >> 16 ) & 0x00FFFF );
		g_str_favo_log.ui16_incom_cnt = ( uint16_t )( flash_log_read[FAVORITE_LOG_CNT]  & 0x00FFFF );
		g_str_favo_log.ui16_outgo_cnt = ( uint16_t )( ( flash_log_read[FAVORITE_LOG_CNT] >> 16 ) & 0x00FFFF );
		
		g_str_favo_log.ui32_incom_time = flash_log_read[FAVORITE_LOG_IN_TIME];
		g_str_favo_log.ui32_outgo_time = flash_log_read[FAVORITE_LOG_OUT_TIME];
	}
}

void favorite_log_write( uint8_t u8_index )
{
	static uint32_t flash_log_data[FAVORITE_LOG_INDEX][FAVORITE_LOG_SIZE];

	int i = 0;

	g_str_favo_log.ui8_index = u8_index;
	// flash read
	for( i = 0; i < FAVORITE_LOG_INDEX; i++ )
	{
		// new data
		if( i == u8_index )
		{
			flash_log_data[i][FAVORITE_LOG_COMP] = 'L';
			flash_log_data[i][FAVORITE_LOG_COMP] |= ( uint32_t )( ( i & 0x00FF ) << 8 );
			flash_log_data[i][FAVORITE_LOG_COMP] |= ( uint32_t )( ( g_str_favo_log.ui16_missed_call & 0x00FFFF ) << 16 );
			
			flash_log_data[i][FAVORITE_LOG_CNT] = ( uint32_t )( g_str_favo_log.ui16_incom_cnt & 0x00FFFF );
			flash_log_data[i][FAVORITE_LOG_CNT] |= ( uint32_t )( ( g_str_favo_log.ui16_outgo_cnt & 0x00FFFF ) << 16 );
			
			flash_log_data[i][FAVORITE_LOG_IN_TIME] = g_str_favo_log.ui32_incom_time;
			flash_log_data[i][FAVORITE_LOG_OUT_TIME] = g_str_favo_log.ui32_outgo_time;
		}
		else
		{
			user_flash_read( USER_FLASH_FAVORITE_LOG, flash_log_data[i], i, (uint32_t)sizeof(flash_log_data[i]));
			
			if( ( flash_log_data[i][FAVORITE_LOG_COMP] & 0x00ff ) != (uint8_t)('L') )
			{
				flash_log_data[i][FAVORITE_LOG_COMP] = 'L';
				flash_log_data[i][FAVORITE_LOG_COMP] |= ( uint32_t )( ( i & 0x00FF ) << 8 );
				flash_log_data[i][FAVORITE_LOG_COMP] &= ( uint32_t )( 0x0000FFFF );
				flash_log_data[i][FAVORITE_LOG_CNT] = 0;
				flash_log_data[i][FAVORITE_LOG_IN_TIME]  = 0;
				flash_log_data[i][FAVORITE_LOG_OUT_TIME]  = 0;
			}
		}
	}
	
	//user_flash_erase( USER_FLASH_FAVORITE_LOG );
	
	user_flash_write(USER_FLASH_FAVORITE_LOG, (uint32_t *)flash_log_data, 0, (uint32_t)sizeof(flash_log_data));
}

void favorite_data_sync( uint8_t u8_index )
{
	if( g_str_bit.b1_favorite_save_ready )
	{
		return;
	}

	uint8_t byte[10] = "";
	uint8_t nus_index = 0;

	favorite_log_read( u8_index );

	byte[++nus_index] = REPORT_FAVORITE_DATA;
	byte[++nus_index] = REP_FAVO_DATA_SYNC;
	byte[++nus_index] = u8_index;
	byte[++nus_index] = g_str_favo_log.ui16_outgo_cnt;
	byte[++nus_index] = g_str_favo_log.ui32_outgo_time / 60;
	byte[++nus_index] = g_str_favo_log.ui16_incom_cnt;
	byte[++nus_index] = g_str_favo_log.ui32_incom_time / 60;
	byte[++nus_index] = g_str_favo_log.ui16_missed_call;
	byte[0] = ++nus_index;
	send_byte_gatt(byte, byte[0]);
}

void favorite_log_process( void )
{
	if( g_str_call_bit.b4_call_status & CALL_STATUS_FAVORITE )
	{
		if( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING )
		{
			//if( g_str_favo_mgr.ui8_log_enable )
			{
				//favo_log_timer_stop();

				favorite_log_write( g_str_favo_log.ui8_index );
			}
		}
	}
}

void favorite_log_delete( uint8_t u8_index )
{
	static uint32_t flash_log_data[FAVORITE_LOG_INDEX][FAVORITE_LOG_SIZE];

	user_flash_read(USER_FLASH_FAVORITE_LOG, (uint32_t *)flash_log_data, 0, (uint32_t)sizeof(flash_log_data));

	flash_log_data[u8_index][FAVORITE_LOG_COMP] = 'L';
	flash_log_data[u8_index][FAVORITE_LOG_COMP] |= ( uint32_t )( ( u8_index & 0x00FF ) << 8 );
	flash_log_data[u8_index][FAVORITE_LOG_COMP] &= ( uint32_t )( 0x0000FFFF );
	flash_log_data[u8_index][FAVORITE_LOG_CNT] = 0;
	flash_log_data[u8_index][FAVORITE_LOG_IN_TIME]  = 0;
	flash_log_data[u8_index][FAVORITE_LOG_OUT_TIME]  = 0;

	//user_flash_erase( USER_FLASH_FAVORITE_LOG );
	
	user_flash_write(USER_FLASH_FAVORITE_LOG, (uint32_t *)flash_log_data, 0, (uint32_t)sizeof(flash_log_data));
}

void favorite_log_init( void )
{
	static uint32_t flash_log_data[FAVORITE_LOG_INDEX][FAVORITE_LOG_SIZE];
	int i = 0;

	// flash read
	for( i = 0; i < FAVORITE_LOG_INDEX; i++ )
	{
		flash_log_data[i][FAVORITE_LOG_COMP] = 'L';
		flash_log_data[i][FAVORITE_LOG_COMP] |= ( uint32_t )( ( i & 0x00FF ) << 8 );
		flash_log_data[i][FAVORITE_LOG_COMP] &= ( uint32_t )( 0x0000FFFF );
		flash_log_data[i][FAVORITE_LOG_CNT] = 0;
		flash_log_data[i][FAVORITE_LOG_IN_TIME]  = 0;
		flash_log_data[i][FAVORITE_LOG_OUT_TIME]  = 0;
	}
	
	//user_flash_erase( USER_FLASH_FAVORITE_LOG );
	
	user_flash_write(USER_FLASH_FAVORITE_LOG, (uint32_t *)flash_log_data, 0, (uint32_t)sizeof(flash_log_data));

}

#endif //ENABLE_FAVO_MANAGER

