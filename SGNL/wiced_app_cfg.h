#ifndef _WICED_APP_CFG_H_
#define _WICED_APP_CFG_H_

#include "wiced_bt_cfg.h"

extern wiced_bt_cfg_settings_t wiced_bt_cfg_settings;
extern const wiced_bt_cfg_buf_pool_t wiced_bt_cfg_buf_pools[WICED_BT_CFG_NUM_BUF_POOLS];

#endif
