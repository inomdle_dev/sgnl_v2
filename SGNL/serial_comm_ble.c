/*
 * send_byte.c
 *
 *  Created on: 2017. 8. 31.
 *      Author: sheum
 */

#include "wiced_bt_gatt.h"
#include "serial_gatt_service.h"
#include "serial_comm_ble.h"
#include "wiced_bt_trace.h"

uint16_t g_ui16_conn_id = 0;

#if 0 
void send_byte_complete(uint16_t con_handle, uint8_t result)
{
    uint8_t   tx_buf [30];
    uint8_t   *p = tx_buf;

    *p++ = con_handle & 0xff;
    *p++ = ( con_handle >> 8 ) & 0xff;
    *p++ = result;

    wiced_transport_send_data ( HCI_CONTROL_GATT_EVENT_WRITE_RESPONSE, tx_buf, ( int )( p - tx_buf ) );
}
#endif

void send_byte_gatt( uint8_t * ui8_param, uint8_t ui8_param_len )
{
    wiced_bt_gatt_status_t status;

    if ( ( status = wiced_bt_gatt_send_notification( g_ui16_conn_id , (uint16_t)HDLC_SERIAL_GATT_SERIAL_DATA_VALUE, (uint16_t)ui8_param_len, ui8_param ) ) != WICED_BT_GATT_SUCCESS )
    {
        //WICED_BT_TRACE( "Failed to send:%x\n", status );
    }
#if 0
    else
    {
        send_byte_complete( g_ui16_conn_id, status );
    }
#endif

}

void send_byte_gatt_connid( uint16_t conn_id, uint8_t * ui8_param, uint8_t ui8_param_len )
{
    wiced_bt_gatt_status_t status;

    if ( ( status = wiced_bt_gatt_send_notification( conn_id, (uint16_t)HDLC_SERIAL_GATT_SERIAL_DATA_VALUE, (uint16_t)ui8_param_len, ui8_param ) ) != WICED_BT_GATT_SUCCESS )
    {
        WICED_BT_TRACE( "Failed to send:%x\n", status );
    }
    else
    {
    	WICED_BT_TRACE("Success:%x (%d)\n", status, conn_id);
    }
#if 0
    else
    {
        send_byte_complete( g_ui16_conn_id, status );
    }
#endif

}

