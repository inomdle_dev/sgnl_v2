#include "stdint.h"
#include "wiced_bt_trace.h"
#include "sgnl_gpio_def.h"

#include "global_define.h"

void Delay_us(uint16_t usec)
{
	//return;
	if( usec > 1000 )
	{
		usec = 1000;
	}
	int i = 0;
	for( i = 0; i < usec; i++)
	{
		g_str_timer.ui32_delay_cnt = 4;
		while( g_str_timer.ui32_delay_cnt-- );
	}
}

void i2c_init(void)
{
	wiced_hal_gpio_configure_pin( SGNL_GPIO_I2C_SCL, SGNL_GPIO_OUTPUT_SETTINGS, GPIO_PIN_OUTPUT_HIGH );
	wiced_hal_gpio_configure_pin( SGNL_GPIO_I2C_SDA, SGNL_GPIO_OUTPUT_SETTINGS, GPIO_PIN_OUTPUT_HIGH );

	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SCL, 0 );
	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 0 );

	Delay_us(1000);

	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SCL, 1 );
	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 1 );

	Delay_us(1000);
}

void i2c_clock(void)
{
	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SCL, 1 );
	//Delay_us(1);
	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SCL, 0 );
	//Delay_us(1);
}
 
void i2c_start(void)
{
    //start
//	wiced_hal_gpio_configure_pin( SGNL_GPIO_I2C_SCL, SGNL_GPIO_OUTPUT_SETTINGS, GPIO_PIN_OUTPUT_HIGH );
//	wiced_hal_gpio_configure_pin( SGNL_GPIO_I2C_SDA, SGNL_GPIO_OUTPUT_SETTINGS, GPIO_PIN_OUTPUT_HIGH );

	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 1 );
    wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SCL, 1 );

	Delay_us(5);
 
	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 0 );
    
	Delay_us(5);

	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SCL, 0 );
	Delay_us(5);
}
 
void i2c_stop(void)
{
	wiced_hal_gpio_configure_pin( SGNL_GPIO_I2C_SDA, SGNL_GPIO_OUTPUT_SETTINGS, GPIO_PIN_OUTPUT_LOW );

	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 0 );
	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SCL, 0 );
	Delay_us(5);
 

	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SCL, 1 );
	Delay_us(5);

 	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 1 );

#if 1// GPIO OUTPUT HIGH
    wiced_hal_gpio_configure_pin( SGNL_GPIO_I2C_SDA, SGNL_GPIO_OUTPUT_SETTINGS, GPIO_PIN_OUTPUT_HIGH );
    wiced_hal_gpio_configure_pin( SGNL_GPIO_I2C_SCL, SGNL_GPIO_OUTPUT_SETTINGS, GPIO_PIN_OUTPUT_HIGH );

#else
	wiced_hal_gpio_configure_pin( SGNL_GPIO_I2C_SDA, SGNL_GPIO_BUTTON_SETTINGS, GPIO_PIN_OUTPUT_LOW );
#endif

	Delay_us(5);
}
 
void write_i2c_byte(unsigned char byte)
{
	unsigned char i = 0;
 
	for (i = 0; i < 8 ; i++)
	{
		if((byte & (0x80>>i)))
		{
	 		wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 1 );
	       }
		else
		{
		 	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 0 );
		}
		i2c_clock();
	}

	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 0 );

	i2c_clock();

	Delay_us(5);
}


void read_addr_stretch(unsigned char byte)
{
	unsigned char i = 0;
 
	for (i = 0; i < 8 ; i++)
	{
		if((byte & (0x80>>i)))
		{
	 		wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 1 );
	       }
		else
		{
		 	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 0 );
		}
		i2c_clock();
	}

	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 0 );

    Delay_us(500);

	i2c_clock();

	Delay_us(5);
}

 
unsigned char read_i2c_byte(unsigned char ch)
{
	unsigned char i, buff=0;

	wiced_hal_gpio_configure_pin( SGNL_GPIO_I2C_SDA, SGNL_GPIO_INPUT_PULL_UP, GPIO_PIN_OUTPUT_LOW );
 
	for(i=0; i<8; i++)
	{
		//Delay_us(1);

		wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SCL, 1 );

		//Delay_us(1);

		buff <<= 1;
 
		// Read data on SDA pin
		if( wiced_hal_gpio_get_pin_input_status(SGNL_GPIO_I2C_SDA) )
		{
			buff |= 0x01;
		}

		wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SCL, 0 );

		//Delay_us(1);
	}

	wiced_hal_gpio_configure_pin( SGNL_GPIO_I2C_SDA, SGNL_GPIO_OUTPUT_SETTINGS, GPIO_PIN_OUTPUT_LOW );
	
	if(ch == 0) //ACK
	{
		wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 1 );
	}
	else //NACK.
	{
		wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 0 );
	}
	i2c_clock();

	wiced_hal_gpio_set_pin_output( SGNL_GPIO_I2C_SDA, 1 );

	Delay_us(2);


	return buff;
}
 
uint8_t read_byte(uint8_t slave, uint8_t page_address)
{
	uint8_t ui8_byte= 0;

	ui8_byte = 0;

	i2c_start();
	write_i2c_byte(slave);
	write_i2c_byte(page_address);

	i2c_start();
	write_i2c_byte(slave+1);

	ui8_byte = read_i2c_byte(1);
	 
	i2c_stop();

	return ui8_byte;
}

void i2c_read_multi_byte(uint8_t addr, uint8_t page_address, uint8_t *bytes, uint8_t length)
{
	while( g_str_bit.b1_i2c_send_wait );

	g_str_bit.b1_i2c_send_wait = 1;

	i2c_start();
	write_i2c_byte(addr);
	write_i2c_byte(page_address);

	i2c_start();

#ifdef ENABLE_FUEL_GAUGE
    if( addr == FUEL_GAUGE_ADDRESS )
    {
    	read_addr_stretch(addr+1);
    }
    else
    {
        write_i2c_byte(addr+1);
    }
#else
	write_i2c_byte(addr+1);
#endif

	int i = 0;
	for( i = 0; i < length-1; i++ )
	{
		bytes[i] = read_i2c_byte(1);
	}
	bytes[i] = read_i2c_byte(0);
	 
	i2c_stop();

	g_str_bit.b1_i2c_send_wait = 0;
}
 
void write_byte(uint8_t addr, uint8_t page_address, uint8_t value)
{
	i2c_start();
	write_i2c_byte(addr);
	write_i2c_byte(page_address);
	write_i2c_byte(value);
	i2c_stop();
}

void i2c_write_multi_byte(uint8_t addr, uint8_t page_address, uint8_t *bytes, uint8_t length)
{
	while( g_str_bit.b1_i2c_send_wait );

	g_str_bit.b1_i2c_send_wait = 1;

	i2c_start();
	write_i2c_byte(addr);
	write_i2c_byte(page_address);

	int i = 0;
	for( i = 0; i < length; i++ )
	{
		write_i2c_byte(bytes[i]);
	}
	i2c_stop();

	g_str_bit.b1_i2c_send_wait = 0;
}

