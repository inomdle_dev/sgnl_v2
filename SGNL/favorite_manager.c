/**
 * File name : favorite_manager.c
 *
 * This file contains the source code for managing favorite call data
 */

#include "global_define.h"

#include "wiced_bt_trace.h"

#define FAVORITE_DATA_INDEX     5
#define FAVORITE_DATA_SIZE		20

#define FAVORITE_COMP			0
#define FAVORITE_INDEX			1
#define FAVORITE_FLAG			2
#define FAVORITE_TARGET		3
#define FAVORITE_NUMBER		4
#define FAVORITE_NAME			12

#ifdef ENABLE_FAVORITE_MANAGER

enum {
	FAVORITE_PARSE_DELETE	= 0,
	FAVORITE_PARSE_NUMBER	= 1,
	FAVORITE_PARSE_NAME		= 2,
	FAVORITE_PARSE_STATUS	= 3,
};

uint16_t favo_mgr_list[5] = {
	USER_FLASH_FAVORITE_0,
	USER_FLASH_FAVORITE_1,
	USER_FLASH_FAVORITE_2,
	USER_FLASH_FAVORITE_3,
	USER_FLASH_FAVORITE_4
};


void favorite_data_delete( uint8_t u8_index )
{
	static uint32_t flash_favorite_data[FAVORITE_DATA_SIZE];

	user_flash_erase( favo_mgr_list[u8_index] );

	favorite_log_delete(u8_index);
}


void favorite_data_write( uint8_t u8_index )
{
	static uint32_t flash_favorite_data[FAVORITE_DATA_SIZE];

	user_flash_read(favo_mgr_list[u8_index], flash_favorite_data, 0, (uint32_t)sizeof(flash_favorite_data));

	if( ( strncmp((char *)(&flash_favorite_data[FAVORITE_NUMBER]), (char *)(g_str_favo_mgr.ui8_number), g_str_favo_mgr.ui8_num_len) )
	|| ( strncmp((char *)(&flash_favorite_data[FAVORITE_NAME]), (char *)(g_str_favo_mgr.ui8_name), g_str_favo_mgr.ui8_name_len) ) )
	{
		memset(&g_str_favo_log, 0, sizeof(favo_log_struct_t));
		favorite_log_write(u8_index);
	}

	strncpy((char *)(&flash_favorite_data[FAVORITE_COMP]), "fav", 3);
	flash_favorite_data[FAVORITE_INDEX] = ( uint32_t )( g_str_favo_mgr.ui8_index & 0x00FF );
	strncpy((char *)(&flash_favorite_data[FAVORITE_NUMBER]), (char *)(g_str_favo_mgr.ui8_number), g_str_favo_mgr.ui8_num_len);

	int j =0;
	uint8_t *tmp_data = (uint8_t *)(&flash_favorite_data[FAVORITE_NAME]);
	for( j = 0; j < g_str_favo_mgr.ui8_name_len; j++ )
	{
		tmp_data[j] = g_str_favo_mgr.ui8_name[j];
	}

	flash_favorite_data[FAVORITE_FLAG] = ( uint32_t )( ( g_str_favo_mgr.ui8_log_type & 0x00FF ) << 24 );
	flash_favorite_data[FAVORITE_FLAG] |= ( uint32_t )( ( g_str_favo_mgr.ui8_log_enable & 0x00FF ) << 16 );
	flash_favorite_data[FAVORITE_FLAG] |= ( uint32_t )( ( g_str_favo_mgr.ui8_num_len & 0x00FF ) << 8 );
	flash_favorite_data[FAVORITE_FLAG] |= ( uint32_t )( g_str_favo_mgr.ui8_name_len & 0x00FF );
	flash_favorite_data[FAVORITE_TARGET] = ( uint32_t )( g_str_favo_mgr.ui16_target_cnt << 16 );
	flash_favorite_data[FAVORITE_TARGET] |= ( uint32_t )( g_str_favo_mgr.ui16_target_time & 0x00FFFF );

	WICED_BT_TRACE("str : 0x%x, flash : 0x%x\n", g_str_favo_mgr.ui8_num_len, flash_favorite_data[FAVORITE_FLAG]);

	//user_flash_erase( USER_FLASH_FAVORITE );

	user_flash_write(favo_mgr_list[u8_index], flash_favorite_data, 0, (uint32_t)sizeof(flash_favorite_data));

	WICED_BT_TRACE("str : 0x%x, flash : 0x%x\n", g_str_favo_mgr.ui8_num_len, flash_favorite_data[FAVORITE_FLAG]);

	//favorite_log_write(u8_index);
}


void favorite_data_parser_group( uint8_t *u8_data, uint8_t u8_data_len )
{
	uint8_t u8_data_type = u8_data[0];

	switch( u8_data_type )
	{
		case REP_FAVO_SEND_FIRST_NUM:
			strncpy((char *)(g_str_favo_mgr.ui8_number), (char *)(&u8_data[1]), u8_data_len);
			g_str_favo_mgr.ui8_num_len = u8_data_len;
			break;
		case REP_FAVO_SEND_ADD_NUM:
			if( g_str_favo_mgr.ui8_num_len > 16 )
			{
				g_str_favo_mgr.ui8_num_len = 16;
			}

			strncpy((char *)(&g_str_favo_mgr.ui8_number[g_str_favo_mgr.ui8_num_len]), (char *)(&u8_data[1]), u8_data_len);
			g_str_favo_mgr.ui8_num_len += u8_data_len;

			if( g_str_favo_mgr.ui8_num_len > 32 )
			{
				g_str_favo_mgr.ui8_num_len = 32;
			}
			break;

		case REP_FAVO_SEND_FIRST_NAME:
			{
				//strncpy((char *)(g_str_favo_mgr.ui8_name), (char *)(&u8_data[1]), u8_data_len);
				uint8_t *tmp_data = &u8_data[1];
				int j =0;
				for( j = 0; j < u8_data_len; j++ )
				{
					g_str_favo_mgr.ui8_name[j] = tmp_data[j];
				}

				g_str_favo_mgr.ui8_name_len = u8_data_len;
				break;
			}
		case REP_FAVO_SEND_ADD_NAME:
			{
				//strncpy((char *)(&g_str_favo_mgr.ui8_name[g_str_favo_mgr.ui8_name_len]), (char *)(&u8_data[1]), u8_data_len);
				uint8_t *tmp_data = &u8_data[1];

				if( g_str_favo_mgr.ui8_name_len > 16 )
				{
					g_str_favo_mgr.ui8_name_len = 16;
				}

				int j =0;
				for( j = 0; j < u8_data_len; j++ )
				{
					g_str_favo_mgr.ui8_name[j+g_str_favo_mgr.ui8_name_len] = tmp_data[j];
				}

				g_str_favo_mgr.ui8_name_len += u8_data_len;

				if( g_str_favo_mgr.ui8_name_len > 32 )
				{
					g_str_favo_mgr.ui8_name_len = 32;
				}
				break;
			}
		case REP_FAVO_SEND_TARGET_DATA :
			if( u8_data[1] > 9 )
			{
				g_str_favo_mgr.ui8_index = u8_data[1] - '0';
			}
			else
			{
				g_str_favo_mgr.ui8_index = u8_data[1];
			}
			g_str_favo_mgr.ui8_log_enable = u8_data[2] - '0';
			g_str_favo_mgr.ui8_log_type = u8_data[3] - '0';

			if( g_str_favo_mgr.ui8_log_type == 1 )
			{
				g_str_favo_mgr.ui16_target_cnt = u8_data[4];
			}
			else
			{
				g_str_favo_mgr.ui16_target_time = u8_data[4] * 60; // second = minute * 60
			}
			favorite_data_write( g_str_favo_mgr.ui8_index );

			g_str_bit.b1_favorite_save_ready = 0;

			break;
		default : break;
	}

	uint8_t byte[5] = "";
	uint8_t nus_index = 0;
	byte[++nus_index] = REPORT_FAVORITE_DATA;
	byte[++nus_index] = REP_FAVO_DATA_SEND;
	byte[++nus_index] = u8_data_type;
	byte[++nus_index] = 0x5A;


	byte[0] = ++nus_index;

	send_byte_gatt(byte, byte[0]);
}

void favorite_read_number( uint8_t u8_index, uint8_t up_down )
{
	static uint32_t flash_favorite_read[FAVORITE_DATA_SIZE];

	memset(&g_str_favo_mgr, 0, sizeof(favo_mgr_struct_t));

	uint8_t u8_chk_num = 0;
	uint8_t u8_find_num = u8_index;
	int cnt = 0;

	for( cnt = 0; cnt < 5; cnt ++ )
	{
		if( up_down ) //up 1 down 0
		{
			u8_find_num = ( u8_index + cnt ) % 5;
		}
		else
		{
			u8_find_num = ( u8_index + 5 - cnt ) % 5;
		}

		user_flash_read( favo_mgr_list[u8_find_num], flash_favorite_read, 0, (uint32_t)sizeof(flash_favorite_read));

		if( !strncmp((char *)flash_favorite_read, "fav", 3) )
		{
			if( !u8_chk_num )
			{
				g_str_favo_mgr.ui8_saved_index[u8_find_num] = 2;
				g_str_favo_mgr.ui8_index = u8_find_num;

				g_str_favo_mgr.ui8_name_len = ( uint8_t )( flash_favorite_read[FAVORITE_FLAG] & 0x0000FF );
				g_str_favo_mgr.ui8_num_len = ( uint8_t )( ( flash_favorite_read[FAVORITE_FLAG] & 0x0000FF00 ) >> 8 );

				strncpy((char *)(g_str_favo_mgr.ui8_number), (char *)(&flash_favorite_read[FAVORITE_NUMBER]), g_str_favo_mgr.ui8_num_len);

				favorite_log_read( g_str_favo_mgr.ui8_index );

				u8_chk_num = 1;
			}
			else
			{
				g_str_favo_mgr.ui8_saved_index[u8_find_num] = 1;
			}

			//u8_find_num = 5;
			//break;
		}
	}

	if( !u8_chk_num )
	{
		g_str_favo_mgr.ui8_num_len = 0;
		return;
	}

#if 0
	g_str_favo_mgr.ui8_name_len = ( uint8_t )( flash_favorite_read[FAVORITE_FLAG] & 0x0000FF );
	g_str_favo_mgr.ui8_num_len = ( uint8_t )( ( flash_favorite_read[FAVORITE_FLAG] & 0x0000FF00 ) >> 8 );
	g_str_favo_mgr.ui8_log_enable = ( uint8_t )( ( flash_favorite_read[FAVORITE_FLAG] & 0x00FF0000 ) >> 16 );
	g_str_favo_mgr.ui8_log_type = ( uint8_t )( ( flash_favorite_read[FAVORITE_FLAG] & 0xFF000000 ) >> 24 );
	g_str_favo_mgr.ui16_target_cnt = ( uint16_t )( ( flash_favorite_read[FAVORITE_TARGET] >> 16 ) & 0x00FFFF );
	g_str_favo_mgr.ui16_target_time = ( uint16_t )( flash_favorite_read[FAVORITE_TARGET] & 0x00FFFF );

	strncpy((char *)(g_str_favo_mgr.ui8_number), (char *)(&flash_favorite_read[FAVORITE_NUMBER]), g_str_favo_mgr.ui8_num_len);
	//strncpy((char *)(g_str_favo_mgr.ui8_name), (char *)(&flash_favorite_read[FAVORITE_NAME]), g_str_favo_mgr.ui8_name_len);
	int j =0;
	uint8_t *tmp_data = (uint8_t *)(&flash_favorite_read[FAVORITE_NAME]);

	for( j = 0; j < g_str_favo_mgr.ui8_name_len; j++ )
	{
		g_str_favo_mgr.ui8_name[j] = tmp_data[j];
	}

	WICED_BT_TRACE("read_fav, num_len %d \n", g_str_favo_mgr.ui8_num_len);
	favorite_log_read( g_str_favo_mgr.ui8_index );
#endif
}

void favorite_read_call_number( uint8_t *call_num )
{
	uint32_t flash_fav_read[FAVORITE_DATA_SIZE];
	uint8_t u8_find_num = 0;
	uint8_t cnt = 0;

	memset(&g_str_favo_mgr, 0, sizeof(favo_mgr_struct_t));
	for( cnt = 0; cnt < 5; cnt ++ )
	{
		u8_find_num = cnt;
		user_flash_read( favo_mgr_list[u8_find_num], flash_fav_read, 0, (uint32_t)sizeof(flash_fav_read));

		if( !strncmp((char *)flash_fav_read, "fav", 3) )
		{
			g_str_favo_mgr.ui8_index = u8_find_num;

			g_str_favo_mgr.ui8_name_len = ( uint8_t )( flash_fav_read[FAVORITE_FLAG] & 0x0000FF );
			g_str_favo_mgr.ui8_num_len = ( uint8_t )( ( flash_fav_read[FAVORITE_FLAG] & 0x0000FF00 ) >> 8 );

			strncpy((char *)(g_str_favo_mgr.ui8_number), (char *)(&flash_fav_read[FAVORITE_NUMBER]), g_str_favo_mgr.ui8_num_len);
			if( ( g_str_favo_mgr.ui8_num_len ) && ( !strncmp(call_num, g_str_favo_mgr.ui8_number, g_str_favo_mgr.ui8_num_len) ) )
			{
				favorite_log_read( g_str_favo_mgr.ui8_index );
				return;
			}
		}
	}

	g_str_favo_mgr.ui8_num_len = 0;

#if 0
	g_str_favo_mgr.ui8_name_len = ( uint8_t )( flash_favorite_read[FAVORITE_FLAG] & 0x0000FF );
	g_str_favo_mgr.ui8_num_len = ( uint8_t )( ( flash_favorite_read[FAVORITE_FLAG] & 0x0000FF00 ) >> 8 );
	g_str_favo_mgr.ui8_log_enable = ( uint8_t )( ( flash_favorite_read[FAVORITE_FLAG] & 0x00FF0000 ) >> 16 );
	g_str_favo_mgr.ui8_log_type = ( uint8_t )( ( flash_favorite_read[FAVORITE_FLAG] & 0xFF000000 ) >> 24 );
	g_str_favo_mgr.ui16_target_cnt = ( uint16_t )( ( flash_favorite_read[FAVORITE_TARGET] >> 16 ) & 0x00FFFF );
	g_str_favo_mgr.ui16_target_time = ( uint16_t )( flash_favorite_read[FAVORITE_TARGET] & 0x00FFFF );

	strncpy((char *)(g_str_favo_mgr.ui8_number), (char *)(&flash_favorite_read[FAVORITE_NUMBER]), g_str_favo_mgr.ui8_num_len);
	//strncpy((char *)(g_str_favo_mgr.ui8_name), (char *)(&flash_favorite_read[FAVORITE_NAME]), g_str_favo_mgr.ui8_name_len);
	int j =0;
	uint8_t *tmp_data = (uint8_t *)(&flash_favorite_read[FAVORITE_NAME]);

	for( j = 0; j < g_str_favo_mgr.ui8_name_len; j++ )
	{
		g_str_favo_mgr.ui8_name[j] = tmp_data[j];
	}

	WICED_BT_TRACE("read_fav, num_len %d \n", g_str_favo_mgr.ui8_num_len);
	favorite_log_read( g_str_favo_mgr.ui8_index );
#endif
}

uint8_t favorite_name_find( uint8_t *u8_data, uint8_t u8_size )
{
	static uint32_t flash_favorite_read[FAVORITE_DATA_SIZE];
	int i = 0;
	// flash read
	for( i = 0; i < FAVORITE_DATA_INDEX; i++ )
	{
		memset(&g_str_favo_mgr, 0, sizeof(favo_mgr_struct_t));

		user_flash_read( favo_mgr_list[i], flash_favorite_read, 0, (uint32_t)sizeof(flash_favorite_read));

		//strncpy((char *)(g_str_favo_mgr.ui8_name), (char *)(&flash_favorite_read[FAVORITE_NAME]), g_str_favo_mgr.ui8_name_len);
		int j =0;
		uint8_t *tmp_data = (uint8_t *)(&flash_favorite_read[FAVORITE_NAME]);

		for( j = 0; j < u8_size; j++ )
		{
			if( tmp_data[j] != u8_data[j] )
			{
				j = 0;
				break;
			}
		}		

		if( j == u8_size )
		{
			g_str_favo_mgr.ui8_index = i;

			favorite_log_read(i);

			return 1;
		}
	}
	return 0;
}


#endif //ENABLE_FAVORITE_MANAGER

