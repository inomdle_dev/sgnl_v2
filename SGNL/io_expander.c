#include "io_expander.h"
#include "wiced_bt_trace.h"

void io_expander_init(void)
{
    uint8_t data;
    data = ( read_byte(PCA9537_I2C_ADDR, PCA9537_CONFIG_REG) & 0xF );
    WICED_BT_TRACE("read input : %x \n", data);

    data = 0xFF;

    write_byte(PCA9537_I2C_ADDR, PCA9537_CONFIG_REG, data);

    data = ( read_byte(PCA9537_I2C_ADDR, PCA9537_CONFIG_REG) & 0xF );
    WICED_BT_TRACE("read input2 : %x \n", data);
}

void io_expander_reset_int(void)
{
	read_byte(PCA9537_I2C_ADDR, PCA9537_IN_PORT_REG);
}


