
#include "sparcommon.h"
#include "wiced_hal_puart.h"
#include "wiced_bt_trace.h"
#include "sgnl_gpio_def.h"
#include "uart.h"
#include "global_define.h"

#define PUART_BUADRATE	(115200)

void paurt_rx_interrupt_callback(void* args)
{
	//if(!g_str_bit.b1_ota_fw_upgrade)
	{
		uint8_t readbyte;
		wiced_hal_puart_read(&readbyte);
		//WICED_BT_TRACE("readbyte:0x%x\n",readbyte);
		if( readbyte == UART_STX )
	        	memset(&uart_buffer, 0, sizeof(uart_buffer));

		uart_buffer.buffer[uart_buffer.tail++] = readbyte;
		if ( uart_buffer.tail >= UART_BUFF_SIZE )
			uart_buffer.tail = 0;

		if ( readbyte == UART_ETX )
			puart_data_proc();

	}
	wiced_hal_puart_reset_puart_interrupt();
}

void init_puart_port(void)
{
	memset(&uart_buffer, 0, sizeof(uart_buffer));
	wiced_hal_puart_init();
	wiced_hal_puart_select_uart_pads( SGNL_PUART_RXD, SGNL_PUART_TXD, 0, 0);
	wiced_hal_puart_set_baudrate(PUART_BUADRATE);
	wiced_hal_puart_flow_off();
	wiced_hal_puart_register_interrupt(paurt_rx_interrupt_callback);
	wiced_hal_puart_enable_tx();
	wiced_hal_puart_enable_rx();
}

void puart_write_byte(uint8_t byte)
{
//////////////////////////////////////////////////////////////////////////////////////////////////
	wiced_hal_gpio_configure_pin( WICED_GPIO_04, GPIO_INPUT_DISABLE, WICED_GPIO_BUTTON_DEFAULT_STATE );
//////////////////////////////////////////////////////////////////////////////////////////////////
	wiced_hal_puart_write(byte);
//	WICED_BT_TRACE("writebyte:0x%x\n",byte);
}

void puart_write_buffer(uint8_t* p_data, uint32_t len)
{
#if 1
	uint32_t i = 0;
	for (i = 0; i < len; ++i)
	{
//////////////////////////////////////////////////////////////////////////////////////////////////
		wiced_hal_gpio_configure_pin( WICED_GPIO_04, GPIO_INPUT_DISABLE, WICED_GPIO_BUTTON_DEFAULT_STATE );
//////////////////////////////////////////////////////////////////////////////////////////////////
		puart_write_byte(p_data[i]);
	}
#else
//////////////////////////////////////////////////////////////////////////////////////////////////
	wiced_hal_gpio_configure_pin( WICED_GPIO_04, GPIO_INPUT_DISABLE, WICED_GPIO_BUTTON_DEFAULT_STATE );
//////////////////////////////////////////////////////////////////////////////////////////////////
	wiced_hal_puart_synchronous_write(p_data, len);
#endif
}

uint8_t get_puart_protocol_data(uint8_t* p_data)
{
	uint8_t len;
	uint8_t found_stx, found_etx, i;
	uint16_t point, tail;
	
	len = i = found_stx = found_etx = 0;

	point = uart_buffer.head;
	tail = uart_buffer.tail;

	if ( point == tail )
		return len;

	while ( WICED_TRUE )
	{
		if ( len > 0 )
		{
			uart_buffer.head = point;
			break;
		}
		if ( point == tail )
			break;

		if ( uart_buffer.buffer[point] == UART_STX )
			found_stx = WICED_TRUE;
		else if ( uart_buffer.buffer[point] == UART_ETX )
			found_etx = WICED_TRUE;
		else if ( found_stx )
			p_data[i++] = uart_buffer.buffer[point];
		
		if ( found_stx && found_etx )
			len = i;
		
		if ( ++point >= UART_BUFF_SIZE ) point = 0;
	}
	return len;
}


