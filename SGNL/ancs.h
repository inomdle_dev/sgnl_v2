#ifndef _ANCS_H_
#define _ANCS_H_

/******************************************************
 *                      Constants
 ******************************************************/
// 7905F431-B5CE-4E99-A40F-4B1E122D00D0
const char ANCS_SERVICE[]			  = {0xD0, 0x00, 0x2D, 0x12, 0x1E, 0x4B, 0x0F, 0xA4, 0x99, 0x4E, 0xCE, 0xB5, 0x31, 0xF4, 0x05, 0x79};
	
// Notification Source: UUID 9FBF120D-6301-42D9-8C58-25E699A21DBD (notifiable)
const char ANCS_NOTIFICATION_SOURCE[] = {0xBD, 0x1D, 0xA2, 0x99, 0xE6, 0x25, 0x58, 0x8C, 0xD9, 0x42, 0x01, 0x63, 0x0D, 0x12, 0xBF, 0x9F};
	
// Control Point: UUID 69D1D8F3-45E1-49A8-9821-9BBDFDAAD9D9 (writeable with response)
const char ANCS_CONTROL_POINT[] 	  = {0xD9, 0xD9, 0xAA, 0xFD, 0xBD, 0x9B, 0x21, 0x98, 0xA8, 0x49, 0xE1, 0x45, 0xF3, 0xD8, 0xD1, 0x69};
	
// Data Source: UUID 22EAC6E9-24D6-4BB5-BE44-B36ACE7C7BFB (notifiable)
const char ANCS_DATA_SOURCE[]		  = {0xFB, 0x7B, 0x7C, 0xCE, 0x6A, 0xB3, 0x44, 0xBE, 0xB5, 0x4B, 0xD6, 0x24, 0xE9, 0xC6, 0xEA, 0x22};
	
// max notifications to queue
#define ANCS_MAX_QUEUED_NOTIFICATIONS                   20
	
#define ANCS_EVENT_ID_NOTIFICATION_ADDED                0
#define ANCS_EVENT_ID_NOTIFICATION_MODIFIED             1
#define ANCS_EVENT_ID_NOTIFICATION_REMOVED              2
#define ANCS_EVENT_ID_MAX                               3


#if 0
#define ANCS_COMMAND_ID_GET_NOTIFICATION_ATTRIBUTES     0
#define ANCS_COMMAND_ID_GET_APP_ATTRIBUTES              1
#define ANCS_COMMAND_ID_PERFORM_NOTIFICATION_ACTION     2
#endif

// Definitions for attributes we are not interested in are commented out
#define ANCS_NOTIFICATION_ATTR_ID_APP_ID                0
#define ANCS_NOTIFICATION_ATTR_ID_TITLE                 1
//#define ANCS_NOTIFICATION_ATTR_ID_SUBTITLE              2
//#define ANCS_NOTIFICATION_ATTR_ID_MESSAGE               3
//#define ANCS_NOTIFICATION_ATTR_ID_MESSAGE_SIZE          4
//#define ANCS_NOTIFICATION_ATTR_ID_DATE                  5
//#define ANCS_NOTIFICATION_ATTR_ID_POSITIVE_ACTION_LABEL 6
//#define ANCS_NOTIFICATION_ATTR_ID_NEGATIVE_ACTION_LABEL 7
#define ANCS_NOTIFICATION_ATTR_ID_MAX                   8

#define ANCS_EVENT_FLAG_SILENT                          (1 << 0)
#define ANCS_EVENT_FLAG_IMPORTANT                       (1 << 1)
#define ANCS_EVENT_FLAG_PREEXISTING                     (1 << 2)
#define ANCS_EVENT_FLAG_POSITIVE_ACTION                 (1 << 3)
#define ANCS_EVENT_FLAG_NEGATIVE_ACTION                 (1 << 4)

// following is the list of notification attributes that we are going
// to request.  Compile out attribute of no interest
uint8_t  ancs_client_notification_attribute[] =
{
    ANCS_NOTIFICATION_ATTR_ID_APP_ID,
    ANCS_NOTIFICATION_ATTR_ID_TITLE,
//    ANCS_NOTIFICATION_ATTR_ID_SUBTITLE,
//    ANCS_NOTIFICATION_ATTR_ID_MESSAGE_SIZE,
//    ANCS_NOTIFICATION_ATTR_ID_MESSAGE,
//    ANCS_NOTIFICATION_ATTR_ID_DATE,
//    ANCS_NOTIFICATION_ATTR_ID_POSITIVE_ACTION_LABEL,
//    ANCS_NOTIFICATION_ATTR_ID_NEGATIVE_ACTION_LABEL,
    0
};
// Maximum length we are going to request.  The values are valid for
// title subtitle and message.  The number of elements should match number
// of elements in the ancs_client_notification_attribute above
uint8_t  ancs_client_notification_attribute_length[] =
{
    32,
    20,
//    20,
//    0,
//    255,
//    0,
//    0,
//    0,
    0
};

// ANCS event description
typedef struct
{
    void        *p_next;
    uint32_t    notification_uid;
    uint8_t     command;
    uint8_t     flags;
    uint8_t     category;
    uint8_t     identifier[32];
    uint8_t     title[20];
//    uint8_t     sub_title[20];
//    uint8_t     message[255];
//    uint8_t     positive_action_label[10];
//    uint8_t     negative_action_label[10];
} ancs_event_t;

// ANCS queued event description. Notification is queued while
// we are busy retrieving data from the current notification.
typedef struct
{
    void        *p_next;
    uint32_t    notification_uid;
    uint8_t     command;
    uint8_t     flags;
    uint8_t     category;
} ancs_queued_event_t;


typedef struct
{
    uint16_t                    conn_id;

#define ANCS_DISCOVERY_STATE_SERVICE    0
#define ANCS_DISCOVERY_STATE_ANCS       1
    uint8_t                     discovery_state;

    uint8_t                     started;

    // Current value of the client configuration descriptor for characteristic 'Report'
    uint16_t                    ancs_s_handle;
    uint16_t                    ancs_e_handle;

    BD_ADDR                     remote_addr;   //address of currently connected client
    wiced_bt_ble_address_type_t addr_type;
} ancs_app_state_t;

extern uint8_t *ancs_client_event_pool;

extern void ancs_client_connection_up(wiced_bt_gatt_connection_status_t *p_conn_status);
extern void ancs_client_connection_down(wiced_bt_gatt_connection_status_t *p_conn_status);
extern void ancs_client_bonded(void);
extern void ancs_client_encryption_changed(void);
extern void ancs_operation_read_response_handler(wiced_bt_gatt_operation_complete_t *p_complete);
extern void ancs_operation_write_response_handler(wiced_bt_gatt_operation_complete_t *p_complete);
extern void ancs_operation_notification_handler(wiced_bt_gatt_operation_complete_t *p_complete);
extern void ancs_operation_indication_handler(wiced_bt_gatt_operation_complete_t *p_complete);
extern wiced_bt_gatt_status_t ancs_discovery_result_handler(wiced_bt_gatt_discovery_result_t *p_result);



//==========================================================================================//


#endif
