#ifndef _YMODEM_PROTOCOL_H_
#define _YMODEM_PROTOCOL_H_

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define PACKET_SEQNO_INDEX      (1)
#define PACKET_SEQNO_COMP_INDEX (2)

#define PACKET_HEADER           (3)
#define PACKET_TRAILER          (2)
#define PACKET_OVERHEAD         (PACKET_HEADER + PACKET_TRAILER)
#define PACKET_SIZE             (128)
//#define PACKET_1K_SIZE          (1024)
#define PACKET_1K_SIZE          (128)//128 ok

#define FILE_NAME_LENGTH        (256)
#define FILE_SIZE_LENGTH        (16)

#define SOH                     (0x01)  /* start of 128-byte data packet */
#define STX                     (0x02)  /* start of 1024-byte data packet */
#define EOT                     (0x04)  /* end of transmission */
#define ACK                     (0x06)  /* acknowledge */
#define NAK                     (0x15)  /* negative acknowledge */
#define CA                      (0x18)  /* two of these in succession aborts transfer */
#define CRC16                   (0x43)  /* 'C' == 0x43, request 16-bit CRC */

#define ABORT1                  (0x47)  /* 'G' == 0x47, abort by user */
#define ABORT2                  (0x67)  /* 'g' == 0x67, abort by user */

#define NAK_TIMEOUT             (0x100000)//0x100000
#define MAX_ERRORS              (50)

/* Exported functions ------------------------------------------------------- */
uint8_t Ymodem_Transmit ( void );

#endif //_YMODEM_PROTOCOL_H_
