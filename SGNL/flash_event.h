/**
 * File name : flash_event.h
 *
 * This file contains the source code for a managing flash R/W/erase
 */

#ifndef _FLASH_EVENT_H_
#define _FLASH_EVENT_H_

typedef enum user_flash_list_{
    USER_FLASH_APP_CONFIG = 0,
    USER_FLASH_PEDOMETER_HOUR = 1,
    USER_FLASH_PEDOMETER_DAILY = 2,
    USER_FLASH_PACKAGE_BLOCK = 3,
    USER_FLASH_FAVORITE_0 = 4,
    USER_FLASH_FAVORITE_1 = 5,
    USER_FLASH_FAVORITE_2 = 6,
    USER_FLASH_FAVORITE_3 = 7,
    USER_FLASH_FAVORITE_4 = 8,
    USER_FLASH_FAVORITE_LOG = 9,

    USER_FLASH_LIST = 10,
}user_flash_list_t;

typedef enum management_flash_list_{
    MANAGE_FLASH_SERIAL_NUMBER  = 0,
    MANAGE_FLASH_FW_VERSION     = 1,

    MANAGE_FLASH_LIST           = 2,

}management_flash_list_t;

void user_flash_init( void );

void user_str_lower( uint8_t *ui8_data, uint8_t ui8_len );

void user_flash_erase( uint16_t list );

void user_flash_all_erase(void);

/**@brief Function for flash write.
 *
 * @param[in]   list        Select Flash page.
 * @param[in]   ui32_data   Write data.
 * @param[in]   ui32_cnt    Write index.
 * @param[in]   ui32_size   Write size.
 */
void user_flash_write( uint16_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size );

/**@brief Function for flash read.
 *
 * @param[in]   list        Select Flash page.
 * @param[in]   ui32_data   read data.
 * @param[in]   ui32_cnt    read index.
 * @param[in]   ui32_size   read size.
 */
void user_flash_read( uint16_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size );

void manage_flash_erase( uint16_t list );

void manage_flash_write( uint16_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size );

/**@brief Function for flash read.
 *
 * @param[in]   list        Select Flash page.
 * @param[in]   ui32_data   read data.
 * @param[in]   ui32_cnt    read index.
 * @param[in]   ui32_size   read size.
 */
void manage_flash_read( uint16_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size );


#endif //_FLASH_EVENT_H_

