/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
*
* WICED Firmware Upgrade internal definitions specific to shim layer
*
* This file provides common functions required to support WICED Smart Ready Upgrade
* whether it is being done over the air, UART, or SPI.  Primarily the
* functionality is provided for storing and retrieving information from  Serial Flash 
* The data being stored is DS portion of burn image generated from CGS.
*/
#include "bt_types.h"
#include "wiced.h"
#include "wiced_hal_sflash.h"
#include "wiced_bt_trace.h"
#include "wiced_bt_fw_upgrade.h"
#include "wiced_hal_wdog.h"
#include "fw_upgrade.h"

#include "p_256_ecc_pp.h"

/******************************************************
 *                      Constants
 ******************************************************/
#define WICED_FW_UPGRADE_SF_SECTOR_SIZE_4K             (4 * 1024) //Serial Flash Sector size
#define WICED_FW_UPGRADE_SF_SECTOR_SIZE_256K           (256 * 1024) //Serial Flash Sector size

#define INDIRECT_MEM_MAP_MASK   0xFF000000
/// indirect memory map for serial flash Read/Write/Erase access
#define INDIRECT_MEM_MAP_SF     0xF8000000

/// Converts a given address into the NV virtual address and returns same.
#define CONVERT_TO_NV_VIRTUAL_ADDRESS(address) (address | INDIRECT_MEM_MAP_SF)

//#define WICED_FW_UPGRADE_NV_IS_EEPROM()  ((Config_and_Firmware_Status & CFA_CONFIG_LOCATION_MASK) == CFA_CONFIG_LOCATION_EEPROM)

#if defined(USE_256K_SECTOR_SIZE)
  #define FLASH_SECTOR_SIZE         (256*1024)
  #define FW_UPGRADE_FLASH_SIZE     (256*FLASH_SECTOR_SIZE)
#else
  #define FLASH_SECTOR_SIZE         (4*1024)
//  #define FW_UPGRADE_FLASH_SIZE     0x80000 // 512  kbyte/4M Bit Sflash for new tag boards
  #define FW_UPGRADE_FLASH_SIZE     0x70000 // user flash data:0x00070000 ~
//  #define FW_UPGRADE_HEX_SIZE     (0x30000 * 2)
#endif
/******************************************************
 *                     Structures
 ******************************************************/
//ws_upgrde global data
typedef struct
{
    uint32_t active_ds_location;
    uint32_t upgrade_ds_location;
    uint32_t upgrade_ds_length;
} wiced_fw_upgrade_t;

/******************************************************
 *               Variables Definitions 
 ******************************************************/
/********************************************************************************************************
* Recommended firmware upgrade 4 MBit Serila flash offsets
 * -------------------------------------------------------------------------------------------------------------------
 * |  SS1 (4K @ 0)  |  Fail safe area(4K @ 0x1000)  |  VS1 (4K @ 0x2000)  | VS2 (4K @ 0x3000)  | DS1 (248K @ 0x4000)  | DS2 (248K @ 0x42000)
 *  -------------------------------------------------------------------------------------------------------------------
 *******************************************************************************************************/
wiced_fw_upgrade_nv_loc_len_t   g_nv_loc_len;
wiced_fw_upgrade_t              g_fw_upgrade;

/******************************************************
 *               External variables 
 ******************************************************/
extern uint32_t Config_VS_Location;
extern uint32_t Config_DS_Location;
extern uint32_t Config_DS_End_Location;
extern uint8_t  sfi_sectorErase256K;

uint32_t ds1_loc;
uint32_t ds2_loc;

/******************************************************
 *               Function Definitions
 ******************************************************/
wiced_bool_t wiced_firmware_upgrade_init(wiced_fw_upgrade_nv_loc_len_t *p_sflash_nv_loc_len, uint32_t sflash_size)
{
    wiced_fw_upgrade_nv_loc_len_t *p_active_nv = &g_nv_loc_len;

    if (p_sflash_nv_loc_len == NULL)
    {
        WICED_BT_TRACE("Error. can not upgrade. Please provide NV location and length \n");
        return WICED_FALSE;
    }

    // Enable SW write protect.
    *((unsigned char*)(0x00201a14)) = 1;

    WICED_BT_TRACE("platform btp: Config_DS_Location 0x%08x, Config_VS_Location:0x%08x \n", Config_DS_Location, Config_VS_Location);

    memcpy(p_active_nv, p_sflash_nv_loc_len, sizeof(wiced_fw_upgrade_nv_loc_len_t));

    if ((Config_DS_Location != p_active_nv->ds1_loc) &&
        (Config_DS_Location != p_active_nv->ds2_loc))
    {
        // If active DS is neither DS1 nor DS2 we expect to see, fail the download.
        WICED_BT_TRACE("WARNING: Upgrade will fail - active DS is not one of the expected locations\n");
        WICED_BT_TRACE("WARNING: Are ConfigDSLocation and DLConfigVSLocation in the .btp set up as in nv_loc_len[]?\n");
        return WICED_FALSE;
    }

    wiced_hal_sflash_set_size( sflash_size );

    return WICED_TRUE;
}


// setup nvram locations to be used during upgrade. if success returns 1, else fails return 0
uint32_t wiced_firmware_upgrade_init_nv_locations(void)
{
    wiced_fw_upgrade_t              *p_gdata        = &g_fw_upgrade;

    if ((Config_DS_Location != g_nv_loc_len.ds1_loc) &&
        (Config_DS_Location != g_nv_loc_len.ds2_loc))
    {
        // If active DS is neither DS1 nor DS2 we expect to see, fail the download.
        WICED_BT_TRACE("Active DS %x is not DS1:%x and not DS2:%x. Cannot upgrade.\n", Config_DS_Location, g_nv_loc_len.ds1_loc, g_nv_loc_len.ds2_loc);
        return 0;
    }

    p_gdata->active_ds_location  = Config_DS_Location;

    p_gdata->upgrade_ds_location = (p_gdata->active_ds_location == g_nv_loc_len.ds1_loc) ?
                                    g_nv_loc_len.ds2_loc : g_nv_loc_len.ds1_loc;
    p_gdata->upgrade_ds_length   = (p_gdata->active_ds_location == g_nv_loc_len.ds1_loc) ?
                                    g_nv_loc_len.ds2_len : g_nv_loc_len.ds1_len;

    WICED_BT_TRACE("Active: 0x%08X, Upgrade: 0x%08X, UG length: 0x%08X", p_gdata->active_ds_location, p_gdata->upgrade_ds_location, p_gdata->upgrade_ds_length);

    Config_DS_End_Location = p_gdata->active_ds_location +
                                    ((p_gdata->active_ds_location == g_nv_loc_len.ds1_loc) ?
                                      g_nv_loc_len.ds1_len : g_nv_loc_len.ds2_len);

//    return 1;
    return p_gdata->active_ds_location;
}

//Erases the Serial Flash Sector
void fw_upgrade_erase_mem(uint32_t erase_addr)
{
    // if Write Serial Flash
    //f8000000
    if ((erase_addr & INDIRECT_MEM_MAP_MASK) == INDIRECT_MEM_MAP_SF)
    {
        erase_addr &= (~INDIRECT_MEM_MAP_MASK);
        if (sfi_sectorErase256K)
        {
            wiced_hal_sflash_erase(erase_addr, WICED_FW_UPGRADE_SF_SECTOR_SIZE_256K);
        }
	else
        {
            wiced_hal_sflash_erase(erase_addr, WICED_FW_UPGRADE_SF_SECTOR_SIZE_4K);
        }
    }
    else
    {
        WICED_BT_TRACE("Not Serial Flash. Can't Erase @addr %x\n", erase_addr);
    }
}

//Reads the given length of data from SF/EEPROM. If success returns len, else returns 0
uint32_t fw_upgrade_read_mem(uint32_t read_from, uint8_t *buf, uint32_t len)
{
    memset(buf, 0xAA, len); // fill with some "bad" value for debug

    // if read from Serial Flash
    // if readFrom & 0xff000000 == 0xf8000000
    if ((read_from & INDIRECT_MEM_MAP_MASK) == INDIRECT_MEM_MAP_SF)
    {
        read_from &= (~INDIRECT_MEM_MAP_MASK);
        // WICED_BT_TRACE("sflash_read from:%x len:%d\n", read_from, len);
        if (wiced_hal_sflash_read(read_from, len, buf) != len)
        {
            WICED_BT_TRACE("sflash_read failed\n");
            return 0;
        }
        return len;
    }

    return 0;
}

// Writes the given length of data to SF. If success returns len, else returns 0
uint32_t fw_upgrade_write_mem(uint32_t write_to, uint8_t *data, uint32_t len)
{
    // if Write Serial Flash
     if ((write_to & INDIRECT_MEM_MAP_MASK) == INDIRECT_MEM_MAP_SF)
    {
        write_to &= (~INDIRECT_MEM_MAP_MASK);

        WICED_BT_TRACE("sflash_write to:%x len:%d\n", write_to, len);
	return wiced_hal_sflash_write(write_to, len, data);
    }

    return 0;
}

// Stores to the physical NV storage medium. if success, return len, else returns 0
uint32_t wiced_firmware_upgrade_store_to_nv(uint32_t offset, uint8_t *data, uint32_t len)
{
    uint32_t _offset = offset;
    uint32_t sector_size = (sfi_sectorErase256K) ? WICED_FW_UPGRADE_SF_SECTOR_SIZE_256K : WICED_FW_UPGRADE_SF_SECTOR_SIZE_4K;
    uint32_t bytes_written;

    //WICED_BT_TRACE("offset1:0x%x\n",offset);
    // The real offset into the NV is the current offset + the upgrade DS location.
    offset += g_fw_upgrade.upgrade_ds_location;
    //WICED_BT_TRACE("offset2:0x%x\n",offset);

    // Now add the NV virtual address to this offset based on the physical part.
    offset = CONVERT_TO_NV_VIRTUAL_ADDRESS(offset);
    //WICED_BT_TRACE("offset3:0x%x\n",offset);

    // if this is a beginning of a new sector erase first.
    if ((offset % sector_size) == 0)
    {
    //WICED_BT_TRACE("offset in:0x%x\n",offset);
        fw_upgrade_erase_mem(offset);
    }

    //WICED_BT_TRACE("offset4:0x%x\n",offset);
    bytes_written = fw_upgrade_write_mem(offset, data, len);
    //WICED_BT_TRACE("offset5:0x%x\n",offset);

    return bytes_written;
}

// Retrieve chunk of data from the physical NV storage medium. if success returns len, else return 0
uint32_t wiced_firmware_upgrade_retrieve_from_nv(uint32_t offset, uint8_t *data, uint32_t len)
{
    // The real offset into the NV is the current offset + the upgrade DS location.
    offset += g_fw_upgrade.upgrade_ds_location;

    // Now add the NV virtual address to this offset based on the physical part.
    offset = CONVERT_TO_NV_VIRTUAL_ADDRESS(offset);

    return fw_upgrade_read_mem(offset, data, len);
}

// In fail safe OTA patch, boot will decide active DS location
// No access SS after OTA verification done
#define DS2_MAGIC_NUMBER_BUFFER_LEN     8
#define SS_ORIGINAL_DSVS_ADDR           0x1B

#pragma pack(1)
typedef union
{
    UINT8  buffer[DS2_MAGIC_NUMBER_BUFFER_LEN + sizeof(UINT32)];
    struct
    {
        UINT8   magicNumber[DS2_MAGIC_NUMBER_BUFFER_LEN];
        UINT32  dsLoc;
    } ds2Info;
} tDs2Record;

typedef struct
{
    UINT8 cfgID;
    UINT16 len;
    UINT32 DS_Loc;
}tDSInfo;
#pragma pack()

UINT8  magic_num_ds2[DS2_MAGIC_NUMBER_BUFFER_LEN + 4] = { 0xAA, 0x55, 0xF0, 0x0F, 0x68, 0xE5, 0x97, 0xD2, 0,0,0,0};

uint8_t fw_upgrade_switch_active_ds(void)
{
    uint8_t dsDetect = 0;
    uint32_t magic_num_loc;
    wiced_fw_upgrade_nv_loc_len_t *p_active_nv = NULL;
    wiced_fw_upgrade_t *p_gdata = &g_fw_upgrade;
    tDSInfo DSInfo;

    p_active_nv = &g_nv_loc_len;

    if (sfi_read(SS_ORIGINAL_DSVS_ADDR, sizeof(tDSInfo), (uint8_t *)&DSInfo) != sizeof(tDSInfo))
    {
        WICED_BT_TRACE("dsinfo@SS_ORIGINAL_DSVS_ADDR read fail \n");
        return 0;
    }
    WICED_BT_TRACE("%s:%d DSInfo id:%x DS_Loc:0x%x Len:%d cfgID:%d\n",__func__,__LINE__, DSInfo.cfgID, DSInfo.DS_Loc, DSInfo.len, DSInfo.cfgID);

    // fail safe OTA patch will not have 0x02 item
    if (DSInfo.cfgID != 0x02)
    {
        // Sector must be erased before writing to it.
        if (sfi_sectorErase256K)
        {
            // adjust location for non-4k erase sector size - this must match location used by fail safe download
            magic_num_loc = CONVERT_TO_NV_VIRTUAL_ADDRESS(2 * WICED_FW_UPGRADE_SF_SECTOR_SIZE_256K) - sizeof(tDs2Record);
	    //WICED_BT_TRACE("%s:%d magic_num_loc:0x%x\n",__func__,__LINE__,magic_num_loc);
            sfi_erase(WICED_FW_UPGRADE_SF_SECTOR_SIZE_256K, WICED_FW_UPGRADE_SF_SECTOR_SIZE_256K);
        }
        else
        {
            magic_num_loc = CONVERT_TO_NV_VIRTUAL_ADDRESS(2 * WICED_FW_UPGRADE_SF_SECTOR_SIZE_4K) - sizeof(tDs2Record);
	    //WICED_BT_TRACE("%s:%d magic_num_loc:0x%x\n",__func__,__LINE__,magic_num_loc);
            sfi_erase(WICED_FW_UPGRADE_SF_SECTOR_SIZE_4K, WICED_FW_UPGRADE_SF_SECTOR_SIZE_4K);
        }

	//WICED_BT_TRACE("%s:%d 0x%x, 0x%x\n",__func__,__LINE__,p_gdata->active_ds_location,p_active_nv->ds1_loc);
        if (p_gdata->active_ds_location == p_active_nv->ds1_loc)
        {
            // Then update the DS with the upgrade DS.
            magic_num_ds2[DS2_MAGIC_NUMBER_BUFFER_LEN]   = (p_gdata->upgrade_ds_location & 0xFF);
            magic_num_ds2[DS2_MAGIC_NUMBER_BUFFER_LEN+1] = (p_gdata->upgrade_ds_location & 0xFF00) >> 8;
            magic_num_ds2[DS2_MAGIC_NUMBER_BUFFER_LEN+2] = (p_gdata->upgrade_ds_location & 0xFF0000) >> 16;
            magic_num_ds2[DS2_MAGIC_NUMBER_BUFFER_LEN+3] = (p_gdata->upgrade_ds_location & 0xFF000000) >> 24;

            // Write this to the upgrade SS.
            if (sfi_write(magic_num_loc , DS2_MAGIC_NUMBER_BUFFER_LEN + 4, magic_num_ds2) != (DS2_MAGIC_NUMBER_BUFFER_LEN + 4))
            {
                WICED_BT_TRACE("Could not update the 2nd SS block w/ magic number and DS location!\n");
                return 0;
            }
        }
        return 1;
    }
    else
    {
        return 0;
    }
}

// After download is completed and verified this function is
// called to switch active partitions with the one that has been
// receiving the new image.
void wiced_firmware_upgrade_finish(void)
{
    if (!fw_upgrade_switch_active_ds())
    {
        WICED_BT_TRACE("No fail safe OTA patch. Cannot upgrade firmware \n");
    }
    wiced_hal_wdog_reset_system();
    // End of the world - will not return.
}

uint32_t get_ds1_loc(void)
{
	WICED_BT_TRACE("ds1_loc:0x%08x",ds1_loc);
	return ds1_loc;
}

uint32_t get_ds2_loc(void)
{
	WICED_BT_TRACE("ds2_loc:0x%08x",ds2_loc);
	return ds2_loc;
}

/*
 * Initialize peripheral UART upgrade procedure
 */


wiced_bool_t wiced_ota_fw_upgrade_init(void *p_public_key, wiced_ota_firmware_upgrade_status_callback_t *p_status_callback)
{
    wiced_fw_upgrade_nv_loc_len_t nv_loc_len;
    //extern DWORD g_config_Info[];
    extern Point *p_ecdsa_public_key;
    extern wiced_ota_firmware_upgrade_status_callback_t *ota_fw_upgrade_status_callback;
    extern wiced_bool_t ota_fw_upgrade_initialized;

    p_ecdsa_public_key = (Point *)p_public_key;

    /* In Serial flash first 8K i.e (addr range 0x0000 to 0x2000)reserved for static section and for fail safe OTA block.
    Caution!!: Application should not modify these sections. */
    /* Remaining portion can be used for Vendor specific data followed by firmware
    It is applications choice to choose size for Vendor specific data and firmware.
    Apart from vendor specific portion, remaining area equally divided for active firmware and upgradable firmware.
    Below are the example offsets for 4M bit Serial flash
    Note: below configuration should not conflict with ConfigDSLocation, DLConfigVSLocation and DLConfigVSLength configured in
    platform sflash .btp file */

    // 4 MB Serial flash offsets for firmware upgrade.
    nv_loc_len.ss_loc   = 0x0000;
    nv_loc_len.vs1_loc  = (FLASH_SECTOR_SIZE + FLASH_SECTOR_SIZE); // one sector after SS for fail safe
    nv_loc_len.vs1_len  = FLASH_SECTOR_SIZE;
    nv_loc_len.vs2_loc  = nv_loc_len.vs1_loc + nv_loc_len.vs1_len;
    nv_loc_len.vs2_len  = FLASH_SECTOR_SIZE;
    nv_loc_len.ds1_loc  = nv_loc_len.vs2_loc + nv_loc_len.vs2_len;
#if defined(USE_256K_SECTOR_SIZE)
    nv_loc_len.ds1_len  = FLASH_SECTOR_SIZE;
    nv_loc_len.ds2_loc  = nv_loc_len.ds1_loc + nv_loc_len.ds1_len;
    nv_loc_len.ds2_len  = FLASH_SECTOR_SIZE;
#else
    {
        uint32_t ds1_ds2_length = (FW_UPGRADE_FLASH_SIZE - nv_loc_len.ds1_loc);
        nv_loc_len.ds1_len  = ds1_ds2_length/2;
        nv_loc_len.ds2_loc  = nv_loc_len.ds1_loc + nv_loc_len.ds1_len;
        nv_loc_len.ds2_len  = ds1_ds2_length/2;
    }
#endif

    ds1_loc = nv_loc_len.ds1_loc;
    ds2_loc = nv_loc_len.ds2_loc;
    WICED_BT_TRACE("ds1:0x%08x, len:0x%08x\n", nv_loc_len.ds1_loc, nv_loc_len.ds1_len);
    WICED_BT_TRACE("ds2:0x%08x, len:0x%08x\n", nv_loc_len.ds2_loc, nv_loc_len.ds2_len);

    if (!wiced_firmware_upgrade_init(&nv_loc_len, FW_UPGRADE_FLASH_SIZE))
    {
        WICED_BT_TRACE("WARNING: Upgrade will fail - active DS is not one of the expected locations\n");
        WICED_BT_TRACE("WARNING: Please build with OTA_FW_UPGRADE=1 and download \n");
        return WICED_FALSE;
    }
    ota_fw_upgrade_init_data();
    ota_fw_upgrade_status_callback = p_status_callback;

    ota_fw_upgrade_initialized = WICED_TRUE;
    return WICED_TRUE;
}
