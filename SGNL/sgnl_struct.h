/*
 * sgnl_struct.h
 *
 *  Created on: 2017. 8. 31.
 *      Author: sheum
 */

#ifndef APPS_HCI_SERIAL_GATT_SERVICE_SGNL_STRUCT_H_
#define APPS_HCI_SERIAL_GATT_SERVICE_SGNL_STRUCT_H_

#include "stdint.h"


typedef enum mcu_protocol_cmd_e_
{
	CMD_MCU_STATUS			= 0x80,
	CMD_CALL_STATUS		= 0x81,
	CMD_POWER_STATUS		= 0x82,
	CMD_APP_NOTI_STATUS		= 0x83,
	CMD_BLE_CONNECTION_STATUS	= 0x84,
	CMD_BT_CONNECTION_STATUS	= 0x85,
	CMD_BATTERY_STATUS		= 0x86,
	CMD_BT_PAIRING_STATUS		= 0x87,
	CMD_SELECT_FAVORITE_STATUS	= 0x88,
	CMD_CHARGER_STATUS		= 0x89,
	CMD_VOLUME_CONTROL		= 0x8A,

	CMD_HFP_A2DP			= 0x8B,
	CMD_DND_STATUS			= 0x8C,
	CMD_AMP_STATUS			= 0x8D,
#ifdef FUNC_PP3_SLEEP_MODE
	CMD_MCU_SLEEP_STATUS		= 0x8E,
#endif
#ifdef ENABLE_OTA_FWUPGRADE
	CMD_FW_UPDATE_STATUS		= 0x8F,
#endif

#ifdef HW_PP4
	CMD_BATTERY_TIMER_STATUS	= 0x90,
#endif
	CMD_CALL_FREQ_STATUS		= 0x91,
	CMD_CGEQ_STATUS		= 0x92,
	
	CMD_SOUND_CHECK		= 0xF0,
#ifdef SMT_CHECK
	CMD_PEDO_STATUS_CHECK		= 0xF1,
	CMD_PEDO_COUNT_CHECK		= 0xF2,
#endif

	CMD_EXHIBITION_MODE		= 0xFD,
	CMD_FACTORY_RESET		= 0xFE,
	CMD_INIT			= 0xFF,
}mcu_protocol_cmd_e;
extern mcu_protocol_cmd_e mcu_protocol_cmd;

typedef enum call_status_e_{
	CALL_STATUS_READY		= 0x00,
	CALL_STATUS_CALLING	= 0x01,
	//CALL_STATUS_END		= 0x02,
	CALL_STATUS_INCOMING	= 0x02,
	CALL_STATUS_OUTGOING	= 0x04,
	CALL_STATUS_DND		= 0x06,
	CALL_STATUS_REJECT	= 0x07,

	CALL_STATUS_FAVORITE	= 0x08,
	CALL_STATUS_FAVORITE_INCOMING = 0x09,
	CALL_STATUS_FAVORITE_OUTGOING = 0x0A,
	CALL_STATUS_FAVORITE_CALLING = 0x0C,
}call_status_e;
typedef enum power_status_e_{
	POWER_STATUS_OFF	= 0x0,
	POWER_STATUS_ON	= 0x1,
	POWER_STATUS_INIT	= 0xF,
}power_status_e;

typedef enum haptic_status_e_
{
	HAPTIC_OFF	= 0x0,
	HAPTIC_ON	= 0x1,

	HAPTIC_INIT	= 0xF,
}haptic_status;
#if 0
extern power_status_e g_power_status;
#endif
typedef enum ble_connection_status_e_{
	BLE_CONNECTION_DISCONNECT	= 0x0,
	BLE_CONNECTION_CONNECT	= 0x1,
	BLE_CONNECTION_INIT		= 0xF,
}ble_connection_status_e;
typedef enum bt_connection_status_e_{
	BT_CONNECTION_DISCONNECT	= 0x0,
	BT_CONNECTION_CONNECT		= 0x1,
	BT_CONNECTION_INIT		= 0xF,
}bt_connection_status_e;
typedef enum bt_pairing_e_{
	BT_PAIRING_START	= 0x1,
	BT_PAIRING_TIMEOUT	= 0x2,
	BT_PAIRING_SUCCESS	= 0x3,
	BT_PAIRING_FAIL		= 0x4,
	BT_PAIRING_UNPAIRED	= 0x5,
	BT_PAIRING_INIT		= 0xF,
} bt_pairing_e;
extern bt_pairing_e g_bt_pairing;

enum {
    HOST_DEVICE_ANDROID = 0,
    HOST_DEVICE_IPHONE = 1,
};

enum {
    DISABLE = 0,
    ENABLE = 1,
} ;

typedef struct bitfield_{

    volatile uint32_t b1_power_on:1;			// 1 

    volatile uint32_t b1_adv_status:1;		// 2

    volatile uint32_t b1_bt_conn_status:1;		// 3
    volatile uint32_t b1_pre_bt_conn_status:1;	// 4
    volatile uint32_t b1_ble_conn_status:1;		// 5
    volatile uint32_t b1_pre_ble_conn_status:1;	// 6

    volatile uint32_t b1_a2dp_start:1;		// 7
    volatile uint32_t b1_nus_enable:1;		// 8

    volatile uint32_t b1_mcu_sleep_status:1;		// 9
    volatile uint32_t b1_selected_codec:1;		// 10
    volatile uint32_t b1_exhibition_mode:1;		// 11

    volatile uint32_t b1_mcu_status:1;		// 13
    volatile uint32_t b1_inspect_mode:1;	// 14
    volatile uint32_t b1_por_status:1;		// 15
    volatile uint32_t b1_pedo_status:1;		// 16
    
    volatile uint32_t b1_daily_step_noti:1;		// 17
    volatile uint32_t b1_daily_active_noti:1;	// 18
    volatile uint32_t b1_ble_send_wait:1;		// 19
    volatile uint32_t b2_incoming_call:2;		// 20, 21

    volatile uint32_t b1_favorite_save_ready:1;	// 22
    volatile uint32_t b1_package_ready:1;		// 23
    volatile uint32_t b1_package_erase:1;		// 24
    volatile uint32_t b1_ancs_received:1;		// 25

    volatile uint32_t b1_pedo_err:1;			// 26
    volatile uint32_t b1_motion_chk:1;		// 27
    volatile uint32_t b1_pedo_transmit:1;		// 28
    volatile uint32_t b1_i2c_send_wait:1;		// 29
    
    volatile uint32_t b2_pedo_mode:2;			// 30, 31

    volatile uint32_t b1_ota_fw_upgrade:1;		// 32

}bitfield_t;

extern bitfield_t g_str_bit;

typedef struct call_bitfield_{

    volatile uint8_t b1_favorite_enable:1;      //1
    volatile uint8_t b2_call_dnd:2;           //2

    volatile call_status_e b4_call_status:4;            //3~6
} call_bitfield_t;

extern call_bitfield_t g_str_call_bit;

#ifdef ENABLE_MCU_TIMEOUT
typedef struct timeout_bitfield_{

    volatile uint16_t b1_pairing_on:1;      //1
    volatile uint16_t b1_receive_call:1;    //2
    volatile uint16_t b1_end_call:1;        //3
    volatile uint16_t b1_make_call:1;       //4

    volatile uint16_t b1_volume_control:1;  //5
    volatile uint16_t b1_factory_reset:1;   //6
    volatile uint16_t b3_timeout_retry:3;   //7~9
} timeout_bitfield_t;

extern timeout_bitfield_t g_str_timeout;
#endif //ENABLE_MCU_TIMEOUT

#ifdef ENABLE_LED
typedef struct pwm_struct_{

    volatile uint16_t ui16_dimming_flag;
    volatile uint16_t ui16_dimming_data;
    volatile uint32_t ui32_pwm;

}pwm_struct_t;

extern pwm_struct_t g_str_led[5];
#endif

typedef struct timer_struct_{

    volatile uint16_t ui16_pattern_cnt;
    volatile uint16_t ui16_sub_pattern_cnt;

    volatile uint16_t ui16_batt_cnt;
    volatile uint16_t ui16_stay_hour;
    volatile uint16_t ui16_pedo_hour;

    volatile uint16_t ui16_disconn_cnt;
    volatile uint16_t ui16_conn_noti_cnt;

    volatile uint8_t ui8_charge_cnt;

    volatile uint8_t ui8_power_on_cnt;

	volatile uint32_t ui32_delay_cnt;
}timer_struct_t;

extern timer_struct_t g_str_timer;


typedef enum battery_status_{

        //BATT_STATUS_DANGER                              = 0x00,
    BATT_STATUS_NO_PLUG             = 0x00,
    BATT_STATUS_LOW                 = 0x01,
    BATT_STATUS_NORMAL              = 0x02,
    BATT_STATUS_HIGH                = 0x03,
    BATT_STATUS_FULL                = 0x04,
    BATT_STATUS_IN_CHARGING         = 0x05,
    BATT_STATUS_CHARGING_COMPLETE   = 0x06,

    BATT_STATUS_INITIAL             = 0xFF,
}battery_status_t;

typedef enum battery_level_{

        BATT_LEVEL_3_0          = 0x00,
        BATT_LEVEL_3_1          = 0x01,
        BATT_LEVEL_3_2          = 0x02,
        BATT_LEVEL_3_3          = 0x03,
        BATT_LEVEL_3_4          = 0x04,
        BATT_LEVEL_3_5          = 0x05,
        BATT_LEVEL_3_6          = 0x06,
        BATT_LEVEL_3_7          = 0x07,
        BATT_LEVEL_3_8          = 0x08,
        BATT_LEVEL_3_9          = 0x09,
        BATT_LEVEL_4_0          = 0x0A,
        BATT_LEVEL_4_1          = 0x0B,
        BATT_LEVEL_4_2          = 0x0C,

        BATT_LEVEL_ERR          = 0xEE,

        BATT_LEVEL_INITIAL      = 0xFF,

}battery_level_t;

typedef enum battery_low_level_{

    BATT_LOW_NONE       = 0x00,
    BATT_LOW_20         = 0x01,
    BATT_LOW_10         = 0x02,
    BATT_LOW_5          = 0x03,

}battery_low_level_t;

typedef struct batt_struct_{
    uint32_t ui8_charger_status;

    volatile battery_low_level_t ui8_low_level;
    volatile battery_low_level_t ui8_perv_low_level;

    volatile uint16_t ui16_level;

    volatile uint8_t ui8_buf_per[8];
    volatile uint8_t ui8_abs_buf_per[8];
    
    volatile uint8_t ui8_percent;
    volatile uint8_t ui8_previous_percent;

    volatile battery_status_t ui16_batt_status;
    volatile battery_level_t ui16_batt_level;
    volatile int8_t i8_batt_temperature;
    volatile int16_t i16_batt_using_current;
    volatile int16_t i16_batt_avg_current;

    volatile uint32_t ui32_relative_state_charge;
    volatile uint32_t ui32_absolute_state_charge;

    volatile uint16_t ui16_batt_usable_capacity;
    volatile uint16_t ui16_batt_remain_capacity;
    volatile uint16_t ui16_batt_full_charge_capacity;
}batt_struct_t;

extern batt_struct_t g_str_batt;

#ifdef ENABLE_FUEL_GAUGE
typedef struct fuel_gauge_batt_status_{

        uint16_t b1_soc_low_detection:1;
        uint16_t b1_soc_high_detection:1;
        uint16_t b1_full_discharge:1;
        uint16_t b1_full_charge:1;

        uint16_t b1_discharge:1;
        uint16_t b1_remaining_run_time_alarm:1;
        uint16_t b1_usable_capacity_low_alarm:1;
        uint16_t b1_battery_degradation_alert:1;

        uint16_t b1_under_temperature:1;
        uint16_t b1_over_temperature:1;

        uint16_t b1_discharge_over_current:1;
        uint16_t b1_charge_over_current:1;
        uint16_t b1_over_discharge:1;
        uint16_t b1_over_charge:1;

}fg_batt_status_struct_t;

typedef struct fuel_gauge_status_{

        uint16_t b4_command_response_status:4;
        uint16_t b1_alert_status:1;
        uint16_t b2_system_failure_alarm:2;
        uint16_t b1_battery_alert:1;

        uint16_t b1_correct_remaining_capacity:1;
        uint16_t b1_update_battery_capacity:1;

        uint16_t b1_detect_stable_current:1;
        uint16_t b1_data_not_ready:1;

        uint16_t ui16_firmware_version;
}fg_status_sturct_t;
extern fg_batt_status_struct_t g_str_fuel_gauge_batt;
extern fg_status_sturct_t g_str_fuel_gauge;
#endif


#ifdef ENABLE_PEDOMETER
typedef struct pedometer_struct_
{
    uint16_t ui16_still_time;
    uint16_t ui16_run_time;
    uint16_t ui16_walk_time;
    uint16_t ui16_bike_time;
    uint16_t ui16_drive_time;

    uint16_t ui16_total_still_time;
    uint16_t ui16_total_run_time;
    uint16_t ui16_total_walk_time;
    uint16_t ui16_total_bike_time;
    uint16_t ui16_total_drive_time;

    uint32_t ui32_run_step;
    uint32_t ui32_walk_step;
    uint32_t ui32_bike_step;
    uint32_t ui32_drive_step;

    uint32_t ui32_total_run_step;
    uint32_t ui32_total_walk_step;
    uint32_t ui32_total_bike_step;
    uint32_t ui32_total_drive_step;

}pedometer_struct_t;

extern pedometer_struct_t g_str_pedo;


typedef struct pedo_back_struct_{
    uint16_t ui16_still_time;
    uint16_t ui16_walk_time;
    uint16_t ui16_run_time;
    uint16_t ui16_bike_time;
    uint16_t ui16_drive_time;

    uint32_t ui32_walk_step;
    uint32_t ui32_run_step;
    uint32_t ui32_bike_step;
    uint32_t ui32_drive_step;   
    
}pedo_back_struct_t;

extern pedo_back_struct_t g_str_pedo_back;


#endif //ENABLE_PEDOMETER

#ifdef ENABLE_FAVORITE_MANAGER

typedef struct favo_mgr_struct_
{
    volatile uint8_t ui8_index;
    volatile uint8_t ui8_saved_index[5];
    volatile uint8_t ui8_num_len;
    volatile uint8_t ui8_name_len;
    volatile uint8_t ui8_log_type;
    volatile uint8_t ui8_log_enable;
    volatile uint16_t ui16_target_cnt;
    volatile uint16_t ui16_target_time;
    volatile uint8_t ui8_number[32];
    volatile uint8_t ui8_name[32];
}favo_mgr_struct_t;

extern favo_mgr_struct_t g_str_favo_mgr;

typedef struct favo_log_struct_
{
    volatile uint8_t ui8_index;
    volatile uint16_t ui16_missed_call;
    volatile uint16_t ui16_outgo_cnt;
    volatile uint16_t ui16_incom_cnt;

    volatile uint32_t ui32_outgo_time;
    volatile uint32_t ui32_incom_time;
}favo_log_struct_t;

extern favo_log_struct_t g_str_favo_log;

#endif //ENABLE_FAVORITE_MANAGER

#ifdef ENABLE_ANCS_PARSER

typedef struct ancs_parse_struct_{
    volatile uint8_t ui8_event;
    volatile uint8_t ui8_category_id;
    volatile uint8_t ui8_category_cnt;
    volatile uint8_t ui8_id_len;
    volatile uint8_t ui8_title_len;
    volatile uint8_t ui8_date_len;
    volatile uint8_t ui8_identifier[32];
    volatile uint8_t ui8_title[32];
    volatile uint8_t ui8_date[32];
}ancs_parse_struct_t;

extern ancs_parse_struct_t g_str_ancs;

#endif //ENABLE_ANCS_PARSER

typedef struct time_info_struct_{
    volatile uint32_t ui32_app_date;
    volatile uint32_t ui32_app_hour;
    volatile uint32_t ui32_date;
    volatile uint32_t ui32_hour;
    volatile uint8_t ui8_minute;
    volatile uint8_t ui8_last_day_of_week;
    volatile uint8_t ui8_day_of_week;
}time_info_struct_t;

extern time_info_struct_t g_str_time;

typedef struct init_status_struct_{

    volatile uint8_t ui8_app_noti_status;
    volatile uint8_t ui8_dnd_status;
    volatile uint8_t ui8_longsit_status;
    volatile uint8_t ui8_step_status;
    volatile uint8_t ui8_activity_status;

    //volatile uint8_t ui8_host_device;
    //volatile uint8_t ui8_audio_bt_paired;
    volatile uint8_t ui8_ble_bonded;

    volatile uint16_t ui16_longsit_start_time;
    volatile uint16_t ui16_longsit_end_time;

    volatile uint16_t ui16_target_steps;
    volatile uint16_t ui16_target_active_time;

    volatile int8_t i8_call_volume;
    volatile uint8_t ui8_max_volume;
    volatile uint8_t ui8_cgeq_status;
}init_status_struct_t;

extern init_status_struct_t g_str_app_status;

typedef struct mcu_msg_buffer_{
    volatile uint8_t ui8_data[16];
}mcu_msg_buffer_t;

#define MSG_QUEUE_SIZE  10

typedef struct mcu_msg_queue_{
    volatile mcu_msg_buffer_t ui8_msg_buffer[MSG_QUEUE_SIZE];
    volatile uint8_t ui8_msg_len[MSG_QUEUE_SIZE];
    volatile uint8_t ui8_head;
    volatile uint8_t ui8_tail;
}mcu_msg_queue_t;

extern mcu_msg_queue_t g_str_msg_q;

#endif /* APPS_HCI_SERIAL_GATT_SERVICE_SGNL_STRUCT_H_ */
