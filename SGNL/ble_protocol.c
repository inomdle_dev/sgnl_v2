/*
 * ble_protocol.c
 *
 *  Created on: 2017. 8. 31.
 *      Author: sheum
 */

#include "wiced_bt_trace.h"

#include "global_define.h"
#include "sgnl_struct.h"
#include "ble_protocol.h"
#include "uart.h"

uint8_t ble_protocol( uint8_t *u8_data, uint8_t u8_size, uint16_t conn_id )
{
	uint8_t u8_command_id = u8_data[1];
	uint8_t u8_data_type = u8_data[2];
	uint8_t mcu_send_data[3] = {0,};
	static uint8_t call_volume_backup = 0;
	
#ifdef ENABLE_FAVORITE_MANAGER
	if( g_str_bit.b1_favorite_save_ready )
	{
		static uint8_t err_cnt = 0;
		if( !( u8_command_id == REPORT_FAVORITE_DATA ) )
		{
			if( ++err_cnt > 5 )
			{
				g_str_bit.b1_favorite_save_ready = 0;
			}
		}
	}
#endif

	switch( u8_command_id )
	{
		case REPORT_CALL_STATUS:
			switch( u8_data_type )
			{
				case REP_CALL_INCOMING:
#if 0
					g_str_call_bit.b4_call_status = CALL_STATUS_INCOMING;
					mcu_send_data[0] = g_str_call_bit.b4_call_status;
					mcu_send_data[1] = 0;	    
					mcu_send_data[2] = (g_str_app_status.ui8_dnd_status == 1)? HAPTIC_OFF : HAPTIC_ON;
					send_mcu_msg(CMD_CALL_STATUS, UART_ID_CYW, 3, mcu_send_data);
					//g_str_bit.b2_incoming_call = 1;
#endif
					WICED_BT_TRACE( "common incoming call\n");
					break;

#ifdef ENABLE_FAVORITE_MANAGER
				case REP_CALL_FAVORITE_INCOME:
#if 0
					g_str_favo_mgr.ui8_index = u8_data[3];
					g_str_call_bit.b4_call_status |= CALL_STATUS_INCOMING;
					g_str_call_bit.b4_call_status |= CALL_STATUS_FAVORITE;
					mcu_send_data[0] = g_str_call_bit.b4_call_status;
					mcu_send_data[1] = g_str_favo_mgr.ui8_index + 1;
					mcu_send_data[2] = (g_str_app_status.ui8_dnd_status == 1)? HAPTIC_OFF : HAPTIC_ON;
					send_mcu_msg(CMD_CALL_STATUS, UART_ID_CYW, 3, mcu_send_data);
					//g_str_bit.b2_incoming_call = 2;
#endif
					WICED_BT_TRACE( "fav incoming call\n");
					break;
#endif //ENABLE_FAVORITE_MANAGER

				case REP_CALL_RECEIVED:
					if(!g_str_bit.b1_bt_conn_status)
					{
#if 0
						g_str_call_bit.b4_call_status |= CALL_STATUS_CALLING;
						mcu_send_data[0] = g_str_call_bit.b4_call_status;
						mcu_send_data[1] = 0;
						send_mcu_msg(CMD_CALL_STATUS, UART_ID_CYW, 2, mcu_send_data);
						WICED_BT_TRACE( "calling\n");
						g_str_bit.b2_incoming_call = 0;
#endif
					}
					break;
				case REP_CALL_END:
					WICED_BT_TRACE( "End call\n");
					if( g_str_call_bit.b4_call_status & CALL_STATUS_INCOMING )
					{
						g_str_bit.b2_incoming_call = 0;
						g_str_call_bit.b4_call_status = CALL_STATUS_READY;
					}

					break;

				default :
					break;
			}
			break;

		case REPORT_NOTIFICATION:
			switch( u8_data_type )
			{
				case REP_NOTI_APP_NOTI:
					if( g_str_app_status.ui8_app_noti_status )
					{
						mcu_send_data[0] = WICED_TRUE;
						mcu_send_data[1] = (g_str_app_status.ui8_dnd_status == 1)? HAPTIC_OFF : HAPTIC_ON;
						send_mcu_msg(CMD_APP_NOTI_STATUS, UART_ID_CYW, 2, mcu_send_data);
						WICED_BT_TRACE( "App noti\n");
					}

					break;

				case REP_NOTI_STEP_NOTI:
					if( g_str_app_status.ui8_step_status )
					{
						if( ( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING ) || ( g_str_call_bit.b1_favorite_enable ) )
						{
							if( !g_str_app_status.ui8_dnd_status )
							{
								WICED_BT_TRACE( "Step noti - haptic\n");
							}
						}
						else
						{
							WICED_BT_TRACE( "Step noti\n");
						}
					}
					break;

				case REP_NOTI_ACT_NOTI:
					if( g_str_app_status.ui8_activity_status )
					{
						if( ( g_str_call_bit.b4_call_status & CALL_STATUS_CALLING ) || ( g_str_call_bit.b1_favorite_enable ) )
						{
							if( !g_str_app_status.ui8_dnd_status )
							{
								WICED_BT_TRACE( "Time noti - haptic\n");
							}
						}
						else
						{
							WICED_BT_TRACE( "Time noti\n");
						}
					}
					break;

				default :
					break;
			}
			break;

		case REPORT_PEDOMETER:
			switch( u8_data_type )
			{
#ifdef ENABLE_PEDOMETER
				case REP_PEDO_LIVE_DATA :
					{
						uint32_t ui32_cur_time, ui32_new_time;
						uint32_t ui32_tmp_date;

						g_str_time.ui32_app_date = (uint32_t)( (u8_data[3]<<16) | (u8_data[4]<<8) | u8_data[5] );
						g_str_time.ui32_app_hour = (uint32_t)u8_data[6];

						uint8_t byte[20] = "";
						uint8_t nus_index = 0;

						ui32_cur_time  = ( uint32_t )( ( g_str_time.ui32_date & 0x00FFFFFF ) << 8 );
						ui32_new_time  = ( uint32_t )( ( g_str_time.ui32_app_date & 0x00FFFFFF ) << 8 );

						ui32_cur_time |= ( uint32_t )( g_str_time.ui32_hour & 0x00ff );
						ui32_new_time |= ( uint32_t )( g_str_time.ui32_app_hour & 0x00ff );

						// check time
						if( ui32_new_time > ui32_cur_time )
						{
							// last time data send
							if( g_str_time.ui32_date != 0 )
							{
								if( g_str_time.ui32_app_date == g_str_time.ui32_date )
								{
#if 0
									byte[++nus_index] = REPORT_PEDOMETER;
									byte[++nus_index] = REP_PEDO_PAST_TIME;
									byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_date & 0xff0000 ) >> 16 );
									byte[++nus_index] = ( uint8_t )( ( g_str_time.ui32_date & 0xff00 ) >> 8 );
									byte[++nus_index] = ( uint8_t )( g_str_time.ui32_date & 0xff );
									byte[++nus_index] = ( uint8_t )( g_str_time.ui32_hour );

									byte[++nus_index] = ( uint8_t )((g_ui32_pedometer_steps&0xff000000)>>24);
									byte[++nus_index] = ( uint8_t )((g_ui32_pedometer_steps&0x00ff0000)>>16);
									byte[++nus_index] = ( uint8_t )((g_ui32_pedometer_steps&0x0000ff00)>>8);
									byte[++nus_index] = ( uint8_t )(g_ui32_pedometer_steps&0x000000ff);
									byte[++nus_index] = ( uint8_t )((g_str_pedo.ui16_total_walk_time&0xff00)>>8);
									byte[++nus_index] = ( uint8_t )(g_str_pedo.ui16_total_walk_time&0x00ff);
									byte[++nus_index] = ( uint8_t )(0);
									byte[++nus_index] = ( uint8_t )(0);
									byte[++nus_index] = ( uint8_t )(0);
									byte[++nus_index] = ( uint8_t )(0);
									byte[++nus_index] = ( uint8_t )((g_str_pedo.ui16_total_run_time&0xff00)>>8);
									byte[++nus_index] = ( uint8_t )(g_str_pedo.ui16_total_run_time&0x00ff);

									byte[0] = ++nus_index;
									send_byte_gatt_connid(conn_id, byte, byte[0]);
#endif
									pedo_flash_hour_write();
								}
							}
						}
						// time update
						ui32_tmp_date = g_str_time.ui32_app_date;
						if( ui32_tmp_date != g_str_time.ui32_date )
						{
							if( g_str_time.ui32_date )
							{
								pedo_flash_daily_write();
							}
							else
							{
								pedo_flash_live_backup();
							}

							g_str_time.ui32_date = ui32_tmp_date;
							day_of_week();
						}

						if( g_str_time.ui32_hour != g_str_time.ui32_app_hour )
						{
							g_str_time.ui8_minute = 0;
						}

						g_str_time.ui32_hour = g_str_time.ui32_app_hour;

						if( g_str_bit.b1_pedo_err )
						{
							uint8_t data[3];
							uint8_t ui8_index = 0;
							data[++ui8_index] = REPORT_ERR_MSG;
							data[++ui8_index] = REP_ERR_PEDO_INIT;
							data[0] = ++ui8_index;

							g_ui32_last_pedo_steps = g_ui32_pedometer_steps;

							send_byte_gatt_connid(conn_id, data,data[0]);

							wiced_hal_wdog_disable();
							if( init_motion_sensor_driver() )
							{
								// pedo error
								g_str_bit.b1_pedo_err = 1;
							}
							else
							{
								g_str_bit.b1_pedo_err = 0;
								pedometer_data_transfer(conn_id);
							}
							wiced_hal_wdog_restart();
						}
						else
						{
							pedometer_data_transfer(conn_id);
						}
					}

					//when sgnl app is executed restart or new, send battery percentage,charger status
					batt_charger_status_send();

					break;
				case REP_PEDO_PAST_TIME :
					g_str_time.ui32_app_date = (uint32_t)( (u8_data[3]<<16) | (u8_data[4]<<8) | u8_data[5] );
					g_str_time.ui32_app_hour = (uint32_t)u8_data[6];

					pedo_flash_hour_read();

					break;

				case REP_PEDO_PAST_DATE :
					g_str_time.ui32_app_date = (uint32_t)( (u8_data[3]<<16) | (u8_data[4]<<8) | u8_data[5] );

					pedo_flash_daily_read();

					break;

				case REP_PEDO_SYNC_DATA :
					{
						uint32_t tmp_steps = 0;
						uint16_t tmp_walk_times = 0;
						uint16_t tmp_run_times = 0;

						g_str_time.ui32_app_date = (uint32_t)( (u8_data[3]<<16) | (u8_data[4]<<8) | u8_data[5] );
						g_str_time.ui32_app_hour = (uint32_t)u8_data[6];

						g_str_time.ui32_date = g_str_time.ui32_app_date;
						g_str_time.ui32_hour = g_str_time.ui32_app_hour;
						g_str_time.ui8_minute = u8_data[7];

						tmp_steps = (uint32_t)( (u8_data[8]<<24) | (u8_data[9]<<16) | (u8_data[10]<<8) | u8_data[11] );
						tmp_walk_times = (uint16_t)( (u8_data[12]<<8) | u8_data[13] );
						tmp_run_times = (uint16_t)( (u8_data[14]<<8) | u8_data[15] );

						WICED_BT_TRACE("cur stpes: %d, tmp steps : %d", g_ui32_pedometer_steps, tmp_steps );
						if( tmp_steps > g_ui32_pedometer_steps )
						{
							g_ui32_pedometer_steps = tmp_steps;
							g_ui32_last_pedo_steps = tmp_steps - g_ui32_cur_pedo_steps;
						}

						if( tmp_walk_times > g_str_pedo.ui16_total_walk_time )
						{
							g_str_pedo.ui16_total_walk_time = tmp_walk_times;
						}

						if( tmp_run_times > g_str_pedo.ui16_total_run_time )
						{
							g_str_pedo.ui16_total_run_time = tmp_run_times;
						}

						g_ui32_pedometer_times = g_str_pedo.ui16_total_walk_time + g_str_pedo.ui16_total_run_time;
					}

					break;

#endif //ENABLE_PEDOMETER
				default :
					break;
			}
			break;

		case REPORT_APP_CONFIG:
			{
				WICED_BT_TRACE( "app config\n");
				uint8_t u8_status = u8_data[3];
				switch( u8_data_type )
				{
					case REP_CONFIG_APP_NOTI_STATUS:
						g_str_app_status.ui8_app_noti_status = u8_status;
						app_status_write();
						break;

					case REP_CONFIG_DND_STATUS:
						g_str_app_status.ui8_dnd_status = u8_status;
						app_status_write();
						break;

					case REP_CONFIG_LONGSIT_STATUS:
						{
							uint8_t u8_param = u8_data[4];
							switch(u8_status)
							{
								case REP_LONGSIT_CONFIG:
									g_str_app_status.ui8_longsit_status = u8_param;
									app_status_write();
									break;
								case REP_LONGSIT_START:
									g_str_app_status.ui16_longsit_start_time = u8_data[4]<<8;
									g_str_app_status.ui16_longsit_start_time |= u8_data[5];
									app_status_write();
									break;
								case REP_LONGSIT_STOP:
									g_str_app_status.ui16_longsit_end_time = u8_data[4]<<8;
									g_str_app_status.ui16_longsit_end_time |= u8_data[5];
									app_status_write();
									break;
							}

							g_str_timer.ui16_stay_hour = 0;
						}
						break;

					case REP_CONFIG_STEP_STATUS:
						g_str_app_status.ui8_step_status = u8_status;
						g_str_bit.b1_daily_step_noti = 1;
						app_status_write();
						break;

					case REP_CONFIG_STEP_CONFIG:
						g_str_app_status.ui16_target_steps = u8_status;
						g_str_bit.b1_daily_step_noti = 1;
						app_status_write();
						break;

					case REP_CONFIG_ACT_STATUS:
						g_str_app_status.ui8_activity_status = u8_status;
						g_str_bit.b1_daily_active_noti = 1;
						app_status_write();
						break;

					case REP_CONFIG_ACT_CONFIG:
						if( u8_data[4] )
						{
							g_str_app_status.ui16_target_active_time = u8_data[3]<<8;
							g_str_app_status.ui16_target_active_time |= u8_data[4];
						}
						else
						{
							g_str_app_status.ui16_target_active_time = u8_data[3];
						}
						g_str_bit.b1_daily_active_noti = 1;
						app_status_write();
						break;

					case REP_CONFIG_BT_PAIR_REQ:

						//bt_pairing_on();
						break;

#ifdef ENABLE_BATT
					case REP_CONFIG_BATT_STATUS:
						{
							batt_charger_status_send();
						}

						break;
#endif
					case REP_CONFIG_BLE_READY :

						break;

					case REP_CONFIG_TIME_SYNC:
						g_str_time.ui32_date = (u8_data[3]&0xFF)<<16;
						g_str_time.ui32_date |= (u8_data[4]&0xFF)<<8;
						g_str_time.ui32_date |= (u8_data[5]&0xFF);
						g_str_time.ui32_hour = u8_data[6];
						g_str_time.ui8_minute = u8_data[7];
						day_of_week();
						break;

#ifdef ENABLE_SFLASH_READ
					case REP_CONFIG_FLASH_CHK:
						{
							user_flash_check();
						}
						break;
#endif

#ifdef ENABLE_OTA_FWUPGRADE
					case REP_CONFIG_OTA_TEST:
						{
							uint8_t byte[5] = "";
							uint8_t gatt_index = 0;

							byte[++gatt_index ] = REPORT_APP_CONFIG;
							byte[++gatt_index ] = REP_CONFIG_OTA_TEST;

							/* OTA Firmware upgrade Initialization */
							if (wiced_ota_fw_upgrade_init(NULL, NULL) == WICED_FALSE)
							{
								WICED_BT_TRACE("OTA upgrade Init failure !!! \n");
								byte[++gatt_index ] = 0x00;
							}
							else
							{
								byte[++gatt_index ] = 0x01;
								//ota f/w upgrade receiving binary data
								g_str_bit.b1_ota_fw_upgrade = 1;
							}

							byte[0] = ++gatt_index;
							send_byte_gatt_connid(conn_id, byte, gatt_index );
						}
						break;
#endif

#ifdef ENABLE_PEDOMETER
#ifdef ENABLE_PEDOMETER_DEBUG
					case REP_CONFIG_PEDO_TIME :
						{
							uint8_t byte[5] = "";
							uint8_t gatt_index = 0;

							byte[++gatt_index ] = REPORT_APP_CONFIG;
							byte[++gatt_index ] = REP_CONFIG_PEDO_TIME;

							byte[++gatt_index ] = (uint8_t)( (g_str_time.ui32_date&0xFF0000)>>16 );
							byte[++gatt_index ] = (uint8_t)( (g_str_time.ui32_date&0xFF00)>>8 );
							byte[++gatt_index ] = (uint8_t)( (g_str_time.ui32_date&0xFF));
							byte[++gatt_index ] = (uint8_t)( (g_str_time.ui32_hour&0xFF));

							byte[++gatt_index ] = (uint8_t)( (g_ui32_pedometer_steps&0xFF000000)>>24 );
							byte[++gatt_index ] = (uint8_t)( (g_ui32_pedometer_steps&0xFF0000)>>16 );
							byte[++gatt_index ] = (uint8_t)( (g_ui32_pedometer_steps&0xFF00)>>8 );
							byte[++gatt_index ] = (uint8_t)( (g_ui32_pedometer_steps&0xFF));

							g_ui32_pedometer_times = g_str_pedo.ui16_total_walk_time + g_str_pedo.ui16_total_run_time;

							byte[++gatt_index ] = (uint8_t)( (g_ui32_pedometer_times&0xFF000000)>>24 );
							byte[++gatt_index ] = (uint8_t)( (g_ui32_pedometer_times&0xFF0000)>>16 );
							byte[++gatt_index ] = (uint8_t)( (g_ui32_pedometer_times&0xFF00)>>8 );
							byte[++gatt_index ] = (uint8_t)( (g_ui32_pedometer_times&0xFF));

							byte[0] = ++gatt_index;
							send_byte_gatt_connid(conn_id, byte, gatt_index );                        
						}
						break;

					case REP_CONFIG_PEDO_TEST :
						{
							uint8_t lMemsWhoAmIRead = 0x00;

							uint8_t byte[5] = "";
							uint8_t gatt_index = 0;

							byte[++gatt_index ] = REPORT_APP_CONFIG;
							byte[++gatt_index ] = REP_CONFIG_PEDO_TEST;

							inv_serial_interface_read_hook(0x00, 1, &lMemsWhoAmIRead);
							WICED_BT_TRACE("lMemsWhoAmIRead:0x%2x",lMemsWhoAmIRead);
							if (lMemsWhoAmIRead != 0xE0)
							{
								byte[++gatt_index ] = 0xEE;
							}
							else
							{
								byte[++gatt_index ] = lMemsWhoAmIRead;
							}

							byte[0] = ++gatt_index;
							send_byte_gatt_connid(conn_id, byte, gatt_index );                        

						}
						break;
#endif
#endif

					case REQ_CONFIG_MAX_VOLUME:
						{
							if(u8_status == 0)//enter the menu
							{
								call_volume_backup = g_str_app_status.i8_call_volume;//original volume backup
								g_str_app_status.i8_call_volume = 0x05;
								mcu_send_data[0] = g_str_app_status.i8_call_volume;
								mcu_send_data[1] = 0x00;
								mcu_send_data[2] = g_str_app_status.ui8_max_volume;
								send_mcu_msg(CMD_VOLUME_CONTROL, UART_ID_CYW, 3, mcu_send_data);
							}
							else if(u8_status == 1)//setting...
							{
								g_str_app_status.ui8_max_volume = u8_data[4];
								mcu_send_data[0] = g_str_app_status.i8_call_volume;
								mcu_send_data[1] = 0x00;
								mcu_send_data[2] = g_str_app_status.ui8_max_volume;
								send_mcu_msg(CMD_VOLUME_CONTROL, UART_ID_CYW, 3, mcu_send_data);
							}
							else if(u8_status == 2)//exit the menu
							{
								g_str_app_status.i8_call_volume = call_volume_backup;//original volume rollback
								app_status_write();
							}
						}
						break;

					case REQ_CONFIG_CGEQ_STATUS:
						{
							g_str_app_status.ui8_cgeq_status = u8_status;
							mcu_send_data[0] = u8_status;
							send_mcu_msg(CMD_CGEQ_STATUS, UART_ID_CYW, 1, mcu_send_data);
						}
						break;

					default :
						break;
				}
			}
			break;

		case REPORT_ANCS_DATA:
			WICED_BT_TRACE( "ANCS manage\n");
			switch( u8_data_type )
			{
				case REP_ANCS_DATA_SEND:
					ancs_data_parcer(&u8_data[3], u8_size-4);
					break;

				default :
					break;
			}
			break;

#ifdef ENABLE_FAVORITE_MANAGER
		case REPORT_FAVORITE_DATA:
			switch( u8_data_type )
			{
				case REP_FAVO_DATA_SEND:
					g_str_bit.b1_favorite_save_ready = 1;
					favorite_data_parser_group(&u8_data[3], u8_size-4);
					break;

				case REP_FAVO_DATA_SYNC:
					//if( g_str_bit.b1_favorite_save_ready )
					{
						//return;
					}
					switch(u8_data[3])
					{
						case 1:
							favorite_data_sync( u8_data[4] );
							break;

						case 2:
							favorite_data_delete( u8_data[4] );
							break;

						default :
							break;
					}
				default:
					break;
			}
			break;
#endif //ENABLE_FAVORITE_MANAGER

		case REPORT_EXHIBITION_MODE:
			switch( u8_data_type )
			{
				case REP_AUDIO_MODE:
					if( u8_data[3] )
					{
						//exhibition mode
						mcu_send_data[0] = ON;
						g_str_bit.b1_exhibition_mode = 1;
						g_str_app_status.i8_call_volume = 5;
						//wiced_bt_dev_set_tx_power(NULL, 10);
						//wiced_bt_dev_set_adv_tx_power(12);		// -16/-12/-8/-4/0/4/8/12
						send_mcu_msg(CMD_EXHIBITION_MODE, UART_ID_CYW, 1, mcu_send_data);
					}
					else
					{
						//mass product mode
						mcu_send_data[0] = OFF;
						g_str_bit.b1_exhibition_mode = 0;
						g_str_app_status.i8_call_volume = 0;
						//wiced_bt_dev_set_tx_power(NULL, 0);
						//wiced_bt_dev_set_adv_tx_power(4);		// -16/-12/-8/-4/0/4/8/12
						send_mcu_msg(CMD_EXHIBITION_MODE, UART_ID_CYW, 1, mcu_send_data);
					}
					break;

				default:
					break;
			}
			break;

		default : break;
	}

	return 1;
}



