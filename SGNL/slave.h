#ifndef __SLAVE_H__
#define __SLAVE_H__

#include <stdint.h>
#include "wiced.h"
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_timer.h"

#define SLAVE_INIT_STATE_NONE	(0)
#define SLAVE_INIT_STATE_GATT	(1)
#define SLAVE_INIT_STATE_GAP		(2)
#define SLAVE_INIT_STATE_TIME	(3)
#define SLAVE_INIT_STATE_FINDME	(4)
#define SLAVE_INIT_STATE_ANCS	(5)

#define NVRAM_FIRST_VALID_NVRAM_ID        0x10
#define NVRAM_INVALID_NVRAM_ID            0x00

typedef void (*init_complete_cback_t) (int);
typedef void (*gap_client_init_complete_cback_t) (int, uint8_t *p_name, uint8_t name_len);
typedef void (*time_notification_cback_t)(uint8_t *time_data, int len);
typedef void (*ancs_notification_cback_t)(void *p_ancs_event);

typedef struct
{

    uint8_t      init_state;
    uint16_t     conn_id;
    BD_ADDR      remote_addr;   //address of currently connected client
    wiced_bt_ble_address_type_t addr_type;
    wiced_bt_transport_t transport;              /**< Transport type of the connection */
    wiced_timer_t timer;
    wiced_bool_t encrypted;
} le_slave_conn_t;

extern le_slave_conn_t le_slave_conn;

void slave_init(void);
void slave_connect_up(wiced_bt_gatt_connection_status_t *p_conn_status);
void slave_connect_down(wiced_bt_gatt_connection_status_t *p_conn_status);
void slave_encryption_status_changed(wiced_bt_dev_encryption_status_t *p_status);
wiced_bt_gatt_status_t set_config_descriptor(uint16_t conn_id, uint16_t handle, uint16_t value);
void send_discover(uint16_t conn_id, wiced_bt_gatt_discovery_type_t type, uint16_t uuid, uint16_t s_handle, uint16_t e_handle);
void slave_connection_timeout(uint32_t count);
wiced_bt_gatt_status_t slave_gatt_operation_complete_callback(wiced_bt_gatt_operation_complete_t *p_complete);
wiced_bt_gatt_status_t slave_gatt_discovery_result_callback(wiced_bt_gatt_discovery_result_t *p_result);
wiced_bt_gatt_status_t slave_gatt_discovery_complete_callback(wiced_bt_gatt_discovery_complete_t *p_complete);


#endif

