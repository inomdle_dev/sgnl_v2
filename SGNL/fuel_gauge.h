#ifndef __FUEL_GAUGE_H__
#define __FUEL_GAUGE_H__

#define FUEL_GAUGE_ADDRESS	(0x9A)
#define FUEL_GAUGE_PERIOD	1000//10 seconds
#define BATT_FULL_CAPACITY	330
typedef enum fuel_gauge_command_e_{
	
	TEMPERATURE			= 0x00,	// 2 bytes(r)	0.1[��E]
	VOLTAGE				= 0x02,	// 2 bytes(r)	[mV]
	USING_CURRENT			= 0x04,	// 2 bytes(r)	[mA]
	AVERAGE_CURRENT			= 0x06,	// 2 bytes(r)	[mA]
	RELATIVE_STATE_CHARGE		= 0x08,	// 2 bytes(r)	1%/256
	ABSOLUTE_STATE_CHARGE		= 0x0A,	// 2 bytes(r)	1%/256
	USABLE_CAPACITY			= 0x0C,	// 2 bytes(r)	[mAh]
	REMAINING_CAPACITY		= 0x0E,	// 2 bytes(r)	[mAh]
	FULL_CHARGE_CAPACITY		= 0x10,	// 2 bytes(r)	[mAh]
	AVERAGE_TIME_EMPTY		= 0x12,	// 2 bytes(r)	[minutes]
	AVERAGE_TIME_FULL		= 0x14,	// 2 bytes(r)	[minutes]
	BATTERY_STATUS			= 0x16,	// 2 bytes(r)	
	CYCLE_COUNT			= 0x18,	// 2 bytes(r)	[counts]
	STATE_BATTERY_CONDITION		= 0x1A,	// 2 bytes(r)	[%]
	INTERNAL_TEMPERATURE		= 0x1C,	// 2 bytes(r)	0.1[��E]
	FUEL_GAUGE_STATUS		= 0x1E,	// 2 bytes(rw)	
	AT_RATE_TIME_EMPTY		= 0x22,	// 2 bytes(rw)	[minutes]
	AT_RATE_TIME_FULL		= 0x24,	// 2 bytes(rw)	[minutes]
	FUEL_GAUGE_CONDITION		= 0x26,	// 2 bytes(rw)	
	FUEL_GAUGE_PARAMETER_OFFSET	= 0x28,	// 2 bytes(rw)	
	FUEL_GAUGE_PARAMETER_DATA	= 0x2A,	// 8 bytes(rw)
	PRODUCT_INFORMATION		= 0x32,	// 8 bytes(r)
	ID_INFORMATION			= 0x3A,	// 8 bytes(r)
	BATTERY_PACK_INFORMATION	= 0x42,	// 8 bytes(r)
	IDENTIFY			= 0x4A,	// 6 bytes(r)
	DESIGN_CAPACITY			= 0x50,	// 2 bytes(r)	[mAh]
	DESIGN_VOLTAGE			= 0x52,	// 2 bytes(r)	[mAh]
	NVM_PARAMTER_OFFSET		= 0x54,	// 2 bytes(rw)	[mAh]
	NVM_PARAMTER_DATA		= 0x56,	// 8 bytes(r)	[mAh]
	
	MON_STATUS			= 0x86,	// 2 bytes
	FUEL_GAUGE_RESET		= 0x88,	// 1 byte
	READ_NVM			= 0x8B,	// 4/16 bytes
	ERASE_NVM			= 0x8C,	// 1 byte
	WRITE_NVM			= 0x8E,	// 8 bytes
	
}fuel_gauge_command_e;

typedef enum fuel_gauge_condition_e_{
	
	ACTIVE_MODE = 0x00,	//90uA
	LOW_POWER_ACTIVE_MODE,	//30.0uA
	SHUTDOWN_MODE,		//1.0uA
	STANDBY_MODE,		//8.5uA
	//INTERVAL_ACTIVE1_MODE,	//2.5uA (active <-> shutdown)
	//INTERVAL_ACTIVE2_MODE,	//9.9uA (active <-> standby)
	OCV_CORRECTION = 0x20,
	
}fuel_gauge_condition_e;

extern void fuel_gauge_init(void);
extern void fuel_gauge_communication(fuel_gauge_command_e cmd);
#endif