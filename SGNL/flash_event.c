#include "wiced_bt_trace.h"
#include "wiced_hal_sflash.h"
#include "flash_event.h"
#include "string.h"
#include "sgnl_struct.h"

#define SERIAL_FLASH_WRITE_ACCEPT		*((unsigned char*)(0x00201a14))

#define SERIAL_FLASH_SECTOR_SIZE_4K	4096

#define USER_FLASH_END 0x7A000


uint32_t flash_addr[USER_FLASH_LIST] = {
	0x70000,// USER_FLASH_APP_CONFIG = 0,
	0x71000,// USER_FLASH_PEDOMETER_HOUR = 1,
	0x72000,// USER_FLASH_PEDOMETER_DAILY = 2,
	0x73000,// USER_FLASH_PACKAGE_BLOCK = 3,
	0x74000,// USER_FLASH_FAVORITE_0 = 4,
    0x75000,// USER_FLASH_FAVORITE_1 = 5,
    0x76000,// USER_FLASH_FAVORITE_2 = 6,
    0x77000,// USER_FLASH_FAVORITE_3 = 7,
    0x78000,// USER_FLASH_FAVORITE_4 = 8,
	0x79000 // USER_FLASH_FAVORITE_LOG = 9,
};

uint32_t dmp_flash_addr = 0x7A000; // 0x7A000~07DFFF 16kb

uint32_t manage_flash_addr[MANAGE_FLASH_LIST] = {
    0x7E000, // MANAGE_FLASH_SERIAL_NUMBER = 0,
    0x7F000, // MANAGE_FLASH_FW_VERSION = 1,
};

void user_flash_init( void )
{
    WICED_BT_TRACE("user_flash_init\n");
}

void user_str_lower( uint8_t *ui8_data, uint8_t ui8_len )
{
    int i = 0;
    for( i = 0; i < ui8_len; i++ )
    {
        if( ( ui8_data[i] >= 'A' ) && ( ui8_data[i] <= 'Z' ) )
        {
            ui8_data[i] += 0x20;
        }
    }
}

void user_flash_erase( uint16_t list )
{
#if 1
	WICED_BT_TRACE("user_flash_erase\n");

	uint32_t erase_addr = flash_addr[list];

	SERIAL_FLASH_WRITE_ACCEPT = 1;

	wiced_hal_sflash_erase(erase_addr, SERIAL_FLASH_SECTOR_SIZE_4K);

	SERIAL_FLASH_WRITE_ACCEPT = 0;

#endif
}

void user_flash_all_erase(void)
{
	WICED_BT_TRACE("user_flash_all_erase\n");

	uint32_t erase_addr = 0;
	int i = 0;

	SERIAL_FLASH_WRITE_ACCEPT = 1;
	for( i = USER_FLASH_APP_CONFIG; i < USER_FLASH_LIST; i++ )
	{
		erase_addr = flash_addr[i];

	        wiced_hal_sflash_erase(erase_addr, SERIAL_FLASH_SECTOR_SIZE_4K);
	}	
	SERIAL_FLASH_WRITE_ACCEPT = 0;
}

/**@brief Function for flash write.
 *
 * @param[in]   list        Select Flash page.
 * @param[in]   ui32_data   Write data.
 * @param[in]   ui32_cnt    Write index.
 * @param[in]   ui32_size   Write size.
 */
void user_flash_write( uint16_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size )
{
	uint32_t start_addr = (flash_addr[list]+(ui32_cnt*ui32_size));

	SERIAL_FLASH_WRITE_ACCEPT = 1;

	if( !ui32_cnt )
	{
		WICED_BT_TRACE("user_flash_erase\n");
	        wiced_hal_sflash_erase(start_addr, SERIAL_FLASH_SECTOR_SIZE_4K);
	}

	int size = 0;
	size = wiced_hal_sflash_write(start_addr, ui32_size, (uint8_t *)ui32_data);

	WICED_BT_TRACE("write size: 0x%x\n", size);

#if 0
	for( i = 0; i < ui32_size/sizeof(uint32_t); i++ )
	{
		WICED_BT_TRACE("write data: 0x%x\n", write_data[i]);
	}
#endif
	SERIAL_FLASH_WRITE_ACCEPT = 0;

}

/**@brief Function for flash read.
 *
 * @param[in]   list        Select Flash page.
 * @param[in]   ui32_data   read data.
 * @param[in]   ui32_cnt    read index.
 * @param[in]   ui32_size   read size.
 */
void user_flash_read( uint16_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size )
{
	uint32_t start_addr = (flash_addr[list]+(ui32_cnt*ui32_size));
	//uint8_t read_data[480];

	int size = 0;
	size = wiced_hal_sflash_read(start_addr, ui32_size, (uint8_t *)ui32_data);

	WICED_BT_TRACE("read size: 0x%x\n", size);

	int i = 0;
#if 0
	for( i = 0; i < ui32_size/sizeof(uint32_t); i++ )
	{
		WICED_BT_TRACE("read data: 0x%x\n", read_data[i]);
	}
#endif

#if 0
	for( i = 0; i < ui32_size/sizeof(uint32_t); i++ )
	{
		ui32_data[i] = read_data[i*4 + 3] << 24;
		ui32_data[i] |= read_data[i*4 + 2] << 16;
		ui32_data[i] |= read_data[i*4 + 1] << 8;
		ui32_data[i] |= read_data[i*4];
	}
#endif
}

void user_flash_dmp( uint8_t *ui8_data, uint32_t ui32_cnt, uint32_t ui32_size )
{
	uint32_t start_addr = dmp_flash_addr + (ui32_cnt * ui32_size);

	int size = 0;
	size = wiced_hal_sflash_read(start_addr, ui32_size, (uint8_t *)ui8_data);    

	//WICED_BT_TRACE("read size: 0x%x\n", size);
}

void manage_flash_erase( uint16_t list )
{
   	uint32_t start_addr = manage_flash_addr[list];

	WICED_BT_TRACE("manage_flash_erase : %d\n", list);

    wiced_hal_sflash_erase(start_addr, SERIAL_FLASH_SECTOR_SIZE_4K);
}

void manage_flash_write( uint16_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size )
{
	uint32_t start_addr = (manage_flash_addr[list]+(ui32_cnt*ui32_size));

	SERIAL_FLASH_WRITE_ACCEPT = 1;

	if( !ui32_cnt )
	{
		WICED_BT_TRACE("manage_flash_erase\n");
        wiced_hal_sflash_erase(start_addr, SERIAL_FLASH_SECTOR_SIZE_4K);
	}

	int size = 0;
	size = wiced_hal_sflash_write(start_addr, ui32_size, (uint8_t *)ui32_data);

	WICED_BT_TRACE("write size: 0x%x\n", size);

	SERIAL_FLASH_WRITE_ACCEPT = 0;

}

/**@brief Function for flash read.
 *
 * @param[in]   list        Select Flash page.
 * @param[in]   ui32_data   read data.
 * @param[in]   ui32_cnt    read index.
 * @param[in]   ui32_size   read size.
 */
void manage_flash_read( uint16_t list, uint32_t *ui32_data, uint32_t ui32_cnt, uint32_t ui32_size )
{
	uint32_t start_addr = (manage_flash_addr[list]+(ui32_cnt*ui32_size));

	int size = 0;
	size = wiced_hal_sflash_read(start_addr, ui32_size, (uint8_t *)ui32_data);

	WICED_BT_TRACE("read size: 0x%x\n", size);
}


#ifdef ENABLE_SFLASH_READ
void user_flash_check( void )
{
    uint8_t read_data[8] = {};
    uint32_t cur_addr = 0;
    uint32_t cur_data = 0;
    
    WICED_BT_TRACE("USER_flash check \n");

    WICED_BT_TRACE("app status :\n");
    cur_addr = flash_addr[0];

    while( cur_addr < USER_FLASH_END )
    {
	    wiced_hal_sflash_read(cur_addr, 4, read_data);

        cur_data = 0;
        cur_data |= read_data[3] << 24;
		cur_data |= read_data[2] << 16;
		cur_data |= read_data[1] << 8;
		cur_data |= read_data[0];

        if( cur_data == 0xFFFFFFFF )
        {
            if( cur_addr > flash_addr[9] )
            {
                WICED_BT_TRACE("read complete \n\n");
                break;
            }
        }
        else
        {
            WICED_BT_TRACE("0x%x : 0x%x\n", cur_addr, cur_data);
        }

        cur_addr += 4;

        if( cur_addr == flash_addr[1] ) 
        {
            WICED_BT_TRACE("pedo hour :\n");
        }

        else if( cur_addr == flash_addr[2] ) 
        {
            WICED_BT_TRACE("pedo day :\n");
        }
        else if( cur_addr == flash_addr[3] ) 
        {
            WICED_BT_TRACE("package :\n");
        }
        else if( cur_addr == flash_addr[4] ) 
        {
            WICED_BT_TRACE("favorite_0 :\n");
        }
        else if( cur_addr == flash_addr[5] ) 
        {
            WICED_BT_TRACE("favorite_1 :\n");
        }
        else if( cur_addr == flash_addr[6] ) 
        {
            WICED_BT_TRACE("favorite_2 :\n");
        }
        else if( cur_addr == flash_addr[7] ) 
        {
            WICED_BT_TRACE("favorite_# :\n");
        }
        else if( cur_addr == flash_addr[8] ) 
        {
            WICED_BT_TRACE("favorite_4 :\n");
        }
        else if( cur_addr == flash_addr[9] ) 
        {
            WICED_BT_TRACE("favorite log :\n");
        }

    }
}
#endif

