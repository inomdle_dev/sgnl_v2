#ifndef _MCU_MSG_QUEUE_H_
#define _MCU_MSG_QUEUE_H_

void push_mcu_msg( uint8_t *buf, uint8_t length );
void pop_mcu_msg( void );
void init_mcu_msg( void );



#endif //_MCU_MSG_QUEUE_H_

