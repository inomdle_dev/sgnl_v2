#ifndef __SGNL_H__
#define __SGNL_H__

#include "wiced_bt_audio.h"
#include "wiced_bt_dev.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_trace.h"

/*******************************************************************
 * Constant Definitions
 ******************************************************************/
#define TRANS_UART_BUFFER_SIZE                          1024	// 1024
#define TRANS_UART_BUFFER_COUNT                         1	// 1

#define HANDSFREE_DISCOVERABLE_DURATION					60
#define HANDSFREE_NVRAM_ID                              WICED_NVRAM_VSID_START
#define HANDSFREE_NVRAM_BLE_ID							(WICED_NVRAM_VSID_START + 1)
#define HANDSFREE_LOCAL_KEYS_VS_ID                      (WICED_NVRAM_VSID_START + 2)

/*******************************************************************
 * Function Prototypes-
 ******************************************************************/
static void 					sgnl_app_init( void );
static wiced_bt_dev_status_t 	sgnl_management_callback( wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data );
static void 					sgnl_set_advertisement_data( void );
static void 					sgnl_advertisement_stopped( void );
static void 					sgnl_advertisement_start( void );
static void 					sgnl_reset_device( void );
/* GATT Registration Callbacks */
static wiced_bt_gatt_status_t 	gatt_connect_callback( wiced_bt_gatt_connection_status_t *p_conn_status );
static wiced_bt_gatt_status_t 	gatt_operation_complete_callback( wiced_bt_gatt_operation_complete_t *p_complete );
static wiced_bt_gatt_status_t 	gatt_req_read_handler( wiced_bt_gatt_read_t *p_read_req, uint16_t conn_id );
static wiced_bt_gatt_status_t 	gatt_req_write_handler( wiced_bt_gatt_write_t *p_write_req, uint16_t conn_id );
static wiced_bt_gatt_status_t 	gatt_req_exec_write_handler( wiced_bt_gatt_exec_flag_t exec_flag, uint16_t conn_id );
static wiced_bt_gatt_status_t	gatt_req_mtu_handler( uint16_t conn_id, uint16_t mtu );
static wiced_bt_gatt_status_t gatt_req_conf_handler( uint16_t conn_id, uint16_t handle );
static wiced_bt_gatt_status_t 	gatt_attr_req_callback(wiced_bt_gatt_attribute_request_t *p_req);
static wiced_bt_gatt_status_t 	gatt_event_handler( wiced_bt_gatt_evt_t  event, wiced_bt_gatt_event_data_t *p_event_data );
static void 					sgnl_fine_timeout(uint32_t finecount );
static uint32_t 				hci_control_process_rx_cmd( uint8_t* p_data, uint32_t len );
#ifdef HCI_TRACE_OVER_TRANSPORT
static void 					sgnl_trace_callback( wiced_bt_hci_trace_type_t type, uint16_t length, uint8_t* p_data );
#endif
static wiced_bool_t 			get_paired_host_info(uint8_t* bda);
static wiced_bool_t 			get_paired_ble_info(uint8_t* bda);
static wiced_bool_t 			read_device_link_keys(wiced_bt_device_link_keys_t *p_keys);
static wiced_bool_t 			read_all_device_link_keys(wiced_bt_device_link_keys_t *p_keys);
static wiced_bool_t 			read_bt_device_link_keys(wiced_bt_device_link_keys_t *p_keys);
static wiced_bool_t read_ble_device_link_keys(wiced_bt_device_link_keys_t *p_keys);
static wiced_bool_t 			delete_device_link_keys(void);
static wiced_bool_t 			save_device_link_keys(wiced_bt_device_link_keys_t *p_keys);
static void 					load_device_keys_to_addr_resolution_db(void);
static void 					set_discoverable(void);
static void 					write_eir(void);
void 							device_connection_status_callback(wiced_bt_device_address_t bd_addr, uint8_t *p_features, wiced_bool_t is_connected, uint16_t handle, wiced_bt_transport_t transport, uint8_t reason);
void							user_init(void);
void 							function_key_process(void);
void 							power_key_process(void);
void							navigation_key_process(uint8_t is_up);
void							do_not_disturb_process(void);
static void 					disconnect_devices(void);
static void 					discovery_timer_callback(uint32_t arg);
static wiced_result_t le_connect_down( wiced_bt_gatt_connection_status_t *p_status );
static wiced_result_t le_connect_up( wiced_bt_gatt_connection_status_t *p_status );
static void gatt_operation_notification_handler( uint16_t conn_id, uint16_t handle, uint8_t *p_data, uint16_t len );
static void gatt_operation_indication_handler( uint16_t conn_id, uint16_t handle, uint8_t *p_data, uint16_t len );
static void gatt_operation_write_response_handler( uint16_t con_handle, uint8_t result );
static void gatt_operation_write_error_handler( uint16_t con_handle, uint8_t write_result );
static void gatt_operation_read_response_handler( uint16_t con_handle, uint8_t *data, int len );
static void gatt_operation_read_error_handler( uint16_t con_handle, uint8_t read_result );

#if 0
extern int write_nvram_id( int nvram_id, int data_len, void *p_data, wiced_bool_t from_host );
extern int read_nvram_id( int nvram_id, void *p_data, int data_len );
extern int find_nvram_id( uint8_t *p_data, int len);
extern void delete_nvram_id( int nvram_id, wiced_bool_t from_host );
extern int alloc_nvram_id( );
#endif
extern void start_sgnl_bt_connection_timer(void);
extern void send_mcu_msg(uint8_t a_cmd, uint8_t a_id, uint8_t a_len, uint8_t *p_data);
extern void set_gpio_wake_up_mcu(uint8_t updown);
#ifdef NEEDLESS_SLEEP_CONTROL
extern void set_app_sleep_allowed(wiced_bool_t value);
#endif

/*******************************************************************
 * Macro Definitions
 ******************************************************************/
// Macro to extract uint16_t from little-endian byte array
#define LITTLE_ENDIAN_BYTE_ARRAY_TO_UINT16(byte_array) \
        (uint16_t)( ((byte_array)[0] | ((byte_array)[1] << 8)) )


/*******************************************************************
 * Variable Definitions
 ******************************************************************/
const wiced_bt_audio_config_buffer_t wiced_bt_audio_buf_config;

// Transport pool for sending RFCOMM data to host
static wiced_transport_buffer_pool_t* transport_pool = NULL;


#endif
