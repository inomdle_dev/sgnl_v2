/*
* Minimalistic implementation of the XModem/YModem protocol suite, including
* a compact version of an CRC16 algorithm. The code is just enough to upload
* an image to a MCU that bootstraps itself over an UART.
*
* Copyright (c) 2014 Daniel Mack <github@zonque.org>
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the
* Free Software Foundation; either version 2.1 of the License, or (at your option)
* any later version.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include "crc16.h"
#include "sparcommon.h"
#include "wiced_hal_puart.h"
#include "wiced_bt_trace.h"
#include "Wiced_timer.h"
#include "sgnl_gpio_def.h"
#include "uart.h"
#include "global_define.h"
#include "ymodem_protocol.h"
#include "ota_fw_upgrade.h"

#define YMODEM_ERROR_CNT	20

/**
* @brief  Prepare the first block
* @param  timeout
*     0: end of transmission
* @retval None
*/

void Send_Byte(uint8_t byte)
{
	puart_write_buffer(&byte, 1);
}

void Ymodem_PrepareIntialPacket(uint8_t *data, const uint8_t* fileName, uint32_t *length)
{
	uint16_t i, j;
	uint8_t file_ptr[10];
	
	/* Make first three packet */
	data[0] = SOH;
	data[1] = 0x00;
	data[2] = 0xff;
	
	/* Filename packet has valid data */
	for (i = 0; (fileName[i] != '\0') && (i < FILE_NAME_LENGTH);i++)
	{
		data[i + PACKET_HEADER] = fileName[i];
	}
	
	data[i + PACKET_HEADER] = 0x00;
	
	sprintf((char *)(file_ptr),"%d",*length);   
	
	for (j =0, i = i + PACKET_HEADER + 1; file_ptr[j] != '\0' ; )
	{
		data[i++] = file_ptr[j++];
	}
	
	for (j = i; j < PACKET_SIZE + PACKET_HEADER; j++)
	{
		data[j] = 0;
	}
}

/**
* @brief  Prepare the data packet
* @param  timeout
*     0: end of transmission
* @retval None
*/
void Ymodem_PreparePacket(uint8_t *SourceBuf, uint8_t *data, uint8_t pktNo, uint32_t sizeBlk)
{
	uint16_t i, size, packetSize;
	uint8_t* file_ptr;
	uint8_t ui8_data = 0;
	
	/* Make first three packet */
	packetSize = sizeBlk >= PACKET_1K_SIZE ? PACKET_1K_SIZE : PACKET_SIZE;
	size = sizeBlk < packetSize ? sizeBlk : packetSize;
	if (packetSize == PACKET_1K_SIZE)
	{
		data[0] = STX;
	}
	else
	{
		data[0] = SOH;
	}
	data[1] = pktNo;
	data[2] = (~pktNo);
	file_ptr = SourceBuf;

	/* Filename packet has valid data */
	for (i = PACKET_HEADER; i < size + PACKET_HEADER;i++)//real flash data (1024)
	{
#if 0
		data[i] = *file_ptr++;
#else
		wiced_hal_sflash_read(file_ptr, 1, &ui8_data);
		data[i] = ui8_data;
		//if(i < 11)
		//	WICED_BT_TRACE("[%s] file_ptr[0x%8x=0x%x]\n",__func__,file_ptr,ui8_data);
		file_ptr++;
#endif
	}
	if ( size <= packetSize )
	{
		for (i = size + PACKET_HEADER; i < packetSize + PACKET_HEADER; i++)
		{
			data[i] = 0x1A; /* EOF (0x1A) or 0x00 */
			//WICED_BT_TRACE("2i:%d data[i]:%x\n",i,data[i]);
		}
	}
}

/**
* @brief  Update CRC16 for input byte
* @param  CRC input value 
* @param  input byte
* @retval None
*/
uint16_t UpdateCRC16(uint16_t crcIn, uint8_t byte)
{
	uint32_t crc = crcIn;
	uint32_t in = byte | 0x100;
	
	do
	{
		crc <<= 1;
		in <<= 1;
		if(in & 0x100)
			++crc;
		if(crc & 0x10000)
			crc ^= 0x1021;
	}
	
	while(!(in & 0x10000));
	
	return crc & 0xffffu;
}


/**
* @brief  Cal CRC16 for YModem Packet
* @param  data
* @param  length
* @retval None
*/
uint16_t Cal_CRC16(const uint8_t* data, uint32_t size)
{
	uint32_t crc = 0;
	const uint8_t* dataEnd = data+size;
	
	while(data < dataEnd)
		crc = UpdateCRC16(crc, *data++);
	
	crc = UpdateCRC16(crc, 0);
	crc = UpdateCRC16(crc, 0);
	
	return crc&0xffffu;
}

/**
* @brief  Cal Check sum for YModem Packet
* @param  data
* @param  length
* @retval None
*/
uint8_t CalChecksum(const uint8_t* data, uint32_t size)
{
	uint32_t sum = 0;
	const uint8_t* dataEnd = data+size;
	
	while(data < dataEnd )
		sum += *data++;
	
	return (sum & 0xffu);
}

/**
* @brief  Transmit a file using the ymodem protocol
* @param  buf: Address of the first byte
* @retval The size of the file
*/
uint8_t Ymodem_Transmit ( void )
{
	uint8_t *buf;
	const uint8_t* sendFileName = "mcu_fw_ver.bin";
	
	uint32_t sizeFile = 0x1A000;//remain flash size to user flash sector
	
	uint8_t packet_data[PACKET_1K_SIZE + PACKET_OVERHEAD];
	uint8_t filename[FILE_NAME_LENGTH];
	uint8_t *buf_ptr, tempCheckSum;
	uint16_t tempCRC;
	uint16_t blkNumber;
	uint8_t receivedC[2], CRC16_F = 0, i;
	uint32_t errors, ackReceived, size = 0, pktSize;

	sizeFile = ota_fw_upgrade_state.total_len - 0x1C000; //(0x20000-0x4000 = 0x1C000);

	errors = 0;
	ackReceived = 0;
	for (i = 0; i < (FILE_NAME_LENGTH - 1); i++)
	{
		filename[i] = sendFileName[i];
	}
	CRC16_F = 1;

	buf = (uint8_t *)(get_ds_location() + 0x1C000);//( FW_UPGRADE_FLASH_SIZE - 0x4000 ) >> 1
	WICED_BT_TRACE("buf addr:0x%x sizeFile:0x%x\n",buf,sizeFile);

	wiced_hal_wdog_disable();
	while(1)
	{
		wiced_hal_puart_read(&receivedC[0]);
		if(receivedC[0] == 's')
		{
			break;
		}
	}
	wiced_hal_wdog_restart();
	WICED_BT_TRACE("Received 's'\n");
	delay_ms(1000);

	/* Prepare first block */
	Ymodem_PrepareIntialPacket(&packet_data[0], filename, &sizeFile);
	
	do 
	{
		/* Send Packet */
		//Ymodem_SendPacket(packet_data, PACKET_SIZE + PACKET_HEADER);
		WICED_BT_TRACE("%d go1\n",__LINE__);
		puart_write_buffer(packet_data, PACKET_SIZE + PACKET_HEADER);
		/* Send CRC or Check Sum based on CRC16_F */
		if (CRC16_F)
		{
			tempCRC = Cal_CRC16(&packet_data[3], PACKET_SIZE);

			Send_Byte(tempCRC >> 8);
			Send_Byte(tempCRC & 0xFF);
		}
		else
		{
			tempCheckSum = CalChecksum (&packet_data[3], PACKET_SIZE);
			Send_Byte(tempCheckSum);
		}
		
		/* Wait for Ack and 'C' */
		while(!wiced_hal_puart_read(&receivedC[0]));
		
		if (receivedC[0] == ACK)
		{ 
			/* Packet transferred correctly */
			ackReceived = 1;
		}
		else
		{
			errors++;
		}
		
	}while (!ackReceived && (errors < YMODEM_ERROR_CNT));
	
	if (errors >= YMODEM_ERROR_CNT)
	{
		WICED_BT_TRACE("%d error:%d\n",__LINE__,errors);
		return errors;
	}
	buf_ptr = buf;
	size = sizeFile;
	blkNumber = 0x01;
	/* Here 1024 bytes package is used to send the packets */
	
	/* Resend packet if NAK  for a count of 10 else end of communication */
	while (size)
	{
		/* Prepare next packet */
		Ymodem_PreparePacket(buf_ptr, &packet_data[0], blkNumber, size);
		ackReceived = 0;
		receivedC[0]= 0;
		errors = 0;
		do
		{
			/* Send next packet */
			if (size >= PACKET_1K_SIZE)
			{
				pktSize = PACKET_1K_SIZE;
				
			}
			else
			{
				pktSize = PACKET_SIZE;
			}
			//Ymodem_SendPacket(packet_data, pktSize + PACKET_HEADER);

			puart_write_buffer(packet_data, pktSize + PACKET_HEADER);
#if 0
			WICED_BT_TRACE("2: buf_ptr:0x%x [3]:%x [4]:%x [5]:%x [6]:%x [7]:%x [8]:%x [9]:%x [10]:%x len:%x\n",
				buf_ptr,packet_data[3],packet_data[4],packet_data[5],packet_data[6],packet_data[7],
				packet_data[8],packet_data[9],packet_data[10],pktSize + PACKET_HEADER);
#endif
			/* Send CRC or Check Sum based on CRC16_F */
			if (CRC16_F)
			{
				tempCRC = Cal_CRC16(&packet_data[3], pktSize);
				
				Send_Byte(tempCRC >> 8);
				Send_Byte(tempCRC & 0xFF);
			}
			else
			{
				tempCheckSum = CalChecksum (&packet_data[3], pktSize);
				Send_Byte(tempCheckSum);
			}
			while(!wiced_hal_puart_read(&receivedC[0]));

			if (receivedC[0] == ACK)
			{
				ackReceived = 1;  
				if (size > pktSize)
				{
					buf_ptr += pktSize;  
					size -= pktSize;
					//if (blkNumber == (USER_FLASH_SIZE/1024))
					{
						//return 0xFF; /*  error */
					}
					//else
					{
						blkNumber++;
					}
				}
				else
				{
					buf_ptr += pktSize;
					size = 0;
				}
			}
			else
			{
				errors++;
			}
		}
		while(!ackReceived && (errors < YMODEM_ERROR_CNT));
		/* Resend packet if NAK  for a count of 10 else end of communication */
		
		if (errors >= YMODEM_ERROR_CNT)
		{
			WICED_BT_TRACE("%d error:%d\n",__LINE__,errors);
			return errors;
		}
		
	}
	
	ackReceived = 0;
	receivedC[0] = 0x00;
	errors = 0;
	do 
	{
		Send_Byte(EOT);
		/* Send (EOT); */
		/* Wait for Ack */
		while(!wiced_hal_puart_read(&receivedC[0]));
		WICED_BT_TRACE("%d go2 [0x%x]\n",__LINE__,receivedC[0]);
		if ( receivedC[0] == ACK )
		{
			ackReceived = 1;  
		}
		else
		{
			errors++;
		}
	}while (!ackReceived && (errors < YMODEM_ERROR_CNT));
	
	if (errors >= YMODEM_ERROR_CNT)
	{
		WICED_BT_TRACE("%d error:%d\n",__LINE__,errors);
		return errors;
	}

	/* Last packet preparation */
	ackReceived = 0;
	receivedC[0] = 0x00;
	errors = 0;
	
	packet_data[0] = SOH;
	packet_data[1] = 0;
	packet_data[2] = 0xFF;
	
	for (i = PACKET_HEADER; i < (PACKET_SIZE + PACKET_HEADER); i++)
	{
		packet_data [i] = 0x00;
	}
	
	do 
	{
		/* Send Packet */
		//Ymodem_SendPacket(packet_data, PACKET_SIZE + PACKET_HEADER);
		puart_write_buffer(packet_data, PACKET_SIZE + PACKET_HEADER);
#if 0
		WICED_BT_TRACE("3: [4]:%d [5]:%d [6]:%d [7]:%d [8]:%d [9]:%d [10]:%d len:%d\n",
			packet_data[4],packet_data[5],packet_data[6],packet_data[7],
			packet_data[8],packet_data[9],packet_data[10],pktSize + PACKET_HEADER);
#endif
		/* Send CRC or Check Sum based on CRC16_F */
		uint8_t u8_byte[2] = {0,};
				
		tempCRC = Cal_CRC16(&packet_data[3], PACKET_SIZE);
		
		u8_byte[0] = tempCRC >> 8;
		u8_byte[1] = tempCRC & 0xFF;
	
		puart_write_buffer(u8_byte, 2);
		
		/* Wait for Ack and 'C' */
		while(!wiced_hal_puart_read(&receivedC[0]));
		WICED_BT_TRACE("%d go2 [0x%x]\n",__LINE__,receivedC[0]);

		if (receivedC[0] == ACK)
		{ 
			/* Packet transferred correctly */
			ackReceived = 1;
		}
		else
		{
			errors++;
		}
	}while (!ackReceived && (errors < YMODEM_ERROR_CNT));
	
	/* Resend packet if NAK  for a count of 10  else end of communication */
	if (errors >= YMODEM_ERROR_CNT)
	{
		WICED_BT_TRACE("%d error:%d\n",__LINE__,errors);
		return errors;
	}  

#if 0
	ackReceived = 0;
	receivedC[0] = 0x00;
	errors = 0;
	WICED_BT_TRACE("%d ready to send EOT\n",__LINE__);
	do 
	{
		Send_Byte(EOT);
		/* Send (EOT); */
		/* Wait for Ack */
		while(!wiced_hal_puart_read(&receivedC[0]));
		WICED_BT_TRACE("%d go2 [0x%x]\n",__LINE__,receivedC[0]);
		if ( receivedC[0] == ACK )
		{
			ackReceived = 1;  
		}
		else
		{
			errors++;
		}
	}while (!ackReceived && (errors < YMODEM_ERROR_CNT));
	
	if (errors >= YMODEM_ERROR_CNT)
	{
		WICED_BT_TRACE("%d error:%d\n",__LINE__,errors);
		return errors;
	}
#endif
	return 0; /* file transmitted successfully */
}
