#include "global_define.h"

#include "uart.h"
#include "wiced_bt_trace.h"
#ifdef ENABLE_MCU_MSG_QUEUE

void push_mcu_msg( uint8_t *buf, uint8_t length )
{
    WICED_BT_TRACE("push_mcu_queue \n");

    //memcpy(&g_str_msg_q.ui8_msg_buffer[g_str_msg_q.ui8_tail];
    int i = 0;
    for( i = 0; i < length; i++ )
    {
        g_str_msg_q.ui8_msg_buffer[g_str_msg_q.ui8_tail].ui8_data[i] = buf[i];
    }

    g_str_msg_q.ui8_msg_len[g_str_msg_q.ui8_tail++] = length;

    g_str_msg_q.ui8_tail %= MSG_QUEUE_SIZE;

    if( g_str_msg_q.ui8_head == g_str_msg_q.ui8_tail )
    {
        //overflow
        // vip push
        // etc ignore
        //if ( ignore flag ) return;

        g_str_msg_q.ui8_head++;
        g_str_msg_q.ui8_head %= MSG_QUEUE_SIZE;
    }

    WICED_BT_TRACE("head : %d, tail : %d Command:0x%2x\n", g_str_msg_q.ui8_head, g_str_msg_q.ui8_tail,buf[1]);
}

void pop_mcu_msg( void )
{
    WICED_BT_TRACE("pop_mcu_queue \n");

    while( g_str_msg_q.ui8_head != g_str_msg_q.ui8_tail )    
    {
        g_str_msg_q.ui8_msg_len[g_str_msg_q.ui8_head];

    	puart_write_buffer((uint8_t *)g_str_msg_q.ui8_msg_buffer[g_str_msg_q.ui8_head].ui8_data, g_str_msg_q.ui8_msg_len[g_str_msg_q.ui8_head]);

        g_str_msg_q.ui8_head++;
        g_str_msg_q.ui8_head %= MSG_QUEUE_SIZE;

        WICED_BT_TRACE("head : %d, tail : %d Command:0x%2x\n", g_str_msg_q.ui8_head, g_str_msg_q.ui8_tail,g_str_msg_q.ui8_msg_buffer[g_str_msg_q.ui8_head].ui8_data[1]);
    }

    WICED_BT_TRACE("mcu_queue empty\n");

    memset(&g_str_msg_q, 0, sizeof(mcu_msg_queue_t));

}

void init_mcu_msg( void )
{
	memset(&g_str_msg_q, 0, sizeof(mcu_msg_queue_t));
}
#endif

