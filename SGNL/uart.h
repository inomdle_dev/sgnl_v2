
#ifndef __UART_H__
#define __UART_H__

#include "wiced_bt_types.h"

#define UART_BUFF_SIZE 	(48)
#define UART_DATA_SIZE	(16)

#define UART_STX			(0xFB)
#define UART_ETX			(0xFA)
#define UART_ID_CYW		(0x00)
#define UART_ID_STM		(0x01)
#define UART_ID_ECHO		(0x10)

typedef struct
{
    uint8_t buffer[UART_BUFF_SIZE];
    uint16_t head;
    uint16_t tail;
} uart_buffer_t;

uart_buffer_t uart_buffer;

void init_puart_port(void);
void puart_write_buffer(uint8_t* p_data, uint32_t len);
uint8_t get_puart_protocol_data(uint8_t* p_data);
extern void puart_data_proc(void);

#endif
