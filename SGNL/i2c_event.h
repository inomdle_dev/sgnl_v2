#ifndef _I2C_EVENT_H_
#define _I2C_EVENT_H_

void i2c_init(void);

uint8_t read_byte(uint8_t slave, uint8_t page_address);
void i2c_read_multi_byte(uint8_t addr, uint8_t page_address, uint8_t *bytes, uint8_t length);
void write_byte(uint8_t addr, uint8_t page_address, uint8_t value);
void i2c_write_multi_byte(uint8_t addr, uint8_t page_address, uint8_t *bytes, uint8_t length);

#endif //_I2C_EVENT_H_

