/**
  ******************************************************************************
  * @file    STM32F4xx_IAP/inc/menu.h 
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    10-October-2011
  * @brief   This file provides all the headers of the menu functions.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MENU_H
#define __MENU_H

/* Includes ------------------------------------------------------------------*/
#include "flash_if.h"
#include <stdarg.h>
#include <stdio.h>

/* Private variables ---------------------------------------------------------*/
typedef  void (*pFunction)(void);

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void Main_Menu(void);



#define USER_RAM		(uint32_t)0x20006000
extern uint32_t *pFlashAdd;
extern uint32_t *pRamAdd;

struct BYTE8
{         
	uint16_t     end_of_file:1;             
	uint16_t     bit1:1;             
	uint16_t     bit2:1;             
	uint16_t     bit3:1;             
	uint16_t     bit4:1;             
	uint16_t     bit5:1;             
	uint16_t     bit6:1;             
	uint16_t     bit7:1;             
}; 

union BYTE_DEF
{
	uint16_t    all;
	struct BYTE8 bit;
};

struct LONG32 
{
	uint16_t    low16:16;
	uint16_t    high16:16;
};

union DIVIDE_LONG
{
	uint32_t    all;
	struct LONG32 Word; 
};

struct HEX_FIELD
{							
	uint16_t DataLength;
	union DIVIDE_LONG Address; //uint32_t			 
	uint16_t RecordType;
	uint16_t Checksum;
	union BYTE_DEF Status; //uint16_t, use 8bit
};

extern volatile struct HEX_FIELD DownLoadingHex;
extern uint32_t fw_update(void);
extern void execute_application(void);
extern void execute_application_addr(uint32_t start_Addr);

#endif  /* __MENU_H */

/*******************(C)COPYRIGHT 2011 STMicroelectronics *****END OF FILE******/
