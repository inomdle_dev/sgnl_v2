#ifndef GLOBAL_DEFINE_H
#define GLOBAL_DEFINE_H

/* Base address of the Flash sectors */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_SEC_END      ((uint32_t)0x08080000) /* Base @ of Sector 7, 128 Kbytes */

#define	FLASH_ADDR_START_APP1		ADDR_FLASH_SECTOR_4
#define	FLASH_ADDR_END_APP1		ADDR_FLASH_SECTOR_6

#define	FLASH_ADDR_BACKUP_APP		ADDR_FLASH_SECTOR_6

/* pre define module */
#define ENABLE_BT_UART
//#define ENABLE_LED

/* include header files */
#include <stdio.h>
#include <string.h>
#include "stm32f4xx.h"
#include "common.h"
#include "flash_if.h"
#include "menu.h"
#include "ymodem.h"

#ifdef ENABLE_BT_UART
#include "bt_uart_event.h"
#endif //ENABLE_BT_UART

#ifdef ENABLE_LED
#include "led_event.h"
#include "timer_event.h"
#endif //ENABLE_LED
#include "pin_enable.h"
#include "struct.h"

#endif //GLOBAL_DEFINE_H
