#ifndef _BT_UART_EVENT_H_
#define _BT_UART_EVENT_H_

/* USART peripheral configuration defines */
#if 0
#define BT_UART					USART2
#define BT_UART_CLK				RCC_APB1Periph_USART2

#define BT_UART_GPIO_PORT			GPIOA
#define BT_UART_GPIO_CLK			RCC_AHB1Periph_GPIOA
#define BT_UART_AF				GPIO_AF_USART2

#define BT_UART_TX_PIN				GPIO_Pin_2
#define BT_UART_TX_SOURCE			GPIO_PinSource2

#define BT_UART_RX_PIN				GPIO_Pin_3
#define BT_UART_RX_SOURCE			GPIO_PinSource3
#else
#define BT_UART					USART1
#define BT_UART_CLK				RCC_APB2Periph_USART1
#define BT_UART_IRQn				USART1_IRQn

#define BT_UART_GPIO_PORT			GPIOA
#define BT_UART_GPIO_CLK			RCC_AHB1Periph_GPIOA
#define BT_UART_AF				GPIO_AF_USART1

#define BT_UART_TX_PIN				GPIO_Pin_9
#define BT_UART_TX_SOURCE			GPIO_PinSource9

#define BT_UART_RX_PIN				GPIO_Pin_10
#define BT_UART_RX_SOURCE			GPIO_PinSource10

#define BT_UART_BAUDRATE			115200//115200*6
#define BT_UART_BUFFERSIZE			2048//256
#endif

extern void bt_uart_init( void );
void bt_uart_config( void );
extern void ble_uart_send( uint8_t* ui8_byte, uint16_t ui16_length );
void ble_uart_send_data( uint8_t byte );

#endif //_BT_UART_EVENT_H_
