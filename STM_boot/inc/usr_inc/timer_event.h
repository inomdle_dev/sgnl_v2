#ifndef TIMER_EVENT_H
#define TIMER_EVENT_H

#define LED_TIM_IRQ		TIM1_CC_IRQn
#define LED_TIM_RCC		RCC_APB2Periph_TIM1
#define LED_TIM_BASE		TIM1

void timer_init( void );

void pattern_timer_config( void );
void pattern_timer_stop( void );
void pattern_timer_start( void );

#define pattern_timer_isr		TIM1_CC_IRQHandler

#endif //TIMER_EVENT_H
