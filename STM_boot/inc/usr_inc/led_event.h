#ifndef LED_EVENT_H
#define LED_EVENT_H

#include <stdint.h>

#define LED1_TIM			TIM2
#define LED1_GPIO_PORT			GPIOA
#define LED1_GPIO_PIN			GPIO_Pin_5
#define LED1_GPIO_RCC			RCC_AHB1Periph_GPIOA
#define LED1_PWM_GPIO_SOURCE		GPIO_PinSource5
#define LED1_PWM_GPIO_RCC		RCC_APB1Periph_TIM2
#define LED1_CCR			LED1_TIM->CCR1

#define LED2_TIM			TIM3
#define LED2_GPIO_PORT			GPIOA
#define LED2_GPIO_PIN			GPIO_Pin_6
#define LED2_GPIO_RCC			RCC_AHB1Periph_GPIOA
#define LED2_PWM_GPIO_SOURCE		GPIO_PinSource6
#define LED2_PWM_GPIO_RCC		RCC_APB1Periph_TIM3
#define LED2_CCR			LED2_TIM->CCR1

#define LED3_TIM			TIM3
#define LED3_GPIO_PORT			GPIOA
#define LED3_GPIO_PIN			GPIO_Pin_7
#define LED3_GPIO_RCC			RCC_AHB1Periph_GPIOA
#define LED3_PWM_GPIO_SOURCE		GPIO_PinSource7
#define LED3_PWM_GPIO_RCC		RCC_APB1Periph_TIM3
#define LED3_CCR			LED3_TIM->CCR2


#define LED4_TIM			TIM3
#define LED4_GPIO_PORT			GPIOB
#define LED4_GPIO_PIN			GPIO_Pin_0
#define LED4_GPIO_RCC			RCC_AHB1Periph_GPIOB
#define LED4_PWM_GPIO_SOURCE		GPIO_PinSource0
#define LED4_PWM_GPIO_RCC		RCC_APB1Periph_TIM3
#define LED4_CCR			LED4_TIM->CCR3


#define LED5_TIM			TIM3
#define LED5_GPIO_PORT			GPIOB
#define LED5_GPIO_PIN			GPIO_Pin_1
#define LED5_GPIO_RCC			RCC_AHB1Periph_GPIOB
#define LED5_PWM_GPIO_SOURCE		GPIO_PinSource1
#define LED5_PWM_GPIO_RCC		RCC_APB1Periph_TIM3
#define LED5_CCR			LED5_TIM->CCR4

#define PATTERN_FW_UPGRADE_CNT		300

extern void led_init( void );
void led_config_gpio( void );

void led_config_pwm_output( void );

void pwm_pulse_led_all(uint16_t ui16_pulse);
void pwm_off( void );
void pwm_enable( void );
void pwm_disable( void );

void pwm_pulse_led1(uint16_t ui16_pulse);
void pwm_pulse_led2(uint16_t ui16_pulse);
void pwm_pulse_led3(uint16_t ui16_pulse);
void pwm_pulse_led4(uint16_t ui16_pulse);
void pwm_pulse_led5(uint16_t ui16_pulse);

void set_pwm( uint16_t index, uint16_t value );

void fw_upgrade_progress(void);

#endif //LED_EVENT_H