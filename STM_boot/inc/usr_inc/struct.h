#ifndef STRUCT_H
#define STRUCT_H

#include "global_define.h"

#ifdef ENABLE_LED
typedef struct timer_struct_{
	
	volatile uint16_t ui16_pattern_cnt;
	volatile uint16_t ui16_sub_pattern_cnt;
}timer_struct_t;
extern timer_struct_t g_str_timer;

typedef struct pwm_struct_{

        volatile uint16_t ui16_dimming_flag;
        volatile uint16_t ui16_dimming_data;
        volatile uint16_t ui16_pwm;

}pwm_struct_t;
extern pwm_struct_t g_str_led[5];
#endif


#endif //STRUCT_H
