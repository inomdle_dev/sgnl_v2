#include "global_define.h"

#ifdef ENABLE_LED
void timer_init(void)
{
	pattern_timer_config();
	pattern_timer_start();
}

/* Timer Configuration
* Use Timer 1
*/
void pattern_timer_config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Enable the TIM2 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = LED_TIM_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	TIM_TimeBaseInitTypeDef TIM_BaseStruct;
	
	TIM_DeInit(LED_TIM_BASE);
	
	/* TIM1 clock enable */
	RCC_APB2PeriphClockCmd(LED_TIM_RCC, ENABLE);
	/* Time base configuration */
	TIM_BaseStruct.TIM_Period = (SystemCoreClock / 10000) - 1; // N (us)
	TIM_BaseStruct.TIM_Prescaler = 100; // 50 MHz Clock down to 1 MHz (adjust per your clock)
	TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(LED_TIM_BASE, &TIM_BaseStruct);
	
	/* TIM IT enable */
	TIM_ITConfig(LED_TIM_BASE, TIM_IT_CC1, ENABLE);
}

void pattern_timer_stop(void)
{
	pwm_disable();
	TIM_Cmd(LED_TIM_BASE, DISABLE);
}

void pattern_timer_start(void)
{
	TIM_Cmd(LED_TIM_BASE, ENABLE);
	pwm_enable();
}

void pattern_timer_isr(void)
{
	if (TIM_GetITStatus(LED_TIM_BASE, TIM_IT_CC1) != RESET)
	{
		TIM_ClearITPendingBit(LED_TIM_BASE, TIM_IT_CC1);
		fw_upgrade_progress();
		g_str_timer.ui16_pattern_cnt--;
	}
}
#endif