#include "global_define.h"

#ifdef ENABLE_STANDBY

void enter_standby_mode(void)
{
	//power off sequence
	if( g_str_timer.ui16_bt_power_off_cnt == 100 )
	{
		BT_MFB_DISABLE;
	}
	
	else if( g_str_timer.ui16_bt_power_off_cnt == 200 )
	{
		BT_NRST_DISABLE;
	}
	
	else if( g_str_timer.ui16_bt_power_off_cnt == 250 )
	{
#ifdef ENABLE_BLE_UART
		ble_command_send_data(MCU_STATUS, MCU_SLEEP);
#endif
	}
	
	else if( g_str_timer.ui16_bt_power_off_cnt >= 300 )
	{
#ifdef ENABLE_SIMPLE_BT
		if( (simple_status == SIMPLE_BT_OFF) && (!g_str_timer.ui16_mfb_cnt) )
		{
#ifdef ENABLE_BLE_UART
			USART_ITConfig(BLE_UART, USART_IT_RXNE, DISABLE);
			USART_Cmd(BLE_UART, DISABLE);
#endif
#ifdef ENABLE_BT_UART
			USART_ITConfig(BT_UART, USART_IT_RXNE, DISABLE);
			USART_Cmd(BT_UART, DISABLE);
#endif
			BT_SystemStatus = BT_SYSTEM_POWER_OFF;
			g_str_bitflag.b1_power_down = 0;
			g_str_timer.ui16_bt_power_off_cnt = 0;
			PWR_HOLD_DISABLE;
			PWR_EnterSTANDBYMode();
		}
#else
#ifdef ENABLE_IS206x
		if(BTAPP_GetStatus() == BT_STATUS_OFF)
#endif
		{
#ifdef ENABLE_BLE_UART
			USART_ITConfig(BLE_UART, USART_IT_RXNE, DISABLE);
			USART_Cmd(BLE_UART, DISABLE);
#endif
#ifdef ENABLE_BT_UART
			USART_ITConfig(BT_UART, USART_IT_RXNE, DISABLE);
			USART_Cmd(BT_UART, DISABLE);
#endif
			g_str_bitflag.b1_power_down = 0;
			g_str_timer.ui16_bt_power_off_cnt = 0;
			
			PWR_HOLD_DISABLE;
			BLE_TX_FLAG_DISABLE;
			/* Go to standby mode */
			PWR_EnterSTANDBYMode();
		}
#endif
	}
}

uint16_t loop = 0;
#ifdef ENABLE_STANDBY
void wakeup_isr(void)
{
	if (EXTI_GetITStatus(EXTI_Line0) != RESET)
	{
#ifdef ENABLE_SIMPLE_BT
		if( ( simple_status == SIMPLE_BT_OFF ) || ( BT_CallStatus == BT_CALL_IDLE ) )
#else
		if( ( BT_SystemStatus != BT_SYSTEM_CONNECTED )
			|| ( BT_CallStatus == BT_CALL_IDLE ) )
#endif
		{
			BLE_TX_FLAG_ENABLE;
			loop++;
			SystemInit();
			struct_init();
			
			PWR_HOLD_ENABLE;
			
			BT_MFB_DISABLE;
#ifdef ENABLE_SIMPLE_BT
			BT_NRST_ENABLE;
#else
			BT_NRST_DISABLE;
#endif
			BTAPP_Init();
#ifdef ENABLE_BLE_UART
			USART_Cmd(BLE_UART, ENABLE);
			USART_ITConfig(BLE_UART, USART_IT_RXNE, ENABLE);
			ble_command_send_data(MCU_STATUS, MCU_READY);
			//pin_enable_ble_gpio();
#endif
#ifdef ENABLE_BT_UART
			USART_Cmd(BT_UART, ENABLE);
			USART_ITConfig(BT_UART, USART_IT_RXNE, ENABLE);
#endif
#ifdef ENABLE_AUDIO
			SPI_I2S_ITConfig(AUDIO_I2S_TX, SPI_I2S_IT_TXE, ENABLE);
			SPI_I2S_ITConfig(AUDIO_I2S_RX, SPI_I2S_IT_RXNE, ENABLE);
			SPI_I2S_ITConfig(AUDIO_I2S_RX, SPI_I2S_IT_TIFRFE, ENABLE);
			audio_rx_start();
#endif
			
#ifdef ENABLE_FAST_CALL
			if( (BT_SystemStatus != BT_SYSTEM_CONNECTED)
			   && (BT_LinkbackStatus != BT_LINK_CONNECTED) )
			{
				BTAPP_TaskReq( BT_REQ_SYSTEM_ON );
			}
#endif
			
#ifdef ENABLE_SIMPLE_BT
			g_str_timer.ui16_mfb_cnt = 90;//90 �̸� �ȵ�
			g_str_bitflag.b1_bt_conn = 1;
			g_str_bitflag.b2_bt_on_off = 1;
#endif
		}
		
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
}
#endif

void set_sleepMode( void )
{
#ifdef ENABLE_STANDBY
	AMP_STATUS_DISABLE;
#ifdef ENABLE_AUDIO
	I2S_Cmd(AUDIO_I2S_TX, DISABLE);
	I2S_Cmd(AUDIO_I2S_RX, DISABLE);
#endif	
	//g_str_bitflag.b1_power_mode_change = 1;

	g_str_bitflag.b1_power_down = 1;
	g_str_timer.ui16_bt_power_off_cnt = 0;
	g_str_timer.ui16_sleep_cnt = 0;
#ifdef ENABLE_IS206x
#ifdef ENABLE_SIMPLE_BT
	if(simple_status != SIMPLE_BT_OFF)
		bt_simple_power_off();
#else
	BTAPP_TaskReq(BT_REQ_SYSTEM_OFF);
#endif
#endif

#else
	AMP_STATUS_DISABLE;
	
	/* Disable I2C first */
	//SENSORS_I2C->CR1 &= ~I2C_CR1_PE;
	
	//goto sleep
	PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
	
	/* After wakeup, call system init to enable PLL as clock source */
	SystemInit();
	
	/* Enable I2C */
	//SENSORS_I2C->CR1 |= I2C_CR1_PE;
#endif
}

#endif //ENABLE_STANDBY
