#include "global_define.h"
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern pFunction Jump_To_Application;
extern uint32_t JumpAddress;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

#ifdef ENABLE_LED
timer_struct_t g_str_timer;
pwm_struct_t g_str_led[5];
#endif

void reset_supervisor(void)
{
	if(RCC_GetFlagStatus(RCC_FLAG_PORRST))//power on reset
	{
		RCC_ClearFlag();
	}
	else if(RCC_GetFlagStatus(RCC_FLAG_SFTRST))
	{
		RCC_ClearFlag();
		//ready to FW update
		pin_enable_init();
		
#ifdef ENABLE_LED
		timer_init();
		led_init();
#endif

#ifdef ENABLE_BT_UART
		bt_uart_init();
#endif
	
		int32_t ret = 0;
		uint8_t ui8_packet;
		uint32_t write_data[2] = {0,};
		uint32_t target_addr = 0;

		ret = fw_update();
		if( ret <= 0 )//fail
		{
			if (ret == -1)
			{
				serial_printf("\n\n\rThe image size is higher than the allowed space memory!\n\r");
				ui8_packet = 'a';
			}
			else if (ret == -2)
			{
				serial_printf("\n\n\rVerification failed!\n\r");
				ui8_packet = 'b';
			}
			else if (ret == -3)
			{
				serial_printf("\r\n\nAborted by user.\n\r");
				ui8_packet = 'c';
			}
			else //size == 0
			{
				serial_printf("\n\rFailed to receive the file!\n\r");
				ui8_packet = 'd';
			}
			
			write_data[1] = 0xAA55AA55;
		}
		else//success
		{
			ui8_packet = 's';
			write_data[1] = FLASH_ADDR_BACKUP_APP;
		}
		
#ifdef ENABLE_LED
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
		pattern_timer_stop();
#endif
		
		//write user_app1 address at flash sector1
		FLASH_If_SectorErase(ADDR_FLASH_SECTOR_1);//erase ota address
		target_addr = ADDR_FLASH_SECTOR_1;
		if (((*(__IO uint32_t*)FLASH_ADDR_START_APP1) & 0x2FFE0000 ) == 0x20000000)
		{
			write_data[0] = FLASH_ADDR_START_APP1;
		}
		
		flash_write_32(target_addr, write_data, 0, sizeof(write_data));
		if( !FLASH_If_GetWriteProtectionStatus(ADDR_FLASH_SECTOR_1) )
			FLASH_If_DisableWriteProtection(ADDR_FLASH_SECTOR_1);

		ble_uart_send(&ui8_packet, 1);
		ble_uart_send(&ui8_packet, 1);
		ble_uart_send(&ui8_packet, 1);
	}
	else if(RCC_GetFlagStatus(RCC_FLAG_PINRST))//mcu is upgrading abnormal routine
	{
		uint32_t read_addr = *(__IO uint32_t*) (ADDR_FLASH_SECTOR_6);
		uint32_t flash_data[4] = {0,};
		int32_t i = 0;
		if(read_addr != 0xFFFFFFFF)
		{
			for(i = 0; i<0x3000; i++)//backup sector6,7 -> user app sector4,5
			{
				flash_read_32(ADDR_FLASH_SECTOR_6, flash_data, i, sizeof(flash_data));
				flash_write_32(ADDR_FLASH_SECTOR_4, flash_data, i, sizeof(flash_data));
			}
			if( !FLASH_If_GetWriteProtectionStatus(ADDR_FLASH_SECTOR_4) )
				FLASH_If_DisableWriteProtection(ADDR_FLASH_SECTOR_4);
			if( !FLASH_If_GetWriteProtectionStatus(ADDR_FLASH_SECTOR_5) )
				FLASH_If_DisableWriteProtection(ADDR_FLASH_SECTOR_5);

			FLASH_If_SectorErase(ADDR_FLASH_SECTOR_6);
			FLASH_If_SectorErase(ADDR_FLASH_SECTOR_7);
		}
		//RCC_ClearFlag();
	}
	else if(RCC_GetFlagStatus(RCC_FLAG_IWDGRST))
	{
		RCC_ClearFlag();
	}
	else if(RCC_GetFlagStatus(RCC_FLAG_WWDGRST))
	{
		RCC_ClearFlag();
	}
	else if(RCC_GetFlagStatus(RCC_FLAG_LPWRRST))
	{
		RCC_ClearFlag();
	}
}

int main( void )
{
	//check user application or FW update
	uint32_t jump_addr = 0;

	//when reset event is occured
	reset_supervisor();

	jump_addr = *(__IO uint32_t*) (ADDR_FLASH_SECTOR_1);
	if(jump_addr == FLASH_ADDR_START_APP1)
	{
		execute_application_addr(jump_addr);
	}
	else if (jump_addr == 0xFFFFFFFF ) //no
	{
		if (((*(__IO uint32_t*)FLASH_ADDR_START_APP1) & 0x2FFE0000 ) == 0x20000000)
		{ 
			jump_addr = FLASH_ADDR_START_APP1;
		}
		
		if( jump_addr != 0xFFFFFFFF )
		{
			execute_application_addr(jump_addr);
		}
	}
	return 0;
}

void HardFault_Handler(void)
{
	SCB->AIRCR = (0x05FA0000) | 0x04;
}
