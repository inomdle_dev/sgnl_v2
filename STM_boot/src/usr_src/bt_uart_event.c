
#include "global_define.h"

#ifdef ENABLE_BT_UART

void bt_uart_init(void)
{
	bt_uart_config();
}

/* UART Configuration
* TxD  PA2
* RxD  PA3
* async. mode
* baudrate 115200
* Word Length = 8 Bits
* one Stop Bit
* No parity
* Hardware flow control disabled (RTS and CTS signals)
* Receive and transmit enabled
*/
void bt_uart_config(void)
{
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	USART_DeInit(BT_UART);	
	
	/* Enable GPIO clock */
	RCC_AHB1PeriphClockCmd(BT_UART_GPIO_CLK, ENABLE);
	
	/* Enable USART clock */
	RCC_APB2PeriphClockCmd(BT_UART_CLK, ENABLE);
	
	/* Connect USART pins to AF7 */
	GPIO_PinAFConfig(BT_UART_GPIO_PORT, BT_UART_TX_SOURCE, BT_UART_AF);
	GPIO_PinAFConfig(BT_UART_GPIO_PORT, BT_UART_RX_SOURCE, BT_UART_AF);
	
	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	
	GPIO_InitStructure.GPIO_Pin = BT_UART_TX_PIN;
	GPIO_Init(BT_UART_GPIO_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = BT_UART_RX_PIN;
	GPIO_Init(BT_UART_GPIO_PORT, &GPIO_InitStructure);
	
	/* USART2 configuration ----------------------------------------------------*/
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(BT_UART, &USART_InitStructure);

#if 0
	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = BT_UART_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Enable USART */
	USART_ITConfig(BT_UART, USART_IT_RXNE, ENABLE);
#endif
	USART_Cmd(BT_UART, ENABLE);
}
#endif //ENABLE_BT_UART


void ble_uart_send_data( uint8_t byte )
{
	USART_SendData(BT_UART, byte);
	
	while (USART_GetFlagStatus(BT_UART, USART_FLAG_TXE) == RESET)
	{}
}

void ble_uart_send( uint8_t* ui8_byte, uint16_t ui16_length )
{
	for( int i = 0; i < ui16_length; i++ )
	{
		ble_uart_send_data(ui8_byte[i]);
	}
}
