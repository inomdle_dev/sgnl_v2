#include "global_define.h"

void pin_enable_init(void)
{
	pin_enable_gpio();
}

/* POWER Configuration
* Power  PA8
*/
void pin_enable_gpio(void)
{
	GPIO_InitTypeDef power_gpio_config = {GPIO_POWER_HOLD_PIN, GPIO_Mode_OUT,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_DOWN};

	RCC_AHB1PeriphClockCmd(GPIO_POWER_HOLD_RCC, ENABLE);
	GPIO_Init(GPIO_POWER_HOLD_PORT, &power_gpio_config);
	
	POWER_HOLD_ENABLE;
}
