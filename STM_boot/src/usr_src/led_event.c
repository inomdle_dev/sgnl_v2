#include "global_define.h"

#ifdef ENABLE_LED
static const GPIO_InitTypeDef LED1_GpioConfiguration  =  {LED1_GPIO_PIN, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
static const GPIO_InitTypeDef LED2_GpioConfiguration  =  {LED2_GPIO_PIN, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
static const GPIO_InitTypeDef LED3_GpioConfiguration  =  {LED3_GPIO_PIN, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
static const GPIO_InitTypeDef LED4_GpioConfiguration  =  {LED4_GPIO_PIN, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};
static const GPIO_InitTypeDef LED5_GpioConfiguration  =  {LED5_GPIO_PIN, GPIO_Mode_AF,  GPIO_Speed_2MHz, GPIO_OType_PP, GPIO_PuPd_UP};

void ( *pwm_pulse_led[5] )( uint16_t ) = { pwm_pulse_led5, pwm_pulse_led4, pwm_pulse_led3, pwm_pulse_led2, pwm_pulse_led1 };


void led_init( void )
{
	led_config_pwm_output();
}

void led_config_pwm_output( void )
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	
	pwm_disable();

	/* GPIOA,B clock enable */
	RCC_AHB1PeriphClockCmd(LED1_GPIO_RCC | LED4_GPIO_RCC, ENABLE);

	GPIO_PinAFConfig(LED1_GPIO_PORT, LED1_PWM_GPIO_SOURCE, GPIO_AF_TIM2);
	GPIO_Init(LED1_GPIO_PORT, (GPIO_InitTypeDef *)&LED1_GpioConfiguration );
	
	GPIO_PinAFConfig(LED2_GPIO_PORT, LED2_PWM_GPIO_SOURCE, GPIO_AF_TIM3);
	GPIO_Init(LED2_GPIO_PORT, (GPIO_InitTypeDef *)&LED2_GpioConfiguration );
	
	GPIO_PinAFConfig(LED3_GPIO_PORT, LED3_PWM_GPIO_SOURCE, GPIO_AF_TIM3);
	GPIO_Init(LED3_GPIO_PORT, (GPIO_InitTypeDef *)&LED3_GpioConfiguration );
	
	GPIO_PinAFConfig(LED4_GPIO_PORT, LED4_PWM_GPIO_SOURCE, GPIO_AF_TIM3);
	GPIO_Init(LED4_GPIO_PORT, (GPIO_InitTypeDef *)&LED4_GpioConfiguration );

	GPIO_PinAFConfig(LED5_GPIO_PORT, LED5_PWM_GPIO_SOURCE, GPIO_AF_TIM3);
	GPIO_Init(LED5_GPIO_PORT, (GPIO_InitTypeDef *)&LED5_GpioConfiguration );

	/* TIM2 clock enable */
	RCC_APB1PeriphClockCmd(LED1_PWM_GPIO_RCC, ENABLE);
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 100-1;
	TIM_TimeBaseStructure.TIM_Prescaler = 25-1;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(LED1_TIM, &TIM_TimeBaseStructure);

	/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(LED4_PWM_GPIO_RCC, ENABLE);
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 100-1;
	TIM_TimeBaseStructure.TIM_Prescaler = 25-1;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(LED4_TIM, &TIM_TimeBaseStructure);

	/* LED 1 */
	/* PWM1 Mode configuration: Channel1 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_Pulse = 100;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;//The output phase, TIM_OCNPolarity_High output from the inverting
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	TIM_OC1Init(LED1_TIM, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(LED1_TIM, TIM_OCPreload_Enable);
	
	/* LED 4 */
	/* PWM1 Mode configuration: Channel1 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_Pulse = 100;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;//The output phase, TIM_OCNPolarity_High output from the inverting
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	TIM_OC1Init(LED2_TIM, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(LED2_TIM, TIM_OCPreload_Enable);
	TIM_OC2Init(LED3_TIM, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(LED3_TIM, TIM_OCPreload_Enable);
	TIM_OC3Init(LED4_TIM, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(LED4_TIM, TIM_OCPreload_Enable);
	TIM_OC4Init(LED5_TIM, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(LED5_TIM, TIM_OCPreload_Enable);
	
	pwm_enable();
}

void pwm_off( void )
{
	LED1_CCR = 100;
	LED2_CCR = 100;
	LED3_CCR = 100;
	LED4_CCR = 100;
	LED5_CCR = 100;
}

void pwm_enable( void )
{
	/* TIM2 enable counter */
	TIM_OC1PreloadConfig(LED1_TIM, TIM_OCPreload_Enable);
	TIM_OC1PreloadConfig(LED2_TIM, TIM_OCPreload_Enable);
	TIM_OC2PreloadConfig(LED3_TIM, TIM_OCPreload_Enable);
	TIM_OC3PreloadConfig(LED4_TIM, TIM_OCPreload_Enable);
	TIM_OC4PreloadConfig(LED5_TIM, TIM_OCPreload_Enable);

	TIM_Cmd(LED1_TIM, ENABLE);
	TIM_Cmd(LED2_TIM, ENABLE);
	TIM_Cmd(LED3_TIM, ENABLE);
	TIM_Cmd(LED4_TIM, ENABLE);
	TIM_Cmd(LED5_TIM, ENABLE);
	
	TIM_CtrlPWMOutputs(LED1_TIM, ENABLE);
	TIM_CtrlPWMOutputs(LED2_TIM, ENABLE);
	TIM_CtrlPWMOutputs(LED3_TIM, ENABLE);
	TIM_CtrlPWMOutputs(LED4_TIM, ENABLE);
	TIM_CtrlPWMOutputs(LED5_TIM, ENABLE);

	pwm_off();
}

void pwm_disable( void )
{
	pwm_off();
	
	/* TIM2 enable counter */
	TIM_Cmd(LED1_TIM, DISABLE);
	TIM_Cmd(LED2_TIM, DISABLE);
	TIM_Cmd(LED3_TIM, DISABLE);
	TIM_Cmd(LED4_TIM, DISABLE);
	TIM_Cmd(LED5_TIM, DISABLE);
	
	TIM_CtrlPWMOutputs(LED1_TIM, DISABLE);
	TIM_CtrlPWMOutputs(LED2_TIM, DISABLE);
	TIM_CtrlPWMOutputs(LED3_TIM, DISABLE);
	TIM_CtrlPWMOutputs(LED4_TIM, DISABLE);
	TIM_CtrlPWMOutputs(LED5_TIM, DISABLE);
	
	TIM_OC1PreloadConfig(LED1_TIM, TIM_OCPreload_Disable);
	TIM_OC1PreloadConfig(LED2_TIM, TIM_OCPreload_Disable);
	TIM_OC2PreloadConfig(LED3_TIM, TIM_OCPreload_Disable);
	TIM_OC3PreloadConfig(LED4_TIM, TIM_OCPreload_Disable);
	TIM_OC4PreloadConfig(LED5_TIM, TIM_OCPreload_Disable);
}

void pwm_pulse_led1(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		LED1_CCR = 100 - ui16_pulse;
	}
}

void pwm_pulse_led2(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		LED2_CCR = 100 - ui16_pulse;
	}
}

void pwm_pulse_led3(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		LED3_CCR = 100 - ui16_pulse;
	}
}

void pwm_pulse_led4(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		LED4_CCR = 100 - ui16_pulse;
	}
}

void pwm_pulse_led5(uint16_t ui16_pulse)
{
	if( ui16_pulse <= 100 )
	{
		LED5_CCR = 100 - ui16_pulse;
	}
}

static void add_pwm( uint16_t index, uint16_t add_val )
{
	pwm_pulse_led[index](g_str_led[index].ui16_pwm);

	g_str_led[index].ui16_pwm += add_val;
	if( g_str_led[index].ui16_pwm > 100 )
	{
		g_str_led[index].ui16_pwm = 100;
	}
}

void set_pwm( uint16_t index, uint16_t value )
{
	pwm_pulse_led[index](value);
}

void fw_upgrade_progress(void)
{
	//led control
	if( g_str_timer.ui16_pattern_cnt >= PATTERN_FW_UPGRADE_CNT )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FW_UPGRADE_CNT - 50 )
	{
		add_pwm(0, 2);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FW_UPGRADE_CNT - 100 )
	{
		set_pwm(0, 0);
		add_pwm(1, 2);
		set_pwm(2, 0);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FW_UPGRADE_CNT - 150 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		add_pwm(2, 2);
		set_pwm(3, 0);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FW_UPGRADE_CNT - 200 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		add_pwm(3, 2);
		set_pwm(4, 0);
	}
	else if( g_str_timer.ui16_pattern_cnt > PATTERN_FW_UPGRADE_CNT - 250 )
	{
		set_pwm(0, 0);
		set_pwm(1, 0);
		set_pwm(2, 0);
		set_pwm(3, 0);
		add_pwm(4, 2);
	}
	else if( g_str_timer.ui16_pattern_cnt == 0 )
	{
		for( int i = 0; i < 5; i++ )
		{
			g_str_led[i].ui16_pwm = 0;
			pwm_pulse_led[i](0);
		}
		g_str_timer.ui16_pattern_cnt = PATTERN_FW_UPGRADE_CNT;
	}
}
#endif //ENABLE_LED
