/**
******************************************************************************
* @file    STM32F4xx_IAP/src/flash_if.c 
* @author  MCD Application Team
* @version V1.0.0
* @date    10-October-2011
* @brief   This file provides all the memory related operation functions.
******************************************************************************
* @attention
*
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
* TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
* DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
* FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
* CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*
* <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
******************************************************************************
*/ 

/** @addtogroup STM32F4xx_IAP
* @{
*/

/* Includes ------------------------------------------------------------------*/
#include "flash_if.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static uint32_t GetSector(uint32_t Address);

/* Private functions ---------------------------------------------------------*/

/**
* @brief  Unlocks Flash for write access
* @param  None
* @retval None
*/
void FLASH_If_Init(void)
{ 
	FLASH_Unlock(); 
	
	/* Clear pending flags (if any) */  
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
			FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
}

uint32_t FLASH_If_RangeErase(uint32_t Start, uint32_t End)
{
	uint32_t StartSector = 0;
	uint32_t EndSector = 0;
	uint32_t i =0;
	
	/* Get the sector where start the user flash area */
	StartSector = GetSector(Start);
	EndSector = GetSector(End);
	
	for(i = StartSector; i <= EndSector; i += 8)
	{
		/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
		be done by word */ 
		if (FLASH_EraseSector(i, VoltageRange_3) != FLASH_COMPLETE)
		{
			/* Error occurred while page erase */
			return (1);
		}
	}
	
	return (0);
}

uint32_t FLASH_If_SectorErase(uint32_t addr)
{
	uint32_t ChoosingSector = GetSector(addr);
	
	FLASH_If_Init();

	/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
	be done by word */ 
	if (FLASH_EraseSector(ChoosingSector, VoltageRange_3) != FLASH_COMPLETE)
	{
		/* Error occurred while page erase */
		return (1);
	}

	if( !FLASH_If_GetWriteProtectionStatus(addr) )
		FLASH_If_DisableWriteProtection(addr);
	
	FLASH_Lock();
	
	return (0);
}

/**
* @brief  This function does an erase of all user flash area
* @param  StartSector: start of user flash area
* @retval 0: user flash area successfully erased
*         1: error occurred
*/
uint32_t FLASH_If_Erase(void)
{
	uint32_t UserStartSector = FLASH_Sector_1, i = 0;
	
	for(i = UserStartSector; i <= FLASH_Sector_7; i += 8)
	{
		/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
		be done by word */ 
		if (FLASH_EraseSector(i, VoltageRange_3) != FLASH_COMPLETE)
		{
			/* Error occurred while page erase */
			return (1);
		}
	}
	
	return (0);
}

/**
* @brief  This function writes a data buffer in flash (data are 32-bit aligned).
* @note   After writing data buffer, the flash content is checked.
* @param  FlashAddress: start address for writing data buffer
* @param  Data: pointer on data buffer
* @param  DataLength: length of data buffer (unit is 32-bit word)   
* @retval 0: Data successfully written to Flash memory
*         1: Error occurred while writing data in Flash memory
*         2: Written Data in flash memory is different from expected one
*/
uint32_t FLASH_If_Write1616(__IO uint32_t* FlashAddress, uint16_t* Data ,uint32_t DataLength)
{
	uint32_t i = 0;
	
	for (i = 0; (i < DataLength) && (*FlashAddress <= (USER_FLASH_END_ADDRESS-4)); i++)
	{
		/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
		be done by word */ 
		if (FLASH_ProgramHalfWord(*FlashAddress, *(uint16_t*)(Data+i)) == FLASH_COMPLETE)
		{
			/* Check the written value */
			if (*(uint16_t*)*FlashAddress != *(uint16_t*)(Data+i))
			{
				/* Flash content doesn't match SRAM content */
				return(2);
			}
			/* Increment FLASH destination address */
			*FlashAddress += 2;
		}
		else
		{
			/* Error occurred while writing data in Flash memory */
			return (1);
		}
	}
	
	return (0);
}

/**
* @brief  This function writes a data buffer in flash (data are 32-bit aligned).
* @note   After writing data buffer, the flash content is checked.
* @param  FlashAddress: start address for writing data buffer
* @param  Data: pointer on data buffer
* @param  DataLength: length of data buffer (unit is 32-bit word)   
* @retval 0: Data successfully written to Flash memory
*         1: Error occurred while writing data in Flash memory
*         2: Written Data in flash memory is different from expected one
*/
uint32_t FLASH_If_Write16(__IO uint32_t* FlashAddress, uint32_t* Data ,uint32_t DataLength)
{
	uint32_t i = 0;
	
	for (i = 0; (i < DataLength) && (*FlashAddress <= (USER_FLASH_END_ADDRESS-4)); i++)
	{
		/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
		be done by word */ 
		if (FLASH_ProgramHalfWord(*FlashAddress, *(uint16_t*)(Data+i)) == FLASH_COMPLETE)
		{
			/* Check the written value */
			if (*(uint16_t*)*FlashAddress != *(uint16_t*)(Data+i))
			{
				/* Flash content doesn't match SRAM content */
				return(2);
			}
			/* Increment FLASH destination address */
			*FlashAddress += 2;
		}
		else
		{
			/* Error occurred while writing data in Flash memory */
			return (1);
		}
	}
	
	return (0);
}

/**
* @brief  This function writes a data buffer in flash (data are 32-bit aligned).
* @note   After writing data buffer, the flash content is checked.
* @param  FlashAddress: start address for writing data buffer
* @param  Data: pointer on data buffer
* @param  DataLength: length of data buffer (unit is 32-bit word)   
* @retval 0: Data successfully written to Flash memory
*         1: Error occurred while writing data in Flash memory
*         2: Written Data in flash memory is different from expected one
*/
uint32_t FLASH_If_Write(__IO uint32_t* FlashAddress, uint32_t* Data ,uint32_t DataLength)
{
	uint32_t i = 0;

	FLASH_Unlock();
	for (i = 0; (i < DataLength) && (*FlashAddress <= (USER_FLASH_END_ADDRESS-4)); i++)
	{
		/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
		be done by word */ 
		if (FLASH_ProgramWord(*FlashAddress, *(uint32_t*)(Data+i)) == FLASH_COMPLETE)
		{
			/* Check the written value */
			if (*(uint32_t*)*FlashAddress != *(uint32_t*)(Data+i))
			{
				/* Flash content doesn't match SRAM content */
				return(2);
			}
			/* Increment FLASH destination address */
			*FlashAddress += 4;
		}
		else
		{
			/* Error occurred while writing data in Flash memory */
			return (1);
		}
	}
	FLASH_Lock();
	
	return (0);
}

/**
* @brief  Returns the write protection status of user flash area.
* @param  None
* @retval 0: Some sectors inside the user flash area are write protected
*         1: No write protected sectors inside the user flash area
*/
uint16_t FLASH_If_GetWriteProtectionStatus(uint32_t addr)
{
	uint32_t UserStartSector = 0;
	
	/* Get the sector where start the user flash area */
	UserStartSector = GetSector(addr);
	
	/* Check if there are write protected sectors inside the user flash area */
	if ((FLASH_OB_GetWRP() >> (UserStartSector/8)) == (0xFFF >> (UserStartSector/8)))
	{ /* No write protected sectors inside the user flash area */
		return 1;
	}
	else
	{ /* Some sectors inside the user flash area are write protected */
		return 0;
	}
}

/**
* @brief  Disables the write protection of user flash area.
* @param  None
* @retval 1: Write Protection successfully disabled
*         2: Error: Flash write unprotection failed
*/
uint32_t FLASH_If_DisableWriteProtection(uint32_t addr)
{
	__IO uint32_t UserStartSector = 0, UserWrpSectors = 0;

	/* Get the sector where start the user flash area */
	UserStartSector = GetSector(addr);
	
	switch(UserStartSector)
	{
		case FLASH_Sector_0:	UserWrpSectors = OB_WRP_Sector_0;	break;
		case FLASH_Sector_1:	UserWrpSectors = OB_WRP_Sector_1;	break;
		case FLASH_Sector_2:	UserWrpSectors = OB_WRP_Sector_2;	break;
		case FLASH_Sector_3:	UserWrpSectors = OB_WRP_Sector_3;	break;
		case FLASH_Sector_4:	UserWrpSectors = OB_WRP_Sector_4;	break;
		case FLASH_Sector_5:	UserWrpSectors = OB_WRP_Sector_5;	break;
		case FLASH_Sector_6:	UserWrpSectors = OB_WRP_Sector_6;	break;
		case FLASH_Sector_7:	UserWrpSectors = OB_WRP_Sector_7;	break;
		case FLASH_Sector_8:	UserWrpSectors = OB_WRP_Sector_8;	break;
		case FLASH_Sector_9:	UserWrpSectors = OB_WRP_Sector_9;	break;
		case FLASH_Sector_10:	UserWrpSectors = OB_WRP_Sector_10;	break;
		case FLASH_Sector_11:	UserWrpSectors = OB_WRP_Sector_11;	break;
		case FLASH_Sector_12:	UserWrpSectors = OB_WRP_Sector_12;	break;
		case FLASH_Sector_13:	UserWrpSectors = OB_WRP_Sector_13;	break;
		case FLASH_Sector_14:	UserWrpSectors = OB_WRP_Sector_14;	break;
		case FLASH_Sector_15:	UserWrpSectors = OB_WRP_Sector_15;	break;
		case FLASH_Sector_16:	UserWrpSectors = OB_WRP_Sector_16;	break;
		case FLASH_Sector_17:	UserWrpSectors = OB_WRP_Sector_17;	break;
		case FLASH_Sector_18:	UserWrpSectors = OB_WRP_Sector_18;	break;
		case FLASH_Sector_19:	UserWrpSectors = OB_WRP_Sector_19;	break;
		case FLASH_Sector_20:	UserWrpSectors = OB_WRP_Sector_20;	break;
		case FLASH_Sector_21:	UserWrpSectors = OB_WRP_Sector_21;	break;
		case FLASH_Sector_22:	UserWrpSectors = OB_WRP_Sector_22;	break;
		case FLASH_Sector_23:	UserWrpSectors = OB_WRP_Sector_23;	break;
	}
	
	/* Mark all sectors inside the user flash area as non protected */  
	UserWrpSectors = 0xFFF-((1 << (UserStartSector/8))-1);
	
	/* Unlock the Option Bytes */
	FLASH_OB_Unlock();
	
	/* Disable the write protection for all sectors inside the user flash area */
	FLASH_OB_WRPConfig(UserWrpSectors, DISABLE);
	
	/* Start the Option Bytes programming process. */  
	if (FLASH_OB_Launch() != FLASH_COMPLETE)
	{
		/* Error: Flash write unprotection failed */
		return (2);
	}
	
	/* Lock the Option Bytes */
	FLASH_OB_Lock();
	
	/* Write Protection successfully disabled */
	return (1);
}

void flash_read_32(uint32_t st_addr, uint32_t *variable, uint32_t interval, uint32_t size)
{
	uint32_t uwAddress = 0;
	uint32_t start_addr = st_addr;
	uint32_t var_size = size;
	uint32_t i = 0;
	uint32_t number = (var_size / sizeof(uint32_t));

	uwAddress = start_addr + (interval * var_size);

	/* Check if the programmed data is OK ***************************************/
	/*  MemoryProgramStatus = 0: data programmed correctly
	MemoryProgramStatus != 0: number of words not programmed correctly */

	for (i = 0; i<number; i++)
	{
		variable[i] = *(__IO uint32_t*)uwAddress;
		uwAddress += sizeof(uint32_t);
	}
}

void flash_write_32(uint32_t st_addr, uint32_t *variable, uint32_t interval, uint32_t size)
{
	uint32_t uwAddress = 0;
	uint32_t start_addr = st_addr;
	uint32_t var_size = size;
	uint32_t i = 0;
	uint32_t number = (var_size / sizeof(uint32_t));
        //__IO uint32_t SectorsWRPStatus;

	uwAddress = start_addr + (interval * var_size);

	/* Unlock the Flash *********************************************************/
	/* Enable the flash control register access */
	FLASH_Unlock();
        
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) */
        for (i = 0; i<number; i++)
        {
                if (FLASH_ProgramWord(uwAddress, variable[i]) == FLASH_COMPLETE)
                        uwAddress += sizeof(uint32_t);
                else;
        }

	FLASH_Lock();
}

void flash_erase(uint32_t start_sec, uint32_t end_sec)
{
	uint32_t uwStartSector = 0;
	uint32_t uwEndSector = 0;
	uint32_t i = 0;

	/* Get the number of the start and end sectors */
	uwStartSector = GetSector(start_sec);
	uwEndSector = GetSector(end_sec);

	/* Unlock the Flash *********************************************************/
	/* Enable the flash control register access */
	FLASH_Unlock();

	/* Erase the user Flash area ************************************************/
	/* area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR */

	/* Clear pending flags (if any) */
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
			FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

	/* Strat the erase operation */
	for (i = uwStartSector; i <= uwEndSector; i += 8)
	{
		/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
		be done by word */
		if (FLASH_EraseSector(i, VoltageRange_3) != FLASH_COMPLETE)
		{
			/* Error occurred while sector erase.
			User can add here some code to deal with this error  */
#ifdef ENABLE_DBG
			dbg_print("FLASH_EraseSector error!!\n");
#endif
			break;		
		}
	}

	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) */
	FLASH_Lock();
}

/**
* @brief  Gets the sector of a given address
* @param  Address: Flash address
* @retval The sector of a given address
*/
static uint32_t GetSector(uint32_t Address)
{
	uint32_t sector = 0;
	
	if((Address < ADDR_FLASH_SECTOR_1) && (Address >= ADDR_FLASH_SECTOR_0))			sector = FLASH_Sector_0;  
	else if((Address < ADDR_FLASH_SECTOR_2) && (Address >= ADDR_FLASH_SECTOR_1))		sector = FLASH_Sector_1;  
	else if((Address < ADDR_FLASH_SECTOR_3) && (Address >= ADDR_FLASH_SECTOR_2))		sector = FLASH_Sector_2;  
	else if((Address < ADDR_FLASH_SECTOR_4) && (Address >= ADDR_FLASH_SECTOR_3))		sector = FLASH_Sector_3;  
	else if((Address < ADDR_FLASH_SECTOR_5) && (Address >= ADDR_FLASH_SECTOR_4))		sector = FLASH_Sector_4;  
	else if((Address < ADDR_FLASH_SECTOR_6) && (Address >= ADDR_FLASH_SECTOR_5))		sector = FLASH_Sector_5;  
	else if((Address < ADDR_FLASH_SECTOR_7) && (Address >= ADDR_FLASH_SECTOR_6))		sector = FLASH_Sector_6;  
	else/*(Address < FLASH_END_ADDR) && (Address >= ADDR_FLASH_SECTOR_11))*/		sector = FLASH_Sector_7;  
	return sector;
}

/**
* @}
*/

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
