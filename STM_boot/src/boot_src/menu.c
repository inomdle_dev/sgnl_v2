/**
******************************************************************************
* @file    STM32F4xx_IAP/src/menu.c 
* @author  MCD Application Team
* @version V1.0.0
* @date    10-October-2011
* @brief   This file provides the software which contains the main menu routine.
*          The main menu gives the options of:
*             - downloading a new binary file, 
*             - uploading internal flash memory,
*             - executing the binary file already loaded 
*             - disabling the write protection of the Flash sectors where the 
*               user loads his binary file.
******************************************************************************
* @attention
*
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
* TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
* DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
* FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
* CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*
* <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
******************************************************************************
*/ 

/** @addtogroup STM32F4xx_IAP
* @{
*/
#define ENABLE_HEX_DOWN
//#define TEST_W_EUM

/* Includes ------------------------------------------------------------------*/
#include "global_define.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
pFunction Jump_To_Application;
uint32_t JumpAddress;
__IO uint32_t FlashProtection = 0;
uint8_t tab_1024[1024] = {0,};
uint8_t FileName[FILE_NAME_LENGTH];

/* Private function prototypes -----------------------------------------------*/
int SerialDownload(void);
void SerialUpload(void);

/* Private functions ---------------------------------------------------------*/

uint32_t fw_update(void)
{
	uint32_t flash_data[4] = {0,};
	uint32_t i = 0;
	
	/* Download user application in the Flash */
	int32_t Size = 0;
	uint8_t ui8_packet = 's';
	
	FLASH_If_SectorErase(ADDR_FLASH_SECTOR_6);
	FLASH_If_SectorErase(ADDR_FLASH_SECTOR_7);

	for(i = 0; i<0x3000; i++)// user app sector4,5 -> backup sector6,7
	{
		flash_read_32(ADDR_FLASH_SECTOR_4, flash_data, i, sizeof(flash_data));
		flash_write_32(ADDR_FLASH_SECTOR_6, flash_data, i, sizeof(flash_data));
	}
	if( !FLASH_If_GetWriteProtectionStatus(ADDR_FLASH_SECTOR_6) )
		FLASH_If_DisableWriteProtection(ADDR_FLASH_SECTOR_6);
	if( !FLASH_If_GetWriteProtectionStatus(ADDR_FLASH_SECTOR_7) )
		FLASH_If_DisableWriteProtection(ADDR_FLASH_SECTOR_7);

	ble_uart_send(&ui8_packet, 1);
	
	Size = Ymodem_Receive(&tab_1024[0]);
	
	if( Size <= 0 ) //fail
	{
		for(i = 0; i<0x3000; i++)//backup sector6,7 -> user app sector4,5
		{
			flash_read_32(ADDR_FLASH_SECTOR_6, flash_data, i, sizeof(flash_data));
			flash_write_32(ADDR_FLASH_SECTOR_4, flash_data, i, sizeof(flash_data));
		}
		if( !FLASH_If_GetWriteProtectionStatus(ADDR_FLASH_SECTOR_4) )
			FLASH_If_DisableWriteProtection(ADDR_FLASH_SECTOR_4);
		if( !FLASH_If_GetWriteProtectionStatus(ADDR_FLASH_SECTOR_5) )
			FLASH_If_DisableWriteProtection(ADDR_FLASH_SECTOR_5);
	}
	else
	{
		serial_printf("\n\n\r Programming Completed Successfully!\n\r--------------------------------\r\n");
	}
	
	FLASH_If_SectorErase(ADDR_FLASH_SECTOR_6);
	FLASH_If_SectorErase(ADDR_FLASH_SECTOR_7);
	
	return Size;
}

/**
* @brief  Download a file via serial port
* @param  None
* @retval None
*/
int SerialDownload(void)
{
	int32_t Size = 0;
	
	Size = Ymodem_Receive(&tab_1024[0]);
	if (Size > 0)
	{
		serial_printf("\n\n\r Programming Completed Successfully!\n\r--------------------------------\r\n");
	}
	else if (Size == -1)
	{
		serial_printf("\n\n\rThe image size is higher than the allowed space memory!\n\r");
	}
	else if (Size == -2)
	{
		serial_printf("\n\n\rVerification failed!\n\r");
	}
	else if (Size == -3)
	{
		serial_printf("\r\n\nAborted by user.\n\r");
	}
	else //size == 0
	{
		serial_printf("\n\rFailed to receive the file!\n\r");
	}
	
	return Size;
}

/**
* @brief  Upload a file via serial port.
* @param  None
* @retval None
*/
void SerialUpload(void)
{
	uint8_t status = 0 ; 
	
	serial_printf("\n\n\rSelect Receive File\n\r");
	
	if (GetKey() == CRC16)
	{
		/* Transmit the flash image through ymodem protocol */
		status = Ymodem_Transmit((uint8_t*)APPLICATION_ADDRESS, (const uint8_t*)"UploadedFlashImage.bin", USER_FLASH_SIZE);
		
		if (status != 0) 
		{
			serial_printf("\n\rError Occurred while Transmitting File\n\r");
		}
		else
		{
			serial_printf("\n\rFile uploaded successfully \n\r");
		}
	}
}

void print_menu(void)
{
	/* Test if any sector of Flash memory where user application will be loaded is write protected */
	if (FLASH_If_GetWriteProtectionStatus(ADDR_FLASH_SECTOR_1) == 0)   
	{
		FlashProtection = 1;
	}
	else
	{
		FlashProtection = 0;
	}
	
	serial_printf("\r\n================== Main Menu ============================\r\n\n");
	serial_printf("  Download Image To the STM32F4xx Internal Flash ------- 1\r\n\n");
	serial_printf("  Upload Image From the STM32F4xx Internal Flash ------- 2\r\n\n");
	serial_printf("  Execute The New Program ------------------------------ 3\r\n\n");
	
	if(FlashProtection != 0)
	{
		serial_printf("  Disable the write protection ------------------------- 4\r\n\n");
	}
	
#ifdef ENABLE_HEX_DOWN
	serial_printf("  Download .Hex file To the STM32F4xx Internal Flash --- 5\r\n\n");
#endif
	serial_printf("  Erase Flash ------------------------------------------ 6\r\n\n");
	serial_printf("==========================================================\r\n\n");
}

void serial_printf(char *Form, ... )
{
#if 0
	char Buff[128] = {0,};
	va_list ArgPtr;
	va_start(ArgPtr,Form);
	vsprintf(Buff, Form, ArgPtr);
	va_end(ArgPtr);
	SerialPutString(Buff);
#endif
}

#ifdef ENABLE_HEX_DOWN
volatile struct HEX_FIELD DownLoadingHex;

uint32_t *pFlashAdd;
uint32_t *pRamAdd;

void SetUserHEXFlashadd(void)
{
	pFlashAdd = (uint32_t *)ADDR_FLASH_SECTOR_7;
}

uint16_t g_cnt = 0;
uint16_t LoadFlashData(void)
{
	static uint16_t Flag = 0;
	uint16_t Data;

	if(Flag == 0)
	{
		Data = ((uint16_t)*pFlashAdd >> 8) & 0xff;
		Flag = 1;
	}
	else
	{
		Data = (uint16_t)*pFlashAdd & 0xff;
		Flag = 0;
		pFlashAdd = (uint32_t *)(APPLICATION_BACKUP_ADDRESS+(2*++g_cnt));
	}

	return Data;
}

char Convert_HEX_AtoI(char Ascii)
{
	if(Ascii >= '0' && Ascii <= '9')return(Ascii-'0');
	else if(Ascii >= 'a' && Ascii<= 'f')return(Ascii-'a'+10);
	else if(Ascii >= 'A' && Ascii <= 'F')return(Ascii-'A'+10);
	else return(0xFF);
}

uint16_t DownHEXFrom( uint16_t NumByte, uint16_t Source)
{
	uint16_t Value, i, Shift_Count;
	char Rcvdata;

	Value = 0;

	Shift_Count = NumByte;

	for ( i = 0; i < NumByte; i++ ) 
	{
		if(Source == 0)
	        	Rcvdata = USART_ReceiveData(BT_UART);
		else
			Rcvdata = LoadFlashData();
		
		Value |= (uint16_t)(Convert_HEX_AtoI( Rcvdata ) << ((--Shift_Count) * 4));
//		Value |= Convert_HEX_AtoI( Rcvdata ) << ( ( (NumByte -1)-i ) * 4 );
	}

	 //check sum 부분
	if( NumByte == 4 )
	{
	    DownLoadingHex.Checksum += (( Value >> 8 ) & 0xff); 
	    DownLoadingHex.Checksum += (Value & 0xff);
	}
	else  // 2바이트일 경우
	{
	    DownLoadingHex.Checksum += (Value & 0xff);
	}

	return Value;
}

uint16_t DownUserProgfrom(uint16_t Source)
{
	int i;
	uint16_t CheckSum;
	uint16_t DataBuffer;
	uint16_t data_size = 0;
	uint32_t temp_buf = 0;
	uint16_t Status = 0;
	
	pRamAdd = (uint32_t *)USER_RAM;
	while(DownLoadingHex.Status.bit.end_of_file==0)
	{  
		//FLASH
		while((i = LoadFlashData()) != ':')
		{
			if(i == 'F')
			{
				serial_printf("\n\r  Flash Invalid!! \n\r");
				return 0;
			}
		}
		
		DownLoadingHex.Checksum = CheckSum = 0;
		
		// hex 포맷의 datalength
		DownLoadingHex.DataLength = DownHEXFrom(2, Source);
		// 32비트 어드레스 상위 16비트 지정
		DownLoadingHex.Address.Word.low16 = DownHEXFrom(4, Source);
		// Data Type
		DownLoadingHex.RecordType = DownHEXFrom(2, Source);
		
		switch (DownLoadingHex.RecordType) 
		{
			
			/*
			'00'       Data Record
			'0l'        End of File Record
			'02'       Extended Segment Address Record
			'03'       Start Segment Address Record
			'04'       Extended Linear Address Record
			'05'       Start Linear Address Record
			*/
			
			// '00'       Data Record
		case 0x00:
			// data lenth *2 = datalenth in byte
			if(DownLoadingHex.Address.all >= 0x08004158)
			{
				DownLoadingHex.Address.all = DownLoadingHex.Address.all;
			}
			
			for( i = 0; i < DownLoadingHex.DataLength; i += 2)
			{
				if( ( DownLoadingHex.DataLength - i ) == 1 )
				{
					DataBuffer = DownHEXFrom(2, Source);
					serial_printf("\n\rHEX converting Error. it is NOT a 16bit data hex\n\r");
				}
				else 
					DataBuffer = DownHEXFrom(4, Source); // 16 bit hex
				
				*(uint16_t *)DownLoadingHex.Address.all = DataBuffer;
				DownLoadingHex.Address.all ++;
				uint32_t temp = ((DataBuffer & 0xff00) >> 8) | ((DataBuffer & 0xff) << 8);
				
				if( !(data_size++ % 2) )
				{
					temp_buf = temp;
				}
				else
				{
					temp_buf |= (temp << 16);
					*pRamAdd = temp_buf;
					pRamAdd++;
				}
			}
			break;

			// '01'        End of File Record
		case 0x01:
			DownLoadingHex.Status.bit.end_of_file = 1;
			break;
						
			// '02'       Extended Segment Address Record	
		case 0x02: //not use
			break;
			//'03'       Start Segment Address Record	
		case 0x03: //not use
			break;
			
			//      Extended Linear Address 
		case 0x04: //use in 32bit addressing
			//상위 16비트 재지정
			DownLoadingHex.Address.Word.high16 = DownHEXFrom(4, Source);
			break;
			
			// '05'       Start Linear Address Record
		case 0x05: //32 bit end address
			DownLoadingHex.Address.Word.high16 = DownHEXFrom(4, Source);
			DownLoadingHex.Address.Word.low16 = DownHEXFrom(4, Source);
			break;
		}
		CheckSum = ((~(DownLoadingHex.Checksum)) + 1) & 0xff; // 2' complement.
		if( CheckSum != DownHEXFrom(2, Source) )
		{
			serial_printf("\n\rCheckSumError");
			return 0;
		}
		
#ifndef TEST_W_EUM
		USART_SendData(BT_UART, '.'); //Dot per one HEX line
#endif
	}
	
	if(DownLoadingHex.Status.bit.end_of_file)
	{
		FLASH_If_Init();
		if(FLASH_If_SectorErase(ADDR_FLASH_SECTOR_7) != 0)
		{
			FLASH_Lock();
			serial_printf("\n\r  Flash Erase Error!! [%s-%s]\n\r",__FILE__,__LINE__);
			return -1;
		}
		else
		{
			serial_printf("\n\r  Flash Erase complete!! ");
		}
		
		pFlashAdd = (uint32_t *)APPLICATION_ADDRESS;
		pRamAdd = (uint32_t *)USER_RAM;

		Status = FLASH_If_Write( (uint32_t *)&pFlashAdd, (uint32_t*) pRamAdd, (uint16_t) data_size/2);
		if(Status != 0)
		{
			FLASH_Lock();
			serial_printf("\n\r  Flash Write Error!! [%s-%s]\n\r",__FILE__,__LINE__);
			return -1;
		}
		else
		{
			serial_printf("\n\r  Flash Write complete!! ");
		}
		
		FLASH_Lock();
	}
	
	return 1;
}

void InitStruct_HexDown(void)
{
	memset((void *)&DownLoadingHex, 0x00, sizeof(DownLoadingHex));
}

void DownFromFlash(void)
{
	SetUserHEXFlashadd();
	InitStruct_HexDown();
	
	if(DownUserProgfrom(1)) 
		serial_printf("\n\r  DownLoading Success !!");
	else 
	{
		serial_printf("\n\r  DownLoading Failure !!"); 
		FLASH_If_Init();
		FLASH_If_SectorErase(APPLICATION_ADDRESS);
		FLASH_If_SectorErase(APPLICATION_BACKUP_ADDRESS);
		FLASH_Lock();
	}
}

void USARTtoFLASH(void)
{
	char RcvData[2];
	uint16_t Header_ignore;
	uint16_t WriteData;
	uint16_t HEXBuf[80];		
	uint16_t ByteCount;
	
	uint16_t Loop;
	uint16_t Status;
	uint32_t SizeofData; //offset_address
		
	serial_printf("\n\r  Send User Program *.Hex\n\r");
	
	pRamAdd   = (uint32_t *)USER_RAM;
	SizeofData = 0;
	
	while((RcvData[0] = USART_ReceiveData(BT_UART))!=':');
	Header_ignore=0;
	ByteCount=0;
	
	for(;;)
	{	
		if (Header_ignore)
		{
			RcvData[0] = USART_ReceiveData(BT_UART);
		}
		else
		{
			RcvData[0] = ':';
			Header_ignore=1;
		}
				
		RcvData[1] = USART_ReceiveData(BT_UART);
		
		WriteData = ((RcvData[0] << 8) + RcvData[1]);
		*(pRamAdd + SizeofData++) = WriteData;
		
		//end of file check
		if(RcvData[0] == ':')
		{
#ifndef TEST_W_EUM
			USART_SendData(BT_UART,'.');
#endif
			ByteCount = 0;
			HEXBuf[ByteCount++] = RcvData[0];
			HEXBuf[ByteCount++] = RcvData[1];
		}
		else
		{
			HEXBuf[ByteCount++] = RcvData[0];
			HEXBuf[ByteCount++] = RcvData[1];
#ifdef DEBUG_FLASHDOWN
			if(RcvData[1] == 0x0d)	serial_printf(",");
#endif
		}
		
		if(ByteCount == 12)
		{
			ByteCount = 0;
			//end of file
			// :00000001FF
			if(HEXBuf[0] == ':')
			if(HEXBuf[1] == '0')
			if(HEXBuf[2] == '0')
			if(HEXBuf[3] == '0')
			if(HEXBuf[4] == '0')
			if(HEXBuf[5] == '0')
			if(HEXBuf[6] == '0')
			if(HEXBuf[7] == '0')
			if(HEXBuf[8] == '1')
			if(HEXBuf[9] == 'F')
			if(HEXBuf[10] == 'F')
				break;
		}
	}
	
	serial_printf("\n\r  %ld Word Download Complete!!\n\r", SizeofData);
	serial_printf("\n\r  %ld Word Download Complete!!\n\r", SizeofData);
	
	// flash erase
	     if(SizeofData > 0x0007C000)	Loop = 7;
	else if(SizeofData > 0x0005C000)	Loop = 6;
	else if(SizeofData > 0x0003C000)	Loop = 5;
	else if(SizeofData > 0x0001C000)	Loop = 4;
	else if(SizeofData > 0x0000C000)	Loop = 3;
	else if(SizeofData > 0x00008000)	Loop = 2;
	else if(SizeofData > 0x00004000)	Loop = 1;

	/* Unlock the Flash Program Erase controller */
	FLASH_If_Init();

	if (Loop == 0) //1 경우 
	{
		serial_printf("\n\r  Erase Flash Sector %d", Loop+1);
		
		Status = FLASH_If_SectorErase(APPLICATION_ADDRESS);// all erase
		if(Status != 0)
		{
			FLASH_Lock();
			serial_printf("\n\r  Flash Erase Error!! [%s-%s]\n\r",__FILE__,__LINE__);
			return;
		}
		else
		{
			serial_printf("\n\r  Flash Erase complete!! ");
		}
	}
	else if (Loop > 6)
	{
		serial_printf("\n\r  User Program Size too Big !!\n\r");
		serial_printf("\n\r  Error!! \n\r");		
		return;
	}
	else //1~7 사이인 경우 
	{
		serial_printf("\n\r  Erase Flash Sector  %d~%d \n\r", 1, Loop+1);
		
		uint32_t addr = APPLICATION_ADDRESS;
		if( Loop+1 == 2 )	addr = ADDR_FLASH_SECTOR_2;
		else if( Loop+1 == 3 )	addr = ADDR_FLASH_SECTOR_3;
		else if( Loop+1 == 4 )	addr = ADDR_FLASH_SECTOR_4;
		else if( Loop+1 == 5 )	addr = ADDR_FLASH_SECTOR_5;
		else if( Loop+1 == 6 )	addr = ADDR_FLASH_SECTOR_6;
		else if( Loop+1 == 7 )	addr = ADDR_FLASH_SECTOR_7;

		Status = FLASH_If_RangeErase(ADDR_FLASH_SECTOR_1, addr);
		if(Status != 0)
		{
			FLASH_Lock();
			serial_printf("\n\r  Flash Erase Error!! [%s-%s]\n\r",__FILE__,__LINE__);
			return;
		}
		else
		{
			serial_printf("\n\r  Flash Range Erase complete!! ");
		}
	}
	
	//Flash Write
	pFlashAdd = (uint32_t *)ADDR_FLASH_SECTOR_7;
	pRamAdd = (uint32_t *)USER_RAM;
	
	Status = FLASH_If_Write( (uint32_t *)&pFlashAdd, (uint32_t*) pRamAdd, (uint16_t) SizeofData/2);
	if(Status != 0)
	{
		FLASH_Lock();
		serial_printf("\n\r  Flash Write Error!! [%s-%s]\n\r",__FILE__,__LINE__);
		return;
	}
	else
	{
		serial_printf("\n\r  Flash Write complete!! ");
	}
	FLASH_Lock();
}
#endif

void change_endian(void)
{
	char RcvData[2];
	uint16_t Header_ignore;
	uint16_t HEXBuf[80];
	uint16_t ByteCount;
	
	uint16_t Status;
	uint32_t SizeofData; //offset_address
	uint32_t SizeofData1; //offset_address
	
	SizeofData = 0;
	SizeofData1 = 0;
	
	SetUserHEXFlashadd();
	pRamAdd = (uint32_t *)USER_RAM;

	while((RcvData[0] = *pFlashAdd)!=':');
	Header_ignore=0;
	ByteCount=0;
	
	for(;;)
	{	
		if (Header_ignore)
		{
			pFlashAdd = (uint32_t *)(APPLICATION_BACKUP_ADDRESS+(++SizeofData));
			RcvData[0] = *pFlashAdd;
			if(RcvData[0] == '\n')
			{
				pFlashAdd = (uint32_t *)(APPLICATION_BACKUP_ADDRESS+(++SizeofData));
				RcvData[0] = *pFlashAdd;
			}
		}
		else
		{
			RcvData[0] = ':';
			Header_ignore=1;
		}

		pFlashAdd = (uint32_t *)(APPLICATION_BACKUP_ADDRESS+(++SizeofData));
		RcvData[1] = *pFlashAdd;
		if(RcvData[1] == '\n')
		{
			pFlashAdd = (uint32_t *)(APPLICATION_BACKUP_ADDRESS+(++SizeofData));
			RcvData[1] = *pFlashAdd;
		}
		
		*pRamAdd = ((RcvData[0] << 8) + RcvData[1]);
		pRamAdd = (uint32_t *)(USER_RAM+(2*++SizeofData1));
		
		//end of file check
		if(RcvData[0] == ':')
		{
			ByteCount = 0;
			HEXBuf[ByteCount++] = RcvData[0];
			HEXBuf[ByteCount++] = RcvData[1];
		}
		else
		{
			HEXBuf[ByteCount++] = RcvData[0];
			HEXBuf[ByteCount++] = RcvData[1];
#ifdef DEBUG_FLASHDOWN
			if(RcvData[1] == 0x0d)	serial_printf(",");
#endif
		}
		
		if(ByteCount == 12)
		{
			ByteCount = 0;
			//end of file
			// :00000001FF
			if(HEXBuf[0] == ':')
			if(HEXBuf[1] == '0')
			if(HEXBuf[2] == '0')
			if(HEXBuf[3] == '0')
			if(HEXBuf[4] == '0')
			if(HEXBuf[5] == '0')
			if(HEXBuf[6] == '0')
			if(HEXBuf[7] == '0')
			if(HEXBuf[8] == '1')
			if(HEXBuf[9] == 'F')
			if(HEXBuf[10] == 'F')
			{
				pFlashAdd = (uint32_t *)APPLICATION_BACKUP_ADDRESS;
				pRamAdd = (uint32_t *)USER_RAM;
				
				//FLASH_If_Init();
				FLASH_If_SectorErase(APPLICATION_BACKUP_ADDRESS);
				Status = FLASH_If_Write( (uint32_t *)&pFlashAdd, (uint32_t*) pRamAdd, (uint16_t) SizeofData1/2);
				if(Status != 0)
				{
					FLASH_Lock();
					serial_printf("\n\r  Flash Write Error!! [%s-%s]\n\r",__FILE__,__LINE__);
					return;
				}
				else
				{
					serial_printf("\n\r  Flash Write complete!! \n\r");
				}
				
				FLASH_Lock();
				break;
			}
		}
	}
}

void execute_application(void)
{
	/* execute the new program */
	JumpAddress = *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);
	/* Jump to user application */
	Jump_To_Application = (pFunction) JumpAddress;
	/* Initialize user application's Stack Pointer */
	__set_MSP(*(__IO uint32_t*) APPLICATION_ADDRESS);
	Jump_To_Application();
}


void execute_application_addr(uint32_t start_Addr)
{
	/* execute the new program */
	JumpAddress = *(__IO uint32_t*) (start_Addr + 4);
	/* Jump to user application */
	Jump_To_Application = (pFunction) JumpAddress;
	/* Initialize user application's Stack Pointer */
	__set_MSP(*(__IO uint32_t*) start_Addr);
	Jump_To_Application();
}

/**
* @brief  Display the Main Menu on HyperTerminal
* @param  None
* @retval None
*/
void Main_Menu(void)
{
	uint8_t key = 0;
	char *fptr = NULL;
	uint32_t file_size = 0;
	uint32_t flash_data[4] = {0,};
	uint32_t i = 0;
	
	serial_printf("\r\n======================================================================");
	serial_printf("\r\n=              (C) COPYRIGHT 2011 STMicroelectronics                 =");
	serial_printf("\r\n=                                                                    =");
	serial_printf("\r\n=  STM32F4xx In-Application Programming Application  (Version 1.0.0) =");
	serial_printf("\r\n=                                                                    =");
	serial_printf("\r\n=                                   By MCD Application Team          =");
	serial_printf("\r\n=                                                                    =");
	serial_printf("\r\n=                               Compiled time : %s %s =",__DATE__,__TIME__);
	serial_printf("\r\n======================================================================");
	serial_printf("\r\n\r\n");
	
#ifndef TEST_W_EUM
	while (1)
#endif
	{
		print_menu();
#ifdef TEST_W_EUM
		serial_printf("Waiting for the file to be sent ... (Press 'g' to abort)\n\r");
		FLASH_If_Init();
		//FLASH_If_Erase();
		
		/* Download user application in the Flash */
		if( SerialDownload() > 0 )
		{
			change_endian();
			
			fptr = strstr((const char *)FileName, "hex");
			if( fptr )
			{
				DownFromFlash();
				execute_application();
			}
		}
		else
		{
			serial_printf("Serial Download Error!! [%s-%s]\n\r",__FILE__,__LINE__);
			FLASH_If_Init();
			FLASH_If_Erase();
			FLASH_Lock();
		}
#else
		/* Receive key */
		while (1)
		{
			if (SerialKeyPressed((uint8_t*)&key)) break;
		}
		
		switch(key)
		{
		case '1'://binary download

			/* Unlock the Flash Program Erase controller */
			FLASH_If_Init();
			serial_printf("Waiting for the file to be sent ... (Press 'g' to abort)\n\r");

#if 01
			FLASH_If_SectorErase(ADDR_FLASH_SECTOR_6);
			FLASH_If_SectorErase(ADDR_FLASH_SECTOR_7);
			for(i = 0; i<0x3000; i++)//backup from sector4,5 to sector6,7
			{
				flash_read_32(ADDR_FLASH_SECTOR_4, flash_data, i, sizeof(flash_data));
				flash_write_32(ADDR_FLASH_SECTOR_6, flash_data, i, sizeof(flash_data));
			}
#endif
			/* Download user application in the Flash */
			file_size = SerialDownload();
			if( file_size > 0 )
			{
				fptr = strstr((const char *)FileName, "hex");
				if( fptr )//hex file
				{
					change_endian();
					DownFromFlash();
				}
			}
			else
			{
				serial_printf("Serial Download Error!! [%s-%s]\n\r",__FILE__,__LINE__);
				FLASH_If_Init();
				FLASH_If_Erase();
				FLASH_Lock();
			}
			break;
			
		case '2':
			/* Upload user application from the Flash */
			SerialUpload();
			break;
			
		case '3':
			execute_application();
			break;
			
		case '4':
			if(FlashProtection == 1)
			{
				/* Disable the write protection */
				switch (FLASH_If_DisableWriteProtection(FLASH_SECTOR_1))
				{
				case 1:
					{
						serial_printf("Write Protection disabled...\r\n");
						FlashProtection = 0;
						break;
					}
				case 2:
					{
						serial_printf("Error: Flash write unprotection failed...\r\n");
						break;
					}
				default:
					{
					}
				}
			}
			else
			{
				if (FlashProtection == 0)
				{
					serial_printf("Invalid Number ! ==> The number should be either 1, 2 or 3\r");
				}
				else
				{
					serial_printf("Invalid Number ! ==> The number should be either 1, 2, 3 or 4\r");
				}
			}
			break;
			
#ifdef ENABLE_HEX_DOWN
		case '5':
			USARTtoFLASH();
			DownFromFlash();
			break;
#endif
		case '6':
			serial_printf("\r Flash erasing! \r");
			FLASH_If_Init();
			FLASH_If_RangeErase(ADDR_FLASH_SECTOR_4, ADDR_FLASH_SECTOR_6);
			//FLASH_If_Erase();
			FLASH_Lock();
			serial_printf("\r Flash is erased! \r\r");
			break;
			
		case 0x0a:
		case 0x0d:
			break;
			
		default:
			serial_printf("Invalid Number ! ==> The number should be either 1~6\r");
			break;
		}
#endif
	}
}

/**
* @}
*/

/*******************(C)COPYRIGHT 2011 STMicroelectronics *****END OF FILE******/
